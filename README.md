# Prestations sociales
Cette application a été générée avec JHipster 4.14.3, vous pouvez trouver toute l'aide et la documentation ici [http://www.jhipster.tech/documentation-archive/v4.14.3](http://www.jhipster.tech/documentation-archive/v4.14.3).

Cette application a pour vocation de gérer les fiches de prestations sociales des cantons suisses romandes. 
Voici l'ensemble des caractéristiques qu'il est possible de gérer en l'état actuel :
* Les caractéristiques générales de la prestation (titre, juridiction, catégorie, fiche parente, description)
* Les adresses utiles à une prestation
* Les sources juridiques d'une prestation
* Les conditions d'octroi d'une prestation

L'ensemble des fonctionnalités du programme sont présentées dans le [Wiki](https://github.com/HEG-Arc/tb-fiches-jestime/wiki) de ce répertoire Github.

## Développement

Avant de pouvoir compiler ce projet, vous devez installer et configurer les dépendances suivantes sur votre machine

1. Java 8
2. [Node.js](https://nodejs.org/en/)
3. [Yarn](https://yarnpkg.com/en/)

Après avoir installé Node, vous devriez être capable d'exécution la commande suivante pour installer les outils de développements.
Vous aurez seulement besoin d'exécuter cette commande lorsque les dépendances changerant dans le fichier [package.json](package.json).

    yarn install

Nous utilisons les scripts yarn et [Webpack](https://webpack.js.org/) comme mécanisme de compilation.

Les fichiers sont actuellement configurés pour communiquer avec un base de données "jestime" sur l'hôte local avec l'utilisateur root sans mot de passe.
Il est cependant possible de changer cette configuration en modifiant les fichiers [application-dev.yml](src/main/resources/config/application-dev.yml), [application-prod.yml](src/main/resources/config/application-prod.yml) et [liquibase.gradle](gradle/liquibase.gradle).

Les deux utilisateurs disponibles sont :

    admin / admin => ROLE_ADMIN
    user / user => ROLE_USER

Exécuter les deux commandes suivantes dans deux terminaux séparées pour que le navigateur se rafraîchisse automatique lorsque vous faîtes des changements dans lors du développement :

    ./gradlew
    yarn start

[Yarn](https://yarnpkg.com/en/) est également utilisé pour gérer les dépendences CSS et JavaScript utilisées dans cette application. Vous pouvez mettre à jour les dépendances en spécificant une nouvelle version dans le fichier [package.json](package.json). Vous pouvez également utiliser les commandes `yarn update` et `yarn install` pour gérer les dépendances 

La commande `yarn run` listera tous les scripts disponibles prêt à être lancé au démarrage de l'application

### Utiliser angular-cli

Vous pouvez également utiliser [Angular CLI](https://cli.angular.io/) pour générer du code côté client.

Par exemple, la commande suivante :

    ng generate component my-component

générera les fichiers suivants :

    create src/main/webapp/app/my-component/my-component.component.html
    create src/main/webapp/app/my-component/my-component.component.ts
    update src/main/webapp/app/app.module.ts


## Compiler pour l'environnement de production

Pour optimisez l'application Jestime pour l'environnement de production, exécutez la commande suivante :

    ./gradlew -Pprod clean bootRepackage

Cela va concaténer et minifier le client CSS et les fichiers JavaScript. Cela modifiera également le fichier `index.html` pour qu'il référence ces nouveaux fichiers.
Pour s'assurer que tout fonctionne, exécutez la commande :

    java -jar build/libs/*.war

Puis rendez-vous sur la page [http://localhost:8080](http://localhost:8080) dans votre navigateur.

## Tests

Pour lancer les tests de l'application, exécutez la commande :

    ./gradlew test

### Tests côté client

Les tests unitaires sont exécutés par [Karma][] et écrit avec [Jasmine][]. Ils sont situés dans le dossier [src/test/javascript/](src/test/javascript/) et peuvent être exécutés grâce à la commande :

    yarn test
    
## Utilisation de base de JHipster

JHipster propose de nombreuses possibilité. La base de cette application a entièrement été créée grâce à la commande :

    jhipster install
    
Une architecture monolithique a été choisie et le support du multi-langues sélectionné.

### Génération d'une entité

Afin de générer une entité, il suffit d'exécuter la commande :
    
    jhipster entity [ENTITY_NAME]
    
L'assistant proposera alors de créer les champs et les relations nécessaires avec les autres entités.

L'ensemble des fichiers renseignés sur cette page [Creating an entity](https://www.jhipster.tech/creating-an-entity/) sont alors créés et configurés automatiquement à la fin de l'assistant.

### Modification des champs disponible

À la fin de la création d'une entité, JHipster ne proposer d'afficher que deux champs en cas de relation : l'ID unique et un champ au choix.

Si vous avez besoin d'autres champs, il suffit dans un premier de s'assurer que l'annotation 'JsonIgnore' n'est pas présente dans le mapping JPA de l'entité, puis il vous suffit d'ajouter la propriété dans le fichier DTO de votre entité.

Concernant les fichiers '...Mapper.java' dans le cas où un seul champ doit être renvoyé, il suffit d'ajouter la propriété en suivant l'exemple du champ 'ID'. Voici un exemple pour récupérer l'URL d'une source juridique depuis une référence :

    @Mapping(source = "source.id", target = "sourceId")
    @Mapping(source = "source.title", target = "sourceTitle")
    @Mapping(source = "source.url", target = "sourceUrl")

Si vous devez récupérer l'entité dans son intégralité, il vous faut alors injecter le Mapper en question dans le Mapper de votre entité.
Voici un exemple pour récuper les valeurs des conditions d'octroi d'une condition :

    @Mapper(componentModel = "spring", uses = {SocialCardMapper.class, UserMapper.class, ConditionValueMapper.class})

Cependant, il faut faire attention de ne pas exécuter un Mappage bidirectionnel avec cette configuration dans le cas d'une relation 'many-to-many' au risque d'engendrer une boucle infinie.
