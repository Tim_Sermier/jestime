package ch.hearc.ig.jestime.repository;

import ch.hearc.ig.jestime.domain.SourceAudit;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SourceAudit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SourceAuditRepository extends JpaRepository<SourceAudit, Long>, JpaSpecificationExecutor<SourceAudit> {

}
