package ch.hearc.ig.jestime.repository;

import ch.hearc.ig.jestime.domain.AddressAudit;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AddressAudit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AddressAuditRepository extends JpaRepository<AddressAudit, Long>, JpaSpecificationExecutor<AddressAudit> {

}
