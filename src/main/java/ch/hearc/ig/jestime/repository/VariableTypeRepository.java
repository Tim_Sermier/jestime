package ch.hearc.ig.jestime.repository;

import ch.hearc.ig.jestime.domain.VariableType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the VariableType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VariableTypeRepository extends JpaRepository<VariableType, Long>, JpaSpecificationExecutor<VariableType> {

}
