package ch.hearc.ig.jestime.repository;

import ch.hearc.ig.jestime.domain.Juridiction;
import java.util.List;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Juridiction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JuridictionRepository extends JpaRepository<Juridiction, Long> {
    
}
