package ch.hearc.ig.jestime.repository;

import ch.hearc.ig.jestime.domain.Source;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the Source entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SourceRepository extends JpaRepository<Source, Long>, JpaSpecificationExecutor<Source> {

    @Query("select source from Source source where source.creator.login = ?#{principal.username}")
    List<Source> findByCreatorIsCurrentUser();

    @Query("select source from Source source where source.updator.login = ?#{principal.username}")
    List<Source> findByUpdatorIsCurrentUser();

    @Query("select distinct source from Source source left join fetch source.references")
    List<Source> findAllWithEagerRelationships();

}
