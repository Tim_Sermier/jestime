package ch.hearc.ig.jestime.repository;

import ch.hearc.ig.jestime.domain.SocialCardAudit;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SocialCardAudit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SocialCardAuditRepository extends JpaRepository<SocialCardAudit, Long>, JpaSpecificationExecutor<SocialCardAudit> {

}
