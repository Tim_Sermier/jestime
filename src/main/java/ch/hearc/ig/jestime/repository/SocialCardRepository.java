package ch.hearc.ig.jestime.repository;

import ch.hearc.ig.jestime.domain.SocialCard;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the SocialCard entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SocialCardRepository extends JpaRepository<SocialCard, Long>, JpaSpecificationExecutor<SocialCard> {

    @Query("select social_card from SocialCard social_card where social_card.creator.login = ?#{principal.username}")
    List<SocialCard> findByCreatorIsCurrentUser();

    @Query("select social_card from SocialCard social_card where social_card.updator.login = ?#{principal.username}")
    List<SocialCard> findByUpdatorIsCurrentUser();
    @Query("select distinct social_card from SocialCard social_card left join fetch social_card.addresses left join fetch social_card.sources")
    List<SocialCard> findAllWithEagerRelationships();

    @Query("select social_card from SocialCard social_card left join fetch social_card.addresses left join fetch social_card.sources where social_card.id =:id")
    SocialCard findOneWithEagerRelationships(@Param("id") Long id);

}
