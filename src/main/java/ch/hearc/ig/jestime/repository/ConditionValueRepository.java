package ch.hearc.ig.jestime.repository;

import ch.hearc.ig.jestime.domain.ConditionValue;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ConditionValue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConditionValueRepository extends JpaRepository<ConditionValue, Long>, JpaSpecificationExecutor<ConditionValue> {

}
