package ch.hearc.ig.jestime.repository;

import ch.hearc.ig.jestime.domain.Condition;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the Condition entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConditionRepository extends JpaRepository<Condition, Long>, JpaSpecificationExecutor<Condition> {

    @Query("select jhi_condition from Condition jhi_condition where jhi_condition.creator.login = ?#{principal.username}")
    List<Condition> findByCreatorIsCurrentUser();

    @Query("select jhi_condition from Condition jhi_condition where jhi_condition.updator.login = ?#{principal.username}")
    List<Condition> findByUpdatorIsCurrentUser();

}
