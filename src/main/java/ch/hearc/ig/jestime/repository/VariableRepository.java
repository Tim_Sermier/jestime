package ch.hearc.ig.jestime.repository;

import ch.hearc.ig.jestime.domain.Variable;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Variable entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VariableRepository extends JpaRepository<Variable, Long>, JpaSpecificationExecutor<Variable> {

}
