package ch.hearc.ig.jestime.repository;

import ch.hearc.ig.jestime.domain.Address;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the Address entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AddressRepository extends JpaRepository<Address, Long>, JpaSpecificationExecutor<Address> {

    @Query("select address from Address address where address.creator.login = ?#{principal.username}")
    List<Address> findByCreatorIsCurrentUser();

    @Query("select address from Address address where address.updator.login = ?#{principal.username}")
    List<Address> findByUpdatorIsCurrentUser();

}
