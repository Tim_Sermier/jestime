package ch.hearc.ig.jestime.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;


import io.github.jhipster.service.filter.LocalDateFilter;



/**
 * Criteria class for the AddressAudit entity. This class is used in AddressAuditResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /address-audits?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AddressAuditCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter action;

    private LocalDateFilter actionDate;

    private IntegerFilter actionUser;

    private StringFilter code;

    private StringFilter name;

    private StringFilter location;

    private StringFilter phone;

    private StringFilter fax;

    private StringFilter email;

    private StringFilter website;

    public AddressAuditCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getAction() {
        return action;
    }

    public void setAction(StringFilter action) {
        this.action = action;
    }

    public LocalDateFilter getActionDate() {
        return actionDate;
    }

    public void setActionDate(LocalDateFilter actionDate) {
        this.actionDate = actionDate;
    }

    public IntegerFilter getActionUser() {
        return actionUser;
    }

    public void setActionUser(IntegerFilter actionUser) {
        this.actionUser = actionUser;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getLocation() {
        return location;
    }

    public void setLocation(StringFilter location) {
        this.location = location;
    }

    public StringFilter getPhone() {
        return phone;
    }

    public void setPhone(StringFilter phone) {
        this.phone = phone;
    }

    public StringFilter getFax() {
        return fax;
    }

    public void setFax(StringFilter fax) {
        this.fax = fax;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getWebsite() {
        return website;
    }

    public void setWebsite(StringFilter website) {
        this.website = website;
    }

    @Override
    public String toString() {
        return "AddressAuditCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (action != null ? "action=" + action + ", " : "") +
                (actionDate != null ? "actionDate=" + actionDate + ", " : "") +
                (actionUser != null ? "actionUser=" + actionUser + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (location != null ? "location=" + location + ", " : "") +
                (phone != null ? "phone=" + phone + ", " : "") +
                (fax != null ? "fax=" + fax + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (website != null ? "website=" + website + ", " : "") +
            "}";
    }

}
