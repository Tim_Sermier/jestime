package ch.hearc.ig.jestime.service;

import ch.hearc.ig.jestime.domain.Reference;
import ch.hearc.ig.jestime.repository.ReferenceRepository;
import ch.hearc.ig.jestime.service.dto.ReferenceDTO;
import ch.hearc.ig.jestime.service.mapper.ReferenceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Reference.
 */
@Service
@Transactional
public class ReferenceService {

    private final Logger log = LoggerFactory.getLogger(ReferenceService.class);

    private final ReferenceRepository referenceRepository;

    private final ReferenceMapper referenceMapper;

    public ReferenceService(ReferenceRepository referenceRepository, ReferenceMapper referenceMapper) {
        this.referenceRepository = referenceRepository;
        this.referenceMapper = referenceMapper;
    }

    /**
     * Save a reference.
     *
     * @param referenceDTO the entity to save
     * @return the persisted entity
     */
    public ReferenceDTO save(ReferenceDTO referenceDTO) {
        log.debug("Request to save Reference : {}", referenceDTO);
        Reference reference = referenceMapper.toEntity(referenceDTO);
        reference = referenceRepository.save(reference);
        return referenceMapper.toDto(reference);
    }

    /**
     * Get all the references.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ReferenceDTO> findAll() {
        log.debug("Request to get all References");
        return referenceRepository.findAll().stream()
            .map(referenceMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one reference by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ReferenceDTO findOne(Long id) {
        log.debug("Request to get Reference : {}", id);
        Reference reference = referenceRepository.findOne(id);
        return referenceMapper.toDto(reference);
    }

    /**
     * Delete the reference by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Reference : {}", id);
        referenceRepository.delete(id);
    }
}
