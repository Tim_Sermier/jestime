package ch.hearc.ig.jestime.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ch.hearc.ig.jestime.domain.VariableType;
import ch.hearc.ig.jestime.domain.*; // for static metamodels
import ch.hearc.ig.jestime.repository.VariableTypeRepository;
import ch.hearc.ig.jestime.service.dto.VariableTypeCriteria;

import ch.hearc.ig.jestime.service.dto.VariableTypeDTO;
import ch.hearc.ig.jestime.service.mapper.VariableTypeMapper;

/**
 * Service for executing complex queries for VariableType entities in the database.
 * The main input is a {@link VariableTypeCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link VariableTypeDTO} or a {@link Page} of {@link VariableTypeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class VariableTypeQueryService extends QueryService<VariableType> {

    private final Logger log = LoggerFactory.getLogger(VariableTypeQueryService.class);


    private final VariableTypeRepository variableTypeRepository;

    private final VariableTypeMapper variableTypeMapper;

    public VariableTypeQueryService(VariableTypeRepository variableTypeRepository, VariableTypeMapper variableTypeMapper) {
        this.variableTypeRepository = variableTypeRepository;
        this.variableTypeMapper = variableTypeMapper;
    }

    /**
     * Return a {@link List} of {@link VariableTypeDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<VariableTypeDTO> findByCriteria(VariableTypeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<VariableType> specification = createSpecification(criteria);
        return variableTypeMapper.toDto(variableTypeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link VariableTypeDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<VariableTypeDTO> findByCriteria(VariableTypeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<VariableType> specification = createSpecification(criteria);
        final Page<VariableType> result = variableTypeRepository.findAll(specification, page);
        return result.map(variableTypeMapper::toDto);
    }

    /**
     * Function to convert VariableTypeCriteria to a {@link Specifications}
     */
    private Specifications<VariableType> createSpecification(VariableTypeCriteria criteria) {
        Specifications<VariableType> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), VariableType_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), VariableType_.code));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), VariableType_.name));
            }
        }
        return specification;
    }

}
