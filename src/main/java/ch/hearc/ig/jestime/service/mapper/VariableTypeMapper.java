package ch.hearc.ig.jestime.service.mapper;

import ch.hearc.ig.jestime.domain.*;
import ch.hearc.ig.jestime.service.dto.VariableTypeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity VariableType and its DTO VariableTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface VariableTypeMapper extends EntityMapper<VariableTypeDTO, VariableType> {



    default VariableType fromId(Long id) {
        if (id == null) {
            return null;
        }
        VariableType variableType = new VariableType();
        variableType.setId(id);
        return variableType;
    }
}
