package ch.hearc.ig.jestime.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;


import io.github.jhipster.service.filter.LocalDateFilter;



/**
 * Criteria class for the Source entity. This class is used in SourceResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /sources?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SourceCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter code;

    private StringFilter title;

    private StringFilter url;

    private LocalDateFilter creationDate;

    private LocalDateFilter editionDate;

    private LongFilter socialCardsId;

    private LongFilter juridictionId;

    private LongFilter creatorId;

    private LongFilter updatorId;

    private LongFilter referencesId;

    public SourceCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public StringFilter getTitle() {
        return title;
    }

    public void setTitle(StringFilter title) {
        this.title = title;
    }

    public StringFilter getUrl() {
        return url;
    }

    public void setUrl(StringFilter url) {
        this.url = url;
    }

    public LocalDateFilter getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateFilter creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateFilter getEditionDate() {
        return editionDate;
    }

    public void setEditionDate(LocalDateFilter editionDate) {
        this.editionDate = editionDate;
    }

    public LongFilter getSocialCardsId() {
        return socialCardsId;
    }

    public void setSocialCardsId(LongFilter socialCardsId) {
        this.socialCardsId = socialCardsId;
    }

    public LongFilter getJuridictionId() {
        return juridictionId;
    }

    public void setJuridictionId(LongFilter juridictionId) {
        this.juridictionId = juridictionId;
    }

    public LongFilter getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(LongFilter creatorId) {
        this.creatorId = creatorId;
    }

    public LongFilter getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(LongFilter updatorId) {
        this.updatorId = updatorId;
    }

    public LongFilter getReferencesId() {
        return referencesId;
    }

    public void setReferencesId(LongFilter referencesId) {
        this.referencesId = referencesId;
    }

    @Override
    public String toString() {
        return "SourceCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (title != null ? "title=" + title + ", " : "") +
                (url != null ? "url=" + url + ", " : "") +
                (creationDate != null ? "creationDate=" + creationDate + ", " : "") +
                (editionDate != null ? "editionDate=" + editionDate + ", " : "") +
                (socialCardsId != null ? "socialCardsId=" + socialCardsId + ", " : "") +
                (juridictionId != null ? "juridictionId=" + juridictionId + ", " : "") +
                (creatorId != null ? "creatorId=" + creatorId + ", " : "") +
                (updatorId != null ? "updatorId=" + updatorId + ", " : "") +
                (referencesId != null ? "referencesId=" + referencesId + ", " : "") +
            "}";
    }

}
