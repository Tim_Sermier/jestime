package ch.hearc.ig.jestime.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ch.hearc.ig.jestime.domain.Reference;
import ch.hearc.ig.jestime.domain.*; // for static metamodels
import ch.hearc.ig.jestime.repository.ReferenceRepository;
import ch.hearc.ig.jestime.service.dto.ReferenceCriteria;

import ch.hearc.ig.jestime.service.dto.ReferenceDTO;
import ch.hearc.ig.jestime.service.mapper.ReferenceMapper;

/**
 * Service for executing complex queries for Reference entities in the database.
 * The main input is a {@link ReferenceCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ReferenceDTO} or a {@link Page} of {@link ReferenceDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ReferenceQueryService extends QueryService<Reference> {

    private final Logger log = LoggerFactory.getLogger(ReferenceQueryService.class);


    private final ReferenceRepository referenceRepository;

    private final ReferenceMapper referenceMapper;

    public ReferenceQueryService(ReferenceRepository referenceRepository, ReferenceMapper referenceMapper) {
        this.referenceRepository = referenceRepository;
        this.referenceMapper = referenceMapper;
    }

    /**
     * Return a {@link List} of {@link ReferenceDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ReferenceDTO> findByCriteria(ReferenceCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Reference> specification = createSpecification(criteria);
        return referenceMapper.toDto(referenceRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ReferenceDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ReferenceDTO> findByCriteria(ReferenceCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Reference> specification = createSpecification(criteria);
        final Page<Reference> result = referenceRepository.findAll(specification, page);
        return result.map(referenceMapper::toDto);
    }

    /**
     * Function to convert ReferenceCriteria to a {@link Specifications}
     */
    private Specifications<Reference> createSpecification(ReferenceCriteria criteria) {
        Specifications<Reference> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Reference_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), Reference_.code));
            }
            if (criteria.getTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitle(), Reference_.title));
            }
            if (criteria.getSourceId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getSourceId(), Reference_.source, Source_.id));
            }
        }
        return specification;
    }

}
