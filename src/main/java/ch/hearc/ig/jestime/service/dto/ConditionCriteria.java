package ch.hearc.ig.jestime.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;


import io.github.jhipster.service.filter.LocalDateFilter;



/**
 * Criteria class for the Condition entity. This class is used in ConditionResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /conditions?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ConditionCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter code;

    private LocalDateFilter creationDate;

    private LocalDateFilter editDate;

    private LongFilter socialCardId;

    private LongFilter creatorId;

    private LongFilter updatorId;

    private LongFilter conditionValuesId;

    public ConditionCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public LocalDateFilter getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateFilter creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateFilter getEditDate() {
        return editDate;
    }

    public void setEditDate(LocalDateFilter editDate) {
        this.editDate = editDate;
    }

    public LongFilter getSocialCardId() {
        return socialCardId;
    }

    public void setSocialCardId(LongFilter socialCardId) {
        this.socialCardId = socialCardId;
    }

    public LongFilter getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(LongFilter creatorId) {
        this.creatorId = creatorId;
    }

    public LongFilter getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(LongFilter updatorId) {
        this.updatorId = updatorId;
    }

    public LongFilter getConditionValuesId() {
        return conditionValuesId;
    }

    public void setConditionValuesId(LongFilter conditionValuesId) {
        this.conditionValuesId = conditionValuesId;
    }

    @Override
    public String toString() {
        return "ConditionCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (creationDate != null ? "creationDate=" + creationDate + ", " : "") +
                (editDate != null ? "editDate=" + editDate + ", " : "") +
                (socialCardId != null ? "socialCardId=" + socialCardId + ", " : "") +
                (creatorId != null ? "creatorId=" + creatorId + ", " : "") +
                (updatorId != null ? "updatorId=" + updatorId + ", " : "") +
                (conditionValuesId != null ? "conditionValuesId=" + conditionValuesId + ", " : "") +
            "}";
    }

}
