package ch.hearc.ig.jestime.service;

import ch.hearc.ig.jestime.domain.Operator;
import ch.hearc.ig.jestime.repository.OperatorRepository;
import ch.hearc.ig.jestime.service.dto.OperatorDTO;
import ch.hearc.ig.jestime.service.mapper.OperatorMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Operator.
 */
@Service
@Transactional
public class OperatorService {

    private final Logger log = LoggerFactory.getLogger(OperatorService.class);

    private final OperatorRepository operatorRepository;

    private final OperatorMapper operatorMapper;

    public OperatorService(OperatorRepository operatorRepository, OperatorMapper operatorMapper) {
        this.operatorRepository = operatorRepository;
        this.operatorMapper = operatorMapper;
    }

    /**
     * Save a operator.
     *
     * @param operatorDTO the entity to save
     * @return the persisted entity
     */
    public OperatorDTO save(OperatorDTO operatorDTO) {
        log.debug("Request to save Operator : {}", operatorDTO);
        Operator operator = operatorMapper.toEntity(operatorDTO);
        operator = operatorRepository.save(operator);
        return operatorMapper.toDto(operator);
    }

    /**
     * Get all the operators.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<OperatorDTO> findAll() {
        log.debug("Request to get all Operators");
        return operatorRepository.findAll().stream()
            .map(operatorMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one operator by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public OperatorDTO findOne(Long id) {
        log.debug("Request to get Operator : {}", id);
        Operator operator = operatorRepository.findOne(id);
        return operatorMapper.toDto(operator);
    }

    /**
     * Delete the operator by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Operator : {}", id);
        operatorRepository.delete(id);
    }
}
