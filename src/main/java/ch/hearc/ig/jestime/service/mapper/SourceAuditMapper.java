package ch.hearc.ig.jestime.service.mapper;

import ch.hearc.ig.jestime.domain.*;
import ch.hearc.ig.jestime.service.dto.SourceAuditDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SourceAudit and its DTO SourceAuditDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SourceAuditMapper extends EntityMapper<SourceAuditDTO, SourceAudit> {



    default SourceAudit fromId(Long id) {
        if (id == null) {
            return null;
        }
        SourceAudit sourceAudit = new SourceAudit();
        sourceAudit.setId(id);
        return sourceAudit;
    }
}
