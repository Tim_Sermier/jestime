package ch.hearc.ig.jestime.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;


import io.github.jhipster.service.filter.LocalDateFilter;



/**
 * Criteria class for the SocialCard entity. This class is used in SocialCardResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /social-cards?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SocialCardCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter code;

    private StringFilter title;

    private StringFilter description;

    private StringFilter general;

    private StringFilter procedure;

    private StringFilter recourse;

    private LocalDateFilter creationDate;

    private LocalDateFilter editDate;

    private LongFilter juridictionId;

    private LongFilter parentSocialCardId;

    private LongFilter categoryId;

    private LongFilter addressesId;

    private LongFilter creatorId;

    private LongFilter updatorId;

    private LongFilter sourcesId;

    private LongFilter conditionsId;

    public SocialCardCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public StringFilter getTitle() {
        return title;
    }

    public void setTitle(StringFilter title) {
        this.title = title;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public StringFilter getGeneral() {
        return general;
    }

    public void setGeneral(StringFilter general) {
        this.general = general;
    }

    public StringFilter getProcedure() {
        return procedure;
    }

    public void setProcedure(StringFilter procedure) {
        this.procedure = procedure;
    }

    public StringFilter getRecourse() {
        return recourse;
    }

    public void setRecourse(StringFilter recourse) {
        this.recourse = recourse;
    }

    public LocalDateFilter getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateFilter creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateFilter getEditDate() {
        return editDate;
    }

    public void setEditDate(LocalDateFilter editDate) {
        this.editDate = editDate;
    }

    public LongFilter getJuridictionId() {
        return juridictionId;
    }

    public void setJuridictionId(LongFilter juridictionId) {
        this.juridictionId = juridictionId;
    }

    public LongFilter getParentSocialCardId() {
        return parentSocialCardId;
    }

    public void setParentSocialCardId(LongFilter parentSocialCardId) {
        this.parentSocialCardId = parentSocialCardId;
    }

    public LongFilter getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(LongFilter categoryId) {
        this.categoryId = categoryId;
    }

    public LongFilter getAddressesId() {
        return addressesId;
    }

    public void setAddressesId(LongFilter addressesId) {
        this.addressesId = addressesId;
    }

    public LongFilter getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(LongFilter creatorId) {
        this.creatorId = creatorId;
    }

    public LongFilter getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(LongFilter updatorId) {
        this.updatorId = updatorId;
    }

    public LongFilter getSourcesId() {
        return sourcesId;
    }

    public void setSourcesId(LongFilter sourcesId) {
        this.sourcesId = sourcesId;
    }

    public LongFilter getConditionsId() {
        return conditionsId;
    }

    public void setConditionsId(LongFilter conditionsId) {
        this.conditionsId = conditionsId;
    }

    @Override
    public String toString() {
        return "SocialCardCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (title != null ? "title=" + title + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (general != null ? "general=" + general + ", " : "") +
                (procedure != null ? "procedure=" + procedure + ", " : "") +
                (recourse != null ? "recourse=" + recourse + ", " : "") +
                (creationDate != null ? "creationDate=" + creationDate + ", " : "") +
                (editDate != null ? "editDate=" + editDate + ", " : "") +
                (juridictionId != null ? "juridictionId=" + juridictionId + ", " : "") +
                (parentSocialCardId != null ? "parentSocialCardId=" + parentSocialCardId + ", " : "") +
                (categoryId != null ? "categoryId=" + categoryId + ", " : "") +
                (addressesId != null ? "addressesId=" + addressesId + ", " : "") +
                (creatorId != null ? "creatorId=" + creatorId + ", " : "") +
                (updatorId != null ? "updatorId=" + updatorId + ", " : "") +
                (sourcesId != null ? "sourcesId=" + sourcesId + ", " : "") +
                (conditionsId != null ? "conditionsId=" + conditionsId + ", " : "") +
            "}";
    }

}
