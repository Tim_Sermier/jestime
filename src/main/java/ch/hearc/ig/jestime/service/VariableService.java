package ch.hearc.ig.jestime.service;

import ch.hearc.ig.jestime.domain.Variable;
import ch.hearc.ig.jestime.repository.VariableRepository;
import ch.hearc.ig.jestime.service.dto.VariableDTO;
import ch.hearc.ig.jestime.service.mapper.VariableMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Variable.
 */
@Service
@Transactional
public class VariableService {

    private final Logger log = LoggerFactory.getLogger(VariableService.class);

    private final VariableRepository variableRepository;

    private final VariableMapper variableMapper;

    public VariableService(VariableRepository variableRepository, VariableMapper variableMapper) {
        this.variableRepository = variableRepository;
        this.variableMapper = variableMapper;
    }

    /**
     * Save a variable.
     *
     * @param variableDTO the entity to save
     * @return the persisted entity
     */
    public VariableDTO save(VariableDTO variableDTO) {
        log.debug("Request to save Variable : {}", variableDTO);
        Variable variable = variableMapper.toEntity(variableDTO);
        variable = variableRepository.save(variable);
        return variableMapper.toDto(variable);
    }

    /**
     * Get all the variables.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<VariableDTO> findAll() {
        log.debug("Request to get all Variables");
        return variableRepository.findAll().stream()
            .map(variableMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one variable by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public VariableDTO findOne(Long id) {
        log.debug("Request to get Variable : {}", id);
        Variable variable = variableRepository.findOne(id);
        return variableMapper.toDto(variable);
    }

    /**
     * Delete the variable by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Variable : {}", id);
        variableRepository.delete(id);
    }
}
