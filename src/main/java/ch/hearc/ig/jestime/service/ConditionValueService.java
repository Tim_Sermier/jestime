package ch.hearc.ig.jestime.service;

import ch.hearc.ig.jestime.domain.ConditionValue;
import ch.hearc.ig.jestime.repository.ConditionValueRepository;
import ch.hearc.ig.jestime.service.dto.ConditionValueDTO;
import ch.hearc.ig.jestime.service.mapper.ConditionValueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing ConditionValue.
 */
@Service
@Transactional
public class ConditionValueService {

    private final Logger log = LoggerFactory.getLogger(ConditionValueService.class);

    private final ConditionValueRepository conditionValueRepository;

    private final ConditionValueMapper conditionValueMapper;

    public ConditionValueService(ConditionValueRepository conditionValueRepository, ConditionValueMapper conditionValueMapper) {
        this.conditionValueRepository = conditionValueRepository;
        this.conditionValueMapper = conditionValueMapper;
    }

    /**
     * Save a conditionValue.
     *
     * @param conditionValueDTO the entity to save
     * @return the persisted entity
     */
    public ConditionValueDTO save(ConditionValueDTO conditionValueDTO) {
        log.debug("Request to save ConditionValue : {}", conditionValueDTO);
        ConditionValue conditionValue = conditionValueMapper.toEntity(conditionValueDTO);
        conditionValue = conditionValueRepository.save(conditionValue);
        return conditionValueMapper.toDto(conditionValue);
    }

    /**
     * Get all the conditionValues.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ConditionValueDTO> findAll() {
        log.debug("Request to get all ConditionValues");
        return conditionValueRepository.findAll().stream()
            .map(conditionValueMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one conditionValue by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ConditionValueDTO findOne(Long id) {
        log.debug("Request to get ConditionValue : {}", id);
        ConditionValue conditionValue = conditionValueRepository.findOne(id);
        return conditionValueMapper.toDto(conditionValue);
    }

    /**
     * Delete the conditionValue by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ConditionValue : {}", id);
        conditionValueRepository.delete(id);
    }
}
