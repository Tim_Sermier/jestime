package ch.hearc.ig.jestime.service.mapper;

import ch.hearc.ig.jestime.domain.*;
import ch.hearc.ig.jestime.service.dto.SourceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Source and its DTO SourceDTO.
 */
@Mapper(componentModel = "spring", uses = {JuridictionMapper.class, UserMapper.class, ReferenceMapper.class})
public interface SourceMapper extends EntityMapper<SourceDTO, Source> {

    @Mapping(source = "juridiction.id", target = "juridictionId")
    @Mapping(source = "juridiction.name", target = "juridictionName")
    @Mapping(source = "creator.id", target = "creatorId")
    @Mapping(source = "creator.login", target = "creatorLogin")
    @Mapping(source = "updator.id", target = "updatorId")
    @Mapping(source = "updator.login", target = "updatorLogin")
    SourceDTO toDto(Source source);

    @Mapping(target = "socialCards", ignore = true)
    @Mapping(source = "juridictionId", target = "juridiction")
    @Mapping(source = "creatorId", target = "creator")
    @Mapping(source = "updatorId", target = "updator")
    Source toEntity(SourceDTO sourceDTO);

    default Source fromId(Long id) {
        if (id == null) {
            return null;
        }
        Source source = new Source();
        source.setId(id);
        return source;
    }
}
