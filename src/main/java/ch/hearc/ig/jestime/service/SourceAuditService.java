package ch.hearc.ig.jestime.service;

import ch.hearc.ig.jestime.domain.SourceAudit;
import ch.hearc.ig.jestime.repository.SourceAuditRepository;
import ch.hearc.ig.jestime.service.dto.SourceAuditDTO;
import ch.hearc.ig.jestime.service.mapper.SourceAuditMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing SourceAudit.
 */
@Service
@Transactional
public class SourceAuditService {

    private final Logger log = LoggerFactory.getLogger(SourceAuditService.class);

    private final SourceAuditRepository sourceAuditRepository;

    private final SourceAuditMapper sourceAuditMapper;

    public SourceAuditService(SourceAuditRepository sourceAuditRepository, SourceAuditMapper sourceAuditMapper) {
        this.sourceAuditRepository = sourceAuditRepository;
        this.sourceAuditMapper = sourceAuditMapper;
    }

    /**
     * Save a sourceAudit.
     *
     * @param sourceAuditDTO the entity to save
     * @return the persisted entity
     */
    public SourceAuditDTO save(SourceAuditDTO sourceAuditDTO) {
        log.debug("Request to save SourceAudit : {}", sourceAuditDTO);
        SourceAudit sourceAudit = sourceAuditMapper.toEntity(sourceAuditDTO);
        sourceAudit = sourceAuditRepository.save(sourceAudit);
        return sourceAuditMapper.toDto(sourceAudit);
    }

    /**
     * Get all the sourceAudits.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<SourceAuditDTO> findAll() {
        log.debug("Request to get all SourceAudits");
        return sourceAuditRepository.findAll().stream()
            .map(sourceAuditMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one sourceAudit by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public SourceAuditDTO findOne(Long id) {
        log.debug("Request to get SourceAudit : {}", id);
        SourceAudit sourceAudit = sourceAuditRepository.findOne(id);
        return sourceAuditMapper.toDto(sourceAudit);
    }

    /**
     * Delete the sourceAudit by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SourceAudit : {}", id);
        sourceAuditRepository.delete(id);
    }
}
