package ch.hearc.ig.jestime.service.mapper;

import ch.hearc.ig.jestime.domain.*;
import ch.hearc.ig.jestime.service.dto.ConditionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Condition and its DTO ConditionDTO.
 */
@Mapper(componentModel = "spring", uses = {SocialCardMapper.class, UserMapper.class, ConditionValueMapper.class})
public interface ConditionMapper extends EntityMapper<ConditionDTO, Condition> {

    @Mapping(source = "socialCard.id", target = "socialCardId")
    @Mapping(source = "socialCard.title", target = "socialCardTitle")
    @Mapping(source = "creator.id", target = "creatorId")
    @Mapping(source = "creator.login", target = "creatorLogin")
    @Mapping(source = "updator.id", target = "updatorId")
    @Mapping(source = "updator.login", target = "updatorLogin")
    ConditionDTO toDto(Condition condition);

    @Mapping(source = "socialCardId", target = "socialCard")
    @Mapping(source = "creatorId", target = "creator")
    @Mapping(source = "updatorId", target = "updator")
    Condition toEntity(ConditionDTO conditionDTO);

    default Condition fromId(Long id) {
        if (id == null) {
            return null;
        }
        Condition condition = new Condition();
        condition.setId(id);
        return condition;
    }
}
