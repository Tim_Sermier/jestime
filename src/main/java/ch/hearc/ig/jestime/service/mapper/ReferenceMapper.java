package ch.hearc.ig.jestime.service.mapper;

import ch.hearc.ig.jestime.domain.*;
import ch.hearc.ig.jestime.service.dto.ReferenceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Reference and its DTO ReferenceDTO.
 */
@Mapper(componentModel = "spring", uses = {SourceMapper.class})
public interface ReferenceMapper extends EntityMapper<ReferenceDTO, Reference> {

    @Mapping(source = "source.id", target = "sourceId")
    @Mapping(source = "source.title", target = "sourceTitle")
    @Mapping(source = "source.url", target = "sourceUrl")
    @Mapping(source = "source.code", target = "sourceCode")
    ReferenceDTO toDto(Reference reference);

    @Mapping(source = "sourceId", target = "source")
    Reference toEntity(ReferenceDTO referenceDTO);

    default Reference fromId(Long id) {
        if (id == null) {
            return null;
        }
        Reference reference = new Reference();
        reference.setId(id);
        return reference;
    }
}
