package ch.hearc.ig.jestime.service;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ch.hearc.ig.jestime.domain.SocialCard;
import ch.hearc.ig.jestime.domain.*; // for static metamodels
import ch.hearc.ig.jestime.repository.SocialCardRepository;
import ch.hearc.ig.jestime.service.dto.SocialCardCriteria;

import ch.hearc.ig.jestime.service.dto.SocialCardDTO;
import ch.hearc.ig.jestime.service.mapper.SocialCardMapper;

/**
 * Service for executing complex queries for SocialCard entities in the database.
 * The main input is a {@link SocialCardCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SocialCardDTO} or a {@link Page} of {@link SocialCardDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SocialCardQueryService extends QueryService<SocialCard> {

    private final Logger log = LoggerFactory.getLogger(SocialCardQueryService.class);


    private final SocialCardRepository socialCardRepository;

    private final SocialCardMapper socialCardMapper;

    public SocialCardQueryService(SocialCardRepository socialCardRepository, SocialCardMapper socialCardMapper) {
        this.socialCardRepository = socialCardRepository;
        this.socialCardMapper = socialCardMapper;
    }

    /**
     * Return a {@link List} of {@link SocialCardDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SocialCardDTO> findByCriteria(SocialCardCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<SocialCard> specification = createSpecification(criteria);
        return socialCardMapper.toDto(socialCardRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SocialCardDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SocialCardDTO> findByCriteria(SocialCardCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<SocialCard> specification = createSpecification(criteria);
        final Page<SocialCard> result = socialCardRepository.findAll(specification, page);
        return result.map(socialCardMapper::toDto);
    }

    /**
     * Function to convert SocialCardCriteria to a {@link Specifications}
     */
    private Specifications<SocialCard> createSpecification(SocialCardCriteria criteria) {
        Specifications<SocialCard> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SocialCard_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), SocialCard_.code));
            }
            if (criteria.getTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitle(), SocialCard_.title));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), SocialCard_.description));
            }
            if (criteria.getGeneral() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGeneral(), SocialCard_.general));
            }
            if (criteria.getProcedure() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProcedure(), SocialCard_.procedure));
            }
            if (criteria.getRecourse() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRecourse(), SocialCard_.recourse));
            }
            if (criteria.getCreationDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreationDate(), SocialCard_.creationDate));
            }
            if (criteria.getEditDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEditDate(), SocialCard_.editDate));
            }
            if (criteria.getJuridictionId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getJuridictionId(), SocialCard_.juridiction, Juridiction_.id));
            }
            if (criteria.getParentSocialCardId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getParentSocialCardId(), SocialCard_.parentSocialCard, SocialCard_.id));
            }
            if (criteria.getCategoryId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getCategoryId(), SocialCard_.category, Category_.id));
            }
            if (criteria.getAddressesId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getAddressesId(), SocialCard_.addresses, Address_.id));
            }
            if (criteria.getCreatorId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getCreatorId(), SocialCard_.creator, User_.id));
            }
            if (criteria.getUpdatorId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getUpdatorId(), SocialCard_.updator, User_.id));
            }
            if (criteria.getSourcesId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getSourcesId(), SocialCard_.sources, Source_.id));
            }
            if (criteria.getConditionsId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getConditionsId(), SocialCard_.conditions, Condition_.id));
            }
        }
        return specification;
    }

}
