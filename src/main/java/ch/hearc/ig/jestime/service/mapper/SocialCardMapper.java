package ch.hearc.ig.jestime.service.mapper;

import ch.hearc.ig.jestime.domain.*;
import ch.hearc.ig.jestime.service.dto.SocialCardDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SocialCard and its DTO SocialCardDTO.
 */
@Mapper(componentModel = "spring", uses = {JuridictionMapper.class, CategoryMapper.class,
    AddressMapper.class, UserMapper.class, SourceMapper.class, ConditionMapper.class})
public interface SocialCardMapper extends EntityMapper<SocialCardDTO, SocialCard> {

    @Mapping(source = "juridiction.id", target = "juridictionId")
    @Mapping(source = "juridiction.name", target = "juridictionName")
    @Mapping(source = "juridiction.code", target = "juridictionCode")
    @Mapping(source = "parentSocialCard.id", target = "parentSocialCardId")
    @Mapping(source = "parentSocialCard.title", target = "parentSocialCardTitle")
    @Mapping(source = "category.id", target = "categoryId")
    @Mapping(source = "category.name", target = "categoryName")
    @Mapping(source = "creator.id", target = "creatorId")
    @Mapping(source = "creator.login", target = "creatorLogin")
    @Mapping(source = "updator.id", target = "updatorId")
    @Mapping(source = "updator.login", target = "updatorLogin")
    SocialCardDTO toDto(SocialCard socialCard);

    @Mapping(source = "juridictionId", target = "juridiction")
    @Mapping(source = "parentSocialCardId", target = "parentSocialCard")
    @Mapping(source = "categoryId", target = "category")
    @Mapping(source = "creatorId", target = "creator")
    @Mapping(source = "updatorId", target = "updator")
    SocialCard toEntity(SocialCardDTO socialCardDTO);

    default SocialCard fromId(Long id) {
        if (id == null) {
            return null;
        }
        SocialCard socialCard = new SocialCard();
        socialCard.setId(id);
        return socialCard;
    }
}
