package ch.hearc.ig.jestime.service;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ch.hearc.ig.jestime.domain.AddressAudit;
import ch.hearc.ig.jestime.domain.*; // for static metamodels
import ch.hearc.ig.jestime.repository.AddressAuditRepository;
import ch.hearc.ig.jestime.service.dto.AddressAuditCriteria;

import ch.hearc.ig.jestime.service.dto.AddressAuditDTO;
import ch.hearc.ig.jestime.service.mapper.AddressAuditMapper;

/**
 * Service for executing complex queries for AddressAudit entities in the database.
 * The main input is a {@link AddressAuditCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AddressAuditDTO} or a {@link Page} of {@link AddressAuditDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AddressAuditQueryService extends QueryService<AddressAudit> {

    private final Logger log = LoggerFactory.getLogger(AddressAuditQueryService.class);


    private final AddressAuditRepository addressAuditRepository;

    private final AddressAuditMapper addressAuditMapper;

    public AddressAuditQueryService(AddressAuditRepository addressAuditRepository, AddressAuditMapper addressAuditMapper) {
        this.addressAuditRepository = addressAuditRepository;
        this.addressAuditMapper = addressAuditMapper;
    }

    /**
     * Return a {@link List} of {@link AddressAuditDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AddressAuditDTO> findByCriteria(AddressAuditCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<AddressAudit> specification = createSpecification(criteria);
        return addressAuditMapper.toDto(addressAuditRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link AddressAuditDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AddressAuditDTO> findByCriteria(AddressAuditCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<AddressAudit> specification = createSpecification(criteria);
        final Page<AddressAudit> result = addressAuditRepository.findAll(specification, page);
        return result.map(addressAuditMapper::toDto);
    }

    /**
     * Function to convert AddressAuditCriteria to a {@link Specifications}
     */
    private Specifications<AddressAudit> createSpecification(AddressAuditCriteria criteria) {
        Specifications<AddressAudit> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AddressAudit_.id));
            }
            if (criteria.getAction() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAction(), AddressAudit_.action));
            }
            if (criteria.getActionDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getActionDate(), AddressAudit_.actionDate));
            }
            if (criteria.getActionUser() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getActionUser(), AddressAudit_.actionUser));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), AddressAudit_.code));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), AddressAudit_.name));
            }
            if (criteria.getLocation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLocation(), AddressAudit_.location));
            }
            if (criteria.getPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhone(), AddressAudit_.phone));
            }
            if (criteria.getFax() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFax(), AddressAudit_.fax));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), AddressAudit_.email));
            }
            if (criteria.getWebsite() != null) {
                specification = specification.and(buildStringSpecification(criteria.getWebsite(), AddressAudit_.website));
            }
        }
        return specification;
    }

}
