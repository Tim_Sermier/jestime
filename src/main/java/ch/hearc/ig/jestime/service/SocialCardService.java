package ch.hearc.ig.jestime.service;

import ch.hearc.ig.jestime.domain.SocialCard;
import ch.hearc.ig.jestime.repository.SocialCardRepository;
import ch.hearc.ig.jestime.service.dto.SocialCardDTO;
import ch.hearc.ig.jestime.service.mapper.SocialCardMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing SocialCard.
 */
@Service
@Transactional
public class SocialCardService {

    private final Logger log = LoggerFactory.getLogger(SocialCardService.class);

    private final SocialCardRepository socialCardRepository;

    private final SocialCardMapper socialCardMapper;

    public SocialCardService(SocialCardRepository socialCardRepository, SocialCardMapper socialCardMapper) {
        this.socialCardRepository = socialCardRepository;
        this.socialCardMapper = socialCardMapper;
    }

    /**
     * Save a socialCard.
     *
     * @param socialCardDTO the entity to save
     * @return the persisted entity
     */
    public SocialCardDTO save(SocialCardDTO socialCardDTO) {
        log.debug("Request to save SocialCard : {}", socialCardDTO);
        SocialCard socialCard = socialCardMapper.toEntity(socialCardDTO);
        socialCard = socialCardRepository.save(socialCard);
        return socialCardMapper.toDto(socialCard);
    }

    /**
     * Get all the socialCards.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<SocialCardDTO> findAll() {
        log.debug("Request to get all SocialCards");
        return socialCardRepository.findAllWithEagerRelationships().stream()
            .map(socialCardMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one socialCard by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public SocialCardDTO findOne(Long id) {
        log.debug("Request to get SocialCard : {}", id);
        SocialCard socialCard = socialCardRepository.findOneWithEagerRelationships(id);
        return socialCardMapper.toDto(socialCard);
    }

    /**
     * Delete the socialCard by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SocialCard : {}", id);
        socialCardRepository.delete(id);
    }
}
