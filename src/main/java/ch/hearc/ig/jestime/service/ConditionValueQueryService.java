package ch.hearc.ig.jestime.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ch.hearc.ig.jestime.domain.ConditionValue;
import ch.hearc.ig.jestime.domain.*; // for static metamodels
import ch.hearc.ig.jestime.repository.ConditionValueRepository;
import ch.hearc.ig.jestime.service.dto.ConditionValueCriteria;

import ch.hearc.ig.jestime.service.dto.ConditionValueDTO;
import ch.hearc.ig.jestime.service.mapper.ConditionValueMapper;

/**
 * Service for executing complex queries for ConditionValue entities in the database.
 * The main input is a {@link ConditionValueCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ConditionValueDTO} or a {@link Page} of {@link ConditionValueDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ConditionValueQueryService extends QueryService<ConditionValue> {

    private final Logger log = LoggerFactory.getLogger(ConditionValueQueryService.class);


    private final ConditionValueRepository conditionValueRepository;

    private final ConditionValueMapper conditionValueMapper;

    public ConditionValueQueryService(ConditionValueRepository conditionValueRepository, ConditionValueMapper conditionValueMapper) {
        this.conditionValueRepository = conditionValueRepository;
        this.conditionValueMapper = conditionValueMapper;
    }

    /**
     * Return a {@link List} of {@link ConditionValueDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ConditionValueDTO> findByCriteria(ConditionValueCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<ConditionValue> specification = createSpecification(criteria);
        return conditionValueMapper.toDto(conditionValueRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ConditionValueDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ConditionValueDTO> findByCriteria(ConditionValueCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<ConditionValue> specification = createSpecification(criteria);
        final Page<ConditionValue> result = conditionValueRepository.findAll(specification, page);
        return result.map(conditionValueMapper::toDto);
    }

    /**
     * Function to convert ConditionValueCriteria to a {@link Specifications}
     */
    private Specifications<ConditionValue> createSpecification(ConditionValueCriteria criteria) {
        Specifications<ConditionValue> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ConditionValue_.id));
            }
            if (criteria.getValue() != null) {
                specification = specification.and(buildStringSpecification(criteria.getValue(), ConditionValue_.value));
            }
            if (criteria.getVariableId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getVariableId(), ConditionValue_.variable, Variable_.id));
            }
            if (criteria.getConditionId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getConditionId(), ConditionValue_.condition, Condition_.id));
            }
            if (criteria.getReferenceId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getReferenceId(), ConditionValue_.reference, Reference_.id));
            }
            if (criteria.getOperatorId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getOperatorId(), ConditionValue_.operator, Operator_.id));
            }
        }
        return specification;
    }

}
