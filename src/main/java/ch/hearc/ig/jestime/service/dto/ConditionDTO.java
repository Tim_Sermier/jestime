package ch.hearc.ig.jestime.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the Condition entity.
 */
public class ConditionDTO implements Serializable {

    private Long id;

    @NotNull
    private String code;

    @Lob
    private String description;

    private LocalDate creationDate;

    private LocalDate editDate;

    private Long socialCardId;

    private String socialCardTitle;

    private Long creatorId;

    private String creatorLogin;

    private Long updatorId;

    private String updatorLogin;

    private Set<ConditionValueDTO> conditionValues = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getEditDate() {
        return editDate;
    }

    public void setEditDate(LocalDate editDate) {
        this.editDate = editDate;
    }

    public Long getSocialCardId() {
        return socialCardId;
    }

    public void setSocialCardId(Long socialCardId) {
        this.socialCardId = socialCardId;
    }

    public String getSocialCardTitle() {
        return socialCardTitle;
    }

    public void setSocialCardTitle(String socialCardTitle) {
        this.socialCardTitle = socialCardTitle;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long userId) {
        this.creatorId = userId;
    }

    public String getCreatorLogin() {
        return creatorLogin;
    }

    public void setCreatorLogin(String userLogin) {
        this.creatorLogin = userLogin;
    }

    public Long getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(Long userId) {
        this.updatorId = userId;
    }

    public String getUpdatorLogin() {
        return updatorLogin;
    }

    public void setUpdatorLogin(String userLogin) {
        this.updatorLogin = userLogin;
    }

    public Set<ConditionValueDTO> getConditionValues() {
        return conditionValues;
    }

    public void setConditionValues(Set<ConditionValueDTO> conditionValues) {
        this.conditionValues = conditionValues;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ConditionDTO conditionDTO = (ConditionDTO) o;
        if(conditionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), conditionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ConditionDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", description='" + getDescription() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", editDate='" + getEditDate() + "'" +
            "}";
    }
}
