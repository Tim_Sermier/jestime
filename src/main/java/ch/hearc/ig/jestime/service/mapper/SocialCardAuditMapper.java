package ch.hearc.ig.jestime.service.mapper;

import ch.hearc.ig.jestime.domain.*;
import ch.hearc.ig.jestime.service.dto.SocialCardAuditDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SocialCardAudit and its DTO SocialCardAuditDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SocialCardAuditMapper extends EntityMapper<SocialCardAuditDTO, SocialCardAudit> {



    default SocialCardAudit fromId(Long id) {
        if (id == null) {
            return null;
        }
        SocialCardAudit socialCardAudit = new SocialCardAudit();
        socialCardAudit.setId(id);
        return socialCardAudit;
    }
}
