package ch.hearc.ig.jestime.service.mapper;

import ch.hearc.ig.jestime.domain.*;
import ch.hearc.ig.jestime.service.dto.AddressAuditDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity AddressAudit and its DTO AddressAuditDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AddressAuditMapper extends EntityMapper<AddressAuditDTO, AddressAudit> {



    default AddressAudit fromId(Long id) {
        if (id == null) {
            return null;
        }
        AddressAudit addressAudit = new AddressAudit();
        addressAudit.setId(id);
        return addressAudit;
    }
}
