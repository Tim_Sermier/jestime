package ch.hearc.ig.jestime.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the SocialCardAudit entity.
 */
public class SocialCardAuditDTO implements Serializable {

    private Long id;

    @NotNull
    private String action;

    @NotNull
    private LocalDate actionDate;

    @NotNull
    private String actionUser;

    private String title;

    private String code;

    @Lob
    private String general;

    @Lob
    private String description;

    @Lob
    private String procedure;

    @Lob
    private String recourse;

    private Integer juridictionId;

    private Integer categoryId;

    private Integer parentSocialCardId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public LocalDate getActionDate() {
        return actionDate;
    }

    public void setActionDate(LocalDate actionDate) {
        this.actionDate = actionDate;
    }

    public String getActionUser() {
        return actionUser;
    }

    public void setActionUser(String actionUser) {
        this.actionUser = actionUser;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getGeneral() {
        return general;
    }

    public void setGeneral(String general) {
        this.general = general;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProcedure() {
        return procedure;
    }

    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    public String getRecourse() {
        return recourse;
    }

    public void setRecourse(String recourse) {
        this.recourse = recourse;
    }

    public Integer getJuridictionId() {
        return juridictionId;
    }

    public void setJuridictionId(Integer juridictionId) {
        this.juridictionId = juridictionId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getParentSocialCardId() {
        return parentSocialCardId;
    }

    public void setParentSocialCardId(Integer parentSocialCardId) {
        this.parentSocialCardId = parentSocialCardId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SocialCardAuditDTO socialCardAuditDTO = (SocialCardAuditDTO) o;
        if(socialCardAuditDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), socialCardAuditDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SocialCardAuditDTO{" +
            "id=" + getId() +
            ", action='" + getAction() + "'" +
            ", actionDate='" + getActionDate() + "'" +
            ", actionUser='" + getActionUser() + "'" +
            ", title='" + getTitle() + "'" +
            ", code='" + getCode() + "'" +
            ", general='" + getGeneral() + "'" +
            ", description='" + getDescription() + "'" +
            ", procedure='" + getProcedure() + "'" +
            ", recourse='" + getRecourse() + "'" +
            ", juridictionId=" + getJuridictionId() +
            ", categoryId=" + getCategoryId() +
            ", parentSocialCardId=" + getParentSocialCardId() +
            "}";
    }
}
