package ch.hearc.ig.jestime.service;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ch.hearc.ig.jestime.domain.SocialCardAudit;
import ch.hearc.ig.jestime.domain.*; // for static metamodels
import ch.hearc.ig.jestime.repository.SocialCardAuditRepository;
import ch.hearc.ig.jestime.service.dto.SocialCardAuditCriteria;

import ch.hearc.ig.jestime.service.dto.SocialCardAuditDTO;
import ch.hearc.ig.jestime.service.mapper.SocialCardAuditMapper;

/**
 * Service for executing complex queries for SocialCardAudit entities in the database.
 * The main input is a {@link SocialCardAuditCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SocialCardAuditDTO} or a {@link Page} of {@link SocialCardAuditDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SocialCardAuditQueryService extends QueryService<SocialCardAudit> {

    private final Logger log = LoggerFactory.getLogger(SocialCardAuditQueryService.class);


    private final SocialCardAuditRepository socialCardAuditRepository;

    private final SocialCardAuditMapper socialCardAuditMapper;

    public SocialCardAuditQueryService(SocialCardAuditRepository socialCardAuditRepository, SocialCardAuditMapper socialCardAuditMapper) {
        this.socialCardAuditRepository = socialCardAuditRepository;
        this.socialCardAuditMapper = socialCardAuditMapper;
    }

    /**
     * Return a {@link List} of {@link SocialCardAuditDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SocialCardAuditDTO> findByCriteria(SocialCardAuditCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<SocialCardAudit> specification = createSpecification(criteria);
        return socialCardAuditMapper.toDto(socialCardAuditRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SocialCardAuditDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SocialCardAuditDTO> findByCriteria(SocialCardAuditCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<SocialCardAudit> specification = createSpecification(criteria);
        final Page<SocialCardAudit> result = socialCardAuditRepository.findAll(specification, page);
        return result.map(socialCardAuditMapper::toDto);
    }

    /**
     * Function to convert SocialCardAuditCriteria to a {@link Specifications}
     */
    private Specifications<SocialCardAudit> createSpecification(SocialCardAuditCriteria criteria) {
        Specifications<SocialCardAudit> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SocialCardAudit_.id));
            }
            if (criteria.getAction() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAction(), SocialCardAudit_.action));
            }
            if (criteria.getActionDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getActionDate(), SocialCardAudit_.actionDate));
            }
            if (criteria.getActionUser() != null) {
                specification = specification.and(buildStringSpecification(criteria.getActionUser(), SocialCardAudit_.actionUser));
            }
            if (criteria.getTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitle(), SocialCardAudit_.title));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), SocialCardAudit_.code));
            }
            if (criteria.getJuridictionId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getJuridictionId(), SocialCardAudit_.juridictionId));
            }
            if (criteria.getCategoryId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCategoryId(), SocialCardAudit_.categoryId));
            }
            if (criteria.getParentSocialCardId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getParentSocialCardId(), SocialCardAudit_.parentSocialCardId));
            }
        }
        return specification;
    }

}
