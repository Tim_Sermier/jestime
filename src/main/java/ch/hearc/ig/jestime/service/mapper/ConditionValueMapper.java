package ch.hearc.ig.jestime.service.mapper;

import ch.hearc.ig.jestime.domain.*;
import ch.hearc.ig.jestime.service.dto.ConditionValueDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ConditionValue and its DTO ConditionValueDTO.
 */
@Mapper(componentModel = "spring", uses = {VariableMapper.class, ConditionMapper.class, ReferenceMapper.class, OperatorMapper.class})
public interface ConditionValueMapper extends EntityMapper<ConditionValueDTO, ConditionValue> {

    @Mapping(source = "variable.id", target = "variableId")
    @Mapping(source = "variable.name", target = "variableName")
    @Mapping(source = "condition.id", target = "conditionId")
    @Mapping(source = "reference.id", target = "referenceId")
    @Mapping(source = "reference.title", target = "referenceTitle")
    @Mapping(source = "operator.id", target = "operatorId")
    @Mapping(source = "operator.name", target = "operatorName")
    @Mapping(source = "reference.code", target = "referenceCode")
    @Mapping(source = "condition.code", target = "conditionCode")
    @Mapping(source = "reference", target = "reference")
    ConditionValueDTO toDto(ConditionValue conditionValue);

    @Mapping(source = "variableId", target = "variable")
    @Mapping(source = "conditionId", target = "condition")
    @Mapping(source = "referenceId", target = "reference")
    @Mapping(source = "operatorId", target = "operator")
    ConditionValue toEntity(ConditionValueDTO conditionValueDTO);

    default ConditionValue fromId(Long id) {
        if (id == null) {
            return null;
        }
        ConditionValue conditionValue = new ConditionValue();
        conditionValue.setId(id);
        return conditionValue;
    }
}
