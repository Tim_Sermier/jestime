package ch.hearc.ig.jestime.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the Source entity.
 */
public class SourceDTO implements Serializable {

    private Long id;

    @NotNull
    private String code;

    @NotNull
    private String title;

    @Lob
    private String description;

    private String url;

    @NotNull
    private LocalDate creationDate;

    private LocalDate editionDate;

    private Long juridictionId;

    private String juridictionName;

    private Long creatorId;

    private String creatorLogin;

    private Long updatorId;

    private String updatorLogin;

    private Set<ReferenceDTO> references = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getEditionDate() {
        return editionDate;
    }

    public void setEditionDate(LocalDate editionDate) {
        this.editionDate = editionDate;
    }

    public Long getJuridictionId() {
        return juridictionId;
    }

    public void setJuridictionId(Long juridictionId) {
        this.juridictionId = juridictionId;
    }

    public String getJuridictionName() {
        return juridictionName;
    }

    public void setJuridictionName(String juridictionName) {
        this.juridictionName = juridictionName;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long userId) {
        this.creatorId = userId;
    }

    public String getCreatorLogin() {
        return creatorLogin;
    }

    public void setCreatorLogin(String userLogin) {
        this.creatorLogin = userLogin;
    }

    public Long getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(Long userId) {
        this.updatorId = userId;
    }

    public String getUpdatorLogin() {
        return updatorLogin;
    }

    public void setUpdatorLogin(String userLogin) {
        this.updatorLogin = userLogin;
    }

    public Set<ReferenceDTO> getReferences() {
        return references;
    }

    public void setReferences(Set<ReferenceDTO> references) {
        this.references = references;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SourceDTO sourceDTO = (SourceDTO) o;
        if(sourceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sourceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SourceDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", url='" + getUrl() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", editionDate='" + getEditionDate() + "'" +
            "}";
    }
}
