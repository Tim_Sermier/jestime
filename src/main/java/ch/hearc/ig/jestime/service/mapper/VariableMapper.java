package ch.hearc.ig.jestime.service.mapper;

import ch.hearc.ig.jestime.domain.*;
import ch.hearc.ig.jestime.service.dto.VariableDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Variable and its DTO VariableDTO.
 */
@Mapper(componentModel = "spring", uses = {VariableTypeMapper.class})
public interface VariableMapper extends EntityMapper<VariableDTO, Variable> {

    @Mapping(source = "type.id", target = "typeId")
    @Mapping(source = "type.name", target = "typeName")
    VariableDTO toDto(Variable variable);

    @Mapping(source = "typeId", target = "type")
    Variable toEntity(VariableDTO variableDTO);

    default Variable fromId(Long id) {
        if (id == null) {
            return null;
        }
        Variable variable = new Variable();
        variable.setId(id);
        return variable;
    }
}
