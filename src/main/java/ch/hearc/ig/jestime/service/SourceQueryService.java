package ch.hearc.ig.jestime.service;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ch.hearc.ig.jestime.domain.Source;
import ch.hearc.ig.jestime.domain.*; // for static metamodels
import ch.hearc.ig.jestime.repository.SourceRepository;
import ch.hearc.ig.jestime.service.dto.SourceCriteria;

import ch.hearc.ig.jestime.service.dto.SourceDTO;
import ch.hearc.ig.jestime.service.mapper.SourceMapper;

/**
 * Service for executing complex queries for Source entities in the database.
 * The main input is a {@link SourceCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SourceDTO} or a {@link Page} of {@link SourceDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SourceQueryService extends QueryService<Source> {

    private final Logger log = LoggerFactory.getLogger(SourceQueryService.class);


    private final SourceRepository sourceRepository;

    private final SourceMapper sourceMapper;

    public SourceQueryService(SourceRepository sourceRepository, SourceMapper sourceMapper) {
        this.sourceRepository = sourceRepository;
        this.sourceMapper = sourceMapper;
    }

    /**
     * Return a {@link List} of {@link SourceDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SourceDTO> findByCriteria(SourceCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Source> specification = createSpecification(criteria);
        return sourceMapper.toDto(sourceRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SourceDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SourceDTO> findByCriteria(SourceCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Source> specification = createSpecification(criteria);
        final Page<Source> result = sourceRepository.findAll(specification, page);
        return result.map(sourceMapper::toDto);
    }

    /**
     * Function to convert SourceCriteria to a {@link Specifications}
     */
    private Specifications<Source> createSpecification(SourceCriteria criteria) {
        Specifications<Source> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Source_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), Source_.code));
            }
            if (criteria.getTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitle(), Source_.title));
            }
            if (criteria.getUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUrl(), Source_.url));
            }
            if (criteria.getCreationDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreationDate(), Source_.creationDate));
            }
            if (criteria.getEditionDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEditionDate(), Source_.editionDate));
            }
            if (criteria.getSocialCardsId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getSocialCardsId(), Source_.socialCards, SocialCard_.id));
            }
            if (criteria.getJuridictionId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getJuridictionId(), Source_.juridiction, Juridiction_.id));
            }
            if (criteria.getCreatorId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getCreatorId(), Source_.creator, User_.id));
            }
            if (criteria.getUpdatorId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getUpdatorId(), Source_.updator, User_.id));
            }
            if (criteria.getReferencesId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getReferencesId(), Source_.references, Reference_.id));
            }
        }
        return specification;
    }

}
