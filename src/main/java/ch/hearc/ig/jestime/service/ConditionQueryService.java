package ch.hearc.ig.jestime.service;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ch.hearc.ig.jestime.domain.Condition;
import ch.hearc.ig.jestime.domain.*; // for static metamodels
import ch.hearc.ig.jestime.repository.ConditionRepository;
import ch.hearc.ig.jestime.service.dto.ConditionCriteria;

import ch.hearc.ig.jestime.service.dto.ConditionDTO;
import ch.hearc.ig.jestime.service.mapper.ConditionMapper;

/**
 * Service for executing complex queries for Condition entities in the database.
 * The main input is a {@link ConditionCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ConditionDTO} or a {@link Page} of {@link ConditionDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ConditionQueryService extends QueryService<Condition> {

    private final Logger log = LoggerFactory.getLogger(ConditionQueryService.class);


    private final ConditionRepository conditionRepository;

    private final ConditionMapper conditionMapper;

    public ConditionQueryService(ConditionRepository conditionRepository, ConditionMapper conditionMapper) {
        this.conditionRepository = conditionRepository;
        this.conditionMapper = conditionMapper;
    }

    /**
     * Return a {@link List} of {@link ConditionDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ConditionDTO> findByCriteria(ConditionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Condition> specification = createSpecification(criteria);
        return conditionMapper.toDto(conditionRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ConditionDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ConditionDTO> findByCriteria(ConditionCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Condition> specification = createSpecification(criteria);
        final Page<Condition> result = conditionRepository.findAll(specification, page);
        return result.map(conditionMapper::toDto);
    }

    /**
     * Function to convert ConditionCriteria to a {@link Specifications}
     */
    private Specifications<Condition> createSpecification(ConditionCriteria criteria) {
        Specifications<Condition> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Condition_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), Condition_.code));
            }
            if (criteria.getCreationDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreationDate(), Condition_.creationDate));
            }
            if (criteria.getEditDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEditDate(), Condition_.editDate));
            }
            if (criteria.getSocialCardId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getSocialCardId(), Condition_.socialCard, SocialCard_.id));
            }
            if (criteria.getCreatorId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getCreatorId(), Condition_.creator, User_.id));
            }
            if (criteria.getUpdatorId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getUpdatorId(), Condition_.updator, User_.id));
            }
            if (criteria.getConditionValuesId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getConditionValuesId(), Condition_.conditionValues, ConditionValue_.id));
            }
        }
        return specification;
    }

}
