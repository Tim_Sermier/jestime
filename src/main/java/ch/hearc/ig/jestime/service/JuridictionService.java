package ch.hearc.ig.jestime.service;

import ch.hearc.ig.jestime.domain.Juridiction;
import ch.hearc.ig.jestime.repository.JuridictionRepository;
import ch.hearc.ig.jestime.service.dto.JuridictionDTO;
import ch.hearc.ig.jestime.service.mapper.JuridictionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Sort;

/**
 * Service Implementation for managing Juridiction.
 */
@Service
@Transactional
public class JuridictionService {

    private final Logger log = LoggerFactory.getLogger(JuridictionService.class);

    private final JuridictionRepository juridictionRepository;

    private final JuridictionMapper juridictionMapper;

    public JuridictionService(JuridictionRepository juridictionRepository, JuridictionMapper juridictionMapper) {
        this.juridictionRepository = juridictionRepository;
        this.juridictionMapper = juridictionMapper;
    }

    /**
     * Save a juridiction.
     *
     * @param juridictionDTO the entity to save
     * @return the persisted entity
     */
    public JuridictionDTO save(JuridictionDTO juridictionDTO) {
        log.debug("Request to save Juridiction : {}", juridictionDTO);
        Juridiction juridiction = juridictionMapper.toEntity(juridictionDTO);
        juridiction = juridictionRepository.save(juridiction);
        return juridictionMapper.toDto(juridiction);
    }

    /**
     * Get all the juridictions.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<JuridictionDTO> findAll() {
        log.debug("Request to get all Juridictions");
        return juridictionRepository.findAll(sortByNameAsc()).stream()
            .map(juridictionMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one juridiction by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public JuridictionDTO findOne(Long id) {
        log.debug("Request to get Juridiction : {}", id);
        Juridiction juridiction = juridictionRepository.findOne(id);
        return juridictionMapper.toDto(juridiction);
    }

    /**
     * Delete the juridiction by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Juridiction : {}", id);
        juridictionRepository.delete(id);
    }
    
    private Sort sortByNameAsc() {
        return new Sort(Sort.Direction.ASC, "name");
    }
}
