package ch.hearc.ig.jestime.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the SocialCard entity.
 */
public class SocialCardDTO implements Serializable {

    private Long id;

    @NotNull
    private String code;

    @NotNull
    private String title;

    private String description;

    private String general;

    private String procedure;

    private String recourse;

    @NotNull
    private LocalDate creationDate;

    private LocalDate editDate;

    private Long juridictionId;

    private String juridictionName;

    private String juridictionCode;

    private Long parentSocialCardId;

    private String parentSocialCardTitle;

    private Long categoryId;

    private String categoryName;

    private Set<AddressDTO> addresses = new HashSet<>();

    private Set<ConditionDTO> conditions = new HashSet<>();

    private Long creatorId;

    private String creatorLogin;

    private Long updatorId;

    private String updatorLogin;

    private Set<SourceDTO> sources = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGeneral() {
        return general;
    }

    public void setGeneral(String general) {
        this.general = general;
    }

    public String getProcedure() {
        return procedure;
    }

    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    public String getRecourse() {
        return recourse;
    }

    public void setRecourse(String recourse) {
        this.recourse = recourse;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getEditDate() {
        return editDate;
    }

    public void setEditDate(LocalDate editDate) {
        this.editDate = editDate;
    }

    public Long getJuridictionId() {
        return juridictionId;
    }

    public void setJuridictionId(Long juridictionId) {
        this.juridictionId = juridictionId;
    }

    public String getJuridictionName() {
        return juridictionName;
    }

    public void setJuridictionName(String juridictionName) {
        this.juridictionName = juridictionName;
    }

    public String getJuridictionCode() {
        return juridictionCode;
    }

    public void setJuridictionCode(String juridictionCode) {
        this.juridictionCode = juridictionCode;
    }

    public Long getParentSocialCardId() {
        return parentSocialCardId;
    }

    public void setParentSocialCardId(Long socialCardId) {
        this.parentSocialCardId = socialCardId;
    }

    public String getParentSocialCardTitle() {
        return parentSocialCardTitle;
    }

    public void setParentSocialCardTitle(String socialCardTitle) {
        this.parentSocialCardTitle = socialCardTitle;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Set<AddressDTO> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<AddressDTO> addresses) {
        this.addresses = addresses;
    }

    public Set<ConditionDTO> getConditions() {
        return conditions;
    }

    public void setConditions(Set<ConditionDTO> conditions) {
        this.conditions = conditions;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long userId) {
        this.creatorId = userId;
    }

    public String getCreatorLogin() {
        return creatorLogin;
    }

    public void setCreatorLogin(String userLogin) {
        this.creatorLogin = userLogin;
    }

    public Long getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(Long userId) {
        this.updatorId = userId;
    }

    public String getUpdatorLogin() {
        return updatorLogin;
    }

    public void setUpdatorLogin(String userLogin) {
        this.updatorLogin = userLogin;
    }

    public Set<SourceDTO> getSources() {
        return sources;
    }

    public void setSources(Set<SourceDTO> sources) {
        this.sources = sources;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SocialCardDTO socialCardDTO = (SocialCardDTO) o;
        if(socialCardDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), socialCardDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SocialCardDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", general='" + getGeneral() + "'" +
            ", procedure='" + getProcedure() + "'" +
            ", recourse='" + getRecourse() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", editDate='" + getEditDate() + "'" +
            "}";
    }
}
