package ch.hearc.ig.jestime.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ch.hearc.ig.jestime.domain.Operator;
import ch.hearc.ig.jestime.domain.*; // for static metamodels
import ch.hearc.ig.jestime.repository.OperatorRepository;
import ch.hearc.ig.jestime.service.dto.OperatorCriteria;

import ch.hearc.ig.jestime.service.dto.OperatorDTO;
import ch.hearc.ig.jestime.service.mapper.OperatorMapper;

/**
 * Service for executing complex queries for Operator entities in the database.
 * The main input is a {@link OperatorCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link OperatorDTO} or a {@link Page} of {@link OperatorDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OperatorQueryService extends QueryService<Operator> {

    private final Logger log = LoggerFactory.getLogger(OperatorQueryService.class);


    private final OperatorRepository operatorRepository;

    private final OperatorMapper operatorMapper;

    public OperatorQueryService(OperatorRepository operatorRepository, OperatorMapper operatorMapper) {
        this.operatorRepository = operatorRepository;
        this.operatorMapper = operatorMapper;
    }

    /**
     * Return a {@link List} of {@link OperatorDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<OperatorDTO> findByCriteria(OperatorCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Operator> specification = createSpecification(criteria);
        return operatorMapper.toDto(operatorRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link OperatorDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<OperatorDTO> findByCriteria(OperatorCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Operator> specification = createSpecification(criteria);
        final Page<Operator> result = operatorRepository.findAll(specification, page);
        return result.map(operatorMapper::toDto);
    }

    /**
     * Function to convert OperatorCriteria to a {@link Specifications}
     */
    private Specifications<Operator> createSpecification(OperatorCriteria criteria) {
        Specifications<Operator> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Operator_.id));
            }
            if (criteria.getSymbol() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSymbol(), Operator_.symbol));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Operator_.name));
            }
        }
        return specification;
    }

}
