package ch.hearc.ig.jestime.service;

import ch.hearc.ig.jestime.domain.Condition;
import ch.hearc.ig.jestime.repository.ConditionRepository;
import ch.hearc.ig.jestime.service.dto.ConditionDTO;
import ch.hearc.ig.jestime.service.mapper.ConditionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Condition.
 */
@Service
@Transactional
public class ConditionService {

    private final Logger log = LoggerFactory.getLogger(ConditionService.class);

    private final ConditionRepository conditionRepository;

    private final ConditionMapper conditionMapper;

    public ConditionService(ConditionRepository conditionRepository, ConditionMapper conditionMapper) {
        this.conditionRepository = conditionRepository;
        this.conditionMapper = conditionMapper;
    }

    /**
     * Save a condition.
     *
     * @param conditionDTO the entity to save
     * @return the persisted entity
     */
    public ConditionDTO save(ConditionDTO conditionDTO) {
        log.debug("Request to save Condition : {}", conditionDTO);
        Condition condition = conditionMapper.toEntity(conditionDTO);
        condition = conditionRepository.save(condition);
        return conditionMapper.toDto(condition);
    }

    /**
     * Get all the conditions.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ConditionDTO> findAll() {
        log.debug("Request to get all Conditions");
        return conditionRepository.findAll().stream()
            .map(conditionMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one condition by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ConditionDTO findOne(Long id) {
        log.debug("Request to get Condition : {}", id);
        Condition condition = conditionRepository.findOne(id);
        return conditionMapper.toDto(condition);
    }

    /**
     * Delete the condition by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Condition : {}", id);
        conditionRepository.delete(id);
    }
}
