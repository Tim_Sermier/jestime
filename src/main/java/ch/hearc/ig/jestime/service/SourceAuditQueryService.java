package ch.hearc.ig.jestime.service;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ch.hearc.ig.jestime.domain.SourceAudit;
import ch.hearc.ig.jestime.domain.*; // for static metamodels
import ch.hearc.ig.jestime.repository.SourceAuditRepository;
import ch.hearc.ig.jestime.service.dto.SourceAuditCriteria;

import ch.hearc.ig.jestime.service.dto.SourceAuditDTO;
import ch.hearc.ig.jestime.service.mapper.SourceAuditMapper;

/**
 * Service for executing complex queries for SourceAudit entities in the database.
 * The main input is a {@link SourceAuditCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SourceAuditDTO} or a {@link Page} of {@link SourceAuditDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SourceAuditQueryService extends QueryService<SourceAudit> {

    private final Logger log = LoggerFactory.getLogger(SourceAuditQueryService.class);


    private final SourceAuditRepository sourceAuditRepository;

    private final SourceAuditMapper sourceAuditMapper;

    public SourceAuditQueryService(SourceAuditRepository sourceAuditRepository, SourceAuditMapper sourceAuditMapper) {
        this.sourceAuditRepository = sourceAuditRepository;
        this.sourceAuditMapper = sourceAuditMapper;
    }

    /**
     * Return a {@link List} of {@link SourceAuditDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SourceAuditDTO> findByCriteria(SourceAuditCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<SourceAudit> specification = createSpecification(criteria);
        return sourceAuditMapper.toDto(sourceAuditRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SourceAuditDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SourceAuditDTO> findByCriteria(SourceAuditCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<SourceAudit> specification = createSpecification(criteria);
        final Page<SourceAudit> result = sourceAuditRepository.findAll(specification, page);
        return result.map(sourceAuditMapper::toDto);
    }

    /**
     * Function to convert SourceAuditCriteria to a {@link Specifications}
     */
    private Specifications<SourceAudit> createSpecification(SourceAuditCriteria criteria) {
        Specifications<SourceAudit> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SourceAudit_.id));
            }
            if (criteria.getAction() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAction(), SourceAudit_.action));
            }
            if (criteria.getActionDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getActionDate(), SourceAudit_.actionDate));
            }
            if (criteria.getActionUser() != null) {
                specification = specification.and(buildStringSpecification(criteria.getActionUser(), SourceAudit_.actionUser));
            }
            if (criteria.getTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitle(), SourceAudit_.title));
            }
            if (criteria.getUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUrl(), SourceAudit_.url));
            }
            if (criteria.getJuridictionId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getJuridictionId(), SourceAudit_.juridictionId));
            }
        }
        return specification;
    }

}
