package ch.hearc.ig.jestime.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;


import io.github.jhipster.service.filter.LocalDateFilter;



/**
 * Criteria class for the SocialCardAudit entity. This class is used in SocialCardAuditResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /social-card-audits?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SocialCardAuditCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter action;

    private LocalDateFilter actionDate;

    private StringFilter actionUser;

    private StringFilter title;

    private StringFilter code;

    private IntegerFilter juridictionId;

    private IntegerFilter categoryId;

    private IntegerFilter parentSocialCardId;

    public SocialCardAuditCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getAction() {
        return action;
    }

    public void setAction(StringFilter action) {
        this.action = action;
    }

    public LocalDateFilter getActionDate() {
        return actionDate;
    }

    public void setActionDate(LocalDateFilter actionDate) {
        this.actionDate = actionDate;
    }

    public StringFilter getActionUser() {
        return actionUser;
    }

    public void setActionUser(StringFilter actionUser) {
        this.actionUser = actionUser;
    }

    public StringFilter getTitle() {
        return title;
    }

    public void setTitle(StringFilter title) {
        this.title = title;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public IntegerFilter getJuridictionId() {
        return juridictionId;
    }

    public void setJuridictionId(IntegerFilter juridictionId) {
        this.juridictionId = juridictionId;
    }

    public IntegerFilter getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(IntegerFilter categoryId) {
        this.categoryId = categoryId;
    }

    public IntegerFilter getParentSocialCardId() {
        return parentSocialCardId;
    }

    public void setParentSocialCardId(IntegerFilter parentSocialCardId) {
        this.parentSocialCardId = parentSocialCardId;
    }

    @Override
    public String toString() {
        return "SocialCardAuditCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (action != null ? "action=" + action + ", " : "") +
                (actionDate != null ? "actionDate=" + actionDate + ", " : "") +
                (actionUser != null ? "actionUser=" + actionUser + ", " : "") +
                (title != null ? "title=" + title + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (juridictionId != null ? "juridictionId=" + juridictionId + ", " : "") +
                (categoryId != null ? "categoryId=" + categoryId + ", " : "") +
                (parentSocialCardId != null ? "parentSocialCardId=" + parentSocialCardId + ", " : "") +
            "}";
    }

}
