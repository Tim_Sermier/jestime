package ch.hearc.ig.jestime.service.mapper;

import ch.hearc.ig.jestime.domain.*;
import ch.hearc.ig.jestime.service.dto.JuridictionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Juridiction and its DTO JuridictionDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface JuridictionMapper extends EntityMapper<JuridictionDTO, Juridiction> {



    default Juridiction fromId(Long id) {
        if (id == null) {
            return null;
        }
        Juridiction juridiction = new Juridiction();
        juridiction.setId(id);
        return juridiction;
    }
}
