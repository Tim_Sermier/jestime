package ch.hearc.ig.jestime.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ch.hearc.ig.jestime.domain.Variable;
import ch.hearc.ig.jestime.domain.*; // for static metamodels
import ch.hearc.ig.jestime.repository.VariableRepository;
import ch.hearc.ig.jestime.service.dto.VariableCriteria;

import ch.hearc.ig.jestime.service.dto.VariableDTO;
import ch.hearc.ig.jestime.service.mapper.VariableMapper;

/**
 * Service for executing complex queries for Variable entities in the database.
 * The main input is a {@link VariableCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link VariableDTO} or a {@link Page} of {@link VariableDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class VariableQueryService extends QueryService<Variable> {

    private final Logger log = LoggerFactory.getLogger(VariableQueryService.class);


    private final VariableRepository variableRepository;

    private final VariableMapper variableMapper;

    public VariableQueryService(VariableRepository variableRepository, VariableMapper variableMapper) {
        this.variableRepository = variableRepository;
        this.variableMapper = variableMapper;
    }

    /**
     * Return a {@link List} of {@link VariableDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<VariableDTO> findByCriteria(VariableCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Variable> specification = createSpecification(criteria);
        return variableMapper.toDto(variableRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link VariableDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<VariableDTO> findByCriteria(VariableCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Variable> specification = createSpecification(criteria);
        final Page<Variable> result = variableRepository.findAll(specification, page);
        return result.map(variableMapper::toDto);
    }

    /**
     * Function to convert VariableCriteria to a {@link Specifications}
     */
    private Specifications<Variable> createSpecification(VariableCriteria criteria) {
        Specifications<Variable> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Variable_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), Variable_.code));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Variable_.name));
            }
            if (criteria.getTypeId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getTypeId(), Variable_.type, VariableType_.id));
            }
        }
        return specification;
    }

}
