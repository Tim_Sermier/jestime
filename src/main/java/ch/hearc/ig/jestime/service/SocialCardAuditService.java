package ch.hearc.ig.jestime.service;

import ch.hearc.ig.jestime.domain.SocialCardAudit;
import ch.hearc.ig.jestime.repository.SocialCardAuditRepository;
import ch.hearc.ig.jestime.service.dto.SocialCardAuditDTO;
import ch.hearc.ig.jestime.service.mapper.SocialCardAuditMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing SocialCardAudit.
 */
@Service
@Transactional
public class SocialCardAuditService {

    private final Logger log = LoggerFactory.getLogger(SocialCardAuditService.class);

    private final SocialCardAuditRepository socialCardAuditRepository;

    private final SocialCardAuditMapper socialCardAuditMapper;

    public SocialCardAuditService(SocialCardAuditRepository socialCardAuditRepository, SocialCardAuditMapper socialCardAuditMapper) {
        this.socialCardAuditRepository = socialCardAuditRepository;
        this.socialCardAuditMapper = socialCardAuditMapper;
    }

    /**
     * Save a socialCardAudit.
     *
     * @param socialCardAuditDTO the entity to save
     * @return the persisted entity
     */
    public SocialCardAuditDTO save(SocialCardAuditDTO socialCardAuditDTO) {
        log.debug("Request to save SocialCardAudit : {}", socialCardAuditDTO);
        SocialCardAudit socialCardAudit = socialCardAuditMapper.toEntity(socialCardAuditDTO);
        socialCardAudit = socialCardAuditRepository.save(socialCardAudit);
        return socialCardAuditMapper.toDto(socialCardAudit);
    }

    /**
     * Get all the socialCardAudits.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<SocialCardAuditDTO> findAll() {
        log.debug("Request to get all SocialCardAudits");
        return socialCardAuditRepository.findAll().stream()
            .map(socialCardAuditMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one socialCardAudit by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public SocialCardAuditDTO findOne(Long id) {
        log.debug("Request to get SocialCardAudit : {}", id);
        SocialCardAudit socialCardAudit = socialCardAuditRepository.findOne(id);
        return socialCardAuditMapper.toDto(socialCardAudit);
    }

    /**
     * Delete the socialCardAudit by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SocialCardAudit : {}", id);
        socialCardAuditRepository.delete(id);
    }
}
