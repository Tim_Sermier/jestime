package ch.hearc.ig.jestime.service;

import ch.hearc.ig.jestime.domain.VariableType;
import ch.hearc.ig.jestime.repository.VariableTypeRepository;
import ch.hearc.ig.jestime.service.dto.VariableTypeDTO;
import ch.hearc.ig.jestime.service.mapper.VariableTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing VariableType.
 */
@Service
@Transactional
public class VariableTypeService {

    private final Logger log = LoggerFactory.getLogger(VariableTypeService.class);

    private final VariableTypeRepository variableTypeRepository;

    private final VariableTypeMapper variableTypeMapper;

    public VariableTypeService(VariableTypeRepository variableTypeRepository, VariableTypeMapper variableTypeMapper) {
        this.variableTypeRepository = variableTypeRepository;
        this.variableTypeMapper = variableTypeMapper;
    }

    /**
     * Save a variableType.
     *
     * @param variableTypeDTO the entity to save
     * @return the persisted entity
     */
    public VariableTypeDTO save(VariableTypeDTO variableTypeDTO) {
        log.debug("Request to save VariableType : {}", variableTypeDTO);
        VariableType variableType = variableTypeMapper.toEntity(variableTypeDTO);
        variableType = variableTypeRepository.save(variableType);
        return variableTypeMapper.toDto(variableType);
    }

    /**
     * Get all the variableTypes.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<VariableTypeDTO> findAll() {
        log.debug("Request to get all VariableTypes");
        return variableTypeRepository.findAll().stream()
            .map(variableTypeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one variableType by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public VariableTypeDTO findOne(Long id) {
        log.debug("Request to get VariableType : {}", id);
        VariableType variableType = variableTypeRepository.findOne(id);
        return variableTypeMapper.toDto(variableType);
    }

    /**
     * Delete the variableType by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete VariableType : {}", id);
        variableTypeRepository.delete(id);
    }
}
