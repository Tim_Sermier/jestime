package ch.hearc.ig.jestime.service;

import ch.hearc.ig.jestime.domain.AddressAudit;
import ch.hearc.ig.jestime.repository.AddressAuditRepository;
import ch.hearc.ig.jestime.service.dto.AddressAuditDTO;
import ch.hearc.ig.jestime.service.mapper.AddressAuditMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing AddressAudit.
 */
@Service
@Transactional
public class AddressAuditService {

    private final Logger log = LoggerFactory.getLogger(AddressAuditService.class);

    private final AddressAuditRepository addressAuditRepository;

    private final AddressAuditMapper addressAuditMapper;

    public AddressAuditService(AddressAuditRepository addressAuditRepository, AddressAuditMapper addressAuditMapper) {
        this.addressAuditRepository = addressAuditRepository;
        this.addressAuditMapper = addressAuditMapper;
    }

    /**
     * Save a addressAudit.
     *
     * @param addressAuditDTO the entity to save
     * @return the persisted entity
     */
    public AddressAuditDTO save(AddressAuditDTO addressAuditDTO) {
        log.debug("Request to save AddressAudit : {}", addressAuditDTO);
        AddressAudit addressAudit = addressAuditMapper.toEntity(addressAuditDTO);
        addressAudit = addressAuditRepository.save(addressAudit);
        return addressAuditMapper.toDto(addressAudit);
    }

    /**
     * Get all the addressAudits.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<AddressAuditDTO> findAll() {
        log.debug("Request to get all AddressAudits");
        return addressAuditRepository.findAll().stream()
            .map(addressAuditMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one addressAudit by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public AddressAuditDTO findOne(Long id) {
        log.debug("Request to get AddressAudit : {}", id);
        AddressAudit addressAudit = addressAuditRepository.findOne(id);
        return addressAuditMapper.toDto(addressAudit);
    }

    /**
     * Delete the addressAudit by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AddressAudit : {}", id);
        addressAuditRepository.delete(id);
    }
}
