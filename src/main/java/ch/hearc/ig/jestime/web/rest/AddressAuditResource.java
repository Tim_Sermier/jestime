package ch.hearc.ig.jestime.web.rest;

import com.codahale.metrics.annotation.Timed;
import ch.hearc.ig.jestime.service.AddressAuditService;
import ch.hearc.ig.jestime.web.rest.errors.BadRequestAlertException;
import ch.hearc.ig.jestime.web.rest.util.HeaderUtil;
import ch.hearc.ig.jestime.service.dto.AddressAuditDTO;
import ch.hearc.ig.jestime.service.dto.AddressAuditCriteria;
import ch.hearc.ig.jestime.service.AddressAuditQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AddressAudit.
 */
@RestController
@RequestMapping("/api")
public class AddressAuditResource {

    private final Logger log = LoggerFactory.getLogger(AddressAuditResource.class);

    private static final String ENTITY_NAME = "addressAudit";

    private final AddressAuditService addressAuditService;

    private final AddressAuditQueryService addressAuditQueryService;

    public AddressAuditResource(AddressAuditService addressAuditService, AddressAuditQueryService addressAuditQueryService) {
        this.addressAuditService = addressAuditService;
        this.addressAuditQueryService = addressAuditQueryService;
    }

    /**
     * POST  /address-audits : Create a new addressAudit.
     *
     * @param addressAuditDTO the addressAuditDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new addressAuditDTO, or with status 400 (Bad Request) if the addressAudit has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/address-audits")
    @Timed
    public ResponseEntity<AddressAuditDTO> createAddressAudit(@Valid @RequestBody AddressAuditDTO addressAuditDTO) throws URISyntaxException {
        log.debug("REST request to save AddressAudit : {}", addressAuditDTO);
        if (addressAuditDTO.getId() != null) {
            throw new BadRequestAlertException("A new addressAudit cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AddressAuditDTO result = addressAuditService.save(addressAuditDTO);
        return ResponseEntity.created(new URI("/api/address-audits/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /address-audits : Updates an existing addressAudit.
     *
     * @param addressAuditDTO the addressAuditDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated addressAuditDTO,
     * or with status 400 (Bad Request) if the addressAuditDTO is not valid,
     * or with status 500 (Internal Server Error) if the addressAuditDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/address-audits")
    @Timed
    public ResponseEntity<AddressAuditDTO> updateAddressAudit(@Valid @RequestBody AddressAuditDTO addressAuditDTO) throws URISyntaxException {
        log.debug("REST request to update AddressAudit : {}", addressAuditDTO);
        if (addressAuditDTO.getId() == null) {
            return createAddressAudit(addressAuditDTO);
        }
        AddressAuditDTO result = addressAuditService.save(addressAuditDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, addressAuditDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /address-audits : get all the addressAudits.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of addressAudits in body
     */
    @GetMapping("/address-audits")
    @Timed
    public ResponseEntity<List<AddressAuditDTO>> getAllAddressAudits(AddressAuditCriteria criteria) {
        log.debug("REST request to get AddressAudits by criteria: {}", criteria);
        List<AddressAuditDTO> entityList = addressAuditQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * GET  /address-audits/:id : get the "id" addressAudit.
     *
     * @param id the id of the addressAuditDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the addressAuditDTO, or with status 404 (Not Found)
     */
    @GetMapping("/address-audits/{id}")
    @Timed
    public ResponseEntity<AddressAuditDTO> getAddressAudit(@PathVariable Long id) {
        log.debug("REST request to get AddressAudit : {}", id);
        AddressAuditDTO addressAuditDTO = addressAuditService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(addressAuditDTO));
    }

    /**
     * DELETE  /address-audits/:id : delete the "id" addressAudit.
     *
     * @param id the id of the addressAuditDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/address-audits/{id}")
    @Timed
    public ResponseEntity<Void> deleteAddressAudit(@PathVariable Long id) {
        log.debug("REST request to delete AddressAudit : {}", id);
        addressAuditService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
