package ch.hearc.ig.jestime.web.rest;

import com.codahale.metrics.annotation.Timed;
import ch.hearc.ig.jestime.service.SocialCardAuditService;
import ch.hearc.ig.jestime.web.rest.errors.BadRequestAlertException;
import ch.hearc.ig.jestime.web.rest.util.HeaderUtil;
import ch.hearc.ig.jestime.service.dto.SocialCardAuditDTO;
import ch.hearc.ig.jestime.service.dto.SocialCardAuditCriteria;
import ch.hearc.ig.jestime.service.SocialCardAuditQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SocialCardAudit.
 */
@RestController
@RequestMapping("/api")
public class SocialCardAuditResource {

    private final Logger log = LoggerFactory.getLogger(SocialCardAuditResource.class);

    private static final String ENTITY_NAME = "socialCardAudit";

    private final SocialCardAuditService socialCardAuditService;

    private final SocialCardAuditQueryService socialCardAuditQueryService;

    public SocialCardAuditResource(SocialCardAuditService socialCardAuditService, SocialCardAuditQueryService socialCardAuditQueryService) {
        this.socialCardAuditService = socialCardAuditService;
        this.socialCardAuditQueryService = socialCardAuditQueryService;
    }

    /**
     * POST  /social-card-audits : Create a new socialCardAudit.
     *
     * @param socialCardAuditDTO the socialCardAuditDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new socialCardAuditDTO, or with status 400 (Bad Request) if the socialCardAudit has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/social-card-audits")
    @Timed
    public ResponseEntity<SocialCardAuditDTO> createSocialCardAudit(@Valid @RequestBody SocialCardAuditDTO socialCardAuditDTO) throws URISyntaxException {
        log.debug("REST request to save SocialCardAudit : {}", socialCardAuditDTO);
        if (socialCardAuditDTO.getId() != null) {
            throw new BadRequestAlertException("A new socialCardAudit cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SocialCardAuditDTO result = socialCardAuditService.save(socialCardAuditDTO);
        return ResponseEntity.created(new URI("/api/social-card-audits/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /social-card-audits : Updates an existing socialCardAudit.
     *
     * @param socialCardAuditDTO the socialCardAuditDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated socialCardAuditDTO,
     * or with status 400 (Bad Request) if the socialCardAuditDTO is not valid,
     * or with status 500 (Internal Server Error) if the socialCardAuditDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/social-card-audits")
    @Timed
    public ResponseEntity<SocialCardAuditDTO> updateSocialCardAudit(@Valid @RequestBody SocialCardAuditDTO socialCardAuditDTO) throws URISyntaxException {
        log.debug("REST request to update SocialCardAudit : {}", socialCardAuditDTO);
        if (socialCardAuditDTO.getId() == null) {
            return createSocialCardAudit(socialCardAuditDTO);
        }
        SocialCardAuditDTO result = socialCardAuditService.save(socialCardAuditDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, socialCardAuditDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /social-card-audits : get all the socialCardAudits.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of socialCardAudits in body
     */
    @GetMapping("/social-card-audits")
    @Timed
    public ResponseEntity<List<SocialCardAuditDTO>> getAllSocialCardAudits(SocialCardAuditCriteria criteria) {
        log.debug("REST request to get SocialCardAudits by criteria: {}", criteria);
        List<SocialCardAuditDTO> entityList = socialCardAuditQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * GET  /social-card-audits/:id : get the "id" socialCardAudit.
     *
     * @param id the id of the socialCardAuditDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the socialCardAuditDTO, or with status 404 (Not Found)
     */
    @GetMapping("/social-card-audits/{id}")
    @Timed
    public ResponseEntity<SocialCardAuditDTO> getSocialCardAudit(@PathVariable Long id) {
        log.debug("REST request to get SocialCardAudit : {}", id);
        SocialCardAuditDTO socialCardAuditDTO = socialCardAuditService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(socialCardAuditDTO));
    }

    /**
     * DELETE  /social-card-audits/:id : delete the "id" socialCardAudit.
     *
     * @param id the id of the socialCardAuditDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/social-card-audits/{id}")
    @Timed
    public ResponseEntity<Void> deleteSocialCardAudit(@PathVariable Long id) {
        log.debug("REST request to delete SocialCardAudit : {}", id);
        socialCardAuditService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
