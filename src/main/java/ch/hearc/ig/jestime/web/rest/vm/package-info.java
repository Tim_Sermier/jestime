/**
 * View Models used by Spring MVC REST controllers.
 */
package ch.hearc.ig.jestime.web.rest.vm;
