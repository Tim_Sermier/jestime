package ch.hearc.ig.jestime.web.rest;

import com.codahale.metrics.annotation.Timed;
import ch.hearc.ig.jestime.service.ConditionValueService;
import ch.hearc.ig.jestime.web.rest.errors.BadRequestAlertException;
import ch.hearc.ig.jestime.web.rest.util.HeaderUtil;
import ch.hearc.ig.jestime.service.dto.ConditionValueDTO;
import ch.hearc.ig.jestime.service.dto.ConditionValueCriteria;
import ch.hearc.ig.jestime.service.ConditionValueQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ConditionValue.
 */
@RestController
@RequestMapping("/api")
public class ConditionValueResource {

    private final Logger log = LoggerFactory.getLogger(ConditionValueResource.class);

    private static final String ENTITY_NAME = "conditionValue";

    private final ConditionValueService conditionValueService;

    private final ConditionValueQueryService conditionValueQueryService;

    public ConditionValueResource(ConditionValueService conditionValueService, ConditionValueQueryService conditionValueQueryService) {
        this.conditionValueService = conditionValueService;
        this.conditionValueQueryService = conditionValueQueryService;
    }

    /**
     * POST  /condition-values : Create a new conditionValue.
     *
     * @param conditionValueDTO the conditionValueDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new conditionValueDTO, or with status 400 (Bad Request) if the conditionValue has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/condition-values")
    @Timed
    public ResponseEntity<ConditionValueDTO> createConditionValue(@Valid @RequestBody ConditionValueDTO conditionValueDTO) throws URISyntaxException {
        log.debug("REST request to save ConditionValue : {}", conditionValueDTO);
        if (conditionValueDTO.getId() != null) {
            throw new BadRequestAlertException("A new conditionValue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ConditionValueDTO result = conditionValueService.save(conditionValueDTO);
        return ResponseEntity.created(new URI("/api/condition-values/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /condition-values : Updates an existing conditionValue.
     *
     * @param conditionValueDTO the conditionValueDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated conditionValueDTO,
     * or with status 400 (Bad Request) if the conditionValueDTO is not valid,
     * or with status 500 (Internal Server Error) if the conditionValueDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/condition-values")
    @Timed
    public ResponseEntity<ConditionValueDTO> updateConditionValue(@Valid @RequestBody ConditionValueDTO conditionValueDTO) throws URISyntaxException {
        log.debug("REST request to update ConditionValue : {}", conditionValueDTO);
        if (conditionValueDTO.getId() == null) {
            return createConditionValue(conditionValueDTO);
        }
        ConditionValueDTO result = conditionValueService.save(conditionValueDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, conditionValueDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /condition-values : get all the conditionValues.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of conditionValues in body
     */
    @GetMapping("/condition-values")
    @Timed
    public ResponseEntity<List<ConditionValueDTO>> getAllConditionValues(ConditionValueCriteria criteria) {
        log.debug("REST request to get ConditionValues by criteria: {}", criteria);
        List<ConditionValueDTO> entityList = conditionValueQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * GET  /condition-values/:id : get the "id" conditionValue.
     *
     * @param id the id of the conditionValueDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the conditionValueDTO, or with status 404 (Not Found)
     */
    @GetMapping("/condition-values/{id}")
    @Timed
    public ResponseEntity<ConditionValueDTO> getConditionValue(@PathVariable Long id) {
        log.debug("REST request to get ConditionValue : {}", id);
        ConditionValueDTO conditionValueDTO = conditionValueService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(conditionValueDTO));
    }

    /**
     * DELETE  /condition-values/:id : delete the "id" conditionValue.
     *
     * @param id the id of the conditionValueDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/condition-values/{id}")
    @Timed
    public ResponseEntity<Void> deleteConditionValue(@PathVariable Long id) {
        log.debug("REST request to delete ConditionValue : {}", id);
        conditionValueService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
