package ch.hearc.ig.jestime.web.rest;

import com.codahale.metrics.annotation.Timed;
import ch.hearc.ig.jestime.service.SourceAuditService;
import ch.hearc.ig.jestime.web.rest.errors.BadRequestAlertException;
import ch.hearc.ig.jestime.web.rest.util.HeaderUtil;
import ch.hearc.ig.jestime.service.dto.SourceAuditDTO;
import ch.hearc.ig.jestime.service.dto.SourceAuditCriteria;
import ch.hearc.ig.jestime.service.SourceAuditQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SourceAudit.
 */
@RestController
@RequestMapping("/api")
public class SourceAuditResource {

    private final Logger log = LoggerFactory.getLogger(SourceAuditResource.class);

    private static final String ENTITY_NAME = "sourceAudit";

    private final SourceAuditService sourceAuditService;

    private final SourceAuditQueryService sourceAuditQueryService;

    public SourceAuditResource(SourceAuditService sourceAuditService, SourceAuditQueryService sourceAuditQueryService) {
        this.sourceAuditService = sourceAuditService;
        this.sourceAuditQueryService = sourceAuditQueryService;
    }

    /**
     * POST  /source-audits : Create a new sourceAudit.
     *
     * @param sourceAuditDTO the sourceAuditDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sourceAuditDTO, or with status 400 (Bad Request) if the sourceAudit has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/source-audits")
    @Timed
    public ResponseEntity<SourceAuditDTO> createSourceAudit(@Valid @RequestBody SourceAuditDTO sourceAuditDTO) throws URISyntaxException {
        log.debug("REST request to save SourceAudit : {}", sourceAuditDTO);
        if (sourceAuditDTO.getId() != null) {
            throw new BadRequestAlertException("A new sourceAudit cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SourceAuditDTO result = sourceAuditService.save(sourceAuditDTO);
        return ResponseEntity.created(new URI("/api/source-audits/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /source-audits : Updates an existing sourceAudit.
     *
     * @param sourceAuditDTO the sourceAuditDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sourceAuditDTO,
     * or with status 400 (Bad Request) if the sourceAuditDTO is not valid,
     * or with status 500 (Internal Server Error) if the sourceAuditDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/source-audits")
    @Timed
    public ResponseEntity<SourceAuditDTO> updateSourceAudit(@Valid @RequestBody SourceAuditDTO sourceAuditDTO) throws URISyntaxException {
        log.debug("REST request to update SourceAudit : {}", sourceAuditDTO);
        if (sourceAuditDTO.getId() == null) {
            return createSourceAudit(sourceAuditDTO);
        }
        SourceAuditDTO result = sourceAuditService.save(sourceAuditDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sourceAuditDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /source-audits : get all the sourceAudits.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of sourceAudits in body
     */
    @GetMapping("/source-audits")
    @Timed
    public ResponseEntity<List<SourceAuditDTO>> getAllSourceAudits(SourceAuditCriteria criteria) {
        log.debug("REST request to get SourceAudits by criteria: {}", criteria);
        List<SourceAuditDTO> entityList = sourceAuditQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * GET  /source-audits/:id : get the "id" sourceAudit.
     *
     * @param id the id of the sourceAuditDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sourceAuditDTO, or with status 404 (Not Found)
     */
    @GetMapping("/source-audits/{id}")
    @Timed
    public ResponseEntity<SourceAuditDTO> getSourceAudit(@PathVariable Long id) {
        log.debug("REST request to get SourceAudit : {}", id);
        SourceAuditDTO sourceAuditDTO = sourceAuditService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(sourceAuditDTO));
    }

    /**
     * DELETE  /source-audits/:id : delete the "id" sourceAudit.
     *
     * @param id the id of the sourceAuditDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/source-audits/{id}")
    @Timed
    public ResponseEntity<Void> deleteSourceAudit(@PathVariable Long id) {
        log.debug("REST request to delete SourceAudit : {}", id);
        sourceAuditService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
