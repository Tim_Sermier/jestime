package ch.hearc.ig.jestime.web.rest;

import com.codahale.metrics.annotation.Timed;
import ch.hearc.ig.jestime.service.SocialCardService;
import ch.hearc.ig.jestime.web.rest.errors.BadRequestAlertException;
import ch.hearc.ig.jestime.web.rest.util.HeaderUtil;
import ch.hearc.ig.jestime.service.dto.SocialCardDTO;
import ch.hearc.ig.jestime.service.dto.SocialCardCriteria;
import ch.hearc.ig.jestime.service.SocialCardQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SocialCard.
 */
@RestController
@RequestMapping("/api")
public class SocialCardResource {

    private final Logger log = LoggerFactory.getLogger(SocialCardResource.class);

    private static final String ENTITY_NAME = "socialCard";

    private final SocialCardService socialCardService;

    private final SocialCardQueryService socialCardQueryService;

    public SocialCardResource(SocialCardService socialCardService, SocialCardQueryService socialCardQueryService) {
        this.socialCardService = socialCardService;
        this.socialCardQueryService = socialCardQueryService;
    }

    /**
     * POST  /social-cards : Create a new socialCard.
     *
     * @param socialCardDTO the socialCardDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new socialCardDTO, or with status 400 (Bad Request) if the socialCard has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/social-cards")
    @Timed
    public ResponseEntity<SocialCardDTO> createSocialCard(@Valid @RequestBody SocialCardDTO socialCardDTO) throws URISyntaxException {
        log.debug("REST request to save SocialCard : {}", socialCardDTO);
        if (socialCardDTO.getId() != null) {
            throw new BadRequestAlertException("A new socialCard cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SocialCardDTO result = socialCardService.save(socialCardDTO);
        return ResponseEntity.created(new URI("/api/social-cards/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /social-cards : Updates an existing socialCard.
     *
     * @param socialCardDTO the socialCardDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated socialCardDTO,
     * or with status 400 (Bad Request) if the socialCardDTO is not valid,
     * or with status 500 (Internal Server Error) if the socialCardDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/social-cards")
    @Timed
    public ResponseEntity<SocialCardDTO> updateSocialCard(@Valid @RequestBody SocialCardDTO socialCardDTO) throws URISyntaxException {
        log.debug("REST request to update SocialCard : {}", socialCardDTO);
        if (socialCardDTO.getId() == null) {
            return createSocialCard(socialCardDTO);
        }
        SocialCardDTO result = socialCardService.save(socialCardDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, socialCardDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /social-cards : get all the socialCards.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of socialCards in body
     */
    @GetMapping("/social-cards")
    @Timed
    public ResponseEntity<List<SocialCardDTO>> getAllSocialCards(SocialCardCriteria criteria) {
        log.debug("REST request to get SocialCards by criteria: {}", criteria);
        List<SocialCardDTO> entityList = socialCardQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * GET  /social-cards/:id : get the "id" socialCard.
     *
     * @param id the id of the socialCardDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the socialCardDTO, or with status 404 (Not Found)
     */
    @GetMapping("/social-cards/{id}")
    @Timed
    public ResponseEntity<SocialCardDTO> getSocialCard(@PathVariable Long id) {
        log.debug("REST request to get SocialCard : {}", id);
        SocialCardDTO socialCardDTO = socialCardService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(socialCardDTO));
    }

    /**
     * DELETE  /social-cards/:id : delete the "id" socialCard.
     *
     * @param id the id of the socialCardDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/social-cards/{id}")
    @Timed
    public ResponseEntity<Void> deleteSocialCard(@PathVariable Long id) {
        log.debug("REST request to delete SocialCard : {}", id);
        socialCardService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
