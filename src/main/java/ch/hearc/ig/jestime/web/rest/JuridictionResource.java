package ch.hearc.ig.jestime.web.rest;

import com.codahale.metrics.annotation.Timed;
import ch.hearc.ig.jestime.service.JuridictionService;
import ch.hearc.ig.jestime.web.rest.errors.BadRequestAlertException;
import ch.hearc.ig.jestime.web.rest.util.HeaderUtil;
import ch.hearc.ig.jestime.service.dto.JuridictionDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Juridiction.
 */
@RestController
@RequestMapping("/api")
public class JuridictionResource {

    private final Logger log = LoggerFactory.getLogger(JuridictionResource.class);

    private static final String ENTITY_NAME = "juridiction";

    private final JuridictionService juridictionService;

    public JuridictionResource(JuridictionService juridictionService) {
        this.juridictionService = juridictionService;
    }

    /**
     * POST  /juridictions : Create a new juridiction.
     *
     * @param juridictionDTO the juridictionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new juridictionDTO, or with status 400 (Bad Request) if the juridiction has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/juridictions")
    @Timed
    public ResponseEntity<JuridictionDTO> createJuridiction(@Valid @RequestBody JuridictionDTO juridictionDTO) throws URISyntaxException {
        log.debug("REST request to save Juridiction : {}", juridictionDTO);
        if (juridictionDTO.getId() != null) {
            throw new BadRequestAlertException("A new juridiction cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JuridictionDTO result = juridictionService.save(juridictionDTO);
        return ResponseEntity.created(new URI("/api/juridictions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /juridictions : Updates an existing juridiction.
     *
     * @param juridictionDTO the juridictionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated juridictionDTO,
     * or with status 400 (Bad Request) if the juridictionDTO is not valid,
     * or with status 500 (Internal Server Error) if the juridictionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/juridictions")
    @Timed
    public ResponseEntity<JuridictionDTO> updateJuridiction(@Valid @RequestBody JuridictionDTO juridictionDTO) throws URISyntaxException {
        log.debug("REST request to update Juridiction : {}", juridictionDTO);
        if (juridictionDTO.getId() == null) {
            return createJuridiction(juridictionDTO);
        }
        JuridictionDTO result = juridictionService.save(juridictionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, juridictionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /juridictions : get all the juridictions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of juridictions in body
     */
    @GetMapping("/juridictions")
    @Timed
    public List<JuridictionDTO> getAllJuridictions() {
        log.debug("REST request to get all Juridictions");
        return juridictionService.findAll();
        }

    /**
     * GET  /juridictions/:id : get the "id" juridiction.
     *
     * @param id the id of the juridictionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the juridictionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/juridictions/{id}")
    @Timed
    public ResponseEntity<JuridictionDTO> getJuridiction(@PathVariable Long id) {
        log.debug("REST request to get Juridiction : {}", id);
        JuridictionDTO juridictionDTO = juridictionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(juridictionDTO));
    }

    /**
     * DELETE  /juridictions/:id : delete the "id" juridiction.
     *
     * @param id the id of the juridictionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/juridictions/{id}")
    @Timed
    public ResponseEntity<Void> deleteJuridiction(@PathVariable Long id) {
        log.debug("REST request to delete Juridiction : {}", id);
        juridictionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
