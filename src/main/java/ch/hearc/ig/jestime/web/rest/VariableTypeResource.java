package ch.hearc.ig.jestime.web.rest;

import com.codahale.metrics.annotation.Timed;
import ch.hearc.ig.jestime.service.VariableTypeService;
import ch.hearc.ig.jestime.web.rest.errors.BadRequestAlertException;
import ch.hearc.ig.jestime.web.rest.util.HeaderUtil;
import ch.hearc.ig.jestime.service.dto.VariableTypeDTO;
import ch.hearc.ig.jestime.service.dto.VariableTypeCriteria;
import ch.hearc.ig.jestime.service.VariableTypeQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing VariableType.
 */
@RestController
@RequestMapping("/api")
public class VariableTypeResource {

    private final Logger log = LoggerFactory.getLogger(VariableTypeResource.class);

    private static final String ENTITY_NAME = "variableType";

    private final VariableTypeService variableTypeService;

    private final VariableTypeQueryService variableTypeQueryService;

    public VariableTypeResource(VariableTypeService variableTypeService, VariableTypeQueryService variableTypeQueryService) {
        this.variableTypeService = variableTypeService;
        this.variableTypeQueryService = variableTypeQueryService;
    }

    /**
     * POST  /variable-types : Create a new variableType.
     *
     * @param variableTypeDTO the variableTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new variableTypeDTO, or with status 400 (Bad Request) if the variableType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/variable-types")
    @Timed
    public ResponseEntity<VariableTypeDTO> createVariableType(@Valid @RequestBody VariableTypeDTO variableTypeDTO) throws URISyntaxException {
        log.debug("REST request to save VariableType : {}", variableTypeDTO);
        if (variableTypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new variableType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VariableTypeDTO result = variableTypeService.save(variableTypeDTO);
        return ResponseEntity.created(new URI("/api/variable-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /variable-types : Updates an existing variableType.
     *
     * @param variableTypeDTO the variableTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated variableTypeDTO,
     * or with status 400 (Bad Request) if the variableTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the variableTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/variable-types")
    @Timed
    public ResponseEntity<VariableTypeDTO> updateVariableType(@Valid @RequestBody VariableTypeDTO variableTypeDTO) throws URISyntaxException {
        log.debug("REST request to update VariableType : {}", variableTypeDTO);
        if (variableTypeDTO.getId() == null) {
            return createVariableType(variableTypeDTO);
        }
        VariableTypeDTO result = variableTypeService.save(variableTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, variableTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /variable-types : get all the variableTypes.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of variableTypes in body
     */
    @GetMapping("/variable-types")
    @Timed
    public ResponseEntity<List<VariableTypeDTO>> getAllVariableTypes(VariableTypeCriteria criteria) {
        log.debug("REST request to get VariableTypes by criteria: {}", criteria);
        List<VariableTypeDTO> entityList = variableTypeQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * GET  /variable-types/:id : get the "id" variableType.
     *
     * @param id the id of the variableTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the variableTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/variable-types/{id}")
    @Timed
    public ResponseEntity<VariableTypeDTO> getVariableType(@PathVariable Long id) {
        log.debug("REST request to get VariableType : {}", id);
        VariableTypeDTO variableTypeDTO = variableTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(variableTypeDTO));
    }

    /**
     * DELETE  /variable-types/:id : delete the "id" variableType.
     *
     * @param id the id of the variableTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/variable-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteVariableType(@PathVariable Long id) {
        log.debug("REST request to delete VariableType : {}", id);
        variableTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
