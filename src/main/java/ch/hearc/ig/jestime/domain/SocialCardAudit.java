package ch.hearc.ig.jestime.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A SocialCardAudit.
 */
@Entity
@Table(name = "social_card_audit")
public class SocialCardAudit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "action", nullable = false)
    private String action;

    @NotNull
    @Column(name = "action_date", nullable = false)
    private LocalDate actionDate;

    @NotNull
    @Column(name = "action_user", nullable = false)
    private String actionUser;

    @Column(name = "title")
    private String title;

    @Column(name = "code")
    private String code;

    @Lob
    @Column(name = "jhi_general")
    private String general;

    @Lob
    @Column(name = "description")
    private String description;

    @Lob
    @Column(name = "jhi_procedure")
    private String procedure;

    @Lob
    @Column(name = "recourse")
    private String recourse;

    @Column(name = "juridiction_id")
    private Integer juridictionId;

    @Column(name = "category_id")
    private Integer categoryId;

    @Column(name = "parent_social_card_id")
    private Integer parentSocialCardId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public SocialCardAudit action(String action) {
        this.action = action;
        return this;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public LocalDate getActionDate() {
        return actionDate;
    }

    public SocialCardAudit actionDate(LocalDate actionDate) {
        this.actionDate = actionDate;
        return this;
    }

    public void setActionDate(LocalDate actionDate) {
        this.actionDate = actionDate;
    }

    public String getActionUser() {
        return actionUser;
    }

    public SocialCardAudit actionUser(String actionUser) {
        this.actionUser = actionUser;
        return this;
    }

    public void setActionUser(String actionUser) {
        this.actionUser = actionUser;
    }

    public String getTitle() {
        return title;
    }

    public SocialCardAudit title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public SocialCardAudit code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getGeneral() {
        return general;
    }

    public SocialCardAudit general(String general) {
        this.general = general;
        return this;
    }

    public void setGeneral(String general) {
        this.general = general;
    }

    public String getDescription() {
        return description;
    }

    public SocialCardAudit description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProcedure() {
        return procedure;
    }

    public SocialCardAudit procedure(String procedure) {
        this.procedure = procedure;
        return this;
    }

    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    public String getRecourse() {
        return recourse;
    }

    public SocialCardAudit recourse(String recourse) {
        this.recourse = recourse;
        return this;
    }

    public void setRecourse(String recourse) {
        this.recourse = recourse;
    }

    public Integer getJuridictionId() {
        return juridictionId;
    }

    public SocialCardAudit juridictionId(Integer juridictionId) {
        this.juridictionId = juridictionId;
        return this;
    }

    public void setJuridictionId(Integer juridictionId) {
        this.juridictionId = juridictionId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public SocialCardAudit categoryId(Integer categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getParentSocialCardId() {
        return parentSocialCardId;
    }

    public SocialCardAudit parentSocialCardId(Integer parentSocialCardId) {
        this.parentSocialCardId = parentSocialCardId;
        return this;
    }

    public void setParentSocialCardId(Integer parentSocialCardId) {
        this.parentSocialCardId = parentSocialCardId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SocialCardAudit socialCardAudit = (SocialCardAudit) o;
        if (socialCardAudit.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), socialCardAudit.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SocialCardAudit{" +
            "id=" + getId() +
            ", action='" + getAction() + "'" +
            ", actionDate='" + getActionDate() + "'" +
            ", actionUser='" + getActionUser() + "'" +
            ", title='" + getTitle() + "'" +
            ", code='" + getCode() + "'" +
            ", general='" + getGeneral() + "'" +
            ", description='" + getDescription() + "'" +
            ", procedure='" + getProcedure() + "'" +
            ", recourse='" + getRecourse() + "'" +
            ", juridictionId=" + getJuridictionId() +
            ", categoryId=" + getCategoryId() +
            ", parentSocialCardId=" + getParentSocialCardId() +
            "}";
    }
}
