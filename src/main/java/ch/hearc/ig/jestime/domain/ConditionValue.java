package ch.hearc.ig.jestime.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A ConditionValue.
 */
@Entity
@Table(name = "condition_value")
public class ConditionValue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "jhi_value", nullable = false)
    private String value;

    @ManyToOne(optional = false)
    @NotNull
    private Variable variable;

    @ManyToOne(optional = false)
    @NotNull
    private Condition condition;

    @ManyToOne
    private Reference reference;

    @ManyToOne(optional = false)
    @NotNull
    private Operator operator;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public ConditionValue value(String value) {
        this.value = value;
        return this;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Variable getVariable() {
        return variable;
    }

    public ConditionValue variable(Variable variable) {
        this.variable = variable;
        return this;
    }

    public void setVariable(Variable variable) {
        this.variable = variable;
    }

    public Condition getCondition() {
        return condition;
    }

    public ConditionValue condition(Condition condition) {
        this.condition = condition;
        return this;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    public Reference getReference() {
        return reference;
    }

    public ConditionValue reference(Reference reference) {
        this.reference = reference;
        return this;
    }

    public void setReference(Reference reference) {
        this.reference = reference;
    }

    public Operator getOperator() {
        return operator;
    }

    public ConditionValue operator(Operator operator) {
        this.operator = operator;
        return this;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ConditionValue conditionValue = (ConditionValue) o;
        if (conditionValue.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), conditionValue.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ConditionValue{" +
            "id=" + getId() +
            ", value='" + getValue() + "'" +
            "}";
    }
}
