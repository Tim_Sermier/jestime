package ch.hearc.ig.jestime.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A SocialCard.
 */
@Entity
@Table(name = "social_card")
public class SocialCard implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "code", nullable = false)
    private String code;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "jhi_general")
    private String general;

    @Column(name = "jhi_procedure")
    private String procedure;

    @Column(name = "recourse")
    private String recourse;

    @NotNull
    @Column(name = "creation_date", nullable = false)
    private LocalDate creationDate;

    @Column(name = "edit_date")
    private LocalDate editDate;

    @ManyToOne(optional = false)
    @NotNull
    private Juridiction juridiction;

    @ManyToOne
    private SocialCard parentSocialCard;

    @ManyToOne(optional = false)
    @NotNull
    private Category category;

    @ManyToMany
    @JoinTable(name = "social_card_addresses",
               joinColumns = @JoinColumn(name="social_cards_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="addresses_id", referencedColumnName="id"))
    private Set<Address> addresses = new HashSet<>();

    @ManyToOne
    private User creator;

    @ManyToOne
    private User updator;

    @ManyToMany
    @JoinTable(name = "social_card_sources",
               joinColumns = @JoinColumn(name="social_cards_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="sources_id", referencedColumnName="id"))
    private Set<Source> sources = new HashSet<>();

    @OneToMany(mappedBy = "socialCard")
    private Set<Condition> conditions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public SocialCard code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public SocialCard title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public SocialCard description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGeneral() {
        return general;
    }

    public SocialCard general(String general) {
        this.general = general;
        return this;
    }

    public void setGeneral(String general) {
        this.general = general;
    }

    public String getProcedure() {
        return procedure;
    }

    public SocialCard procedure(String procedure) {
        this.procedure = procedure;
        return this;
    }

    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    public String getRecourse() {
        return recourse;
    }

    public SocialCard recourse(String recourse) {
        this.recourse = recourse;
        return this;
    }

    public void setRecourse(String recourse) {
        this.recourse = recourse;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public SocialCard creationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getEditDate() {
        return editDate;
    }

    public SocialCard editDate(LocalDate editDate) {
        this.editDate = editDate;
        return this;
    }

    public void setEditDate(LocalDate editDate) {
        this.editDate = editDate;
    }

    public Juridiction getJuridiction() {
        return juridiction;
    }

    public SocialCard juridiction(Juridiction juridiction) {
        this.juridiction = juridiction;
        return this;
    }

    public void setJuridiction(Juridiction juridiction) {
        this.juridiction = juridiction;
    }

    public SocialCard getParentSocialCard() {
        return parentSocialCard;
    }

    public SocialCard parentSocialCard(SocialCard socialCard) {
        this.parentSocialCard = socialCard;
        return this;
    }

    public void setParentSocialCard(SocialCard socialCard) {
        this.parentSocialCard = socialCard;
    }

    public Category getCategory() {
        return category;
    }

    public SocialCard category(Category category) {
        this.category = category;
        return this;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public SocialCard addresses(Set<Address> addresses) {
        this.addresses = addresses;
        return this;
    }

    public SocialCard addAddresses(Address address) {
        this.addresses.add(address);
        address.getSocialCards().add(this);
        return this;
    }

    public SocialCard removeAddresses(Address address) {
        this.addresses.remove(address);
        address.getSocialCards().remove(this);
        return this;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    public User getCreator() {
        return creator;
    }

    public SocialCard creator(User user) {
        this.creator = user;
        return this;
    }

    public void setCreator(User user) {
        this.creator = user;
    }

    public User getUpdator() {
        return updator;
    }

    public SocialCard updator(User user) {
        this.updator = user;
        return this;
    }

    public void setUpdator(User user) {
        this.updator = user;
    }

    public Set<Source> getSources() {
        return sources;
    }

    public SocialCard sources(Set<Source> sources) {
        this.sources = sources;
        return this;
    }

    public SocialCard addSources(Source source) {
        this.sources.add(source);
        source.getSocialCards().add(this);
        return this;
    }

    public SocialCard removeSources(Source source) {
        this.sources.remove(source);
        source.getSocialCards().remove(this);
        return this;
    }

    public void setSources(Set<Source> sources) {
        this.sources = sources;
    }

    public Set<Condition> getConditions() {
        return conditions;
    }

    public SocialCard conditions(Set<Condition> conditions) {
        this.conditions = conditions;
        return this;
    }

    public SocialCard addConditions(Condition condition) {
        this.conditions.add(condition);
        condition.setSocialCard(this);
        return this;
    }

    public SocialCard removeConditions(Condition condition) {
        this.conditions.remove(condition);
        condition.setSocialCard(null);
        return this;
    }

    public void setConditions(Set<Condition> conditions) {
        this.conditions = conditions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SocialCard socialCard = (SocialCard) o;
        if (socialCard.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), socialCard.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SocialCard{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", general='" + getGeneral() + "'" +
            ", procedure='" + getProcedure() + "'" +
            ", recourse='" + getRecourse() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", editDate='" + getEditDate() + "'" +
            "}";
    }
}
