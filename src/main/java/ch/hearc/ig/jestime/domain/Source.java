package ch.hearc.ig.jestime.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Source.
 */
@Entity
@Table(name = "source")
public class Source implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "code", nullable = false)
    private String code;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @Lob
    @Column(name = "description")
    private String description;

    @Column(name = "url")
    private String url;

    @NotNull
    @Column(name = "creation_date", nullable = false)
    private LocalDate creationDate;

    @Column(name = "edition_date")
    private LocalDate editionDate;

    @ManyToMany(mappedBy = "sources", fetch = FetchType.EAGER)
    private Set<SocialCard> socialCards = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    private Juridiction juridiction;

    @ManyToOne
    private User creator;

    @ManyToOne
    private User updator;

    @OneToMany(mappedBy = "source", fetch = FetchType.EAGER)
    private Set<Reference> references = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public Source code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public Source title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public Source description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public Source url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public Source creationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getEditionDate() {
        return editionDate;
    }

    public Source editionDate(LocalDate editionDate) {
        this.editionDate = editionDate;
        return this;
    }

    public void setEditionDate(LocalDate editionDate) {
        this.editionDate = editionDate;
    }

    public Set<SocialCard> getSocialCards() {
        return socialCards;
    }

    public Source socialCards(Set<SocialCard> socialCards) {
        this.socialCards = socialCards;
        return this;
    }

    public Source addSocialCards(SocialCard socialCard) {
        this.socialCards.add(socialCard);
        socialCard.getSources().add(this);
        return this;
    }

    public Source removeSocialCards(SocialCard socialCard) {
        this.socialCards.remove(socialCard);
        socialCard.getSources().remove(this);
        return this;
    }

    public void setSocialCards(Set<SocialCard> socialCards) {
        this.socialCards = socialCards;
    }

    public Juridiction getJuridiction() {
        return juridiction;
    }

    public Source juridiction(Juridiction juridiction) {
        this.juridiction = juridiction;
        return this;
    }

    public void setJuridiction(Juridiction juridiction) {
        this.juridiction = juridiction;
    }

    public User getCreator() {
        return creator;
    }

    public Source creator(User user) {
        this.creator = user;
        return this;
    }

    public void setCreator(User user) {
        this.creator = user;
    }

    public User getUpdator() {
        return updator;
    }

    public Source updator(User user) {
        this.updator = user;
        return this;
    }

    public void setUpdator(User user) {
        this.updator = user;
    }

    public Set<Reference> getReferences() {
        return references;
    }

    public Source references(Set<Reference> references) {
        this.references = references;
        return this;
    }

    public Source addReferences(Reference reference) {
        this.references.add(reference);
        reference.setSource(this);
        return this;
    }

    public Source removeReferences(Reference reference) {
        this.references.remove(reference);
        reference.setSource(null);
        return this;
    }

    public void setReferences(Set<Reference> references) {
        this.references = references;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Source source = (Source) o;
        if (source.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), source.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Source{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", url='" + getUrl() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", editionDate='" + getEditionDate() + "'" +
            "}";
    }
}
