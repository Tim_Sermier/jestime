package ch.hearc.ig.jestime.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Condition.
 */
@Entity
@Table(name = "jhi_condition")
public class Condition implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "code", nullable = false)
    private String code;

    @Lob
    @Column(name = "description")
    private String description;

    @Column(name = "creation_date")
    private LocalDate creationDate;

    @Column(name = "edit_date")
    private LocalDate editDate;

    @ManyToOne(optional = false)
    @NotNull
    private SocialCard socialCard;

    @ManyToOne
    private User creator;

    @ManyToOne
    private User updator;

    @OneToMany(mappedBy = "condition")
    private Set<ConditionValue> conditionValues = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public Condition code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public Condition description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public Condition creationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getEditDate() {
        return editDate;
    }

    public Condition editDate(LocalDate editDate) {
        this.editDate = editDate;
        return this;
    }

    public void setEditDate(LocalDate editDate) {
        this.editDate = editDate;
    }

    public SocialCard getSocialCard() {
        return socialCard;
    }

    public Condition socialCard(SocialCard socialCard) {
        this.socialCard = socialCard;
        return this;
    }

    public void setSocialCard(SocialCard socialCard) {
        this.socialCard = socialCard;
    }

    public User getCreator() {
        return creator;
    }

    public Condition creator(User user) {
        this.creator = user;
        return this;
    }

    public void setCreator(User user) {
        this.creator = user;
    }

    public User getUpdator() {
        return updator;
    }

    public Condition updator(User user) {
        this.updator = user;
        return this;
    }

    public void setUpdator(User user) {
        this.updator = user;
    }

    public Set<ConditionValue> getConditionValues() {
        return conditionValues;
    }

    public Condition conditionValues(Set<ConditionValue> conditionValues) {
        this.conditionValues = conditionValues;
        return this;
    }

    public Condition addConditionValues(ConditionValue conditionValue) {
        this.conditionValues.add(conditionValue);
        conditionValue.setCondition(this);
        return this;
    }

    public Condition removeConditionValues(ConditionValue conditionValue) {
        this.conditionValues.remove(conditionValue);
        conditionValue.setCondition(null);
        return this;
    }

    public void setConditionValues(Set<ConditionValue> conditionValues) {
        this.conditionValues = conditionValues;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Condition condition = (Condition) o;
        if (condition.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), condition.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Condition{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", description='" + getDescription() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", editDate='" + getEditDate() + "'" +
            "}";
    }
}
