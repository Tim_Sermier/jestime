import { BaseEntity } from './../../shared';

export class Address implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public name?: string,
        public location?: string,
        public phone?: string,
        public fax?: string,
        public email?: string,
        public website?: string,
        public creationDate?: any,
        public editionDate?: any,
        public socialCards?: BaseEntity[],
        public creatorLogin?: string,
        public creatorId?: number,
        public updatorLogin?: string,
        public updatorId?: number,
    ) {
    }
}
