import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Juridiction } from './juridiction.model';
import { JuridictionPopupService } from './juridiction-popup.service';
import { JuridictionService } from './juridiction.service';

@Component({
    selector: 'jhi-juridiction-delete-dialog',
    templateUrl: './juridiction-delete-dialog.component.html'
})
export class JuridictionDeleteDialogComponent {

    juridiction: Juridiction;

    constructor(
        private juridictionService: JuridictionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.juridictionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'juridictionListModification',
                content: 'Deleted an juridiction'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-juridiction-delete-popup',
    template: ''
})
export class JuridictionDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private juridictionPopupService: JuridictionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.juridictionPopupService
                .open(JuridictionDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
