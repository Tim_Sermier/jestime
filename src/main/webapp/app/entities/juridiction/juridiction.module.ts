import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JestimeSharedModule } from '../../shared';
import {
    JuridictionService,
    JuridictionPopupService,
    JuridictionComponent,
    JuridictionDetailComponent,
    JuridictionDialogComponent,
    JuridictionPopupComponent,
    JuridictionDeletePopupComponent,
    JuridictionDeleteDialogComponent,
    juridictionRoute,
    juridictionPopupRoute,
} from './';

const ENTITY_STATES = [
    ...juridictionRoute,
    ...juridictionPopupRoute,
];

@NgModule({
    imports: [
        JestimeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        JuridictionComponent,
        JuridictionDetailComponent,
        JuridictionDialogComponent,
        JuridictionDeleteDialogComponent,
        JuridictionPopupComponent,
        JuridictionDeletePopupComponent,
    ],
    entryComponents: [
        JuridictionComponent,
        JuridictionDialogComponent,
        JuridictionPopupComponent,
        JuridictionDeleteDialogComponent,
        JuridictionDeletePopupComponent,
    ],
    providers: [
        JuridictionService,
        JuridictionPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JestimeJuridictionModule {}
