import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Juridiction } from './juridiction.model';
import { JuridictionService } from './juridiction.service';

@Component({
    selector: 'jhi-juridiction-detail',
    templateUrl: './juridiction-detail.component.html'
})
export class JuridictionDetailComponent implements OnInit, OnDestroy {

    juridiction: Juridiction;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private juridictionService: JuridictionService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInJuridictions();
    }

    load(id) {
        this.juridictionService.find(id)
            .subscribe((juridictionResponse: HttpResponse<Juridiction>) => {
                this.juridiction = juridictionResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInJuridictions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'juridictionListModification',
            (response) => this.load(this.juridiction.id)
        );
    }
}
