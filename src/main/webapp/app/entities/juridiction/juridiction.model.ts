import { BaseEntity } from './../../shared';

export class Juridiction implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public name?: string,
        public image?: string,
    ) {
    }
}
