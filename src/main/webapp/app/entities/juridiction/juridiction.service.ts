import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Juridiction } from './juridiction.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Juridiction>;

@Injectable()
export class JuridictionService {

    private resourceUrl =  SERVER_API_URL + 'api/juridictions';
    constructor(private http: HttpClient) { }

    create(juridiction: Juridiction): Observable<EntityResponseType> {
        const copy = this.convert(juridiction);
        return this.http.post<Juridiction>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(juridiction: Juridiction): Observable<EntityResponseType> {
        const copy = this.convert(juridiction);
        return this.http.put<Juridiction>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Juridiction>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Juridiction[]>> {
        const options = createRequestOption(req);
        return this.http.get<Juridiction[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Juridiction[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Juridiction = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Juridiction[]>): HttpResponse<Juridiction[]> {
        const jsonResponse: Juridiction[] = res.body;
        const body: Juridiction[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Juridiction.
     */
    private convertItemFromServer(juridiction: Juridiction): Juridiction {
        const copy: Juridiction = Object.assign({}, juridiction);
        return copy;
    }

    /**
     * Convert a Juridiction to a JSON which can be sent to the server.
     */
    private convert(juridiction: Juridiction): Juridiction {
        const copy: Juridiction = Object.assign({}, juridiction);
        return copy;
    }
}
