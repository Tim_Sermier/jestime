import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Juridiction } from './juridiction.model';
import { JuridictionService } from './juridiction.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-juridiction',
    templateUrl: './juridiction.component.html'
})
export class JuridictionComponent implements OnInit, OnDestroy {
juridictions: Juridiction[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private juridictionService: JuridictionService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.juridictionService.query().subscribe(
            (res: HttpResponse<Juridiction[]>) => {
                this.juridictions = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInJuridictions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Juridiction) {
        return item.id;
    }
    registerChangeInJuridictions() {
        this.eventSubscriber = this.eventManager.subscribe('juridictionListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
