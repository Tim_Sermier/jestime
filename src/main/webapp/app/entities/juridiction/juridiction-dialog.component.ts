import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Juridiction } from './juridiction.model';
import { JuridictionPopupService } from './juridiction-popup.service';
import { JuridictionService } from './juridiction.service';

@Component({
    selector: 'jhi-juridiction-dialog',
    templateUrl: './juridiction-dialog.component.html'
})
export class JuridictionDialogComponent implements OnInit {

    juridiction: Juridiction;
    isSaving: boolean;
    fileToUpload: File = null;

    constructor(
        public activeModal: NgbActiveModal,
        private juridictionService: JuridictionService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.juridiction.id !== undefined) {
            this.subscribeToSaveResponse(
                this.juridictionService.update(this.juridiction));
        } else {
            this.subscribeToSaveResponse(
                this.juridictionService.create(this.juridiction));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Juridiction>>) {
        result.subscribe((res: HttpResponse<Juridiction>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Juridiction) {
        this.eventManager.broadcast({ name: 'juridictionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-juridiction-popup',
    template: ''
})
export class JuridictionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private juridictionPopupService: JuridictionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.juridictionPopupService
                    .open(JuridictionDialogComponent as Component, params['id']);
            } else {
                this.juridictionPopupService
                    .open(JuridictionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
