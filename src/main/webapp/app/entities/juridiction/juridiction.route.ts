import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JuridictionComponent } from './juridiction.component';
import { JuridictionDetailComponent } from './juridiction-detail.component';
import { JuridictionPopupComponent } from './juridiction-dialog.component';
import { JuridictionDeletePopupComponent } from './juridiction-delete-dialog.component';

export const juridictionRoute: Routes = [
    {
        path: 'juridiction',
        component: JuridictionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.juridiction.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'juridiction/:id',
        component: JuridictionDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.juridiction.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const juridictionPopupRoute: Routes = [
    {
        path: 'juridiction-new',
        component: JuridictionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.juridiction.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'juridiction/:id/edit',
        component: JuridictionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.juridiction.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'juridiction/:id/delete',
        component: JuridictionDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.juridiction.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
