import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { SocialCardAudit } from './social-card-audit.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<SocialCardAudit>;

@Injectable()
export class SocialCardAuditService {

    private resourceUrl =  SERVER_API_URL + 'api/social-card-audits';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(socialCardAudit: SocialCardAudit): Observable<EntityResponseType> {
        const copy = this.convert(socialCardAudit);
        return this.http.post<SocialCardAudit>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(socialCardAudit: SocialCardAudit): Observable<EntityResponseType> {
        const copy = this.convert(socialCardAudit);
        return this.http.put<SocialCardAudit>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<SocialCardAudit>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<SocialCardAudit[]>> {
        const options = createRequestOption(req);
        return this.http.get<SocialCardAudit[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<SocialCardAudit[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: SocialCardAudit = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<SocialCardAudit[]>): HttpResponse<SocialCardAudit[]> {
        const jsonResponse: SocialCardAudit[] = res.body;
        const body: SocialCardAudit[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to SocialCardAudit.
     */
    private convertItemFromServer(socialCardAudit: SocialCardAudit): SocialCardAudit {
        const copy: SocialCardAudit = Object.assign({}, socialCardAudit);
        copy.actionDate = this.dateUtils
            .convertLocalDateFromServer(socialCardAudit.actionDate);
        return copy;
    }

    /**
     * Convert a SocialCardAudit to a JSON which can be sent to the server.
     */
    private convert(socialCardAudit: SocialCardAudit): SocialCardAudit {
        const copy: SocialCardAudit = Object.assign({}, socialCardAudit);
        copy.actionDate = this.dateUtils
            .convertLocalDateToServer(socialCardAudit.actionDate);
        return copy;
    }
}
