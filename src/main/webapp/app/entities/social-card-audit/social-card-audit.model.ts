import { BaseEntity } from './../../shared';

export class SocialCardAudit implements BaseEntity {
    constructor(
        public id?: number,
        public action?: string,
        public actionDate?: any,
        public actionUser?: string,
        public title?: string,
        public code?: string,
        public general?: any,
        public description?: any,
        public procedure?: any,
        public recourse?: any,
        public juridictionId?: number,
        public categoryId?: number,
        public parentSocialCardId?: number,
    ) {
    }
}
