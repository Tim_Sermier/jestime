import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SocialCardAudit } from './social-card-audit.model';
import { SocialCardAuditPopupService } from './social-card-audit-popup.service';
import { SocialCardAuditService } from './social-card-audit.service';

@Component({
    selector: 'jhi-social-card-audit-delete-dialog',
    templateUrl: './social-card-audit-delete-dialog.component.html'
})
export class SocialCardAuditDeleteDialogComponent {

    socialCardAudit: SocialCardAudit;

    constructor(
        private socialCardAuditService: SocialCardAuditService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.socialCardAuditService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'socialCardAuditListModification',
                content: 'Deleted an socialCardAudit'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-social-card-audit-delete-popup',
    template: ''
})
export class SocialCardAuditDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private socialCardAuditPopupService: SocialCardAuditPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.socialCardAuditPopupService
                .open(SocialCardAuditDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
