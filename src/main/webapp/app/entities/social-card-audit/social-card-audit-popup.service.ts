import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { SocialCardAudit } from './social-card-audit.model';
import { SocialCardAuditService } from './social-card-audit.service';

@Injectable()
export class SocialCardAuditPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private socialCardAuditService: SocialCardAuditService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.socialCardAuditService.find(id)
                    .subscribe((socialCardAuditResponse: HttpResponse<SocialCardAudit>) => {
                        const socialCardAudit: SocialCardAudit = socialCardAuditResponse.body;
                        if (socialCardAudit.actionDate) {
                            socialCardAudit.actionDate = {
                                year: socialCardAudit.actionDate.getFullYear(),
                                month: socialCardAudit.actionDate.getMonth() + 1,
                                day: socialCardAudit.actionDate.getDate()
                            };
                        }
                        this.ngbModalRef = this.socialCardAuditModalRef(component, socialCardAudit);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.socialCardAuditModalRef(component, new SocialCardAudit());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    socialCardAuditModalRef(component: Component, socialCardAudit: SocialCardAudit): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.socialCardAudit = socialCardAudit;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
