import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { SocialCardAudit } from './social-card-audit.model';
import { SocialCardAuditService } from './social-card-audit.service';

@Component({
    selector: 'jhi-social-card-audit-detail',
    templateUrl: './social-card-audit-detail.component.html'
})
export class SocialCardAuditDetailComponent implements OnInit, OnDestroy {

    socialCardAudit: SocialCardAudit;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private socialCardAuditService: SocialCardAuditService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSocialCardAudits();
    }

    load(id) {
        this.socialCardAuditService.find(id)
            .subscribe((socialCardAuditResponse: HttpResponse<SocialCardAudit>) => {
                this.socialCardAudit = socialCardAuditResponse.body;
            });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSocialCardAudits() {
        this.eventSubscriber = this.eventManager.subscribe(
            'socialCardAuditListModification',
            (response) => this.load(this.socialCardAudit.id)
        );
    }
}
