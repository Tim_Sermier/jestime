import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { SocialCardAuditComponent } from './social-card-audit.component';
import { SocialCardAuditDetailComponent } from './social-card-audit-detail.component';
import { SocialCardAuditPopupComponent } from './social-card-audit-dialog.component';
import { SocialCardAuditDeletePopupComponent } from './social-card-audit-delete-dialog.component';

export const socialCardAuditRoute: Routes = [
    {
        path: 'social-card-audit',
        component: SocialCardAuditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.socialCardAudit.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'social-card-audit/:id',
        component: SocialCardAuditDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.socialCardAudit.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const socialCardAuditPopupRoute: Routes = [
    {
        path: 'social-card-audit-new',
        component: SocialCardAuditPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.socialCardAudit.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'social-card-audit/:id/edit',
        component: SocialCardAuditPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.socialCardAudit.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'social-card-audit/:id/delete',
        component: SocialCardAuditDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.socialCardAudit.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
