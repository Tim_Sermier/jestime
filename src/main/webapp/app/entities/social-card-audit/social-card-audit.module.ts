import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JestimeSharedModule } from '../../shared';
import {
    SocialCardAuditService,
    SocialCardAuditPopupService,
    SocialCardAuditComponent,
    SocialCardAuditDetailComponent,
    SocialCardAuditDialogComponent,
    SocialCardAuditPopupComponent,
    SocialCardAuditDeletePopupComponent,
    SocialCardAuditDeleteDialogComponent,
    socialCardAuditRoute,
    socialCardAuditPopupRoute,
} from './';

const ENTITY_STATES = [
    ...socialCardAuditRoute,
    ...socialCardAuditPopupRoute,
];

@NgModule({
    imports: [
        JestimeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SocialCardAuditComponent,
        SocialCardAuditDetailComponent,
        SocialCardAuditDialogComponent,
        SocialCardAuditDeleteDialogComponent,
        SocialCardAuditPopupComponent,
        SocialCardAuditDeletePopupComponent,
    ],
    entryComponents: [
        SocialCardAuditComponent,
        SocialCardAuditDialogComponent,
        SocialCardAuditPopupComponent,
        SocialCardAuditDeleteDialogComponent,
        SocialCardAuditDeletePopupComponent,
    ],
    providers: [
        SocialCardAuditService,
        SocialCardAuditPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JestimeSocialCardAuditModule {}
