import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { SocialCardAudit } from './social-card-audit.model';
import { SocialCardAuditService } from './social-card-audit.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-social-card-audit',
    templateUrl: './social-card-audit.component.html'
})
export class SocialCardAuditComponent implements OnInit, OnDestroy {
socialCardAudits: SocialCardAudit[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private socialCardAuditService: SocialCardAuditService,
        private jhiAlertService: JhiAlertService,
        private dataUtils: JhiDataUtils,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.socialCardAuditService.query().subscribe(
            (res: HttpResponse<SocialCardAudit[]>) => {
                this.socialCardAudits = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInSocialCardAudits();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: SocialCardAudit) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    registerChangeInSocialCardAudits() {
        this.eventSubscriber = this.eventManager.subscribe('socialCardAuditListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
