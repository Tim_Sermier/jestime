import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { SocialCardAudit } from './social-card-audit.model';
import { SocialCardAuditPopupService } from './social-card-audit-popup.service';
import { SocialCardAuditService } from './social-card-audit.service';

@Component({
    selector: 'jhi-social-card-audit-dialog',
    templateUrl: './social-card-audit-dialog.component.html'
})
export class SocialCardAuditDialogComponent implements OnInit {

    socialCardAudit: SocialCardAudit;
    isSaving: boolean;
    actionDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private socialCardAuditService: SocialCardAuditService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.socialCardAudit.id !== undefined) {
            this.subscribeToSaveResponse(
                this.socialCardAuditService.update(this.socialCardAudit));
        } else {
            this.subscribeToSaveResponse(
                this.socialCardAuditService.create(this.socialCardAudit));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<SocialCardAudit>>) {
        result.subscribe((res: HttpResponse<SocialCardAudit>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: SocialCardAudit) {
        this.eventManager.broadcast({ name: 'socialCardAuditListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-social-card-audit-popup',
    template: ''
})
export class SocialCardAuditPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private socialCardAuditPopupService: SocialCardAuditPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.socialCardAuditPopupService
                    .open(SocialCardAuditDialogComponent as Component, params['id']);
            } else {
                this.socialCardAuditPopupService
                    .open(SocialCardAuditDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
