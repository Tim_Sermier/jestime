import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { Variable } from './variable.model';
import { VariablePopupService } from './variable-popup.service';
import { VariableService } from './variable.service';
import { VariableType, VariableTypeService } from '../variable-type';

@Component({
    selector: 'jhi-variable-dialog',
    templateUrl: './variable-dialog.component.html'
})
export class VariableDialogComponent implements OnInit {

    variable: Variable;
    isSaving: boolean;

    variabletypes: VariableType[];

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private jhiAlertService: JhiAlertService,
        private variableService: VariableService,
        private variableTypeService: VariableTypeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.variableTypeService.query()
            .subscribe((res: HttpResponse<VariableType[]>) => { this.variabletypes = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.variable.id !== undefined) {
            this.subscribeToSaveResponse(
                this.variableService.update(this.variable));
        } else {
            this.subscribeToSaveResponse(
                this.variableService.create(this.variable));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Variable>>) {
        result.subscribe((res: HttpResponse<Variable>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Variable) {
        this.eventManager.broadcast({ name: 'variableListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackVariableTypeById(index: number, item: VariableType) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-variable-popup',
    template: ''
})
export class VariablePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private variablePopupService: VariablePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.variablePopupService
                    .open(VariableDialogComponent as Component, params['id']);
            } else {
                this.variablePopupService
                    .open(VariableDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
