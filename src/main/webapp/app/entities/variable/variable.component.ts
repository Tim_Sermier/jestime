import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { Variable } from './variable.model';
import { VariableService } from './variable.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-variable',
    templateUrl: './variable.component.html'
})
export class VariableComponent implements OnInit, OnDestroy {
variables: Variable[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private variableService: VariableService,
        private jhiAlertService: JhiAlertService,
        private dataUtils: JhiDataUtils,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.variableService.query().subscribe(
            (res: HttpResponse<Variable[]>) => {
                this.variables = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInVariables();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Variable) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    registerChangeInVariables() {
        this.eventSubscriber = this.eventManager.subscribe('variableListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
