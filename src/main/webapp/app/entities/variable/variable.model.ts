import { BaseEntity } from './../../shared';

export class Variable implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public name?: string,
        public description?: any,
        public typeName?: string,
        public typeId?: number,
    ) {
    }
}
