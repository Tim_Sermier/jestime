import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { Variable } from './variable.model';
import { VariableService } from './variable.service';

@Component({
    selector: 'jhi-variable-detail',
    templateUrl: './variable-detail.component.html'
})
export class VariableDetailComponent implements OnInit, OnDestroy {

    variable: Variable;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private variableService: VariableService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInVariables();
    }

    load(id) {
        this.variableService.find(id)
            .subscribe((variableResponse: HttpResponse<Variable>) => {
                this.variable = variableResponse.body;
            });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInVariables() {
        this.eventSubscriber = this.eventManager.subscribe(
            'variableListModification',
            (response) => this.load(this.variable.id)
        );
    }
}
