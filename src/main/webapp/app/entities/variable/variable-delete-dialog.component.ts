import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Variable } from './variable.model';
import { VariablePopupService } from './variable-popup.service';
import { VariableService } from './variable.service';

@Component({
    selector: 'jhi-variable-delete-dialog',
    templateUrl: './variable-delete-dialog.component.html'
})
export class VariableDeleteDialogComponent {

    variable: Variable;

    constructor(
        private variableService: VariableService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.variableService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'variableListModification',
                content: 'Deleted an variable'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-variable-delete-popup',
    template: ''
})
export class VariableDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private variablePopupService: VariablePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.variablePopupService
                .open(VariableDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
