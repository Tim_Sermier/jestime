import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { VariableComponent } from './variable.component';
import { VariableDetailComponent } from './variable-detail.component';
import { VariablePopupComponent } from './variable-dialog.component';
import { VariableDeletePopupComponent } from './variable-delete-dialog.component';

export const variableRoute: Routes = [
    {
        path: 'variable',
        component: VariableComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.variable.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'variable/:id',
        component: VariableDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.variable.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const variablePopupRoute: Routes = [
    {
        path: 'variable-new',
        component: VariablePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.variable.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'variable/:id/edit',
        component: VariablePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.variable.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'variable/:id/delete',
        component: VariableDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.variable.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
