import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Variable } from './variable.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Variable>;

@Injectable()
export class VariableService {

    private resourceUrl =  SERVER_API_URL + 'api/variables';

    constructor(private http: HttpClient) { }

    create(variable: Variable): Observable<EntityResponseType> {
        const copy = this.convert(variable);
        return this.http.post<Variable>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(variable: Variable): Observable<EntityResponseType> {
        const copy = this.convert(variable);
        return this.http.put<Variable>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Variable>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Variable[]>> {
        const options = createRequestOption(req);
        return this.http.get<Variable[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Variable[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Variable = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Variable[]>): HttpResponse<Variable[]> {
        const jsonResponse: Variable[] = res.body;
        const body: Variable[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Variable.
     */
    private convertItemFromServer(variable: Variable): Variable {
        const copy: Variable = Object.assign({}, variable);
        return copy;
    }

    /**
     * Convert a Variable to a JSON which can be sent to the server.
     */
    private convert(variable: Variable): Variable {
        const copy: Variable = Object.assign({}, variable);
        return copy;
    }
}
