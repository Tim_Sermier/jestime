import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JestimeSharedModule } from '../../shared';
import {
    VariableService,
    VariablePopupService,
    VariableComponent,
    VariableDetailComponent,
    VariableDialogComponent,
    VariablePopupComponent,
    VariableDeletePopupComponent,
    VariableDeleteDialogComponent,
    variableRoute,
    variablePopupRoute,
} from './';

const ENTITY_STATES = [
    ...variableRoute,
    ...variablePopupRoute,
];

@NgModule({
    imports: [
        JestimeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        VariableComponent,
        VariableDetailComponent,
        VariableDialogComponent,
        VariableDeleteDialogComponent,
        VariablePopupComponent,
        VariableDeletePopupComponent,
    ],
    entryComponents: [
        VariableComponent,
        VariableDialogComponent,
        VariablePopupComponent,
        VariableDeleteDialogComponent,
        VariableDeletePopupComponent,
    ],
    providers: [
        VariableService,
        VariablePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JestimeVariableModule {}
