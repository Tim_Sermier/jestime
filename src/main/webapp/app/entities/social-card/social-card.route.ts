import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { SocialCardComponent } from './social-card.component';
import { SocialCardDetailComponent } from './social-card-detail.component';
import { SocialCardPopupComponent } from './social-card-dialog.component';
import { SocialCardDeletePopupComponent } from './social-card-delete-dialog.component';

export const socialCardRoute: Routes = [
    {
        path: 'social-card',
        component: SocialCardComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.socialCard.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'social-card/:id',
        component: SocialCardDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.socialCard.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const socialCardPopupRoute: Routes = [
    {
        path: 'social-card-new',
        component: SocialCardPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.socialCard.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'social-card/:id/edit',
        component: SocialCardPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.socialCard.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'social-card/:id/delete',
        component: SocialCardDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.socialCard.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
