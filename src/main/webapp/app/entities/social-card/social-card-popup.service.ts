import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { SocialCard } from './social-card.model';
import { SocialCardService } from './social-card.service';

@Injectable()
export class SocialCardPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private socialCardService: SocialCardService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.socialCardService.find(id)
                    .subscribe((socialCardResponse: HttpResponse<SocialCard>) => {
                        const socialCard: SocialCard = socialCardResponse.body;
                        if (socialCard.creationDate) {
                            socialCard.creationDate = {
                                year: socialCard.creationDate.getFullYear(),
                                month: socialCard.creationDate.getMonth() + 1,
                                day: socialCard.creationDate.getDate()
                            };
                        }
                        if (socialCard.editDate) {
                            socialCard.editDate = {
                                year: socialCard.editDate.getFullYear(),
                                month: socialCard.editDate.getMonth() + 1,
                                day: socialCard.editDate.getDate()
                            };
                        }
                        this.ngbModalRef = this.socialCardModalRef(component, socialCard);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.socialCardModalRef(component, new SocialCard());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    socialCardModalRef(component: Component, socialCard: SocialCard): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.socialCard = socialCard;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
