import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SocialCard } from './social-card.model';
import { SocialCardPopupService } from './social-card-popup.service';
import { SocialCardService } from './social-card.service';
import { Juridiction, JuridictionService } from '../juridiction';
import { Category, CategoryService } from '../category';
import { Address, AddressService } from '../address';
import { User, UserService } from '../../shared';
import { Source, SourceService } from '../source';

@Component({
    selector: 'jhi-social-card-dialog',
    templateUrl: './social-card-dialog.component.html'
})
export class SocialCardDialogComponent implements OnInit {

    socialCard: SocialCard;
    isSaving: boolean;

    juridictions: Juridiction[];

    socialcards: SocialCard[];

    categories: Category[];

    addresses: Address[];

    users: User[];

    sources: Source[];
    creationDateDp: any;
    editDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private socialCardService: SocialCardService,
        private juridictionService: JuridictionService,
        private categoryService: CategoryService,
        private addressService: AddressService,
        private userService: UserService,
        private sourceService: SourceService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.juridictionService.query()
            .subscribe((res: HttpResponse<Juridiction[]>) => { this.juridictions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.socialCardService.query()
            .subscribe((res: HttpResponse<SocialCard[]>) => { this.socialcards = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.categoryService.query()
            .subscribe((res: HttpResponse<Category[]>) => { this.categories = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.addressService.query()
            .subscribe((res: HttpResponse<Address[]>) => { this.addresses = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.userService.query()
            .subscribe((res: HttpResponse<User[]>) => { this.users = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.sourceService.query()
            .subscribe((res: HttpResponse<Source[]>) => { this.sources = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.socialCard.id !== undefined) {
            this.subscribeToSaveResponse(
                this.socialCardService.update(this.socialCard));
        } else {
            this.subscribeToSaveResponse(
                this.socialCardService.create(this.socialCard));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<SocialCard>>) {
        result.subscribe((res: HttpResponse<SocialCard>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: SocialCard) {
        this.eventManager.broadcast({ name: 'socialCardListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackJuridictionById(index: number, item: Juridiction) {
        return item.id;
    }

    trackSocialCardById(index: number, item: SocialCard) {
        return item.id;
    }

    trackCategoryById(index: number, item: Category) {
        return item.id;
    }

    trackAddressById(index: number, item: Address) {
        return item.id;
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }

    trackSourceById(index: number, item: Source) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-social-card-popup',
    template: ''
})
export class SocialCardPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private socialCardPopupService: SocialCardPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.socialCardPopupService
                    .open(SocialCardDialogComponent as Component, params['id']);
            } else {
                this.socialCardPopupService
                    .open(SocialCardDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
