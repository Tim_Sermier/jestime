import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SocialCard } from './social-card.model';
import { SocialCardPopupService } from './social-card-popup.service';
import { SocialCardService } from './social-card.service';

@Component({
    selector: 'jhi-social-card-delete-dialog',
    templateUrl: './social-card-delete-dialog.component.html'
})
export class SocialCardDeleteDialogComponent {

    socialCard: SocialCard;

    constructor(
        private socialCardService: SocialCardService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.socialCardService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'socialCardListModification',
                content: 'Deleted an socialCard'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-social-card-delete-popup',
    template: ''
})
export class SocialCardDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private socialCardPopupService: SocialCardPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.socialCardPopupService
                .open(SocialCardDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
