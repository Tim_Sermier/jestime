import { BaseEntity } from './../../shared';

export class SocialCard implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public title?: string,
        public description?: string,
        public general?: string,
        public procedure?: string,
        public recourse?: string,
        public creationDate?: any,
        public editDate?: any,
        public juridictionName?: string,
        public juridictionId?: number,
        public juridictionCode?: string,
        public parentSocialCardTitle?: string,
        public parentSocialCardId?: number,
        public categoryName?: string,
        public categoryId?: number,
        public addresses?: BaseEntity[],
        public creatorLogin?: string,
        public creatorId?: number,
        public updatorLogin?: string,
        public updatorId?: number,
        public sources?: BaseEntity[],
        public conditions?: BaseEntity[],
    ) {
    }
}
