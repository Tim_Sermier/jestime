import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { SocialCard } from './social-card.model';
import { SocialCardService } from './social-card.service';

@Component({
    selector: 'jhi-social-card-detail',
    templateUrl: './social-card-detail.component.html'
})
export class SocialCardDetailComponent implements OnInit, OnDestroy {

    socialCard: SocialCard;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private socialCardService: SocialCardService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSocialCards();
    }

    load(id) {
        this.socialCardService.find(id)
            .subscribe((socialCardResponse: HttpResponse<SocialCard>) => {
                this.socialCard = socialCardResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSocialCards() {
        this.eventSubscriber = this.eventManager.subscribe(
            'socialCardListModification',
            (response) => this.load(this.socialCard.id)
        );
    }
}
