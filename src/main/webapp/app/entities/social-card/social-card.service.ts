import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { SocialCard } from './social-card.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<SocialCard>;

@Injectable()
export class SocialCardService {

    private resourceUrl =  SERVER_API_URL + 'api/social-cards';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(socialCard: SocialCard): Observable<EntityResponseType> {
        const copy = this.convert(socialCard);
        return this.http.post<SocialCard>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(socialCard: SocialCard): Observable<EntityResponseType> {
        const copy = this.convert(socialCard);
        return this.http.put<SocialCard>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<SocialCard>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    findByTitle(filter: string, req?: any): Observable<HttpResponse<SocialCard[]>> {
        const options = createRequestOption(req);
        return this.http.get<SocialCard[]>(this.resourceUrl + '?title.contains=' + filter, { params: options, observe: 'response' })
            .map((res: HttpResponse<SocialCard[]>) => this.convertArrayResponse(res));
    }

    findByCriterias(filter: string, req?: any): Observable<HttpResponse<SocialCard[]>> {
        const options = createRequestOption(req);
        return this.http.get<SocialCard[]>(this.resourceUrl + filter, { params: options, observe: 'response' })
            .map((res: HttpResponse<SocialCard[]>) => this.convertArrayResponse(res));
    }

    query(req?: any): Observable<HttpResponse<SocialCard[]>> {
        const options = createRequestOption(req);
        return this.http.get<SocialCard[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<SocialCard[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: SocialCard = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<SocialCard[]>): HttpResponse<SocialCard[]> {
        const jsonResponse: SocialCard[] = res.body;
        const body: SocialCard[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to SocialCard.
     */
    private convertItemFromServer(socialCard: SocialCard): SocialCard {
        const copy: SocialCard = Object.assign({}, socialCard);
        copy.creationDate = this.dateUtils
            .convertLocalDateFromServer(socialCard.creationDate);
        return copy;
    }

    /**
     * Convert a SocialCard to a JSON which can be sent to the server.
     */
    private convert(socialCard: SocialCard): SocialCard {
        const copy: SocialCard = Object.assign({}, socialCard);
        return copy;
    }

    generateCode(socialCard: SocialCard) {
        const code = 'PRES_' + socialCard.juridictionId + '_' + socialCard.categoryId + '_' + Date.now();
        return code;
    }
}
