import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JestimeSharedModule } from '../../shared';
import { JestimeAdminModule } from '../../admin/admin.module';
import {
    SocialCardService,
    SocialCardPopupService,
    SocialCardComponent,
    SocialCardDetailComponent,
    SocialCardDialogComponent,
    SocialCardPopupComponent,
    SocialCardDeletePopupComponent,
    SocialCardDeleteDialogComponent,
    socialCardRoute,
    socialCardPopupRoute,
} from './';

const ENTITY_STATES = [
    ...socialCardRoute,
    ...socialCardPopupRoute,
];

@NgModule({
    imports: [
        JestimeSharedModule,
        JestimeAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SocialCardComponent,
        SocialCardDetailComponent,
        SocialCardDialogComponent,
        SocialCardDeleteDialogComponent,
        SocialCardPopupComponent,
        SocialCardDeletePopupComponent,
    ],
    entryComponents: [
        SocialCardComponent,
        SocialCardDialogComponent,
        SocialCardPopupComponent,
        SocialCardDeleteDialogComponent,
        SocialCardDeletePopupComponent,
    ],
    providers: [
        SocialCardService,
        SocialCardPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JestimeSocialCardModule {}
