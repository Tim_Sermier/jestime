import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SocialCard } from './social-card.model';
import { SocialCardService } from './social-card.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-social-card',
    templateUrl: './social-card.component.html'
})
export class SocialCardComponent implements OnInit, OnDestroy {
socialCards: SocialCard[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private socialCardService: SocialCardService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.socialCardService.query().subscribe(
            (res: HttpResponse<SocialCard[]>) => {
                this.socialCards = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInSocialCards();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: SocialCard) {
        return item.id;
    }
    registerChangeInSocialCards() {
        this.eventSubscriber = this.eventManager.subscribe('socialCardListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
