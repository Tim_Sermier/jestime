import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ConditionValue } from './condition-value.model';
import { ConditionValuePopupService } from './condition-value-popup.service';
import { ConditionValueService } from './condition-value.service';

@Component({
    selector: 'jhi-condition-value-delete-dialog',
    templateUrl: './condition-value-delete-dialog.component.html'
})
export class ConditionValueDeleteDialogComponent {

    conditionValue: ConditionValue;

    constructor(
        private conditionValueService: ConditionValueService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.conditionValueService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'conditionValueListModification',
                content: 'Deleted an conditionValue'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-condition-value-delete-popup',
    template: ''
})
export class ConditionValueDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private conditionValuePopupService: ConditionValuePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.conditionValuePopupService
                .open(ConditionValueDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
