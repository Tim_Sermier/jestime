import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ConditionValue } from './condition-value.model';
import { ConditionValueService } from './condition-value.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-condition-value',
    templateUrl: './condition-value.component.html'
})
export class ConditionValueComponent implements OnInit, OnDestroy {
conditionValues: ConditionValue[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private conditionValueService: ConditionValueService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.conditionValueService.query().subscribe(
            (res: HttpResponse<ConditionValue[]>) => {
                this.conditionValues = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInConditionValues();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ConditionValue) {
        return item.id;
    }
    registerChangeInConditionValues() {
        this.eventSubscriber = this.eventManager.subscribe('conditionValueListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
