import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ConditionValue } from './condition-value.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ConditionValue>;

@Injectable()
export class ConditionValueService {

    private resourceUrl =  SERVER_API_URL + 'api/condition-values';

    constructor(private http: HttpClient) { }

    create(conditionValue: ConditionValue): Observable<EntityResponseType> {
        const copy = this.convert(conditionValue);
        return this.http.post<ConditionValue>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(conditionValue: ConditionValue): Observable<EntityResponseType> {
        const copy = this.convert(conditionValue);
        return this.http.put<ConditionValue>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ConditionValue>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ConditionValue[]>> {
        const options = createRequestOption(req);
        return this.http.get<ConditionValue[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ConditionValue[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ConditionValue = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ConditionValue[]>): HttpResponse<ConditionValue[]> {
        const jsonResponse: ConditionValue[] = res.body;
        const body: ConditionValue[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ConditionValue.
     */
    private convertItemFromServer(conditionValue: ConditionValue): ConditionValue {
        const copy: ConditionValue = Object.assign({}, conditionValue);
        return copy;
    }

    /**
     * Convert a ConditionValue to a JSON which can be sent to the server.
     */
    private convert(conditionValue: ConditionValue): ConditionValue {
        const copy: ConditionValue = Object.assign({}, conditionValue);
        return copy;
    }
}
