import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ConditionValueComponent } from './condition-value.component';
import { ConditionValueDetailComponent } from './condition-value-detail.component';
import { ConditionValuePopupComponent } from './condition-value-dialog.component';
import { ConditionValueDeletePopupComponent } from './condition-value-delete-dialog.component';

export const conditionValueRoute: Routes = [
    {
        path: 'condition-value',
        component: ConditionValueComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.conditionValue.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'condition-value/:id',
        component: ConditionValueDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.conditionValue.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const conditionValuePopupRoute: Routes = [
    {
        path: 'condition-value-new',
        component: ConditionValuePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.conditionValue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'condition-value/:id/edit',
        component: ConditionValuePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.conditionValue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'condition-value/:id/delete',
        component: ConditionValueDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.conditionValue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
