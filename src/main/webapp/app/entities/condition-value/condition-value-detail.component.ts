import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ConditionValue } from './condition-value.model';
import { ConditionValueService } from './condition-value.service';

@Component({
    selector: 'jhi-condition-value-detail',
    templateUrl: './condition-value-detail.component.html'
})
export class ConditionValueDetailComponent implements OnInit, OnDestroy {

    conditionValue: ConditionValue;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private conditionValueService: ConditionValueService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInConditionValues();
    }

    load(id) {
        this.conditionValueService.find(id)
            .subscribe((conditionValueResponse: HttpResponse<ConditionValue>) => {
                this.conditionValue = conditionValueResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInConditionValues() {
        this.eventSubscriber = this.eventManager.subscribe(
            'conditionValueListModification',
            (response) => this.load(this.conditionValue.id)
        );
    }
}
