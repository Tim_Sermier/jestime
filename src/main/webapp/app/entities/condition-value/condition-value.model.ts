import { BaseEntity } from './../../shared';

export class ConditionValue implements BaseEntity {
    constructor(
        public id?: number,
        public value?: string,
        public variableName?: string,
        public variableId?: number,
        public conditionId?: number,
        public referenceTitle?: string,
        public referenceId?: number,
        public operatorName?: string,
        public operatorId?: number,
        public referenceCode?: string,
        public conditionCode?: string,
        public reference?: BaseEntity,
    ) {
    }
}
