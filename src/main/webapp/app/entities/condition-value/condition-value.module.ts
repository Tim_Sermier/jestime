import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JestimeSharedModule } from '../../shared';
import {
    ConditionValueService,
    ConditionValuePopupService,
    ConditionValueComponent,
    ConditionValueDetailComponent,
    ConditionValueDialogComponent,
    ConditionValuePopupComponent,
    ConditionValueDeletePopupComponent,
    ConditionValueDeleteDialogComponent,
    conditionValueRoute,
    conditionValuePopupRoute,
} from './';

const ENTITY_STATES = [
    ...conditionValueRoute,
    ...conditionValuePopupRoute,
];

@NgModule({
    imports: [
        JestimeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ConditionValueComponent,
        ConditionValueDetailComponent,
        ConditionValueDialogComponent,
        ConditionValueDeleteDialogComponent,
        ConditionValuePopupComponent,
        ConditionValueDeletePopupComponent,
    ],
    entryComponents: [
        ConditionValueComponent,
        ConditionValueDialogComponent,
        ConditionValuePopupComponent,
        ConditionValueDeleteDialogComponent,
        ConditionValueDeletePopupComponent,
    ],
    providers: [
        ConditionValueService,
        ConditionValuePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JestimeConditionValueModule {}
