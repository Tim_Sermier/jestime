import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ConditionValue } from './condition-value.model';
import { ConditionValuePopupService } from './condition-value-popup.service';
import { ConditionValueService } from './condition-value.service';
import { Variable, VariableService } from '../variable';
import { Condition, ConditionService } from '../condition';
import { Reference, ReferenceService } from '../reference';
import { Operator, OperatorService } from '../operator';

@Component({
    selector: 'jhi-condition-value-dialog',
    templateUrl: './condition-value-dialog.component.html'
})
export class ConditionValueDialogComponent implements OnInit {

    conditionValue: ConditionValue;
    isSaving: boolean;

    variables: Variable[];

    conditions: Condition[];

    references: Reference[];

    operators: Operator[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private conditionValueService: ConditionValueService,
        private variableService: VariableService,
        private conditionService: ConditionService,
        private referenceService: ReferenceService,
        private operatorService: OperatorService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.variableService.query()
            .subscribe((res: HttpResponse<Variable[]>) => { this.variables = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.conditionService.query()
            .subscribe((res: HttpResponse<Condition[]>) => { this.conditions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.referenceService.query()
            .subscribe((res: HttpResponse<Reference[]>) => { this.references = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.operatorService.query()
            .subscribe((res: HttpResponse<Operator[]>) => { this.operators = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.conditionValue.id !== undefined) {
            this.subscribeToSaveResponse(
                this.conditionValueService.update(this.conditionValue));
        } else {
            this.subscribeToSaveResponse(
                this.conditionValueService.create(this.conditionValue));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ConditionValue>>) {
        result.subscribe((res: HttpResponse<ConditionValue>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ConditionValue) {
        this.eventManager.broadcast({ name: 'conditionValueListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackVariableById(index: number, item: Variable) {
        return item.id;
    }

    trackConditionById(index: number, item: Condition) {
        return item.id;
    }

    trackReferenceById(index: number, item: Reference) {
        return item.id;
    }

    trackOperatorById(index: number, item: Operator) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-condition-value-popup',
    template: ''
})
export class ConditionValuePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private conditionValuePopupService: ConditionValuePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.conditionValuePopupService
                    .open(ConditionValueDialogComponent as Component, params['id']);
            } else {
                this.conditionValuePopupService
                    .open(ConditionValueDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
