import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Condition } from './condition.model';
import { createRequestOption } from '../../shared';
import {Source} from '../source/source.model';

export type EntityResponseType = HttpResponse<Condition>;

@Injectable()
export class ConditionService {

    private resourceUrl =  SERVER_API_URL + 'api/conditions';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(condition: Condition): Observable<EntityResponseType> {
        const copy = this.convert(condition);
        return this.http.post<Condition>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(condition: Condition): Observable<EntityResponseType> {
        const copy = this.convert(condition);
        return this.http.put<Condition>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Condition>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Condition[]>> {
        const options = createRequestOption(req);
        return this.http.get<Condition[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Condition[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Condition = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Condition[]>): HttpResponse<Condition[]> {
        const jsonResponse: Condition[] = res.body;
        const body: Condition[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Condition.
     */
    private convertItemFromServer(condition: Condition): Condition {
        const copy: Condition = Object.assign({}, condition);
        return copy;
    }

    /**
     * Convert a Condition to a JSON which can be sent to the server.
     */
    private convert(condition: Condition): Condition {
        const copy: Condition = Object.assign({}, condition);
        return copy;
    }

    generateCode(condition: Condition) {
        const code = 'COND_' + Date.now();
        return code;
    }
}
