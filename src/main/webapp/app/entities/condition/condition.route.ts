import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ConditionComponent } from './condition.component';
import { ConditionDetailComponent } from './condition-detail.component';
import { ConditionPopupComponent } from './condition-dialog.component';
import { ConditionDeletePopupComponent } from './condition-delete-dialog.component';

export const conditionRoute: Routes = [
    {
        path: 'condition',
        component: ConditionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.condition.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'condition/:id',
        component: ConditionDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.condition.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const conditionPopupRoute: Routes = [
    {
        path: 'condition-new',
        component: ConditionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.condition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'condition/:id/edit',
        component: ConditionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.condition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'condition/:id/delete',
        component: ConditionDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.condition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
