import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { Condition } from './condition.model';
import { ConditionService } from './condition.service';

@Component({
    selector: 'jhi-condition-detail',
    templateUrl: './condition-detail.component.html'
})
export class ConditionDetailComponent implements OnInit, OnDestroy {

    condition: Condition;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private conditionService: ConditionService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInConditions();
    }

    load(id) {
        this.conditionService.find(id)
            .subscribe((conditionResponse: HttpResponse<Condition>) => {
                this.condition = conditionResponse.body;
            });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInConditions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'conditionListModification',
            (response) => this.load(this.condition.id)
        );
    }
}
