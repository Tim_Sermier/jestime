import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JestimeSharedModule } from '../../shared';
import { JestimeAdminModule } from '../../admin/admin.module';
import {
    ConditionService,
    ConditionPopupService,
    ConditionComponent,
    ConditionDetailComponent,
    ConditionDialogComponent,
    ConditionPopupComponent,
    ConditionDeletePopupComponent,
    ConditionDeleteDialogComponent,
    conditionRoute,
    conditionPopupRoute,
} from './';

const ENTITY_STATES = [
    ...conditionRoute,
    ...conditionPopupRoute,
];

@NgModule({
    imports: [
        JestimeSharedModule,
        JestimeAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ConditionComponent,
        ConditionDetailComponent,
        ConditionDialogComponent,
        ConditionDeleteDialogComponent,
        ConditionPopupComponent,
        ConditionDeletePopupComponent,
    ],
    entryComponents: [
        ConditionComponent,
        ConditionDialogComponent,
        ConditionPopupComponent,
        ConditionDeleteDialogComponent,
        ConditionDeletePopupComponent,
    ],
    providers: [
        ConditionService,
        ConditionPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JestimeConditionModule {}
