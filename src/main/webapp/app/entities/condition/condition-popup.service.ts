import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { Condition } from './condition.model';
import { ConditionService } from './condition.service';

@Injectable()
export class ConditionPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private conditionService: ConditionService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.conditionService.find(id)
                    .subscribe((conditionResponse: HttpResponse<Condition>) => {
                        const condition: Condition = conditionResponse.body;
                        if (condition.creationDate) {
                            condition.creationDate = {
                                year: condition.creationDate.getFullYear(),
                                month: condition.creationDate.getMonth() + 1,
                                day: condition.creationDate.getDate()
                            };
                        }
                        if (condition.editDate) {
                            condition.editDate = {
                                year: condition.editDate.getFullYear(),
                                month: condition.editDate.getMonth() + 1,
                                day: condition.editDate.getDate()
                            };
                        }
                        this.ngbModalRef = this.conditionModalRef(component, condition);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.conditionModalRef(component, new Condition());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    conditionModalRef(component: Component, condition: Condition): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.condition = condition;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
