import { BaseEntity } from './../../shared';

export class Condition implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public description?: any,
        public creationDate?: any,
        public editDate?: any,
        public socialCardTitle?: string,
        public socialCardId?: number,
        public creatorLogin?: string,
        public creatorId?: number,
        public updatorLogin?: string,
        public updatorId?: number,
        public conditionValues?: BaseEntity[],
    ) {
    }
}
