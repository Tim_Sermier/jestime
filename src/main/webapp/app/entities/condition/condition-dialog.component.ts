import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { Condition } from './condition.model';
import { ConditionPopupService } from './condition-popup.service';
import { ConditionService } from './condition.service';
import { SocialCard, SocialCardService } from '../social-card';
import { User, UserService } from '../../shared';

@Component({
    selector: 'jhi-condition-dialog',
    templateUrl: './condition-dialog.component.html'
})
export class ConditionDialogComponent implements OnInit {

    condition: Condition;
    isSaving: boolean;

    socialcards: SocialCard[];

    users: User[];
    creationDateDp: any;
    editDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private jhiAlertService: JhiAlertService,
        private conditionService: ConditionService,
        private socialCardService: SocialCardService,
        private userService: UserService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.socialCardService.query()
            .subscribe((res: HttpResponse<SocialCard[]>) => { this.socialcards = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.userService.query()
            .subscribe((res: HttpResponse<User[]>) => { this.users = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.condition.id !== undefined) {
            this.subscribeToSaveResponse(
                this.conditionService.update(this.condition));
        } else {
            this.subscribeToSaveResponse(
                this.conditionService.create(this.condition));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Condition>>) {
        result.subscribe((res: HttpResponse<Condition>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Condition) {
        this.eventManager.broadcast({ name: 'conditionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackSocialCardById(index: number, item: SocialCard) {
        return item.id;
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-condition-popup',
    template: ''
})
export class ConditionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private conditionPopupService: ConditionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.conditionPopupService
                    .open(ConditionDialogComponent as Component, params['id']);
            } else {
                this.conditionPopupService
                    .open(ConditionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
