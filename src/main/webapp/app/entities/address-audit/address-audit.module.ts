import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JestimeSharedModule } from '../../shared';
import {
    AddressAuditService,
    AddressAuditPopupService,
    AddressAuditComponent,
    AddressAuditDetailComponent,
    AddressAuditDialogComponent,
    AddressAuditPopupComponent,
    AddressAuditDeletePopupComponent,
    AddressAuditDeleteDialogComponent,
    addressAuditRoute,
    addressAuditPopupRoute,
} from './';

const ENTITY_STATES = [
    ...addressAuditRoute,
    ...addressAuditPopupRoute,
];

@NgModule({
    imports: [
        JestimeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        AddressAuditComponent,
        AddressAuditDetailComponent,
        AddressAuditDialogComponent,
        AddressAuditDeleteDialogComponent,
        AddressAuditPopupComponent,
        AddressAuditDeletePopupComponent,
    ],
    entryComponents: [
        AddressAuditComponent,
        AddressAuditDialogComponent,
        AddressAuditPopupComponent,
        AddressAuditDeleteDialogComponent,
        AddressAuditDeletePopupComponent,
    ],
    providers: [
        AddressAuditService,
        AddressAuditPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JestimeAddressAuditModule {}
