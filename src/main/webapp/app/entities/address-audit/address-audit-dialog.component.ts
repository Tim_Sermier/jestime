import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AddressAudit } from './address-audit.model';
import { AddressAuditPopupService } from './address-audit-popup.service';
import { AddressAuditService } from './address-audit.service';

@Component({
    selector: 'jhi-address-audit-dialog',
    templateUrl: './address-audit-dialog.component.html'
})
export class AddressAuditDialogComponent implements OnInit {

    addressAudit: AddressAudit;
    isSaving: boolean;
    actionDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private addressAuditService: AddressAuditService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.addressAudit.id !== undefined) {
            this.subscribeToSaveResponse(
                this.addressAuditService.update(this.addressAudit));
        } else {
            this.subscribeToSaveResponse(
                this.addressAuditService.create(this.addressAudit));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<AddressAudit>>) {
        result.subscribe((res: HttpResponse<AddressAudit>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: AddressAudit) {
        this.eventManager.broadcast({ name: 'addressAuditListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-address-audit-popup',
    template: ''
})
export class AddressAuditPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private addressAuditPopupService: AddressAuditPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.addressAuditPopupService
                    .open(AddressAuditDialogComponent as Component, params['id']);
            } else {
                this.addressAuditPopupService
                    .open(AddressAuditDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
