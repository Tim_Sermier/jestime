import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AddressAudit } from './address-audit.model';
import { AddressAuditService } from './address-audit.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-address-audit',
    templateUrl: './address-audit.component.html'
})
export class AddressAuditComponent implements OnInit, OnDestroy {
addressAudits: AddressAudit[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private addressAuditService: AddressAuditService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.addressAuditService.query().subscribe(
            (res: HttpResponse<AddressAudit[]>) => {
                this.addressAudits = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInAddressAudits();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: AddressAudit) {
        return item.id;
    }
    registerChangeInAddressAudits() {
        this.eventSubscriber = this.eventManager.subscribe('addressAuditListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
