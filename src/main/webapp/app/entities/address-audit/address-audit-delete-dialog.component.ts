import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AddressAudit } from './address-audit.model';
import { AddressAuditPopupService } from './address-audit-popup.service';
import { AddressAuditService } from './address-audit.service';

@Component({
    selector: 'jhi-address-audit-delete-dialog',
    templateUrl: './address-audit-delete-dialog.component.html'
})
export class AddressAuditDeleteDialogComponent {

    addressAudit: AddressAudit;

    constructor(
        private addressAuditService: AddressAuditService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.addressAuditService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'addressAuditListModification',
                content: 'Deleted an addressAudit'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-address-audit-delete-popup',
    template: ''
})
export class AddressAuditDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private addressAuditPopupService: AddressAuditPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.addressAuditPopupService
                .open(AddressAuditDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
