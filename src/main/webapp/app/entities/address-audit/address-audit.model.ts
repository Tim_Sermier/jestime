import { BaseEntity } from './../../shared';

export class AddressAudit implements BaseEntity {
    constructor(
        public id?: number,
        public action?: string,
        public actionDate?: any,
        public actionUser?: number,
        public code?: string,
        public name?: string,
        public location?: string,
        public phone?: string,
        public fax?: string,
        public email?: string,
        public website?: string,
    ) {
    }
}
