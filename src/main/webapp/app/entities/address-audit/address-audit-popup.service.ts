import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { AddressAudit } from './address-audit.model';
import { AddressAuditService } from './address-audit.service';

@Injectable()
export class AddressAuditPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private addressAuditService: AddressAuditService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.addressAuditService.find(id)
                    .subscribe((addressAuditResponse: HttpResponse<AddressAudit>) => {
                        const addressAudit: AddressAudit = addressAuditResponse.body;
                        if (addressAudit.actionDate) {
                            addressAudit.actionDate = {
                                year: addressAudit.actionDate.getFullYear(),
                                month: addressAudit.actionDate.getMonth() + 1,
                                day: addressAudit.actionDate.getDate()
                            };
                        }
                        this.ngbModalRef = this.addressAuditModalRef(component, addressAudit);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.addressAuditModalRef(component, new AddressAudit());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    addressAuditModalRef(component: Component, addressAudit: AddressAudit): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.addressAudit = addressAudit;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
