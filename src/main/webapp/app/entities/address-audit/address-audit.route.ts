import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { AddressAuditComponent } from './address-audit.component';
import { AddressAuditDetailComponent } from './address-audit-detail.component';
import { AddressAuditPopupComponent } from './address-audit-dialog.component';
import { AddressAuditDeletePopupComponent } from './address-audit-delete-dialog.component';

export const addressAuditRoute: Routes = [
    {
        path: 'address-audit',
        component: AddressAuditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.addressAudit.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'address-audit/:id',
        component: AddressAuditDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.addressAudit.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const addressAuditPopupRoute: Routes = [
    {
        path: 'address-audit-new',
        component: AddressAuditPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.addressAudit.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'address-audit/:id/edit',
        component: AddressAuditPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.addressAudit.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'address-audit/:id/delete',
        component: AddressAuditDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.addressAudit.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
