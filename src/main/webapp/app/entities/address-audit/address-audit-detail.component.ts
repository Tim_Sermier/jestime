import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { AddressAudit } from './address-audit.model';
import { AddressAuditService } from './address-audit.service';

@Component({
    selector: 'jhi-address-audit-detail',
    templateUrl: './address-audit-detail.component.html'
})
export class AddressAuditDetailComponent implements OnInit, OnDestroy {

    addressAudit: AddressAudit;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private addressAuditService: AddressAuditService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAddressAudits();
    }

    load(id) {
        this.addressAuditService.find(id)
            .subscribe((addressAuditResponse: HttpResponse<AddressAudit>) => {
                this.addressAudit = addressAuditResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAddressAudits() {
        this.eventSubscriber = this.eventManager.subscribe(
            'addressAuditListModification',
            (response) => this.load(this.addressAudit.id)
        );
    }
}
