import { BaseEntity } from './../../shared';

export class Reference implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public title?: string,
        public sourceTitle?: string,
        public sourceId?: number,
        public sourceUrl?: string,
        public sourceCode?: string,
    ) {
    }
}
