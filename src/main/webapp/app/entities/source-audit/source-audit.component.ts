import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { SourceAudit } from './source-audit.model';
import { SourceAuditService } from './source-audit.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-source-audit',
    templateUrl: './source-audit.component.html'
})
export class SourceAuditComponent implements OnInit, OnDestroy {
sourceAudits: SourceAudit[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private sourceAuditService: SourceAuditService,
        private jhiAlertService: JhiAlertService,
        private dataUtils: JhiDataUtils,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.sourceAuditService.query().subscribe(
            (res: HttpResponse<SourceAudit[]>) => {
                this.sourceAudits = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInSourceAudits();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: SourceAudit) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    registerChangeInSourceAudits() {
        this.eventSubscriber = this.eventManager.subscribe('sourceAuditListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
