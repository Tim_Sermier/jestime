import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JestimeSharedModule } from '../../shared';
import {
    SourceAuditService,
    SourceAuditPopupService,
    SourceAuditComponent,
    SourceAuditDetailComponent,
    SourceAuditDialogComponent,
    SourceAuditPopupComponent,
    SourceAuditDeletePopupComponent,
    SourceAuditDeleteDialogComponent,
    sourceAuditRoute,
    sourceAuditPopupRoute,
} from './';

const ENTITY_STATES = [
    ...sourceAuditRoute,
    ...sourceAuditPopupRoute,
];

@NgModule({
    imports: [
        JestimeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SourceAuditComponent,
        SourceAuditDetailComponent,
        SourceAuditDialogComponent,
        SourceAuditDeleteDialogComponent,
        SourceAuditPopupComponent,
        SourceAuditDeletePopupComponent,
    ],
    entryComponents: [
        SourceAuditComponent,
        SourceAuditDialogComponent,
        SourceAuditPopupComponent,
        SourceAuditDeleteDialogComponent,
        SourceAuditDeletePopupComponent,
    ],
    providers: [
        SourceAuditService,
        SourceAuditPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JestimeSourceAuditModule {}
