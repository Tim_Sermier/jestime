import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { SourceAuditComponent } from './source-audit.component';
import { SourceAuditDetailComponent } from './source-audit-detail.component';
import { SourceAuditPopupComponent } from './source-audit-dialog.component';
import { SourceAuditDeletePopupComponent } from './source-audit-delete-dialog.component';

export const sourceAuditRoute: Routes = [
    {
        path: 'source-audit',
        component: SourceAuditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.sourceAudit.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'source-audit/:id',
        component: SourceAuditDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.sourceAudit.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const sourceAuditPopupRoute: Routes = [
    {
        path: 'source-audit-new',
        component: SourceAuditPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.sourceAudit.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'source-audit/:id/edit',
        component: SourceAuditPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.sourceAudit.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'source-audit/:id/delete',
        component: SourceAuditDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.sourceAudit.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
