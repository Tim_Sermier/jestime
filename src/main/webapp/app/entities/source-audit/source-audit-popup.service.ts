import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { SourceAudit } from './source-audit.model';
import { SourceAuditService } from './source-audit.service';

@Injectable()
export class SourceAuditPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private sourceAuditService: SourceAuditService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.sourceAuditService.find(id)
                    .subscribe((sourceAuditResponse: HttpResponse<SourceAudit>) => {
                        const sourceAudit: SourceAudit = sourceAuditResponse.body;
                        if (sourceAudit.actionDate) {
                            sourceAudit.actionDate = {
                                year: sourceAudit.actionDate.getFullYear(),
                                month: sourceAudit.actionDate.getMonth() + 1,
                                day: sourceAudit.actionDate.getDate()
                            };
                        }
                        this.ngbModalRef = this.sourceAuditModalRef(component, sourceAudit);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.sourceAuditModalRef(component, new SourceAudit());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    sourceAuditModalRef(component: Component, sourceAudit: SourceAudit): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.sourceAudit = sourceAudit;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
