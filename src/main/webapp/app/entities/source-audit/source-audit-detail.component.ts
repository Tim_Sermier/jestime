import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { SourceAudit } from './source-audit.model';
import { SourceAuditService } from './source-audit.service';

@Component({
    selector: 'jhi-source-audit-detail',
    templateUrl: './source-audit-detail.component.html'
})
export class SourceAuditDetailComponent implements OnInit, OnDestroy {

    sourceAudit: SourceAudit;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private sourceAuditService: SourceAuditService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSourceAudits();
    }

    load(id) {
        this.sourceAuditService.find(id)
            .subscribe((sourceAuditResponse: HttpResponse<SourceAudit>) => {
                this.sourceAudit = sourceAuditResponse.body;
            });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSourceAudits() {
        this.eventSubscriber = this.eventManager.subscribe(
            'sourceAuditListModification',
            (response) => this.load(this.sourceAudit.id)
        );
    }
}
