import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SourceAudit } from './source-audit.model';
import { SourceAuditPopupService } from './source-audit-popup.service';
import { SourceAuditService } from './source-audit.service';

@Component({
    selector: 'jhi-source-audit-delete-dialog',
    templateUrl: './source-audit-delete-dialog.component.html'
})
export class SourceAuditDeleteDialogComponent {

    sourceAudit: SourceAudit;

    constructor(
        private sourceAuditService: SourceAuditService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.sourceAuditService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'sourceAuditListModification',
                content: 'Deleted an sourceAudit'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-source-audit-delete-popup',
    template: ''
})
export class SourceAuditDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private sourceAuditPopupService: SourceAuditPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.sourceAuditPopupService
                .open(SourceAuditDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
