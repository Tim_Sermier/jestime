import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { SourceAudit } from './source-audit.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<SourceAudit>;

@Injectable()
export class SourceAuditService {

    private resourceUrl =  SERVER_API_URL + 'api/source-audits';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(sourceAudit: SourceAudit): Observable<EntityResponseType> {
        const copy = this.convert(sourceAudit);
        return this.http.post<SourceAudit>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(sourceAudit: SourceAudit): Observable<EntityResponseType> {
        const copy = this.convert(sourceAudit);
        return this.http.put<SourceAudit>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<SourceAudit>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<SourceAudit[]>> {
        const options = createRequestOption(req);
        return this.http.get<SourceAudit[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<SourceAudit[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: SourceAudit = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<SourceAudit[]>): HttpResponse<SourceAudit[]> {
        const jsonResponse: SourceAudit[] = res.body;
        const body: SourceAudit[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to SourceAudit.
     */
    private convertItemFromServer(sourceAudit: SourceAudit): SourceAudit {
        const copy: SourceAudit = Object.assign({}, sourceAudit);
        copy.actionDate = this.dateUtils
            .convertLocalDateFromServer(sourceAudit.actionDate);
        return copy;
    }

    /**
     * Convert a SourceAudit to a JSON which can be sent to the server.
     */
    private convert(sourceAudit: SourceAudit): SourceAudit {
        const copy: SourceAudit = Object.assign({}, sourceAudit);
        copy.actionDate = this.dateUtils
            .convertLocalDateToServer(sourceAudit.actionDate);
        return copy;
    }
}
