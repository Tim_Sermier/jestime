import { BaseEntity } from './../../shared';

export class SourceAudit implements BaseEntity {
    constructor(
        public id?: number,
        public action?: string,
        public actionDate?: any,
        public actionUser?: string,
        public title?: string,
        public description?: any,
        public url?: string,
        public juridictionId?: number,
    ) {
    }
}
