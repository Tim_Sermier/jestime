import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { SourceAudit } from './source-audit.model';
import { SourceAuditPopupService } from './source-audit-popup.service';
import { SourceAuditService } from './source-audit.service';

@Component({
    selector: 'jhi-source-audit-dialog',
    templateUrl: './source-audit-dialog.component.html'
})
export class SourceAuditDialogComponent implements OnInit {

    sourceAudit: SourceAudit;
    isSaving: boolean;
    actionDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private sourceAuditService: SourceAuditService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.sourceAudit.id !== undefined) {
            this.subscribeToSaveResponse(
                this.sourceAuditService.update(this.sourceAudit));
        } else {
            this.subscribeToSaveResponse(
                this.sourceAuditService.create(this.sourceAudit));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<SourceAudit>>) {
        result.subscribe((res: HttpResponse<SourceAudit>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: SourceAudit) {
        this.eventManager.broadcast({ name: 'sourceAuditListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-source-audit-popup',
    template: ''
})
export class SourceAuditPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private sourceAuditPopupService: SourceAuditPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.sourceAuditPopupService
                    .open(SourceAuditDialogComponent as Component, params['id']);
            } else {
                this.sourceAuditPopupService
                    .open(SourceAuditDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
