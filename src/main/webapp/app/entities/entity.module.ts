import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { JestimeJuridictionModule } from './juridiction/juridiction.module';
import { JestimeSocialCardModule } from './social-card/social-card.module';
import { JestimeCategoryModule } from './category/category.module';
import { JestimeAddressModule } from './address/address.module';
import { JestimeSocialCardAuditModule } from './social-card-audit/social-card-audit.module';
import { JestimeAddressAuditModule } from './address-audit/address-audit.module';
import { JestimeSourceModule } from './source/source.module';
import { JestimeSourceAuditModule } from './source-audit/source-audit.module';
import { JestimeConditionModule } from './condition/condition.module';
import { JestimeVariableModule } from './variable/variable.module';
import { JestimeConditionValueModule } from './condition-value/condition-value.module';
import { JestimeReferenceModule } from './reference/reference.module';
import { JestimeVariableTypeModule } from './variable-type/variable-type.module';
import { JestimeOperatorModule } from './operator/operator.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        JestimeJuridictionModule,
        JestimeSocialCardModule,
        JestimeCategoryModule,
        JestimeAddressModule,
        JestimeSocialCardAuditModule,
        JestimeAddressAuditModule,
        JestimeSourceModule,
        JestimeSourceAuditModule,
        JestimeConditionModule,
        JestimeVariableModule,
        JestimeConditionValueModule,
        JestimeReferenceModule,
        JestimeVariableTypeModule,
        JestimeOperatorModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JestimeEntityModule {}
