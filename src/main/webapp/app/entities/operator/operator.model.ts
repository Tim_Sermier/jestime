import { BaseEntity } from './../../shared';

export class Operator implements BaseEntity {
    constructor(
        public id?: number,
        public symbol?: string,
        public name?: string,
    ) {
    }
}
