import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JestimeSharedModule } from '../../shared';
import { JestimeAdminModule } from '../../admin/admin.module';
import {
    SourceService,
    SourcePopupService,
    SourceComponent,
    SourceDetailComponent,
    SourceDialogComponent,
    SourcePopupComponent,
    SourceDeletePopupComponent,
    SourceDeleteDialogComponent,
    sourceRoute,
    sourcePopupRoute,
} from './';

const ENTITY_STATES = [
    ...sourceRoute,
    ...sourcePopupRoute,
];

@NgModule({
    imports: [
        JestimeSharedModule,
        JestimeAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SourceComponent,
        SourceDetailComponent,
        SourceDialogComponent,
        SourceDeleteDialogComponent,
        SourcePopupComponent,
        SourceDeletePopupComponent,
    ],
    entryComponents: [
        SourceComponent,
        SourceDialogComponent,
        SourcePopupComponent,
        SourceDeleteDialogComponent,
        SourceDeletePopupComponent,
    ],
    providers: [
        SourceService,
        SourcePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JestimeSourceModule {}
