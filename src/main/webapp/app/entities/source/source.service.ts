import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Source } from './source.model';
import { createRequestOption } from '../../shared';
import {SocialCard} from '../social-card/social-card.model';

export type EntityResponseType = HttpResponse<Source>;

@Injectable()
export class SourceService {

    private resourceUrl =  SERVER_API_URL + 'api/sources';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(source: Source): Observable<EntityResponseType> {
        const copy = this.convert(source);
        return this.http.post<Source>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(source: Source): Observable<EntityResponseType> {
        const copy = this.convert(source);
        return this.http.put<Source>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Source>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Source[]>> {
        const options = createRequestOption(req);
        return this.http.get<Source[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Source[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    findByCriteria(filter: string, req?: any): Observable<HttpResponse<Source[]>> {
        const options = createRequestOption(req);
        return this.http.get<Source[]>(this.resourceUrl + filter, { params: options, observe: 'response' })
            .map((res: HttpResponse<Source[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Source = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Source[]>): HttpResponse<Source[]> {
        const jsonResponse: Source[] = res.body;
        const body: Source[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Source.
     */
    private convertItemFromServer(source: Source): Source {
        const copy: Source = Object.assign({}, source);
        return copy;
    }

    /**
     * Convert a Source to a JSON which can be sent to the server.
     */
    private convert(source: Source): Source {
        const copy: Source = Object.assign({}, source);
        return copy;
    }

    generateCode(source: Source) {
        const code = 'SRC_' + Date.now();
        return code;
    }
}
