import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { Source } from './source.model';
import { SourceService } from './source.service';

@Injectable()
export class SourcePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private sourceService: SourceService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.sourceService.find(id)
                    .subscribe((sourceResponse: HttpResponse<Source>) => {
                        const source: Source = sourceResponse.body;
                        if (source.creationDate) {
                            source.creationDate = {
                                year: source.creationDate.getFullYear(),
                                month: source.creationDate.getMonth() + 1,
                                day: source.creationDate.getDate()
                            };
                        }
                        if (source.editionDate) {
                            source.editionDate = {
                                year: source.editionDate.getFullYear(),
                                month: source.editionDate.getMonth() + 1,
                                day: source.editionDate.getDate()
                            };
                        }
                        this.ngbModalRef = this.sourceModalRef(component, source);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.sourceModalRef(component, new Source());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    sourceModalRef(component: Component, source: Source): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.source = source;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
