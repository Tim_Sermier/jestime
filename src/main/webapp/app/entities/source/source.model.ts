import { BaseEntity } from './../../shared';

export class Source implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public title?: string,
        public description?: any,
        public url?: string,
        public creationDate?: any,
        public editionDate?: any,
        public socialCards?: BaseEntity[],
        public juridictionName?: string,
        public juridictionId?: number,
        public creatorLogin?: string,
        public creatorId?: number,
        public updatorLogin?: string,
        public updatorId?: number,
        public references?: BaseEntity[],
    ) {
    }
}
