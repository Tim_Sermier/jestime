import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { VariableType } from './variable-type.model';
import { VariableTypeService } from './variable-type.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-variable-type',
    templateUrl: './variable-type.component.html'
})
export class VariableTypeComponent implements OnInit, OnDestroy {
variableTypes: VariableType[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private variableTypeService: VariableTypeService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.variableTypeService.query().subscribe(
            (res: HttpResponse<VariableType[]>) => {
                this.variableTypes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInVariableTypes();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: VariableType) {
        return item.id;
    }
    registerChangeInVariableTypes() {
        this.eventSubscriber = this.eventManager.subscribe('variableTypeListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
