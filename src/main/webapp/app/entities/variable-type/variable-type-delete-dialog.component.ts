import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { VariableType } from './variable-type.model';
import { VariableTypePopupService } from './variable-type-popup.service';
import { VariableTypeService } from './variable-type.service';

@Component({
    selector: 'jhi-variable-type-delete-dialog',
    templateUrl: './variable-type-delete-dialog.component.html'
})
export class VariableTypeDeleteDialogComponent {

    variableType: VariableType;

    constructor(
        private variableTypeService: VariableTypeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.variableTypeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'variableTypeListModification',
                content: 'Deleted an variableType'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-variable-type-delete-popup',
    template: ''
})
export class VariableTypeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private variableTypePopupService: VariableTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.variableTypePopupService
                .open(VariableTypeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
