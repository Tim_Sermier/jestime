import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { VariableType } from './variable-type.model';
import { VariableTypeService } from './variable-type.service';

@Component({
    selector: 'jhi-variable-type-detail',
    templateUrl: './variable-type-detail.component.html'
})
export class VariableTypeDetailComponent implements OnInit, OnDestroy {

    variableType: VariableType;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private variableTypeService: VariableTypeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInVariableTypes();
    }

    load(id) {
        this.variableTypeService.find(id)
            .subscribe((variableTypeResponse: HttpResponse<VariableType>) => {
                this.variableType = variableTypeResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInVariableTypes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'variableTypeListModification',
            (response) => this.load(this.variableType.id)
        );
    }
}
