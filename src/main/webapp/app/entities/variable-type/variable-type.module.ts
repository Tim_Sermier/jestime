import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JestimeSharedModule } from '../../shared';
import {
    VariableTypeService,
    VariableTypePopupService,
    VariableTypeComponent,
    VariableTypeDetailComponent,
    VariableTypeDialogComponent,
    VariableTypePopupComponent,
    VariableTypeDeletePopupComponent,
    VariableTypeDeleteDialogComponent,
    variableTypeRoute,
    variableTypePopupRoute,
} from './';

const ENTITY_STATES = [
    ...variableTypeRoute,
    ...variableTypePopupRoute,
];

@NgModule({
    imports: [
        JestimeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        VariableTypeComponent,
        VariableTypeDetailComponent,
        VariableTypeDialogComponent,
        VariableTypeDeleteDialogComponent,
        VariableTypePopupComponent,
        VariableTypeDeletePopupComponent,
    ],
    entryComponents: [
        VariableTypeComponent,
        VariableTypeDialogComponent,
        VariableTypePopupComponent,
        VariableTypeDeleteDialogComponent,
        VariableTypeDeletePopupComponent,
    ],
    providers: [
        VariableTypeService,
        VariableTypePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JestimeVariableTypeModule {}
