import { BaseEntity } from './../../shared';

export class VariableType implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public name?: string,
    ) {
    }
}
