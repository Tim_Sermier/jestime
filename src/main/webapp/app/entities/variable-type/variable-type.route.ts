import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { VariableTypeComponent } from './variable-type.component';
import { VariableTypeDetailComponent } from './variable-type-detail.component';
import { VariableTypePopupComponent } from './variable-type-dialog.component';
import { VariableTypeDeletePopupComponent } from './variable-type-delete-dialog.component';

export const variableTypeRoute: Routes = [
    {
        path: 'variable-type',
        component: VariableTypeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.variableType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'variable-type/:id',
        component: VariableTypeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.variableType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const variableTypePopupRoute: Routes = [
    {
        path: 'variable-type-new',
        component: VariableTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.variableType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'variable-type/:id/edit',
        component: VariableTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.variableType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'variable-type/:id/delete',
        component: VariableTypeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.variableType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
