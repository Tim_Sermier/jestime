import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { VariableType } from './variable-type.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<VariableType>;

@Injectable()
export class VariableTypeService {

    private resourceUrl =  SERVER_API_URL + 'api/variable-types';

    constructor(private http: HttpClient) { }

    create(variableType: VariableType): Observable<EntityResponseType> {
        const copy = this.convert(variableType);
        return this.http.post<VariableType>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(variableType: VariableType): Observable<EntityResponseType> {
        const copy = this.convert(variableType);
        return this.http.put<VariableType>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<VariableType>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<VariableType[]>> {
        const options = createRequestOption(req);
        return this.http.get<VariableType[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<VariableType[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: VariableType = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<VariableType[]>): HttpResponse<VariableType[]> {
        const jsonResponse: VariableType[] = res.body;
        const body: VariableType[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to VariableType.
     */
    private convertItemFromServer(variableType: VariableType): VariableType {
        const copy: VariableType = Object.assign({}, variableType);
        return copy;
    }

    /**
     * Convert a VariableType to a JSON which can be sent to the server.
     */
    private convert(variableType: VariableType): VariableType {
        const copy: VariableType = Object.assign({}, variableType);
        return copy;
    }
}
