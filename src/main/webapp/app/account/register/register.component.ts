import {Component, OnInit, AfterViewInit, Renderer, ElementRef} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiLanguageService} from 'ng-jhipster';

import {Register} from './register.service';
import {LoginModalService, EMAIL_ALREADY_USED_TYPE, LOGIN_ALREADY_USED_TYPE} from '../../shared';
import {User} from '../../shared/user/user.model';
import {Juridiction} from '../../entities/juridiction/juridiction.model';
import {JuridictionService} from '../../entities/juridiction/juridiction.service';

@Component({
    selector: 'jhi-register',
    templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit, AfterViewInit {

    confirmPassword: string;
    doNotMatch: string;
    error: string;
    errorEmailExists: string;
    errorUserExists: string;
    registerAccount: any;
    success: boolean;
    modalRef: NgbModalRef;
    isSaving: boolean;
    juridictions: Juridiction[];

    constructor(private languageService: JhiLanguageService,
                private loginModalService: LoginModalService,
                private registerService: Register,
                private elementRef: ElementRef,
                private renderer: Renderer,
                private jhiAlertService: JhiAlertService,
                private juridictionService: JuridictionService) {
    }

    ngOnInit() {
        console.log('START');
        this.success = false;
        this.registerAccount = {};
        this.isSaving = false;
        this.juridictionService
            .query({filter: 'all'})
            .subscribe((res: HttpResponse<Juridiction[]>) => {
                if (!this.registerAccount.juridictionId) {
                    this.juridictions = res.body;

                    console.log(this.juridictions);
                } else {
                    this.juridictionService
                        .find(this.registerAccount.juridictionId)
                        .subscribe((subRes: HttpResponse<Juridiction>) => {
                            this.juridictions = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    ngAfterViewInit() {
        this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#login'), 'focus', []);
    }

    register() {
        if (this.registerAccount.password !== this.confirmPassword) {
            this.doNotMatch = 'ERROR';
        } else {
            this.doNotMatch = null;
            this.error = null;
            this.errorUserExists = null;
            this.errorEmailExists = null;
            this.languageService.getCurrent().then((key) => {
                this.registerAccount.langKey = key;
                this.registerService.save(this.registerAccount).subscribe(() => {
                    this.success = true;
                }, (response) => this.processError(response));
            });
        }
    }

    openLogin() {
        this.modalRef = this.loginModalService.open();
    }

    private processError(response: HttpErrorResponse) {
        this.success = null;
        if (response.status === 400 && response.error.type === LOGIN_ALREADY_USED_TYPE) {
            this.errorUserExists = 'ERROR';
        } else if (response.status === 400 && response.error.type === EMAIL_ALREADY_USED_TYPE) {
            this.errorEmailExists = 'ERROR';
        } else {
            this.error = 'ERROR';
        }
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackJuridictionById(index: number, item: Juridiction) {
        return item.id;
    }
}
