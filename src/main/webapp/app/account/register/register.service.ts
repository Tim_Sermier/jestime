import {Injectable, OnInit} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';
import {User} from '../../shared/user/user.model';
import {Juridiction} from '../../entities/juridiction/juridiction.model';
import {JuridictionService} from '../../entities/juridiction/juridiction.service';
import {JhiAlertService} from 'ng-jhipster';

@Injectable()
export class Register {
    constructor(
        private http: HttpClient,
        private jhiAlertService: JhiAlertService,
        private juridictionService: JuridictionService
    ) {}

    save(account: any): Observable<any> {
        return this.http.post(SERVER_API_URL + 'api/register', account);
    }
}
