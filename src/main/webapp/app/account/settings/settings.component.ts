import { Component, OnInit } from '@angular/core';
import {JhiAlertService, JhiLanguageService} from 'ng-jhipster';

import { Principal, AccountService, JhiLanguageHelper } from '../../shared';
import {JuridictionService} from '../../entities/juridiction/juridiction.service';
import {Juridiction} from '../../entities/juridiction/juridiction.model';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

@Component({
    selector: 'jhi-settings',
    templateUrl: './settings.component.html'
})
export class SettingsComponent implements OnInit {
    error: string;
    success: string;
    settingsAccount: any;
    languages: any[];
    juridictions: Juridiction[];

    constructor(
        private account: AccountService,
        private principal: Principal,
        private languageService: JhiLanguageService,
        private languageHelper: JhiLanguageHelper,
        private jhiAlertService: JhiAlertService,
        private juridictionService: JuridictionService
    ) {
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.settingsAccount = this.copyAccount(account);
        });
        this.languageHelper.getAll().then((languages) => {
            this.languages = languages;
        });
        this.juridictionService
            .query({filter: 'all'})
            .subscribe((res: HttpResponse<Juridiction[]>) => {
                this.juridictions = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    save() {
        this.account.save(this.settingsAccount).subscribe(() => {
            this.error = null;
            this.success = 'OK';
            this.principal.identity(true).then((account) => {
                this.settingsAccount = this.copyAccount(account);
            });
            this.languageService.getCurrent().then((current) => {
                if (this.settingsAccount.langKey !== current) {
                    this.languageService.changeLanguage(this.settingsAccount.langKey);
                }
            });
        }, () => {
            this.success = null;
            this.error = 'ERROR';
        });
    }

    copyAccount(account) {
        return {
            activated: account.activated,
            email: account.email,
            firstName: account.firstName,
            langKey: account.langKey,
            lastName: account.lastName,
            login: account.login,
            imageUrl: account.imageUrl,
            juridictionId: account.juridictionId
        };
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackJuridictionById(index: number, item: Juridiction) {
        return item.id;
    }
}
