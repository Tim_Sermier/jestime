import {Component, OnInit} from '@angular/core';
import {SocialCard, SocialCardService} from '../entities/social-card';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';
import {Address} from '../entities/address';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {AddressDetailDialogComponent} from '../address-detail/address-detail-dialog.component';
import {MatDialog} from '@angular/material';
import {SocialCardDetailDialogComponent} from './social-card-detail-dialog.component';
import {SocialCardDetailDeleteComponent} from './social-card-detail-delete.component';
import {Subscription} from 'rxjs/Subscription';
import {User, UserService} from '../shared';
import {SocialCardDetailDeleteConditionDialogComponent} from './social-card-detail-delete-condition-dialog.component';
import {Condition} from '../entities/condition';

@Component({
    selector: 'jhi-social-card-detail',
    templateUrl: './social-card-detail.component.html',
    styles: []
})
export class SocialCardDetailComponent implements OnInit {
    id: number;
    private sub: any;
    socialCard: SocialCard;
    children: SocialCard[];
    creator: User;
    updator: User;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    informationsIndicator: number;
    addressesIndicator: number;
    sourcesIndicator: number;
    conditionsIndicator: number;

    constructor(
        private socialCardService: SocialCardService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private userService: UserService,
        private route: ActivatedRoute,
        private router: Router,
        private _location: Location,
        public dialog: MatDialog,
    ) {
    }

    /**
     * À l'initialisation, on récupère la source juridique selon l'ID passé en paramètre
     */
    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSocialCards();
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    /**
     * Permet de se rendre sur le détail d'une adresse
     * @param {Address} address
     */
    gotoAddress(address: Address) {
        this.router.navigate(['/address-detail', address.id]);
    }

    goBack() {
        this._location.back();
    }

    /**
     * Permet d'afficher la fenêtre modale de modification d'une fiche de prestation
     */
    openDialog(): void {
        const dialogRef = this.dialog.open(SocialCardDetailDialogComponent, {
            width: '80%',
            height: '80%',
            disableClose: true,
            data: { socialCard: this.socialCard }
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result != null) {
                this.socialCardService.find(result.id)
                    .subscribe((socialCardResponse: HttpResponse<SocialCard>) => {
                        this.socialCard = socialCardResponse.body;
                        this.calcIndicators();
                    });
            } else {
                this.socialCardService.find(this.socialCard.id)
                    .subscribe((socialCardResponse: HttpResponse<SocialCard>) => {
                        this.socialCard = socialCardResponse.body;
                        this.calcIndicators();
                    });
            }
        });
    }

    /**
     * Ouvre la fenêtre modale de confirmation de suppression
     */
    openDeleteDialog(): void {
        const dialogRef = this.dialog.open(SocialCardDetailDeleteComponent, {
            width: '80%',
            data: { socialCard: this.socialCard }
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                this.router.navigate(['/']);
            }
        });
    }

    /**
     * Méthode permettant de vérifier si la fiche a des enfants afin de bloquer la suppression
     * @param {number} id
     */
    hasChildren(id: number) {
        this.socialCardService.findByCriterias('?parentSocialCardId.equals=' + id).subscribe(
            (res: HttpResponse<SocialCard[]>) => {
                this.children = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    load(id) {
        this.socialCardService.find(id)
            .subscribe((socialCardResponse: HttpResponse<SocialCard>) => {
                this.socialCard = socialCardResponse.body;
                this.hasChildren(this.socialCard.id);
                this.userService.find(this.socialCard.creatorLogin).subscribe((response) => {
                    this.creator = response.body;
                });
                if (this.socialCard.updatorLogin) {
                    this.userService.find(this.socialCard.updatorLogin).subscribe((response) => {
                        this.updator = response.body;
                    });
                }
                this.calcIndicators();
            });
    }

    /**
     * Méthode permettant de calculer les indicateurs de complétion de la fiche
     */
    calcIndicators() {
        this.informationsIndicator = 0;
        if (this.socialCard.general) {
            this.informationsIndicator += 25;
        }
        if (this.socialCard.description) {
            this.informationsIndicator += 25;
        }
        if (this.socialCard.procedure) {
            this.informationsIndicator += 25;
        }
        if (this.socialCard.recourse) {
            this.informationsIndicator += 25;
        }

        if (this.socialCard.addresses.length > 0) {
            this.addressesIndicator = 100;
        } else {
            this.addressesIndicator = 0;
        }

        if (this.socialCard.sources.length > 0) {
            this.sourcesIndicator = 100;
        } else {
            this.sourcesIndicator = 0;
        }

        if (this.socialCard.conditions.length > 0) {
            this.conditionsIndicator = 100;
        } else {
            this.conditionsIndicator = 0;
        }
    }

    registerChangeInSocialCards() {
        this.eventSubscriber = this.eventManager.subscribe(
            'addressListModification',
            (response) => this.load(this.socialCard.id)
        );
    }
}
