import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';
import {Address, AddressService} from '../entities/address';
import {ActivatedRoute} from '@angular/router';
import {DatePipe, Location} from '@angular/common';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {Category, CategoryService} from '../entities/category';
import {SocialCard, SocialCardService} from '../entities/social-card';
import {AddressDialogComponent, ParentDialogComponent} from '../creation-wizard/creation-wizard.component';
import {Observable} from 'rxjs/Observable';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Juridiction, JuridictionService} from '../entities/juridiction';
import {Account, Principal, UserService} from '../shared';
import {Source, SourceService} from '../entities/source';
import {CreationWizardSourceDialogComponent} from '../creation-wizard/creation-wizard-source-dialog.component';
import {Condition, ConditionService} from '../entities/condition';
import {SocialCardDetailDeleteConditionDialogComponent} from './social-card-detail-delete-condition-dialog.component';
import {Reference, ReferenceService} from '../entities/reference';
import {ConditionValue, ConditionValueService} from '../entities/condition-value';
import {CreationWizardConditionDialogComponent} from '../creation-wizard/creation-wizard-condition-dialog.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {ConditionHelpComponent} from '../creation-wizard/condition-help.component';
import {SocialCardDetailConfirmDialogCloseComponent} from './social-card-detail-confirm-dialog-close.component';

@Component({
    selector: 'jhi-social-card-detail-dialog',
    templateUrl: './social-card-detail-dialog.component.html',
    styles: []
})
export class SocialCardDetailDialogComponent implements OnInit {
    socialCard: SocialCard;
    generalFormFirstStep: FormGroup;
    generalFormSecondStep: FormGroup;
    juridictions: Juridiction[];
    categories: Category[];
    parentSocialCard: SocialCard;
    address: Address;
    source: Source;
    newAddresses: Address[];
    selectedAddresses: Address[];
    currentAccount: Account;
    userId: number;
    oldTitle: string;
    oldJuridictionId: number;
    selectedSources: Source[];
    newSources: Source[];
    condition: Condition;
    newConditions: Condition[];
    newConditionValues: ConditionValue[];
    conditionSaved: boolean;
    addressesSaved: boolean;
    referenceSaved: boolean;
    valueSaved: boolean;
    loading: boolean;

    constructor(
        public dialogRef: MatDialogRef<SocialCardDetailDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private socialCardService: SocialCardService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        public dialog: MatDialog,
        private _formBuilder: FormBuilder,
        private juridictionService: JuridictionService,
        private categoryService: CategoryService,
        private datePipe: DatePipe,
        private parentDialog: MatDialog,
        private addressService: AddressService,
        private addressDialog: MatDialog,
        private principal: Principal,
        private userService: UserService,
        private sourceService: SourceService,
        private conditionService: ConditionService,
        private conditionValueService: ConditionValueService,
        private referenceService: ReferenceService
    ) {
        this.loading = false;
    }

    /**
     * À l'initialisation, on récupère la fiche de prestation sociale, les juridictions
     * , les catégories ainsi que l'utilisateur en train d'effectuer la modification
     */
    ngOnInit() {
        this.addressesSaved = false;
        this.referenceSaved = false;
        this.valueSaved = false;
        this.conditionSaved = false;
        this.socialCardService.find(this.data.socialCard.id)
            .subscribe((socialCardResponse: HttpResponse<SocialCard>) => {
                this.socialCard = socialCardResponse.body;
                this.oldJuridictionId = this.socialCard.juridictionId;
                this.oldTitle = this.socialCard.title;
                this.newAddresses = [];
                this.newSources = [];
                this.newConditions = [];
                this.newConditionValues = [];
                this.selectedAddresses = this.socialCard.addresses;
                this.selectedSources = this.socialCard.sources;
                this.address = new Address;
                this.source = new Source;
                this.condition = new Condition();
            });
        this.juridictionService.query()
            .subscribe((res: HttpResponse<Juridiction[]>) => { this.juridictions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.categoryService.query()
            .subscribe((res: HttpResponse<Category[]>) => { this.categories = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            this.userService.find(this.currentAccount.login).subscribe((response) => {
                this.userId = response.body.id;
            });
        });
    }

    /**
     * Permet d'ouvrir l'aide contextuelle des conditions d'octroi
     */
    openConditionHelp(): void {
        const dialogRefHelp = this.dialog.open(ConditionHelpComponent, {
            width: '800px'
        });
    }

    /**
     * Permet d'ouvrir la fenêtre de demande d'enregistrement
     */
    openConfirm(): void {
        const dialogRef = this.dialog.open(SocialCardDetailConfirmDialogCloseComponent, {
            width: '800px',
            disableClose: true
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                this.save();
            } else {
                this.dialogRef.close();
            }
        });
    }

    /**
     * Sur annulation on demande une confirmation
     */
    cancel() {
        this.openConfirm();
    }

    /**
     * À l'enregistrement, on vérifie que la nouvelle fiche est unique
     */
    save() {
        if (this.parentSocialCard != null) {
            this.socialCard.parentSocialCardId = this.parentSocialCard.id;
        }
        this.checkIsUnique();
    }

    /**
     * Si la fiche est unique, on persiste les modifications ainsi que les nouveaux
     * objets ayant été créés
     */
    checkIsUnique() {
        let existingSocialCards = [];
        this.socialCardService.findByCriterias('?title.equals=' + this.socialCard.title + '&juridictionId.equals=' + this.socialCard.juridictionId).subscribe(
            (res: HttpResponse<SocialCard[]>) => {
                existingSocialCards = res.body;
                if (existingSocialCards.length === 0 || (this.oldJuridictionId === this.socialCard.juridictionId && this.oldTitle === this.socialCard.title)) {
                    if (this.socialCard.title !== '' && this.socialCard.juridictionId !== null) {
                        this.loading = true;
                        this.socialCard.creationDate = this.datePipe.transform(this.socialCard.creationDate, 'yyyy-MM-dd');
                        this.socialCard.editDate = this.datePipe.transform(Date.now(), 'yyyy-MM-dd');
                        this.socialCard.updatorId = this.userId;
                        if (this.newSources.length > 0) {
                            for (let i = 0, len = this.newSources.length; i < len; i++) {
                                this.subscribeToSaveResponseSource(
                                    this.sourceService.create(this.newSources[i])
                                );
                            }
                        } else {
                            if (this.newAddresses.length > 0) {
                                this.createAddresses();
                            } else {
                                if (this.newConditions.length > 0) {
                                    this.createConditions();
                                } else {
                                    this.subscribeToSaveResponse(
                                        this.socialCardService.update(this.socialCard)
                                    );
                                }
                            }
                        }
                    }
                    this.jhiAlertService.success('jestimeApp.socialCard.updated', {'title': this.socialCard.title}, null);
                } else {
                    this.jhiAlertService.error('jestimeApp.socialCard.notUnique', {}, null);
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Address>>) {
        result.subscribe((res: HttpResponse<SocialCard>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private subscribeToSaveResponseSource(result: Observable<HttpResponse<Source>>) {
        result.subscribe((res: HttpResponse<Source>) =>
            this.onSaveSuccessSource(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    /**
     * Lorsqu'une source est persistée, on créé les références nécessaires,
     * puis les conditions d'octroi et les adresses
     * @param {Source} result
     */
    private onSaveSuccessSource(result: Source) {
        this.eventManager.broadcast({ name: 'sourceListModification', content: 'OK'});
        this.socialCard.sources.push(result);
        if (result.references.length > 0) {
            for (let i = 0, len = result.references.length; i < len; i++) {
                if (i === len - 1) {
                    this.referenceSaved = true;
                }
                const currentRef: Reference = result.references[i];
                currentRef.sourceId = result.id;
                if (!currentRef.id) {
                    if (this.newConditions.length > 0) {
                        this.conditionSaved = true;
                    }
                    this.referenceService.create(currentRef).subscribe(
                        (res: HttpResponse<Reference>) => {
                            for (let j = 0, len2 = this.newConditionValues.length; j < len2; j++) {
                                const currentVal = this.newConditionValues[j];
                                if (currentVal.referenceCode === res.body.code) {
                                    currentVal.referenceId = res.body.id;
                                    if (j === len2 - 1) {
                                        if (this.newConditions.length > 0) {
                                            this.createConditions();
                                        } else {
                                            if (this.newAddresses.length > 0) {
                                                this.createAddresses();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    );
                }
            }
        }
        if (!this.conditionSaved) {
            if (this.newConditions.length > 0) {
                this.conditionSaved = true;
            }
            this.createConditions();
        }
        if (this.newConditions.length === 0 && this.newAddresses.length === 0) {
            this.subscribeToSaveResponse(
                this.socialCardService.update(this.socialCard)
            );
        }
    }

    /**
     * Lorsque les modifications ont réussis sur la fiche de pestation,
     * on crée les références si ce n'est pas déjà fait, puis les valeurs
     * des conditions d'octroi
     * @param {SocialCard} result
     */
    private onSaveSuccess(result: SocialCard) {
        for (let i = 0, len = this.selectedSources.length; i < len; i++) {
            const currentSource: Source = this.selectedSources[i];
            if (!this.referenceSaved) {
                for (let j = 0, len2 = currentSource.references.length; j < len2; j++) {
                    let currentRef: Reference = currentSource.references[j];
                    if (!currentRef.id && !this.referenceSaved) {
                        if (this.newConditionValues.length > 0) {
                            this.valueSaved = true;
                        }
                        currentRef.sourceId = currentSource.id;
                        this.referenceService.create(currentRef).subscribe(
                            (res: HttpResponse<Reference>) => {
                                currentRef = res.body;
                                for (let k = 0, len3 = this.newConditionValues.length; k < len3; k++) {
                                    const currentVal: ConditionValue = this.newConditionValues[k];
                                    if (currentVal.referenceCode === res.body.code) {
                                        currentVal.referenceId = res.body.id;
                                    }
                                }
                                if (j === len2 - 1) {
                                    for (let k = 0, len3 = this.newConditionValues.length; k < len3; k++) {
                                        const currentVal: ConditionValue = this.newConditionValues[k];
                                        if (currentVal.referenceId && !currentVal.id) {
                                            this.conditionValueService.create(currentVal).subscribe(
                                                (valRes: HttpResponse<ConditionValue>) => {
                                                    if (k === len3 - 1) {
                                                        this.socialCard = result;
                                                        this.dialogRef.close(this.socialCard);
                                                    }
                                                }
                                            );
                                        }
                                    }
                                    if (this.newConditionValues.length === 0) {
                                        this.socialCard = result;
                                        this.dialogRef.close(this.socialCard);
                                    }
                                }
                            }
                        );
                    }
                }
            }
            if (i === len - 1) {
                this.referenceSaved = true;
            }
        }
        if (!this.valueSaved) {
            for (let i = 0, len = this.newConditionValues.length; i < len; i++) {
                let currentVal: ConditionValue = this.newConditionValues[i];
                if (currentVal.referenceId && !currentVal.id) {
                    this.conditionValueService.create(currentVal).subscribe(
                        (valRes: HttpResponse<ConditionValue>) => {
                            currentVal = valRes.body;
                            if (i === len - 1) {
                                this.socialCard = result;
                                this.dialogRef.close(this.socialCard);
                            }
                        }
                    );
                    this.valueSaved = true;
                }
            }
            if (this.newConditionValues.length === 0) {
                this.socialCard = result;
                this.dialogRef.close(this.socialCard);
            }
        }
    }

    /**
     * Permet de persister les adresses, puis les références si ce n'est
     * pas déjà fait, puis finalement les conditions d'octroi
     */
    private createAddresses() {
        for (let i = 0, len = this.newAddresses.length; i < len; i++) {
            this.addressesSaved = true;
            this.addressService.create(this.newAddresses[i]).subscribe(
                (res: HttpResponse<Address>) => {
                    this.socialCard.addresses.push(res.body);
                    for (let j = 0, len2 = this.socialCard.sources.length; j < len2; j++) {
                        const currentSource: Source = this.socialCard.sources[j];
                        for (let k = 0, len3 = currentSource.references.length; k < len3; k++) {
                            const currentReference: Reference = currentSource.references[k];
                            if (!currentReference.id && !this.referenceSaved) {
                                this.referenceService.create(currentReference).subscribe(
                                    (refRes: HttpResponse<Reference>) => {
                                        for (let l = 0, len4 = this.newConditionValues.length; l < len4; l++) {
                                            const currentVal = this.newConditionValues[l];
                                            if (currentVal.referenceCode === refRes.body.code) {
                                                currentVal.referenceId = refRes.body.id;
                                            }
                                        }
                                    }
                                );
                            }
                        }
                    }
                    if (i === len - 1) {
                        this.referenceSaved = true;
                        if (this.newConditions.length > 0 && !this.conditionSaved) {
                            this.createConditions();
                        } else {
                            this.subscribeToSaveResponse(
                                this.socialCardService.update(this.socialCard)
                            );
                        }
                    }
                }
            );
        }
    }

    /**
     * Permet de persister les conditions d'octroi, puis les références
     * si nécessaire
     */
    private createConditions() {
        let needToCreateReference = false;
        for (let i = 0, len = this.socialCard.sources.length; i < len; i++) {
            const currentSource: Source = this.socialCard.sources[i];
            for (let j = 0, len2 = currentSource.references.length; j < len2; j++) {
                const currentRef: Reference = currentSource.references[j];
                if (!currentRef.id) {
                    needToCreateReference = true;
                }
            }
        }
        let valueCreated = false;
        for (let i = 0, len = this.newConditions.length; i < len; i++) {
            if (this.newConditionValues.length > 0) {
                valueCreated = true;
            }
            this.conditionService.create(this.newConditions[i]).subscribe(
                (condRes: HttpResponse<Condition>) => {
                    for (let j = 0, len2 = this.newConditionValues.length; j < len2; j++) {
                        const currentVal: ConditionValue = this.newConditionValues[j];
                        if (currentVal.conditionCode === condRes.body.code) {
                            currentVal.conditionId = condRes.body.id;
                        }
                    }
                    if (!this.referenceSaved && needToCreateReference) {
                        for (let j = 0, len2 = this.socialCard.sources.length; j < len2; j++) {
                            const currentSource: Source = this.socialCard.sources[j];
                            for (let k = 0, len3 = currentSource.references.length; k < len3; k++) {
                                const currentReference: Reference = currentSource.references[k];
                                console.log(this.referenceSaved);
                                if (!currentReference.id && !this.referenceSaved) {
                                    this.referenceService.create(currentReference).subscribe(
                                        (res: HttpResponse<Reference>) => {
                                            for (let l = 0, len4 = this.newConditionValues.length; l < len4; l++) {
                                                const currentVal = this.newConditionValues[l];
                                                if (currentVal.referenceCode === res.body.code) {
                                                    currentVal.referenceId = res.body.id;
                                                }
                                            }
                                            if (i === len - 1) {
                                                this.referenceSaved = true;
                                                if (this.newAddresses.length > 0 && !this.addressesSaved) {
                                                    this.conditionSaved = true;
                                                    this.createAddresses();
                                                } else {
                                                    this.subscribeToSaveResponse(
                                                        this.socialCardService.update(this.socialCard)
                                                    );
                                                }
                                            }
                                        }
                                    );
                                }
                            }
                        }
                    } else {
                        if (i === len - 1) {
                            if (this.newAddresses.length > 0 && !this.addressesSaved) {
                                console.log('OUI');
                                this.conditionSaved = true;
                                this.createAddresses();
                            } else {
                                this.subscribeToSaveResponse(
                                    this.socialCardService.update(this.socialCard)
                                );
                            }
                        }
                    }
                }
            );
        }
    }

    private onSaveError() {
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    /**
     * Permet de créer un objet source juridique
     */
    createSource() {
        this.juridictionService.find(this.source.juridictionId)
            .subscribe((juridictionResponse: HttpResponse<Juridiction>) => {
                const juridiction = juridictionResponse.body;
                this.source.juridictionName = juridiction.name;
                this.source.code = this.sourceService.generateCode(this.source);
                this.source.creationDate = this.datePipe.transform(Date.now(), 'yyyy-MM-dd');
                this.source.creatorId = this.userId;
                this.source.updatorId = this.userId;
                this.source.references = [];
                this.checkIsUniqueSource();
            });
    }

    /**
     * Permet de supprimer un objet source venant d'être créé
     * @param {Source} source
     */
    removeNewSource(source: Source) {
        const index = this.newSources.indexOf(source);
        this.newSources.splice(index, 1);
    }

    /**
     * Permet de délier une source juridique
     * @param {Source} source
     */
    removeSelectedSource(source: Source) {
        const index = this.selectedSources.indexOf(source);
        this.selectedSources.splice(index, 1);
    }

    trackJuridictionById(index: number, item: Juridiction) {
        return item.id;
    }

    trackCategoryById(index: number, item: Category) {
        return item.id;
    }

    /**
     * Ouvre la fenêtre modale de recherche d'une fiche parente
     */
    openParentDialog(): void {
        const dialogRef = this.parentDialog.open(ParentDialogComponent, {
            width: '800px'
        });

        dialogRef.afterClosed().subscribe((result) => {
            this.socialCardService.find(result)
                .subscribe((socialCardResponse: HttpResponse<SocialCard>) => {
                    this.parentSocialCard = socialCardResponse.body;
                    this.socialCard.parentSocialCardId = this.parentSocialCard.id;
                    this.socialCard.parentSocialCardTitle = this.parentSocialCard.title;
                });
        });
    }

    /**
     * Ouvre la fenêtre modale de recherche d'une adresse
     */
    openAddressDialog(): void {
        const dialogRef = this.addressDialog.open(AddressDialogComponent, {
            width: '800px',
            data: { selectedAddresses: this.selectedAddresses }
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result != null) {
                this.socialCard.addresses.push(result);
            }
        });
    }

    /**
     * Ouvre la fenêtre modale de recherche d'une source juridique
     */
    openSourceDialog(): void {
        const dialogRef = this.addressDialog.open(CreationWizardSourceDialogComponent, {
            width: '800px',
            data: { selectedSources: this.selectedSources }
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result != null) {
                this.socialCard.sources.push(result);
            }
        });
    }

    /**
     * Ouvre la fenêtre modale de confirmation de suppression d'une condition d'octroi
     * @param {Condition} condition
     */
    openDeleteConditionDialog(condition: Condition): void {
        const dialogRef = this.dialog.open(SocialCardDetailDeleteConditionDialogComponent, {
            width: '80%',
            data: { selectedCondition: condition, selectedSocialCard: this.socialCard }
        });
    }

    /**
     * Ouvre la fenêtre modale permettant d'ajouter un couple variable/valeur à une condition
     * @param {Condition} condition
     * @param {number} index
     */
    manageCondition(condition: Condition, index: number): void {
        const dialogRef = this.dialog.open(CreationWizardConditionDialogComponent, {
            width: '800px',
            data: {selectedSources: this.selectedSources, newSources: this.newSources, selectedCondition: condition, selectedIndex: index}
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result != null) {
                this.newConditionValues.push(result.conditionValue);
                result.condition.conditionValues.push(result.conditionValue);
            }
        });
    }

    /**
     * Permet de supprimer une valeur de condition d'octroi
     * @param {Condition} condition
     * @param {ConditionValue} value
     */
    removeValue(condition: Condition, value: ConditionValue) {
        const index = condition.conditionValues.indexOf(value);
        condition.conditionValues.splice(index, 1);
    }

    /**
     * Permet de délier la fiche parente
     */
    deleteParent() {
        this.socialCard.parentSocialCardId = null;
        this.socialCard.parentSocialCardTitle = null;
    }

    /**
     * Vérifie l'unicité d'une source juridique
     */
    checkIsUniqueSource() {
        let existingSources = [];
        this.sourceService.findByCriteria('?title.equals=' + this.source.title + '&juridictionId.equals=' + this.source.juridictionId).subscribe(
            (res: HttpResponse<Source[]>) => {
                existingSources = res.body;
                if (existingSources.length === 0) {
                    if (this.source.title !== '' && this.source.juridictionId !== null) {
                        this.newSources.push(this.source);
                        this.source = new Source();
                    }
                } else {
                    this.jhiAlertService.error('jestimeApp.source.notUnique', {}, null);
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    /**
     * Vérifie l'unicité d'une adresse
     */
    checkIsUniqueAddress() {
        let existingAddresses = [];
        this.addressService.findByCriteria('?name.equals=' + this.address.name).subscribe(
            (res: HttpResponse<Address[]>) => {
                existingAddresses = res.body;
                if (existingAddresses.length === 0) {
                    if (this.address.name !== '' && this.address.location !== '' && this.address.phone !== '' && this.address.email !== '') {
                        this.newAddresses.push(this.address);
                        this.address = new Address();
                        this.generalFormSecondStep.reset();
                    }
                } else {
                    this.jhiAlertService.error('jestimeApp.address.notUnique', {}, null);
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    /**
     * Permet de créer un objet adresse
     */
    createAddress() {
        this.address.code = this.addressService.generateCode(this.address);
        this.address.creationDate = this.datePipe.transform(Date.now(), 'yyyy-MM-dd');
        this.address.creatorId = this.userId;
        this.address.updatorId = this.userId;
        this.checkIsUniqueAddress();
    }

    /**
     * Permet de supprimer une nouvelle adresse
     * @param {Address} address
     */
    removeNewAddress(address: Address) {
        const index = this.newAddresses.indexOf(address);
        this.newAddresses.splice(index, 1);
    }

    /**
     * Permet de délier une adresse
     * @param {Address} address
     */
    removeSelectedAddress(address: Address) {
        const index = this.selectedAddresses.indexOf(address);
        this.selectedAddresses.splice(index, 1);
    }

    /**
     * Permet de créer une condition d'octroi
     */
    createCondition() {
        this.condition.code = this.conditionService.generateCode(this.condition);
        this.condition.creationDate = this.datePipe.transform(Date.now(), 'yyyy-MM-dd');
        this.condition.creatorId = this.userId;
        this.condition.updatorId = this.userId;
        this.condition.socialCardId = this.socialCard.id;
        this.newConditions.push(this.condition);
        this.condition = new Condition();
    }

    /**
     * Permet de supprimer une nouvelle condition d'octroi
     * @param {Condition} condition
     */
    removeNewCondition(condition: Condition) {
        this.newConditions.splice(this.newConditions.indexOf(condition), 1);
    }
}
