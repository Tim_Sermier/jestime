import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';
import {Address, AddressService} from '../entities/address';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe, Location} from '@angular/common';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {Category, CategoryService} from '../entities/category';
import {SocialCard, SocialCardService} from '../entities/social-card';
import {AddressDialogComponent, ParentDialogComponent} from '../creation-wizard/creation-wizard.component';
import {Observable} from 'rxjs/Observable';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Juridiction, JuridictionService} from '../entities/juridiction';

@Component({
    selector: 'jhi-social-card-detail-confirm-dialog-close',
    templateUrl: './social-card-detail-confirm-dialog-close.component.html',
    styles: []
})
export class SocialCardDetailConfirmDialogCloseComponent implements OnInit {
    socialCard: SocialCard;

    constructor(
        public dialogRef: MatDialogRef<SocialCardDetailConfirmDialogCloseComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private jhiAlertService: JhiAlertService
    ) { }

    ngOnInit() {
    }

    /**
     * Sur fermeture de la fenêtre d'édition par un clic sur le bouton "Retour"
     * on demande à l'utilisateur s'il veut enregistrer ses modifications
     */
    save() {
        this.dialogRef.close(true);
    }

    /**
     * Refus de l'enregistrement des modifications
     */
    cancel() {
        this.dialogRef.close(false);
    }
}
