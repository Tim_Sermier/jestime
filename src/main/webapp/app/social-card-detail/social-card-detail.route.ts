import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../shared';
import {SocialCardDetailComponent} from './social-card-detail.component';

export const socialCardDetailRoute: Routes = [
    {
        path: 'social-card-detail/:id',
        component: SocialCardDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.socialCard.titles.detail'
        },
        canActivate: [UserRouteAccessService]
    }
];
