import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';
import {Address, AddressService} from '../entities/address';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe, Location} from '@angular/common';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {Category, CategoryService} from '../entities/category';
import {SocialCard, SocialCardService} from '../entities/social-card';
import {AddressDialogComponent, ParentDialogComponent} from '../creation-wizard/creation-wizard.component';
import {Observable} from 'rxjs/Observable';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Juridiction, JuridictionService} from '../entities/juridiction';

@Component({
    selector: 'jhi-social-card-detail-delete',
    templateUrl: './social-card-detail-delete.component.html',
    styles: []
})
export class SocialCardDetailDeleteComponent implements OnInit {
    socialCard: SocialCard;

    constructor(
        public dialogRef: MatDialogRef<SocialCardDetailDeleteComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private socialCardService: SocialCardService,
        private jhiAlertService: JhiAlertService
    ) { }

    /**
     * À l'initialisation, on récupère la fiche de prestation sociale
     */
    ngOnInit() {
        this.socialCardService.find(this.data.socialCard.id)
            .subscribe((socialCardResponse: HttpResponse<SocialCard>) => {
                this.socialCard = socialCardResponse.body;
            });
    }
    onNoClick(): void {
        this.dialogRef.close(false);
    }

    /**
     * Sur confirmation, on supprime la fiche de prestation sociale
     */
    remove() {
        this.socialCardService.delete(this.socialCard.id).subscribe((response) => {
            this.jhiAlertService.success('jestimeApp.socialCard.deleted', {'title': this.socialCard.title}, null);
            this.dialogRef.close(true);
        });
    }
}
