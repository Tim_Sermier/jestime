import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';
import {Address, AddressService} from '../entities/address';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe, Location} from '@angular/common';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {Category, CategoryService} from '../entities/category';
import {SocialCard, SocialCardService} from '../entities/social-card';
import {AddressDialogComponent, ParentDialogComponent} from '../creation-wizard/creation-wizard.component';
import {Observable} from 'rxjs/Observable';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Juridiction, JuridictionService} from '../entities/juridiction';
import {Condition, ConditionService} from '../entities/condition';
import {ConditionValueService} from '../entities/condition-value';

@Component({
    selector: 'jhi-social-card-detail-delete-condition-dialog',
    templateUrl: './social-card-detail-delete-condition-dialog.component.html',
    styles: []
})
export class SocialCardDetailDeleteConditionDialogComponent implements OnInit {
    condition: Condition;
    socialCard: SocialCard;

    constructor(
        public dialogRef: MatDialogRef<SocialCardDetailDeleteConditionDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private conditionService: ConditionService,
        private conditionValueService: ConditionValueService,
        private jhiAlertService: JhiAlertService
    ) { }

    /**
     * À l'initialisation, on récupère la condition et la fiche de prestation sociale
     */
    ngOnInit() {
        this.condition = this.data.selectedCondition;
        this.socialCard = this.data.selectedSocialCard;
    }

    onNoClick(): void {
        this.dialogRef.close(false);
    }

    /**
     * Sur confirmation de suppression, on supprime l'ensemble des valeurs de la condition ainsi que la condition elle-même
     */
    remove() {
        if (this.condition.conditionValues.length > 0) {
            for (let i = 0, len = this.condition.conditionValues.length; i < len; i++) {
                this.conditionValueService.delete(this.condition.conditionValues[i].id).subscribe((response) => {
                    if (i === len - 1) {
                        this.conditionService.delete(this.condition.id).subscribe((response2) => {
                            this.socialCard.conditions.splice(this.socialCard.conditions.indexOf(this.condition), 1);
                            this.jhiAlertService.success('jestimeApp.condition.deleted', null);
                            this.dialogRef.close(true);
                        });
                    }
                });
            }
        } else {
            this.conditionService.delete(this.condition.id).subscribe((response2) => {
                this.socialCard.conditions.splice(this.socialCard.conditions.indexOf(this.condition), 1);
                this.jhiAlertService.success('jestimeApp.condition.deleted', null);
                this.dialogRef.close(true);
            });
        }
    }
}
