import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JestimeSharedModule } from '../shared';
import {SocialCardService} from '../entities/social-card';
import {SocialCardDetailComponent} from './social-card-detail.component';
import {socialCardDetailRoute} from './social-card-detail.route';
import {MatProgressSpinnerModule, MatTabsModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SocialCardDetailDialogComponent} from './social-card-detail-dialog.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {SocialCardDetailDeleteComponent} from './social-card-detail-delete.component';
import {SocialCardDetailDeleteConditionDialogComponent} from './social-card-detail-delete-condition-dialog.component';
import {NgCircleProgressModule} from 'ng-circle-progress';
import {SocialCardDetailConfirmDialogCloseComponent} from './social-card-detail-confirm-dialog-close.component';

const ENTITY_STATES = [
    ...socialCardDetailRoute,
];

@NgModule({
    imports: [
        JestimeSharedModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(ENTITY_STATES),
        MatTabsModule,
        BrowserAnimationsModule,
        MatProgressSpinnerModule,
        NgCircleProgressModule.forRoot({
            // set defaults here
            radius: 50,
            outerStrokeWidth: 16,
            innerStrokeWidth: 8,
            backgroundStroke: '#C7E596',
            outerStrokeColor: '#78C000',
            innerStrokeColor: '#C7E596',
            animationDuration: 300,
        })
    ],
    declarations: [
        SocialCardDetailComponent,
        SocialCardDetailDialogComponent,
        SocialCardDetailDeleteComponent,
        SocialCardDetailDeleteConditionDialogComponent,
        SocialCardDetailConfirmDialogCloseComponent
    ],
    entryComponents: [
        SocialCardDetailComponent,
        SocialCardDetailDialogComponent,
        SocialCardDetailDeleteComponent,
        SocialCardDetailDeleteConditionDialogComponent,
        SocialCardDetailConfirmDialogCloseComponent
    ],
    providers: [
        SocialCardService,
        DatePipe
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SocialCardDetailModule {}
