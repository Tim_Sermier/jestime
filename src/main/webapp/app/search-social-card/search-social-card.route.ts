import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../shared';
import { SearchSocialCardComponent } from './search-social-card.component';

export const searchSocialCardRoute: Routes = [
    {
        path: 'search-social-card/:filter',
        component: SearchSocialCardComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.socialCard.titles.search'
        },
        canActivate: [UserRouteAccessService]
    }
];
