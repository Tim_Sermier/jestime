import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JestimeSharedModule } from '../shared';
import {searchSocialCardRoute} from './search-social-card.route';
import {SearchSocialCardComponent} from './search-social-card.component';
import {SocialCardService} from '../entities/social-card';

const ENTITY_STATES = [
    ...searchSocialCardRoute,
];

@NgModule({
    imports: [
        JestimeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SearchSocialCardComponent
    ],
    entryComponents: [
        SearchSocialCardComponent
    ],
    providers: [
        SocialCardService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SearchSocialCardModule {}
