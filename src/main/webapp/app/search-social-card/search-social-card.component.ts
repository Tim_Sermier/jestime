import {Component, OnInit} from '@angular/core';
import {SocialCard, SocialCardService} from '../entities/social-card';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
    selector: 'jhi-search-social-card',
    templateUrl: './search-social-card.component.html',
    styles: []
})
export class SearchSocialCardComponent implements OnInit {
    filter: string;
    private sub: any;
    socialCards: SocialCard[];
    constructor(
        private socialCardService: SocialCardService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        private router: Router,
        private _location: Location
    ) {
        this.sub = this.route.params.subscribe((params) => {
            this.filter = params['filter'];
        });
    }

    /**
     * On recherche les fiches de prestations selon le filtre envoyé en paramètre
     */
    ngOnInit() {
        this.socialCardService.findByCriterias(this.filter).subscribe(
            (res: HttpResponse<SocialCard[]>) => {
                this.socialCards = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    /**
     * Permet de se rendre sur le détail de la fiche de prestation sélectionnée
     * @param {SocialCard} socialCard
     */
    gotoSocialCard(socialCard: SocialCard) {
        this.router.navigate(['/social-card-detail', socialCard.id]);
    }

    goBack() {
        this._location.back();
    }
}
