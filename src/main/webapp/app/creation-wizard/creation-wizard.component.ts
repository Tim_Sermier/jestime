import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators, ReactiveFormsModule} from '@angular/forms';
import {SocialCard, SocialCardService} from '../entities/social-card';
import {Observable} from 'rxjs/Observable';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Juridiction} from '../entities/juridiction/juridiction.model';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JuridictionService} from '../entities/juridiction';
import {Category, CategoryService} from '../entities/category';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatStepper} from '@angular/material';
import {Address, AddressService} from '../entities/address';
import {getAngularEmitterTransformFactory} from '@angular/compiler-cli/src/transformers/node_emitter_transform';
import {forEach} from '@angular/router/src/utils/collection';
import {DatePipe} from '@angular/common';
import {Account, Principal, UserService} from '../shared';
import {Router} from '@angular/router';
import {Source, SourceService} from '../entities/source';
import {CreationWizardSourceDialogComponent} from './creation-wizard-source-dialog.component';
import {Condition, ConditionService} from '../entities/condition';
import {CreationWizardConditionDialogComponent} from './creation-wizard-condition-dialog.component';
import {ConditionValue, ConditionValueService} from '../entities/condition-value';
import {Reference, ReferenceService} from '../entities/reference';
import {ConditionHelpComponent} from './condition-help.component';

@Component({
    selector: 'jhi-creation-wizard',
    templateUrl: './creation-wizard.component.html',
    styles: []
})
export class CreationWizardComponent implements OnInit {
    isLinear = true;
    socialCard: SocialCard;
    isSaving: boolean;
    juridictions: Juridiction[];
    categories: Category[];
    generalFormFirstStep: FormGroup;
    generalFormSecondStep: FormGroup;
    parentSocialCard: SocialCard;
    address: Address;
    newAddresses: Address[];
    selectedAddresses: Address[];
    currentAccount: Account;
    userId: number;
    source: Source;
    newSources: Source[];
    selectedSources: Source[];
    condition: Condition;
    newConditions: Condition[];
    newConditionValues: ConditionValue[];

    constructor(
        private _formBuilder: FormBuilder,
        private socialCardService: SocialCardService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private juridictionService: JuridictionService,
        private categoryService: CategoryService,
        private parentDialog: MatDialog,
        private addressDialog: MatDialog,
        private conditionDialog: MatDialog,
        private addressService: AddressService,
        private datePipe: DatePipe,
        private principal: Principal,
        private userService: UserService,
        private router: Router,
        private sourceService: SourceService,
        private conditionService: ConditionService,
        private referenceService: ReferenceService,
        private conditionValueService: ConditionValueService
    ) {
    }

    /**
     * Permet d'ouvrir la fenêtre modale d'aide des conditions d'octroi
     */
    openConditionHelp(): void {
        const dialogRef = this.conditionDialog.open(ConditionHelpComponent, {
            width: '800px'
        });
    }

    /**
     * Ouvre la fenêtre de sélection d'une fiche parente et affecte les champs d'informations
     * selon cette dernière
     */
    openParentDialog(): void {
        const dialogRef = this.parentDialog.open(ParentDialogComponent, {
            width: '800px'
        });

        dialogRef.afterClosed().subscribe((result) => {
            this.socialCardService.find(result)
                .subscribe((socialCardResponse: HttpResponse<SocialCard>) => {
                    this.parentSocialCard = socialCardResponse.body;
                    this.generalFormFirstStep.get('general').setValue(this.parentSocialCard.general);
                    this.generalFormFirstStep.get('procedure').setValue(this.parentSocialCard.procedure);
                    this.generalFormFirstStep.get('description').setValue(this.parentSocialCard.description);
                    this.generalFormFirstStep.get('recourse').setValue(this.parentSocialCard.recourse);
                });
        });
    }

    /**
     * Ouvre la fenêtre de sélection d'une adresse existante
     */
    openAddressDialog(): void {
        const dialogRef = this.addressDialog.open(AddressDialogComponent, {
            width: '800px',
            data: { selectedAddresses: this.selectedAddresses }
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result != null) {
                this.selectedAddresses.push(result);
            }
        });
    }

    /**
     * Ouvre la fenêtre de sélection d'une source juridique existante
     */
    openSourceDialog(): void {
        const dialogRef = this.addressDialog.open(CreationWizardSourceDialogComponent, {
            width: '800px',
            data: { selectedSources: this.selectedSources }
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result != null) {
                this.selectedSources.push(result);
            }
        });
    }

    /**
     * Ouvre la fenêtre de création d'un couple variable / valeur pour une condition d'octroi créée
     * @param {Condition} condition
     * @param {number} index
     */
    manageCondition(condition: Condition, index: number): void {
        const dialogRef = this.conditionDialog.open(CreationWizardConditionDialogComponent, {
            width: '800px',
            data: { selectedSources: this.selectedSources, newSources: this.newSources, selectedCondition: condition, selectedIndex: index}
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result != null) {
                this.newConditionValues.push(result.conditionValue);
                result.condition.conditionValues.push(result.conditionValue);
            }
        });
    }

    /**
     * On initialise l'ensemble des objets
     * Puis on récupère les juridictions et catégories existantes
     * Finalement on récupère l'utilisateur en train de créer la fiche
     */
    ngOnInit() {
        this.isSaving = false;
        this.socialCard = new SocialCard();
        this.socialCard.conditions = [];
        this.address = new Address();
        this.source = new Source();
        this.condition = new Condition();
        this.socialCard.addresses = [];
        this.socialCard.sources = [];
        this.socialCard.conditions = [];
        this.newAddresses = [];
        this.newConditionValues = [];
        this.selectedAddresses = [];
        this.selectedSources = [];
        this.newSources = [];
        this.newConditions = [];
        this.juridictionService.query()
            .subscribe((res: HttpResponse<Juridiction[]>) => { this.juridictions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.categoryService.query()
            .subscribe((res: HttpResponse<Category[]>) => { this.categories = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.generalFormFirstStep = this._formBuilder.group({
            title: ['', Validators.required],
            id: [''],
            juridiction: ['', Validators.required],
            category: ['', Validators.required],
            general: [''],
            description: [''],
            procedure: [''],
            recourse: [''],
        });
        this.generalFormSecondStep = this._formBuilder.group({
            address_id: [''],
            address_name: ['', Validators.required],
            address_location: ['', Validators.required],
            address_phone: ['', Validators.required],
            address_fax: [''],
            address_email: ['', Validators.required],
            address_website: ['']
        });
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            this.userService.find(this.currentAccount.login).subscribe((response) => {
                this.userId = response.body.id;
            });
        });
    }

    /**
     * Permet de supprimer un couple variable / valeur d'une condition d'octroi
     * @param {Condition} condition
     * @param {ConditionValue} value
     */
    removeValue(condition: Condition, value: ConditionValue) {
        const index = condition.conditionValues.indexOf(value);
        condition.conditionValues.splice(index, 1);
    }

    /**
     * Permet de supprimer une condition d'octroi
     * @param {Condition} condition
     */
    removeCondition(condition: Condition) {
        const index = this.newConditions.indexOf(condition);
        this.newConditions.splice(index, 1);
    }

    clear() {
    }

    save() {
        this.isSaving = true;
        this.checkIsUnique();
    }

    /**
     * Permet de créer une nouvelle adresse et de l'ajouter à la liste des adresses créées
     */
    createAddress() {
        this.address.name = this.generalFormSecondStep.get('address_name').value;
        this.address.location = this.generalFormSecondStep.get('address_location').value;
        this.address.phone = this.generalFormSecondStep.get('address_phone').value;
        this.address.fax = this.generalFormSecondStep.get('address_fax').value;
        this.address.email = this.generalFormSecondStep.get('address_email').value;
        this.address.website = this.generalFormSecondStep.get('address_website').value;
        this.address.code = this.addressService.generateCode(this.address);
        this.address.creationDate = this.datePipe.transform(Date.now(), 'yyyy-MM-dd');
        this.address.creatorId = this.userId;
        this.address.updatorId = this.userId;
        this.checkIsUniqueAddress();
    }

    /**
     * Vérifie que l'adresse créée est unique
     */
    checkIsUniqueAddress() {
        let existingAddresses = [];
        this.addressService.findByCriteria('?name.equals=' + this.address.name).subscribe(
            (res: HttpResponse<Address[]>) => {
                existingAddresses = res.body;
                if (existingAddresses.length === 0) {
                    if (this.address.name !== '' && this.address.location !== '' && this.address.phone !== '' && this.address.email !== '') {
                        this.newAddresses.push(this.address);
                        this.address = new Address();
                        this.generalFormSecondStep.reset();
                    }
                } else {
                    this.jhiAlertService.error('jestimeApp.address.notUnique', {}, null);
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    /**
     * Vérifie que la source juridique créée est unique
     */
    checkIsUniqueSource() {
        let existingSources = [];
        this.sourceService.findByCriteria('?title.equals=' + this.source.title + '&juridictionId.equals=' + this.source.juridictionId).subscribe(
            (res: HttpResponse<Source[]>) => {
                existingSources = res.body;
                if (existingSources.length === 0) {
                    if (this.source.title !== '' && this.source.juridictionId !== null) {
                        this.newSources.push(this.source);
                        this.source = new Source();
                    }
                } else {
                    this.jhiAlertService.error('jestimeApp.source.notUnique', {}, null);
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    /**
     * Permet de supprimer une adresse créée au préalable
     * @param {Address} address
     */
    removeNewAddress(address: Address) {
        const index = this.newAddresses.indexOf(address);
        this.newAddresses.splice(index, 1);
    }

    /**
     * Permet de supprimer une source juridique créée au préalable
     * @param {Source} source
     */
    removeNewSource(source: Source) {
        const index = this.newSources.indexOf(source);
        this.newSources.splice(index, 1);
    }

    /**
     * Permet de créer une nouvelle source juridique en vérifiant qu'elle est unique
     */
    createSource() {
        this.juridictionService.find(this.source.juridictionId)
            .subscribe((juridictionResponse: HttpResponse<Juridiction>) => {
                const juridiction = juridictionResponse.body;
                this.source.juridictionName = juridiction.name;
                this.source.code = this.sourceService.generateCode(this.source);
                this.source.creationDate = this.datePipe.transform(Date.now(), 'yyyy-MM-dd');
                this.source.creatorId = this.userId;
                this.source.updatorId = this.userId;
                this.source.references = [];
                this.checkIsUniqueSource();
            });
    }

    /**
     * Permet de créer une nouvelle condition d'octroi
     */
    createCondition() {
        this.condition.code = this.conditionService.generateCode(this.condition);
        this.condition.creationDate = this.datePipe.transform(Date.now(), 'yyyy-MM-dd');
        this.condition.creatorId = this.userId;
        this.condition.updatorId = this.userId;
        this.newConditions.push(this.condition);
        this.socialCard.conditions.push(this.condition);
        this.condition = new Condition();
    }

    /**
     * Supprime une adresse liée préalablement de la liste des adresses de la fiche de prestation
     * @param {Address} address
     */
    removeSelectedAddress(address: Address) {
        const index = this.selectedAddresses.indexOf(address);
        this.selectedAddresses.splice(index, 1);
    }

    /**
     * Supprime une source juridique liée préalablement de la liste des sources juridiques de la fiche de prestation
     * @param {Source} source
     */
    removeSelectedSource(source: Source) {
        const index = this.selectedSources.indexOf(source);
        this.selectedSources.splice(index, 1);
    }

    /**
     * Déselection de la fiche parente
     */
    deleteParent() {
        this.parentSocialCard = null;
        this.generalFormFirstStep.get('general').setValue('');
        this.generalFormFirstStep.get('description').setValue('');
        this.generalFormFirstStep.get('procedure').setValue('');
        this.generalFormFirstStep.get('recourse').setValue('');
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<SocialCard>>) {
        result.subscribe((res: HttpResponse<SocialCard>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private subscribeToSaveResponseAddress(result: Observable<HttpResponse<Address>>) {
        result.subscribe((res: HttpResponse<Address>) =>
            this.onSaveSuccessAddress(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private subscribeToSaveResponseSource(result: Observable<HttpResponse<Source>>) {
        result.subscribe((res: HttpResponse<Source>) =>
            this.onSaveSuccessSource(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private subscribeToSaveResponseReference(result: Observable<HttpResponse<Reference>>) {
        result.subscribe((res: HttpResponse<Reference>) =>
            this.onSaveSuccessReference(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private subscribeToSaveResponseCondition(result: Observable<HttpResponse<Condition>>) {
        result.subscribe((res: HttpResponse<Reference>) =>
            this.onSaveSuccessCondition(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private subscribeToSaveResponseConditionValue(result: Observable<HttpResponse<ConditionValue>>) {
        result.subscribe((res: HttpResponse<ConditionValue>) =>
            this.onSaveSuccessConditionValue(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    /**
     * Lorsque la fiche de prestation a bien été persistée en base de données,
     * on créé les nouvelles conditions d'octroi en les liant à cette dernière
     * @param {SocialCard} result
     */
    private onSaveSuccess(result: SocialCard) {
        this.socialCard = result;
        for (let i = 0, len = this.newConditions.length; i < len; i++) {
            this.newConditions[i].socialCardId = result.id;
            console.log(this.newConditions[i]);
            this.subscribeToSaveResponseCondition(
                this.conditionService.create(this.newConditions[i])
            );
        }
        if (this.newConditions.length === 0) {
            this.jhiAlertService.success('jestimeApp.socialCard.created', {'title': this.socialCard.title}, null);
            this.router.navigate(['/social-card-detail', result.id]);
        }
    }

    private onSaveSuccessConditionValue(result: ConditionValue) {
        this.jhiAlertService.success('jestimeApp.socialCard.created', {'title': this.socialCard.title}, null);
        this.router.navigate(['/social-card-detail', this.socialCard.id]);
    }

    /**
     * Lorsque la condition d'octroi a bien été persistée en base de données,
     * on persiste les couples variables / valeurs correspondants
     * @param {Condition} result
     */
    private onSaveSuccessCondition(result: Condition) {
        for (let i = 0, len = this.newConditionValues.length; i < len; i++) {
            const currentConditionValue: ConditionValue = this.newConditionValues[i];
            if (currentConditionValue.conditionCode === result.code) {
                currentConditionValue.conditionId = result.id;
                this.subscribeToSaveResponseConditionValue(
                    this.conditionValueService.create(this.newConditionValues[i])
                );
            }
        }
        if (this.newConditionValues.length === 0) {
            this.jhiAlertService.success('jestimeApp.socialCard.created', {'title': this.socialCard.title}, null);
            this.router.navigate(['/social-card-detail', this.socialCard.id]);
        }
    }

    /**
     * Lorsqu'une nouvelle référence est persistée,
     * on l'affecte aux couples variables / valeurs
     * @param {Reference} result
     */
    private onSaveSuccessReference(result: Reference) {
        for (let i = 0, len = this.newConditions.length; i < len; i++) {
            for (let j = 0, len2 = this.newConditions[i].conditionValues.length; j < len2; j++) {
                const currentConditionValue: ConditionValue = this.newConditions[i].conditionValues[j];
                if (result.code === currentConditionValue.referenceCode) {
                    currentConditionValue.referenceId = result.id;
                }
            }
        }
    }

    /**
     * Lorsqu'une source juridique a été persistée,
     * on l'affecte aux référence correspondantes
     * @param {Source} result
     */
    private onSaveSuccessSource(result: Source) {
        for (let i = 0, len = result.references.length; i < len; i++) {
            const currentRef: Reference = result.references[i];
            if (currentRef.sourceId === null) {
                currentRef.sourceId = result.id;
                this.subscribeToSaveResponseReference(
                    this.referenceService.create(result.references[i])
                );
            }
        }
        this.eventManager.broadcast({ name: 'sourceListModification', content: 'OK'});
        this.socialCard.sources.push(result);
        if (result.code === this.newSources[this.newSources.length - 1].code) {
            console.log(this.socialCard);
            this.subscribeToSaveResponse(
                this.socialCardService.create(this.socialCard));
        }
    }

    /**
     * Lorsque la dernière adresses a été persistées,
     * on passe à la création des sources juridiques s'il y en a,
     * autrement on passe à la persistance de la fiche de prestation
     * @param {Address} result
     */
    private onSaveSuccessAddress(result: Address) {
        this.eventManager.broadcast({ name: 'addressListModification', content: 'OK'});
        this.socialCard.addresses.push(result);
        if (result.code === this.newAddresses[this.newAddresses.length - 1].code) {
            if (this.newSources.length === 0) {
                this.subscribeToSaveResponse(
                    this.socialCardService.create(this.socialCard));
            } else {
                for (let i = 0, len = this.newSources.length; i < len; i++) {
                    this.subscribeToSaveResponseSource(
                        this.sourceService.create(this.newSources[i]));
                }
            }
        }
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackJuridictionById(index: number, item: Juridiction) {
        return item.id;
    }

    trackAddressById(index: number, item: Address) {
        return item.id;
    }

    trackCategoryById(index: number, item: Category) {
        return item.id;
    }

    /**
     * Permet de vérifier que la fiche de prestation est unique,
     * puis de passer à la persistance des objets
     */
    checkIsUnique() {
        let existingSocialCards = [];
        this.socialCardService.findByCriterias('?title.equals=' + this.socialCard.title + '&juridictionId.equals=' + this.socialCard.juridictionId).subscribe(
            (res: HttpResponse<SocialCard[]>) => {
                existingSocialCards = res.body;
                if (existingSocialCards.length === 0) {
                    if (this.socialCard.title !== '' && this.socialCard.juridictionId !== null) {
                        if (this.selectedAddresses.length > 0) {
                            for (let i = 0, len = this.selectedAddresses.length; i < len; i++) {
                                this.socialCard.addresses.push(this.selectedAddresses[i]);
                            }
                        }
                        if (this.selectedSources.length > 0) {
                            for (let i = 0, len = this.selectedSources.length; i < len; i++) {
                                for (let j = 0, len2 = this.selectedSources[i].references.length; j < len2; j++) {
                                    if (!this.selectedSources[i].references[j].id) {
                                        this.subscribeToSaveResponseReference(
                                            this.referenceService.create(this.selectedSources[i].references[j])
                                        );
                                    }
                                }
                                this.socialCard.sources.push(this.selectedSources[i]);
                            }
                        }
                        if (this.newAddresses.length > 0) {
                            for (let i = 0, len = this.newAddresses.length; i < len; i++) {
                                this.subscribeToSaveResponseAddress(
                                    this.addressService.create(this.newAddresses[i]));
                            }
                        } else {
                            if (this.newSources.length > 0) {
                                for (let i = 0, len = this.newSources.length; i < len; i++) {
                                    this.subscribeToSaveResponseSource(
                                        this.sourceService.create(this.newSources[i]));
                                }
                            } else {
                                this.subscribeToSaveResponse(
                                    this.socialCardService.create(this.socialCard));
                            }
                        }
                    }
                } else {
                    this.jhiAlertService.error('jestimeApp.socialCard.notUnique', {}, null);
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    /**
     * Permet de mettre à jour l'aperçu de la fiche de prestation sociale
     */
    updateSocialCard() {
        this.socialCard.title = this.generalFormFirstStep.get('title').value;
        if (!this.generalFormFirstStep.get('juridiction').value) {
            this.socialCard.juridictionId = null;
        } else {
            this.socialCard.juridictionId = this.generalFormFirstStep.get('juridiction').value.id;
            this.socialCard.juridictionName = this.generalFormFirstStep.get('juridiction').value.name;
        }
        if (this.generalFormFirstStep.get('category').value === '') {
            this.socialCard.categoryId = null;
        } else {
            this.socialCard.categoryId = this.generalFormFirstStep.get('category').value.id;
            this.socialCard.categoryName = this.generalFormFirstStep.get('category').value.name;
        }
        this.socialCard.general = this.generalFormFirstStep.get('general').value;
        this.socialCard.description = this.generalFormFirstStep.get('description').value;
        this.socialCard.procedure = this.generalFormFirstStep.get('procedure').value;
        this.socialCard.recourse = this.generalFormFirstStep.get('recourse').value;
        this.socialCard.code = this.socialCardService.generateCode(this.socialCard);
        this.socialCard.creationDate = this.datePipe.transform(Date.now(), 'yyyy-MM-dd');
        this.socialCard.creatorId = this.userId;
        this.socialCard.updatorId = this.userId;
        if (this.parentSocialCard != null) {
            this.socialCard.parentSocialCardId = this.parentSocialCard.id;
            this.socialCard.parentSocialCardTitle = this.parentSocialCard.title;
        }
    }

}

/**
 * Fenêtre modale de la recherche d'une fiche parente
 */
@Component({
    selector: 'jhi-parent-dialog',
    templateUrl: 'creation-wizard-parent-dialog.html',
})
export class ParentDialogComponent {
    parentTitle: string;
    socialCards: SocialCard[];
    constructor(
        public dialogRef: MatDialogRef<ParentDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private socialCardService: SocialCardService,
        private jhiAlertService: JhiAlertService) {
        this.socialCards = [];
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    /**
     * Effectue la recherche de fiches de prestations sociales selon le titre saisi
     */
    searchParent() {
        this.socialCardService.findByTitle(this.parentTitle)
            .subscribe((res: HttpResponse<Category[]>) => { this.socialCards = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    /**
     * À la sélection d'une fiche de prestation on ferme la fenêtre modale en renvoyant l'ID
     * @param {number} id
     */
    selectParent(id: number) {
        this.dialogRef.close(id);
    }

    trackById(index: number, item: SocialCard) {
        return item.id;
    }
}

/**
 * Fenêtre modale de recherche et liaison d'adresses
 */
@Component({
    selector: 'jhi-address-dialog',
    templateUrl: 'creation-wizard-address-dialog.html',
})
export class AddressDialogComponent implements OnInit {
    addressName: string;
    addressLocation: string;
    addressEmail: string;
    addressPhone: string;
    addresses: Address[];
    selectedAddresses: Address[];
    ids: number[];
    constructor(
        public dialogRef: MatDialogRef<AddressDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private addressService: AddressService,
        private jhiAlertService: JhiAlertService) {
        this.addresses = [];
        this.addressName = '';
        this.addressLocation = '';
        this.addressEmail = '';
        this.addressPhone = '';
    }

    /**
     * À l'initialisation, on récupère la liste des adresses sélectionnées
     */
    ngOnInit() {
        this.selectedAddresses = this.data.selectedAddresses;
        this.ids = [];
        for (let i = 0, len = this.selectedAddresses.length; i < len; i++) {
            this.ids.push(this.selectedAddresses[i].id);
        }
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    /**
     * Permet d'effectuer la recherche d'adresses selon les critères entrés
     */
    searchAddress() {
        let filter = '?';
        let filterNumber = 0;
        if (this.addressName !== '') {
            filter += 'name.contains=' + this.addressName;
            filterNumber++;
        }

        if (this.addressLocation !== '') {
            if (filterNumber > 0) {
                filter += '&';
            }
            filter += 'location.contains=' + this.addressLocation;
            filterNumber++;
        }

        if (this.addressEmail !== '') {
            if (filterNumber > 0) {
                filter += '&';
            }
            filter += 'email.contains=' + this.addressEmail;
            filterNumber++;
        }

        if (this.addressPhone !== '') {
            if (filterNumber > 0) {
                filter += '&';
            }
            filter += 'phone.contains=' + this.addressPhone;
            filterNumber++;
        }

        this.addressService.findByCriteria(filter)
            .subscribe((res: HttpResponse<Address[]>) => {
                this.addresses = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    /**
     * À la sélection de l'adresse, on ferme la fenêtre modale en renvoyant cette dernière
     * @param {Address} address
     */
    selectAddress(address: Address) {
        this.dialogRef.close(address);
    }

    trackById(index: number, item: Address) {
        return item.id;
    }
}
