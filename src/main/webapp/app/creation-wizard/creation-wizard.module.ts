import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { JestimeSharedModule } from '../shared';
import {creationWizardRoute} from './creation-wizard.route';
import {AddressDialogComponent, CreationWizardComponent, ParentDialogComponent} from './creation-wizard.component';
import {MatStepperModule, MatTabsModule, MatFormFieldModule, MatDialogModule, MatTooltipModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SocialCardService} from '../entities/social-card';
import {DatePipe} from '@angular/common';
import {CreationWizardSourceDialogComponent} from './creation-wizard-source-dialog.component';
import {CreationWizardConditionDialogComponent} from './creation-wizard-condition-dialog.component';
import {CreationWizardReferenceDialogComponent} from './creation-wizard-reference-dialog.component';
import {CreationWizardVariableDialogComponent} from './creation-wizard-variable-dialog.component';
import {ConditionHelpComponent} from './condition-help.component';

const ENTITY_STATES = [
    ...creationWizardRoute,
];

@NgModule({
    imports: [
        JestimeSharedModule,
        FormsModule,
        ReactiveFormsModule,
        MatStepperModule,
        MatTabsModule,
        MatDialogModule,
        BrowserAnimationsModule,
        RouterModule.forChild(ENTITY_STATES),
        MatTooltipModule
    ],
    declarations: [
        CreationWizardComponent,
        ParentDialogComponent,
        AddressDialogComponent,
        CreationWizardSourceDialogComponent,
        CreationWizardConditionDialogComponent,
        CreationWizardReferenceDialogComponent,
        CreationWizardVariableDialogComponent,
        ConditionHelpComponent
    ],
    entryComponents: [
        CreationWizardComponent,
        ParentDialogComponent,
        AddressDialogComponent,
        CreationWizardSourceDialogComponent,
        CreationWizardConditionDialogComponent,
        CreationWizardReferenceDialogComponent,
        CreationWizardVariableDialogComponent,
        ConditionHelpComponent
    ],
    providers: [
        SocialCardService,
        DatePipe
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreationWizardModule {}
