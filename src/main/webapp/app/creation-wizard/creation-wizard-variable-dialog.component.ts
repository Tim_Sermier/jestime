import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {JhiAlertService} from 'ng-jhipster';
import {Source, SourceService} from '../entities/source';
import {VariableType, VariableTypeService} from '../entities/variable-type';
import {Variable, VariableService} from '../entities/variable';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Component({
    selector: 'jhi-variable-dialog',
    templateUrl: 'creation-wizard-variable-dialog.component.html',
})
export class CreationWizardVariableDialogComponent implements OnInit {
    variable: Variable;
    variableTypes: VariableType[];
    constructor(
        public dialogRef: MatDialogRef<CreationWizardVariableDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private sourceService: SourceService,
        private jhiAlertService: JhiAlertService,
        private variableTypeService: VariableTypeService,
        private variableService: VariableService) {
    }

    /**
     * À l'initialisation, on récupère tous les types de variables
     */
    ngOnInit() {
        this.variableTypeService.query()
            .subscribe((res: HttpResponse<VariableType[]>) => {
                this.variableTypes = res.body;
                this.variable = new Variable();
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    /**
     * On persiste la variable créée
     */
    save() {
        if (this.variable.code && this.variable.name && this.variable.typeId) {
            this.subscribeToSaveResponseVariable(
                this.variableService.create(this.variable));
        }
    }

    private subscribeToSaveResponseVariable(result: Observable<HttpResponse<Variable>>) {
        result.subscribe((res: HttpResponse<Variable>) =>
            this.onSaveSuccessVariable(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccessVariable(result: Variable) {
        this.dialogRef.close(result);
    }

    private onSaveError() {
        this.dialogRef.close(null);
    }
}
