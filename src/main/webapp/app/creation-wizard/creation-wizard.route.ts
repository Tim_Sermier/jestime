import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../shared';
import { CreationWizardComponent } from './creation-wizard.component';

export const creationWizardRoute: Routes = [
    {
        path: 'create_card',
        component: CreationWizardComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'wizard.socialCard.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
