import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {JhiAlertService} from 'ng-jhipster';
import {Source, SourceService} from '../entities/source';
import {Juridiction, JuridictionService} from '../entities/juridiction';

@Component({
    selector: 'jhi-source-dialog',
    templateUrl: 'creation-wizard-source-dialog.component.html',
})
export class CreationWizardSourceDialogComponent implements OnInit {
    sourceTitle: string;
    sourceJuridiction: number;
    sources: Source[];
    selectedSources: Source[];
    ids: number[];
    juridictions: Juridiction[];
    constructor(
        public dialogRef: MatDialogRef<CreationWizardSourceDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private sourceService: SourceService,
        private jhiAlertService: JhiAlertService,
        private juridictionService: JuridictionService) {
        this.sources = [];
        this.sourceTitle = '';
        this.sourceJuridiction = 0;
        this.juridictions = [];
    }

    /**
     * À l'initialisation, on récupère la liste des sources juridiques
     */
    ngOnInit() {
        this.selectedSources = this.data.selectedSources;
        this.ids = [];
        for (let i = 0, len = this.selectedSources.length; i < len; i++) {
            this.ids.push(this.selectedSources[i].id);
        }
        this.juridictionService.query()
            .subscribe((res: HttpResponse<Juridiction[]>) => { this.juridictions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    /**
     * Recherche des sources juridiques selon les critères entrés
     */
    searchSource() {
        let filter = '?';
        let filterNumber = 0;
        if (this.sourceTitle !== '') {
            filter += 'title.contains=' + this.sourceTitle;
            filterNumber++;
        }

        if (this.sourceJuridiction !== 0) {
            if (filterNumber > 0) {
                filter += '&';
            }
            filter += 'juridictionId.equals=' + this.sourceJuridiction;
            filterNumber++;
        }

        this.sourceService.findByCriteria(filter)
            .subscribe((res: HttpResponse<Source[]>) => {
                this.sources = res.body;
                console.log(this.sources);
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    /**
     * Lors de la sélection d'une source, on ferme la fenêtre modale en renvoyant cette dernière
     * @param {Source} source
     */
    selectSource(source: Source) {
        this.dialogRef.close(source);
    }

    trackById(index: number, item: Source) {
        return item.id;
    }

    trackJuridictionById(index: number, item: Juridiction) {
        return item.id;
    }
}
