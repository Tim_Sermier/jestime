import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {JhiAlertService} from 'ng-jhipster';

/**
 * Ce composant permet simplement d'afficher l'aide de la gestion des conditions d'octroi
 */
@Component({
    selector: 'jhi-condition-help',
    templateUrl: 'condition-help.component.html',
})
export class ConditionHelpComponent implements OnInit {
    constructor(
        public dialogRef: MatDialogRef<ConditionHelpComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private jhiAlertService: JhiAlertService) {
    }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }
}
