import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {JhiAlertService} from 'ng-jhipster';
import {Source, SourceService} from '../entities/source';
import {Juridiction, JuridictionService} from '../entities/juridiction';
import {Reference, ReferenceService} from '../entities/reference';

@Component({
    selector: 'jhi-reference-dialog',
    templateUrl: 'creation-wizard-reference-dialog.component.html',
})
export class CreationWizardReferenceDialogComponent implements OnInit {
    source: Source;
    reference: Reference;
    constructor(
        public dialogRef: MatDialogRef<CreationWizardReferenceDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private sourceService: SourceService,
        private jhiAlertService: JhiAlertService,
        private referenceService: ReferenceService) {
    }

    /**
     * À l'initialisation, on récupère la source juridique sélectionnée
     * et on instancie une nouvelle référence
     */
    ngOnInit() {
        this.source = this.data.selectedSource;
        this.reference = new Reference();
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    /**
     * Permet de créer la référence (pas de persistance ici) et de fermer la fenêtre modale
     */
    save() {
        if (this.source.id) {
            this.reference.sourceId = this.source.id;
        }
        this.reference.sourceTitle = this.source.title;
        this.reference.sourceUrl = this.source.url;
        this.reference.code = this.referenceService.generateCode(this.reference);
        this.reference.sourceCode = this.source.code;
        this.dialogRef.close(this.reference);
    }

    trackById(index: number, item: Source) {
        return item.id;
    }

    trackJuridictionById(index: number, item: Juridiction) {
        return item.id;
    }
}
