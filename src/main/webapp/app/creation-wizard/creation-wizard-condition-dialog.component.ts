import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {JhiAlertService} from 'ng-jhipster';
import {Condition} from '../entities/condition';
import {Variable, VariableService} from '../entities/variable';
import {ConditionValue} from '../entities/condition-value';
import {Source, SourceService} from '../entities/source';
import {Reference, ReferenceService} from '../entities/reference';
import {SocialCard} from '../entities/social-card';
import {Juridiction} from '../entities/juridiction/juridiction.model';
import {CreationWizardReferenceDialogComponent} from './creation-wizard-reference-dialog.component';
import {Operator, OperatorService} from '../entities/operator';
import {CreationWizardVariableDialogComponent} from './creation-wizard-variable-dialog.component';

@Component({
    selector: 'jhi-condition-dialog',
    templateUrl: 'creation-wizard-condition-dialog.component.html',
})
export class CreationWizardConditionDialogComponent implements OnInit {
    condition: Condition;
    variables: Variable[];
    sources: Source[];
    conditionValue: ConditionValue;
    selectedSourceId: number;
    selectedSource: Source;
    references: Reference[];
    operators: Operator[];
    selectedOperator: Operator;
    selectedReference: Reference;
    constructor(
        public dialogRef: MatDialogRef<CreationWizardConditionDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private jhiAlertService: JhiAlertService,
        private variableService: VariableService,
        private sourceService: SourceService,
        private referenceService: ReferenceService,
        private referenceDialog: MatDialog,
        private operatorService: OperatorService,
        private variableDialog: MatDialog) {
    }

    /**
     * À l'initialisation, on récupère les sources juridiques sélectionnées et créées,
     * ainsi que l'ensemble des variables et opérateurs existants
     */
    ngOnInit() {
        this.selectedSource = null;
        this.sources = [];
        for (let i = 0, len = this.data.selectedSources.length; i < len; i++) {
            this.sources.push(this.data.selectedSources[i]);
        }
        for (let i = 0, len = this.data.newSources.length; i < len; i++) {
            this.sources.push(this.data.newSources[i]);
        }
        this.variables = [];
        this.condition = this.data.selectedCondition;
        this.conditionValue = new ConditionValue();
        if (!this.condition.conditionValues) {
            this.condition.conditionValues = [];
        }
        this.variableService.query()
            .subscribe((res: HttpResponse<Variable[]>) => {
                this.variables = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.operatorService.query()
            .subscribe((res: HttpResponse<Operator[]>) => {
                this.operators = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    /**
     * Permet d'ouvrir la fenêtre modale de création d'une référence
     */
    openReferenceDialog(): void {
        const dialogRef = this.referenceDialog.open(CreationWizardReferenceDialogComponent, {
            width: '800px',
            data: { selectedSource: this.selectedSource }
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result != null) {
                this.selectedSource.references.push(result);
            }
        });
    }

    /**
     * Permet d'ouvrir la fenêtre modale de création d'une variable
     */
    openVariableDialog(): void {
        const dialogRef = this.referenceDialog.open(CreationWizardVariableDialogComponent, {
            width: '800px'
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result != null) {
                this.variableService.query()
                    .subscribe((res: HttpResponse<Variable[]>) => {
                        this.variables = res.body;
                    }, (res: HttpErrorResponse) => this.onError(res.message));
            }
        });
    }

    checkChange() {
        console.log(this.selectedSource);
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackById(index: number, item: Condition) {
        return item.id;
    }

    trackVariableById(index: number, item: Variable) {
        return item.id;
    }

    trackByConditionValueId(index: number, item: ConditionValue) {
        return item.id;
    }

    trackBySourceValueId(index: number, item: Source) {
        return item.id;
    }

    trackByReferenceValueId(index: number, item: Reference) {
        return item.id;
    }

    /**
     * Récupère l'opérateur sélectionné et affecte son id à la valeur de la condition d'octroi
     */
    affectOperator() {
        this.conditionValue.operatorId = this.selectedOperator.id;
    }

    /**
     * Récupère la référence sélectionnée et l'affecte à la valeur de la condition d'octroi
     */
    affectReference() {
        this.conditionValue.referenceId = this.selectedReference.id;
        this.conditionValue.referenceCode = this.selectedReference.code;
        this.conditionValue.reference = this.selectedReference;
    }

    /**
     * Permet d'enregistrer le couple variable / valeur
     */
    addValue() {
        if (this.conditionValue.operatorId && this.conditionValue.variableId && this.conditionValue.referenceCode && this.conditionValue.value) {
            this.conditionValue.conditionCode = this.condition.code;
            if (this.condition.id) {
                this.conditionValue.conditionId = this.condition.id;
            }
            this.variableService.find(this.conditionValue.variableId)
                .subscribe((variableResponse: HttpResponse<Variable>) => {
                    this.conditionValue.variableName = variableResponse.body.name;
                    this.conditionValue.operatorName = this.selectedOperator.name;
                    this.dialogRef.close({condition: this.condition, conditionValue: this.conditionValue});
                });
        }
    }

    /**
     * Permet de supprimer un couple variable / valeur
     * @param {ConditionValue} value
     */
    removeValue(value: ConditionValue) {
        const index = this.condition.conditionValues.indexOf(value);
        this.condition.conditionValues.splice(index, 1);
    }
}
