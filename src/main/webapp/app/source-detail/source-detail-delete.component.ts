import {Component, Inject, OnInit} from '@angular/core';
import {HttpResponse} from '@angular/common/http';
import {JhiAlertService} from 'ng-jhipster';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Source, SourceService} from '../entities/source';

@Component({
    selector: 'jhi-source-detail-delete',
    templateUrl: './source-detail-delete.component.html',
    styles: []
})
export class SourceDetailDeleteComponent implements OnInit {
    source: Source;

    constructor(
        public dialogRef: MatDialogRef<SourceDetailDeleteComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private sourceService: SourceService,
        private jhiAlertService: JhiAlertService
    ) { }

    /**
     * On récupère la source juridique à supprimer
     */
    ngOnInit() {
        this.sourceService.find(this.data.source.id)
            .subscribe((sourceResponse: HttpResponse<Source>) => {
                this.source = sourceResponse.body;
            });
    }
    onNoClick(): void {
        this.dialogRef.close(false);
    }

    /**
     * En cas de confirmation, on supprime la source juridique
     */
    remove() {
        this.sourceService.delete(this.source.id).subscribe((response) => {
            this.jhiAlertService.success('jestimeApp.source.deleted', {'title': this.source.title}, null);
            this.dialogRef.close(true);
        });
    }
}
