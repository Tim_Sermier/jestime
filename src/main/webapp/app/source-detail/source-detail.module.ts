import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JestimeSharedModule } from '../shared';
import {MatTabsModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AddressService} from '../entities/address';
import {SourceDetailComponent} from './source-detail.component';
import {sourceDetailRoute} from './source-detail.route';
import {SourceDetailDialogComponent} from './source-detail-dialog.component';
import {SourceDetailDeleteComponent} from './source-detail-delete.component';

const ENTITY_STATES = [
    ...sourceDetailRoute,
];

@NgModule({
    imports: [
        JestimeSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MatTabsModule,
        BrowserAnimationsModule
    ],
    declarations: [
        SourceDetailComponent,
        SourceDetailDialogComponent,
        SourceDetailDeleteComponent
    ],
    entryComponents: [
        SourceDetailComponent,
        SourceDetailDialogComponent,
        SourceDetailDeleteComponent
    ],
    providers: [
        AddressService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SourceDetailModule {}
