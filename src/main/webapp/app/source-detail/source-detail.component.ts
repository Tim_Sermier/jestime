import {Component, OnInit} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {MatDialog} from '@angular/material';
import {Subscription} from 'rxjs/Subscription';
import {SocialCard, SocialCardService} from '../entities/social-card';
import {User, UserService} from '../shared';
import {Source, SourceService} from '../entities/source';
import {SourceDetailDialogComponent} from './source-detail-dialog.component';
import {SourceDetailDeleteComponent} from './source-detail-delete.component';

@Component({
    selector: 'jhi-source-detail',
    templateUrl: './source-detail.component.html',
    styles: []
})
export class SourceDetailComponent implements OnInit {
    id: number;
    private sub: any;
    source: Source;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    socialCards: SocialCard[];
    creator: User;
    updator: User;

    constructor(
        private sourceService: SourceService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        private _location: Location,
        public dialog: MatDialog,
        private socialCardService: SocialCardService,
        private router: Router,
        private userService: UserService
    ) {
    }

    /**
     * On récupère la source juridique selon l'ID envoyé en paramètre
     */
    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSources();
    }

    /**
     * Permet d'ouvrir la fenêtre modale de modification
     */
    openDialog(): void {
        const dialogRef = this.dialog.open(SourceDetailDialogComponent, {
            width: '500px',
            data: { source: this.source }
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result != null) {
                this.sourceService.find(result.id)
                    .subscribe((sourceResponse: HttpResponse<Source>) => {
                        this.source = sourceResponse.body;
                        console.log(this.source);
                    });
            }
        });
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    goBack() {
        this._location.back();
    }

    /**
     * On récupère la source juridique selon l'ID envoyé en paramètre ainsi que
     * l'utilisateur en train de faire la modification
     * @param id
     */
    load(id) {
        this.sourceService.find(id)
            .subscribe((sourceResponse: HttpResponse<Source>) => {
                this.source = sourceResponse.body;
                this.socialCardService.findByCriterias('?sourcesId.equals=' + id).subscribe(
                    (res: HttpResponse<SocialCard[]>) => {
                        this.socialCards = res.body;
                    },
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
                this.userService.find(this.source.creatorLogin).subscribe((response) => {
                    this.creator = response.body;
                });
                if (this.source.updatorLogin) {
                    this.userService.find(this.source.updatorLogin).subscribe((response) => {
                        this.updator = response.body;
                    });
                }
            });
    }

    registerChangeInSources() {
        this.eventSubscriber = this.eventManager.subscribe(
            'sourceListModification',
            (response) => this.load(this.source.id)
        );
    }

    /**
     * Ouvre la fenêtre modale de confirmation de la suppression
     */
    openDeleteDialog(): void {
        const dialogRef = this.dialog.open(SourceDetailDeleteComponent, {
            width: '80%',
            data: { source: this.source }
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                this.router.navigate(['/']);
            }
        });
    }
}
