import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../shared';
import {SourceDetailComponent} from './source-detail.component';

export const sourceDetailRoute: Routes = [
    {
        path: 'source-detail/:id',
        component: SourceDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.source.titles.detail'
        },
        canActivate: [UserRouteAccessService]
    }
];
