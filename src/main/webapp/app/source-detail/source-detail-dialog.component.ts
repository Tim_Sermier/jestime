import {Component, Inject, OnInit} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';
import {DatePipe} from '@angular/common';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Observable} from 'rxjs/Observable';
import {Account, Principal, UserService} from '../shared';
import {Source, SourceService} from '../entities/source';
import {Juridiction, JuridictionService} from '../entities/juridiction';

@Component({
    selector: 'jhi-source-detail-dialog',
    templateUrl: './source-detail-dialog.component.html',
    styles: []
})
export class SourceDetailDialogComponent implements OnInit {
    source: Source;
    currentAccount: Account;
    userId: number;
    oldTitle: string;
    oldJuridictionId: number;
    juridictions: Juridiction[];
    constructor(
        public dialogRef: MatDialogRef<SourceDetailDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private sourceService: SourceService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal,
        private userService: UserService,
        private datePipe: DatePipe,
        private juridictionService: JuridictionService
    ) { }

    /**
     * On récupère la source juridique à modifier et on stocke son ancien nom et son ancienne juridiction
     * (pour vérification ultérieure de l'unicité)
     */
    ngOnInit() {
        this.source = this.data.source;
        this.oldTitle = this.data.source.title;
        this.oldJuridictionId = this.data.source.juridictionId;
        this.juridictionService.query()
            .subscribe((res: HttpResponse<Juridiction[]>) => { this.juridictions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            this.userService.find(this.currentAccount.login).subscribe((response) => {
                this.userId = response.body.id;
            });
        });
    }
    onNoClick(): void {
        this.dialogRef.close();
    }

    /**
     * On vérifie l'unicité de la source modifiée lors de l'enregistrement
     */
    save() {
        this.source.editionDate = this.datePipe.transform(Date.now(), 'yyyy-MM-dd');
        this.source.updatorId = this.userId;
        this.checkIsUniqueSource();
    }

    /**
     * Vérification de l'unicité de la source juridique modifiée
     */
    checkIsUniqueSource() {
        if (this.oldTitle !== this.source.title || this.source.juridictionId !== this.oldJuridictionId) {
            let existingSources = [];
            this.sourceService.findByCriteria('?title.equals=' + this.source.title + '&juridictionId.equals=' + this.source.juridictionId).subscribe(
                (res: HttpResponse<Source[]>) => {
                    existingSources = res.body;
                    if (existingSources.length === 0) {
                        if (this.source.title !== '') {
                            this.subscribeToSaveResponse(
                                this.sourceService.update(this.source));
                        }
                    } else {
                        this.jhiAlertService.error('jestimeApp.source.notUnique', {}, null);
                    }
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
        } else {
            if (this.source.title !== '') {
                this.subscribeToSaveResponse(
                    this.sourceService.update(this.source));
            }
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Source>>) {
        result.subscribe((res: HttpResponse<Source>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    /**
     * Une fois l'enregistrement terminé, on ferme la fenêtre modale en renvoyant la source
     * @param {Source} result
     */
    private onSaveSuccess(result: Source) {
        this.jhiAlertService.success('jestimeApp.source.updated', {'title': this.source.title}, null);
        this.dialogRef.close(this.source);
    }

    private onSaveError() {
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackJuridictionById(index: number, item: Juridiction) {
        return item.id;
    }
}
