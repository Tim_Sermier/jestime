import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JestimeSharedModule } from '../shared';
import {SocialCardService} from '../entities/social-card';
import {MatTabsModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {addressDetailRoute} from './address-detail.route';
import {AddressDetailComponent} from './address-detail.component';
import {AddressDetailDialogComponent} from './address-detail-dialog.component';
import {AddressDetailDeleteComponent} from './address-detail-delete.component';
import {AddressService} from '../entities/address';

const ENTITY_STATES = [
    ...addressDetailRoute,
];

@NgModule({
    imports: [
        JestimeSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MatTabsModule,
        BrowserAnimationsModule
    ],
    declarations: [
        AddressDetailComponent,
        AddressDetailDialogComponent,
        AddressDetailDeleteComponent
    ],
    entryComponents: [
        AddressDetailComponent,
        AddressDetailDialogComponent,
        AddressDetailDeleteComponent
    ],
    providers: [
        AddressService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AddressDetailModule {}
