import {Component, Inject, OnInit} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';
import {Address, AddressService} from '../entities/address';
import {ActivatedRoute} from '@angular/router';
import {DatePipe, Location} from '@angular/common';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Category} from '../entities/category';
import {SocialCard, SocialCardService} from '../entities/social-card';
import {ParentDialogComponent} from '../creation-wizard/creation-wizard.component';
import {Observable} from 'rxjs/Observable';
import {Account, Principal, UserService} from '../shared';

@Component({
    selector: 'jhi-address-detail-dialog',
    templateUrl: './address-detail-dialog.component.html',
    styles: []
})
export class AddressDetailDialogComponent implements OnInit {
    address: Address;
    currentAccount: Account;
    userId: number;
    oldName: string;
    constructor(
        public dialogRef: MatDialogRef<AddressDetailDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private addressService: AddressService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal,
        private userService: UserService,
        private datePipe: DatePipe
    ) { }

    /**
     * À l'initialisation, on récupère l'adresse à modifier ainsi que l'utilisateur en train de modifier cette dernière
     */
    ngOnInit() {
        this.address = this.data.address;
        this.oldName = this.address.name;
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            this.userService.find(this.currentAccount.login).subscribe((response) => {
                this.userId = response.body.id;
            });
        });
    }

    /**
     * En cas de clic en dehors de la fenêtre, on ferme cette dernière sans prendre en compte les modifications
     */
    onNoClick(): void {
        this.dialogRef.close();
    }

    /**
     * En cas d'enregistrement, on met à jour les informations d'édition puis on vérifie que l'adresse est unique
     */
    save() {
        this.address.editionDate = this.datePipe.transform(Date.now(), 'yyyy-MM-dd');
        this.address.updatorId = this.userId;
        this.checkIsUniqueAddress();
    }

    /**
     * Si l'adresse est unique, on enregistre les changements
     */
    checkIsUniqueAddress() {
        if (this.oldName !== this.address.name) {
            let existingAddresses = [];
            this.addressService.findByCriteria('?name.equals=' + this.address.name).subscribe(
                (res: HttpResponse<Address[]>) => {
                    existingAddresses = res.body;
                    if (existingAddresses.length === 0) {
                        if (this.address.name !== '' && this.address.location !== '' && this.address.phone !== '' && this.address.email !== '') {
                            this.subscribeToSaveResponse(
                                this.addressService.update(this.address));
                        }
                    } else {
                        this.jhiAlertService.error('jestimeApp.address.notUnique', {}, null);
                    }
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
        } else {
            if (this.address.name !== '' && this.address.location !== '' && this.address.phone !== '' && this.address.email !== '') {
                this.subscribeToSaveResponse(
                    this.addressService.update(this.address));
            }
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Address>>) {
        result.subscribe((res: HttpResponse<Address>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Address) {
        this.jhiAlertService.success('jestimeApp.address.updated', {'name': this.address.name}, null);
        this.dialogRef.close(this.address);
    }

    private onSaveError() {
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
