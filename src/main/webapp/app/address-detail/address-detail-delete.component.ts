import {Component, Inject, OnInit} from '@angular/core';
import {HttpResponse} from '@angular/common/http';
import {JhiAlertService} from 'ng-jhipster';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Address, AddressService} from '../entities/address';

@Component({
    selector: 'jhi-address-detail-delete',
    templateUrl: './address-detail-delete.component.html',
    styles: []
})
export class AddressDetailDeleteComponent implements OnInit {
    address: Address;

    constructor(
        public dialogRef: MatDialogRef<AddressDetailDeleteComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private addressService: AddressService,
        private jhiAlertService: JhiAlertService
    ) { }

    /**
     * À l'initialisation, on récupère l'adresse que l'on veut supprimer
     */
    ngOnInit() {
        this.address = this.data.address;
    }

    /**
     * En cas de clic en dehors de la fenêtre on ne supprime pas l'adresse
     */
    onNoClick(): void {
        this.dialogRef.close(false);
    }

    /**
     * Si la suppression est confirmée, on envoie la requête HTTP de suppression
     */
    remove() {
        this.addressService.delete(this.address.id).subscribe((response) => {
            this.jhiAlertService.success('jestimeApp.address.deleted', {'name': this.address.name}, null);
            this.dialogRef.close(true);
        });
    }
}
