import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../shared';
import {AddressDetailComponent} from './address-detail.component';

export const addressDetailRoute: Routes = [
    {
        path: 'address-detail/:id',
        component: AddressDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jestimeApp.address.titles.detail'
        },
        canActivate: [UserRouteAccessService]
    }
];
