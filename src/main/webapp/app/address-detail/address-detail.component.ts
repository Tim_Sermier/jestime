import {Component, OnInit} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';
import {Address, AddressService} from '../entities/address';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {MatDialog} from '@angular/material';
import {AddressDetailDialogComponent} from './address-detail-dialog.component';
import {Subscription} from 'rxjs/Subscription';
import {SocialCard, SocialCardService} from '../entities/social-card';
import {SocialCardDetailDeleteComponent} from '../social-card-detail/social-card-detail-delete.component';
import {AddressDetailDeleteComponent} from './address-detail-delete.component';
import {User, UserService} from '../shared';

@Component({
    selector: 'jhi-address-detail',
    templateUrl: './address-detail.component.html',
    styles: []
})
export class AddressDetailComponent implements OnInit {
    id: number;
    private sub: any;
    address: Address;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    socialCards: SocialCard[];
    creator: User;
    updator: User;

    constructor(
        private addressService: AddressService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        private _location: Location,
        public dialog: MatDialog,
        private socialCardService: SocialCardService,
        private router: Router,
        private userService: UserService
    ) {
    }

    /**
     * À l'initialisation, on récupère l'adresse selon l'id passé en paramètre
     */
    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAddresses();
    }

    /**
     * Permet d'ouvrir la fenêtre modale pour modifier les informations d'une adresse
     */
    openDialog(): void {
        const dialogRef = this.dialog.open(AddressDetailDialogComponent, {
            width: '500px',
            data: { address: this.address }
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result != null) {
                this.address = result;
            }
        });
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    /**
     * Permet le retour à la page précédente
     */
    goBack() {
        this._location.back();
    }

    /**
     * Cette méthode exécute la requête HTTP nécessaire pour récupérer l'adresse selon l'id
     * Elle s'occuper également de récupérer toutes les fiches de prestations liées
     * @param id
     */
    load(id) {
        this.addressService.find(id)
            .subscribe((addressResponse: HttpResponse<Address>) => {
                this.address = addressResponse.body;
                this.socialCardService.findByCriterias('?addressesId.equals=' + id).subscribe(
                    (res: HttpResponse<SocialCard[]>) => {
                        this.socialCards = res.body;
                    },
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
                this.userService.find(this.address.creatorLogin).subscribe((response) => {
                    this.creator = response.body;
                });
                if (this.address.updatorLogin) {
                    this.userService.find(this.address.updatorLogin).subscribe((response) => {
                        this.updator = response.body;
                    });
                }
            });
    }

    registerChangeInAddresses() {
        this.eventSubscriber = this.eventManager.subscribe(
            'addressListModification',
            (response) => this.load(this.address.id)
        );
    }

    /**
     * Méthode pour ouvrir la fenêtre modale de confirmation de suppression
     */
    openDeleteDialog(): void {
        const dialogRef = this.dialog.open(AddressDetailDeleteComponent, {
            width: '80%',
            data: { address: this.address }
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                this.router.navigate(['/']);
            }
        });
    }
}
