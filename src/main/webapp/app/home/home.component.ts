import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import { Account, LoginModalService, Principal } from '../shared';
import {Category, CategoryService} from '../entities/category';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Juridiction, JuridictionService} from '../entities/juridiction';
import {SocialCard, SocialCardService} from '../entities/social-card';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: [
        'home.css'
    ]

})
export class HomeComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    searchInput: string;
    categories: Category[];
    categorySelections: Category[];
    juridictions: Juridiction[];
    juridictionSelections: Juridiction[];
    socialCards: SocialCard[];

    constructor(
        private principal: Principal,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private categoryService: CategoryService,
        private jhiAlertService: JhiAlertService,
        private juridictionService: JuridictionService,
        private socialCardService: SocialCardService,
        private route: ActivatedRoute,
        private router: Router
    ) {
        this.searchInput = '';
        this.categorySelections = [];
        this.juridictionSelections = [];
        this.socialCards = [];
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
        });
        this.registerAuthenticationSuccess();
        this.categoryService.query()
            .subscribe((res: HttpResponse<Category[]>) => { this.categories = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.juridictionService.query()
            .subscribe((res: HttpResponse<Juridiction[]>) => { this.juridictions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', (message) => {
            this.principal.identity().then((account) => {
                this.categoryService.query()
                    .subscribe((res: HttpResponse<Category[]>) => {
                        this.categories = res.body;
                        this.account = account;
                    }, (res: HttpErrorResponse) => this.onError(res.message));
            });
        });
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    search() {
        let filter = '?';
        let filterNumber = 0;
        if (this.searchInput !== '') {
            filter += 'title.contains=' + this.searchInput;
            filterNumber++;
        }
        if (this.categorySelections.length > 0) {
            if (filterNumber > 0) {
                filter += '&';
            }
            filter += 'categoryId.in=';
            for (let i = 0, len = this.categorySelections.length; i < len; i++) {
                filter += this.categorySelections[i].id + ',';
            }
        }
        if (this.juridictionSelections.length > 0) {
            if (filterNumber > 0) {
                filter += '&';
            }
            filter += 'juridictionId.in=';
            for (let i = 0, len = this.juridictionSelections.length; i < len; i++) {
                filter += this.juridictionSelections[i].id + ',';
            }
        }
        this.router.navigate(['/search-social-card', filter]);
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    toggleSelectionCategory(category) {
        const idx = this.categorySelections.indexOf(category);

        // Is currently selected
        if (idx > -1) {
            this.categorySelections.splice(idx, 1);
        } else {
            this.categorySelections.push(category);
        }

        console.log(this.categorySelections);
    }

    toggleSelectionJuridiction(juridiction) {
        const idx = this.juridictionSelections.indexOf(juridiction);

        // Is currently selected
        if (idx > -1) {
            this.juridictionSelections.splice(idx, 1);
        } else {
            this.juridictionSelections.push(juridiction);
        }

        console.log(this.juridictionSelections);
    }

    trackCategoryById(index: number, item: Category) {
        return item.id;
    }

    trackJuridictionById(index: number, item: Juridiction) {
        return item.id;
    }
}
