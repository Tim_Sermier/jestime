import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { User, UserService } from '../../shared';
import {Juridiction} from '../../entities/juridiction/juridiction.model';
import {JuridictionService} from '../../entities/juridiction/juridiction.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {JhiAlertService} from 'ng-jhipster';

@Component({
    selector: 'jhi-user-mgmt-detail',
    templateUrl: './user-management-detail.component.html'
})
export class UserMgmtDetailComponent implements OnInit, OnDestroy {

    user: User;
    private subscription: Subscription;
    juridictions: Juridiction[];

    constructor(
        private userService: UserService,
        private route: ActivatedRoute,
        private alertService: JhiAlertService,
        private juridictionService: JuridictionService
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['login']);
        });
        this.juridictionService
            .query({filter: 'all'})
            .subscribe((res: HttpResponse<Array<Juridiction>>) => {
                this.juridictions = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    load(login) {
        this.userService.find(login).subscribe((response) => {
            this.user = response.body;
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    private onError(error) {
        this.alertService.error(error.error, error.message, null);
    }

}
