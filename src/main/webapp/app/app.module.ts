import './vendor.ts';

import { NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Ng2Webstorage } from 'ngx-webstorage';
import { JhiEventManager } from 'ng-jhipster';

import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { JestimeSharedModule, UserRouteAccessService } from './shared';
import { JestimeAppRoutingModule} from './app-routing.module';
import { JestimeHomeModule } from './home/home.module';
import { JestimeAdminModule } from './admin/admin.module';
import { JestimeAccountModule } from './account/account.module';
import { JestimeEntityModule } from './entities/entity.module';
import { PaginationConfig } from './blocks/config/uib-pagination.config';
import { StateStorageService } from './shared/auth/state-storage.service';

// jhipster-needle-angular-add-module-import JHipster will add new module here
import {
    JhiMainComponent,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ActiveMenuDirective,
    ErrorComponent
} from './layouts';
import {CreationWizardModule} from './creation-wizard/creation-wizard.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SearchSocialCardModule} from './search-social-card/search-social-card.module';
import {SocialCardDetailModule} from './social-card-detail/social-card-detail.module';
import {AddressDetailModule} from './address-detail/address-detail.module';
import {SourceDetailModule} from './source-detail/source-detail.module';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        JestimeAppRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        JestimeSharedModule,
        JestimeHomeModule,
        JestimeAdminModule,
        JestimeAccountModule,
        JestimeEntityModule,
        CreationWizardModule,
        SearchSocialCardModule,
        SocialCardDetailModule,
        AddressDetailModule,
        SourceDetailModule
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        ActiveMenuDirective,
        FooterComponent,
    ],
    providers: [
        ProfileService,
        PaginationConfig,
        UserRouteAccessService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthExpiredInterceptor,
            multi: true,
            deps: [
                StateStorageService,
                Injector
            ]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorHandlerInterceptor,
            multi: true,
            deps: [
                JhiEventManager
            ]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: NotificationInterceptor,
            multi: true,
            deps: [
                Injector
            ]
        }
    ],
    bootstrap: [ JhiMainComponent ]
})
export class JestimeAppModule {}
