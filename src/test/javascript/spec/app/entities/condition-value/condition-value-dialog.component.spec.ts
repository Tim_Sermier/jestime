/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { JestimeTestModule } from '../../../test.module';
import { ConditionValueDialogComponent } from '../../../../../../main/webapp/app/entities/condition-value/condition-value-dialog.component';
import { ConditionValueService } from '../../../../../../main/webapp/app/entities/condition-value/condition-value.service';
import { ConditionValue } from '../../../../../../main/webapp/app/entities/condition-value/condition-value.model';
import { VariableService } from '../../../../../../main/webapp/app/entities/variable';
import { ConditionService } from '../../../../../../main/webapp/app/entities/condition';
import { ReferenceService } from '../../../../../../main/webapp/app/entities/reference';
import { OperatorService } from '../../../../../../main/webapp/app/entities/operator';

describe('Component Tests', () => {

    describe('ConditionValue Management Dialog Component', () => {
        let comp: ConditionValueDialogComponent;
        let fixture: ComponentFixture<ConditionValueDialogComponent>;
        let service: ConditionValueService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [ConditionValueDialogComponent],
                providers: [
                    VariableService,
                    ConditionService,
                    ReferenceService,
                    OperatorService,
                    ConditionValueService
                ]
            })
            .overrideTemplate(ConditionValueDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ConditionValueDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConditionValueService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ConditionValue(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.conditionValue = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'conditionValueListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ConditionValue();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.conditionValue = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'conditionValueListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
