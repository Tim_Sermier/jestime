/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { JestimeTestModule } from '../../../test.module';
import { ConditionValueDetailComponent } from '../../../../../../main/webapp/app/entities/condition-value/condition-value-detail.component';
import { ConditionValueService } from '../../../../../../main/webapp/app/entities/condition-value/condition-value.service';
import { ConditionValue } from '../../../../../../main/webapp/app/entities/condition-value/condition-value.model';

describe('Component Tests', () => {

    describe('ConditionValue Management Detail Component', () => {
        let comp: ConditionValueDetailComponent;
        let fixture: ComponentFixture<ConditionValueDetailComponent>;
        let service: ConditionValueService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [ConditionValueDetailComponent],
                providers: [
                    ConditionValueService
                ]
            })
            .overrideTemplate(ConditionValueDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ConditionValueDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConditionValueService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ConditionValue(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.conditionValue).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
