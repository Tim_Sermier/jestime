/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JestimeTestModule } from '../../../test.module';
import { ConditionValueComponent } from '../../../../../../main/webapp/app/entities/condition-value/condition-value.component';
import { ConditionValueService } from '../../../../../../main/webapp/app/entities/condition-value/condition-value.service';
import { ConditionValue } from '../../../../../../main/webapp/app/entities/condition-value/condition-value.model';

describe('Component Tests', () => {

    describe('ConditionValue Management Component', () => {
        let comp: ConditionValueComponent;
        let fixture: ComponentFixture<ConditionValueComponent>;
        let service: ConditionValueService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [ConditionValueComponent],
                providers: [
                    ConditionValueService
                ]
            })
            .overrideTemplate(ConditionValueComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ConditionValueComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConditionValueService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ConditionValue(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.conditionValues[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
