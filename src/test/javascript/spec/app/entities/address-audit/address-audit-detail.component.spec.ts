/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { JestimeTestModule } from '../../../test.module';
import { AddressAuditDetailComponent } from '../../../../../../main/webapp/app/entities/address-audit/address-audit-detail.component';
import { AddressAuditService } from '../../../../../../main/webapp/app/entities/address-audit/address-audit.service';
import { AddressAudit } from '../../../../../../main/webapp/app/entities/address-audit/address-audit.model';

describe('Component Tests', () => {

    describe('AddressAudit Management Detail Component', () => {
        let comp: AddressAuditDetailComponent;
        let fixture: ComponentFixture<AddressAuditDetailComponent>;
        let service: AddressAuditService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [AddressAuditDetailComponent],
                providers: [
                    AddressAuditService
                ]
            })
            .overrideTemplate(AddressAuditDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AddressAuditDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AddressAuditService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new AddressAudit(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.addressAudit).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
