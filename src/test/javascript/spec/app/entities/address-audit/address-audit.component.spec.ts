/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JestimeTestModule } from '../../../test.module';
import { AddressAuditComponent } from '../../../../../../main/webapp/app/entities/address-audit/address-audit.component';
import { AddressAuditService } from '../../../../../../main/webapp/app/entities/address-audit/address-audit.service';
import { AddressAudit } from '../../../../../../main/webapp/app/entities/address-audit/address-audit.model';

describe('Component Tests', () => {

    describe('AddressAudit Management Component', () => {
        let comp: AddressAuditComponent;
        let fixture: ComponentFixture<AddressAuditComponent>;
        let service: AddressAuditService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [AddressAuditComponent],
                providers: [
                    AddressAuditService
                ]
            })
            .overrideTemplate(AddressAuditComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AddressAuditComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AddressAuditService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new AddressAudit(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.addressAudits[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
