/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JestimeTestModule } from '../../../test.module';
import { VariableComponent } from '../../../../../../main/webapp/app/entities/variable/variable.component';
import { VariableService } from '../../../../../../main/webapp/app/entities/variable/variable.service';
import { Variable } from '../../../../../../main/webapp/app/entities/variable/variable.model';

describe('Component Tests', () => {

    describe('Variable Management Component', () => {
        let comp: VariableComponent;
        let fixture: ComponentFixture<VariableComponent>;
        let service: VariableService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [VariableComponent],
                providers: [
                    VariableService
                ]
            })
            .overrideTemplate(VariableComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(VariableComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(VariableService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Variable(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.variables[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
