/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { JestimeTestModule } from '../../../test.module';
import { VariableDetailComponent } from '../../../../../../main/webapp/app/entities/variable/variable-detail.component';
import { VariableService } from '../../../../../../main/webapp/app/entities/variable/variable.service';
import { Variable } from '../../../../../../main/webapp/app/entities/variable/variable.model';

describe('Component Tests', () => {

    describe('Variable Management Detail Component', () => {
        let comp: VariableDetailComponent;
        let fixture: ComponentFixture<VariableDetailComponent>;
        let service: VariableService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [VariableDetailComponent],
                providers: [
                    VariableService
                ]
            })
            .overrideTemplate(VariableDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(VariableDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(VariableService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Variable(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.variable).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
