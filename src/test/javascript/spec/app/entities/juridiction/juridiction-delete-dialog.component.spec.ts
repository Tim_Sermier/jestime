/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { JestimeTestModule } from '../../../test.module';
import { JuridictionDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/juridiction/juridiction-delete-dialog.component';
import { JuridictionService } from '../../../../../../main/webapp/app/entities/juridiction/juridiction.service';

describe('Component Tests', () => {

    describe('Juridiction Management Delete Component', () => {
        let comp: JuridictionDeleteDialogComponent;
        let fixture: ComponentFixture<JuridictionDeleteDialogComponent>;
        let service: JuridictionService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [JuridictionDeleteDialogComponent],
                providers: [
                    JuridictionService
                ]
            })
            .overrideTemplate(JuridictionDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(JuridictionDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(JuridictionService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
