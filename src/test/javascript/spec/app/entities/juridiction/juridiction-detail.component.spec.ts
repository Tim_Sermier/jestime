/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { JestimeTestModule } from '../../../test.module';
import { JuridictionDetailComponent } from '../../../../../../main/webapp/app/entities/juridiction/juridiction-detail.component';
import { JuridictionService } from '../../../../../../main/webapp/app/entities/juridiction/juridiction.service';
import { Juridiction } from '../../../../../../main/webapp/app/entities/juridiction/juridiction.model';

describe('Component Tests', () => {

    describe('Juridiction Management Detail Component', () => {
        let comp: JuridictionDetailComponent;
        let fixture: ComponentFixture<JuridictionDetailComponent>;
        let service: JuridictionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [JuridictionDetailComponent],
                providers: [
                    JuridictionService
                ]
            })
            .overrideTemplate(JuridictionDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(JuridictionDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(JuridictionService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Juridiction(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.juridiction).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
