/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JestimeTestModule } from '../../../test.module';
import { JuridictionComponent } from '../../../../../../main/webapp/app/entities/juridiction/juridiction.component';
import { JuridictionService } from '../../../../../../main/webapp/app/entities/juridiction/juridiction.service';
import { Juridiction } from '../../../../../../main/webapp/app/entities/juridiction/juridiction.model';

describe('Component Tests', () => {

    describe('Juridiction Management Component', () => {
        let comp: JuridictionComponent;
        let fixture: ComponentFixture<JuridictionComponent>;
        let service: JuridictionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [JuridictionComponent],
                providers: [
                    JuridictionService
                ]
            })
            .overrideTemplate(JuridictionComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(JuridictionComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(JuridictionService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Juridiction(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.juridictions[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
