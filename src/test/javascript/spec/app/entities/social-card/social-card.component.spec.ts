/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JestimeTestModule } from '../../../test.module';
import { SocialCardComponent } from '../../../../../../main/webapp/app/entities/social-card/social-card.component';
import { SocialCardService } from '../../../../../../main/webapp/app/entities/social-card/social-card.service';
import { SocialCard } from '../../../../../../main/webapp/app/entities/social-card/social-card.model';

describe('Component Tests', () => {

    describe('SocialCard Management Component', () => {
        let comp: SocialCardComponent;
        let fixture: ComponentFixture<SocialCardComponent>;
        let service: SocialCardService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [SocialCardComponent],
                providers: [
                    SocialCardService
                ]
            })
            .overrideTemplate(SocialCardComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SocialCardComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SocialCardService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new SocialCard(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.socialCards[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
