/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { JestimeTestModule } from '../../../test.module';
import { SocialCardDialogComponent } from '../../../../../../main/webapp/app/entities/social-card/social-card-dialog.component';
import { SocialCardService } from '../../../../../../main/webapp/app/entities/social-card/social-card.service';
import { SocialCard } from '../../../../../../main/webapp/app/entities/social-card/social-card.model';
import { JuridictionService } from '../../../../../../main/webapp/app/entities/juridiction';
import { CategoryService } from '../../../../../../main/webapp/app/entities/category';
import { AddressService } from '../../../../../../main/webapp/app/entities/address';
import { UserService } from '../../../../../../main/webapp/app/shared';
import { SourceService } from '../../../../../../main/webapp/app/entities/source';

describe('Component Tests', () => {

    describe('SocialCard Management Dialog Component', () => {
        let comp: SocialCardDialogComponent;
        let fixture: ComponentFixture<SocialCardDialogComponent>;
        let service: SocialCardService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [SocialCardDialogComponent],
                providers: [
                    JuridictionService,
                    CategoryService,
                    AddressService,
                    UserService,
                    SourceService,
                    SocialCardService
                ]
            })
            .overrideTemplate(SocialCardDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SocialCardDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SocialCardService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new SocialCard(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.socialCard = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'socialCardListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new SocialCard();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.socialCard = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'socialCardListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
