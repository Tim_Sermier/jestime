/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { JestimeTestModule } from '../../../test.module';
import { SocialCardDetailComponent } from '../../../../../../main/webapp/app/entities/social-card/social-card-detail.component';
import { SocialCardService } from '../../../../../../main/webapp/app/entities/social-card/social-card.service';
import { SocialCard } from '../../../../../../main/webapp/app/entities/social-card/social-card.model';

describe('Component Tests', () => {

    describe('SocialCard Management Detail Component', () => {
        let comp: SocialCardDetailComponent;
        let fixture: ComponentFixture<SocialCardDetailComponent>;
        let service: SocialCardService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [SocialCardDetailComponent],
                providers: [
                    SocialCardService
                ]
            })
            .overrideTemplate(SocialCardDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SocialCardDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SocialCardService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new SocialCard(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.socialCard).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
