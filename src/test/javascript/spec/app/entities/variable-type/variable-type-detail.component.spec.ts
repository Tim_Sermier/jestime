/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { JestimeTestModule } from '../../../test.module';
import { VariableTypeDetailComponent } from '../../../../../../main/webapp/app/entities/variable-type/variable-type-detail.component';
import { VariableTypeService } from '../../../../../../main/webapp/app/entities/variable-type/variable-type.service';
import { VariableType } from '../../../../../../main/webapp/app/entities/variable-type/variable-type.model';

describe('Component Tests', () => {

    describe('VariableType Management Detail Component', () => {
        let comp: VariableTypeDetailComponent;
        let fixture: ComponentFixture<VariableTypeDetailComponent>;
        let service: VariableTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [VariableTypeDetailComponent],
                providers: [
                    VariableTypeService
                ]
            })
            .overrideTemplate(VariableTypeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(VariableTypeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(VariableTypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new VariableType(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.variableType).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
