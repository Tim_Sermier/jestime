/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JestimeTestModule } from '../../../test.module';
import { VariableTypeComponent } from '../../../../../../main/webapp/app/entities/variable-type/variable-type.component';
import { VariableTypeService } from '../../../../../../main/webapp/app/entities/variable-type/variable-type.service';
import { VariableType } from '../../../../../../main/webapp/app/entities/variable-type/variable-type.model';

describe('Component Tests', () => {

    describe('VariableType Management Component', () => {
        let comp: VariableTypeComponent;
        let fixture: ComponentFixture<VariableTypeComponent>;
        let service: VariableTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [VariableTypeComponent],
                providers: [
                    VariableTypeService
                ]
            })
            .overrideTemplate(VariableTypeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(VariableTypeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(VariableTypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new VariableType(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.variableTypes[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
