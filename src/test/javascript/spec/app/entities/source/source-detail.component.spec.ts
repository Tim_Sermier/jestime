/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { JestimeTestModule } from '../../../test.module';
import { SourceDetailComponent } from '../../../../../../main/webapp/app/entities/source/source-detail.component';
import { SourceService } from '../../../../../../main/webapp/app/entities/source/source.service';
import { Source } from '../../../../../../main/webapp/app/entities/source/source.model';

describe('Component Tests', () => {

    describe('Source Management Detail Component', () => {
        let comp: SourceDetailComponent;
        let fixture: ComponentFixture<SourceDetailComponent>;
        let service: SourceService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [SourceDetailComponent],
                providers: [
                    SourceService
                ]
            })
            .overrideTemplate(SourceDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SourceDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SourceService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Source(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.source).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
