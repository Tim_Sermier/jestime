/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JestimeTestModule } from '../../../test.module';
import { SourceComponent } from '../../../../../../main/webapp/app/entities/source/source.component';
import { SourceService } from '../../../../../../main/webapp/app/entities/source/source.service';
import { Source } from '../../../../../../main/webapp/app/entities/source/source.model';

describe('Component Tests', () => {

    describe('Source Management Component', () => {
        let comp: SourceComponent;
        let fixture: ComponentFixture<SourceComponent>;
        let service: SourceService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [SourceComponent],
                providers: [
                    SourceService
                ]
            })
            .overrideTemplate(SourceComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SourceComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SourceService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Source(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.sources[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
