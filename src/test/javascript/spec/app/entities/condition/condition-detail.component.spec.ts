/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { JestimeTestModule } from '../../../test.module';
import { ConditionDetailComponent } from '../../../../../../main/webapp/app/entities/condition/condition-detail.component';
import { ConditionService } from '../../../../../../main/webapp/app/entities/condition/condition.service';
import { Condition } from '../../../../../../main/webapp/app/entities/condition/condition.model';

describe('Component Tests', () => {

    describe('Condition Management Detail Component', () => {
        let comp: ConditionDetailComponent;
        let fixture: ComponentFixture<ConditionDetailComponent>;
        let service: ConditionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [ConditionDetailComponent],
                providers: [
                    ConditionService
                ]
            })
            .overrideTemplate(ConditionDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ConditionDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConditionService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Condition(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.condition).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
