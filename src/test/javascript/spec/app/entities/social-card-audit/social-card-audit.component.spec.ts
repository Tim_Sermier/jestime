/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JestimeTestModule } from '../../../test.module';
import { SocialCardAuditComponent } from '../../../../../../main/webapp/app/entities/social-card-audit/social-card-audit.component';
import { SocialCardAuditService } from '../../../../../../main/webapp/app/entities/social-card-audit/social-card-audit.service';
import { SocialCardAudit } from '../../../../../../main/webapp/app/entities/social-card-audit/social-card-audit.model';

describe('Component Tests', () => {

    describe('SocialCardAudit Management Component', () => {
        let comp: SocialCardAuditComponent;
        let fixture: ComponentFixture<SocialCardAuditComponent>;
        let service: SocialCardAuditService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [SocialCardAuditComponent],
                providers: [
                    SocialCardAuditService
                ]
            })
            .overrideTemplate(SocialCardAuditComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SocialCardAuditComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SocialCardAuditService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new SocialCardAudit(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.socialCardAudits[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
