/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { JestimeTestModule } from '../../../test.module';
import { SocialCardAuditDetailComponent } from '../../../../../../main/webapp/app/entities/social-card-audit/social-card-audit-detail.component';
import { SocialCardAuditService } from '../../../../../../main/webapp/app/entities/social-card-audit/social-card-audit.service';
import { SocialCardAudit } from '../../../../../../main/webapp/app/entities/social-card-audit/social-card-audit.model';

describe('Component Tests', () => {

    describe('SocialCardAudit Management Detail Component', () => {
        let comp: SocialCardAuditDetailComponent;
        let fixture: ComponentFixture<SocialCardAuditDetailComponent>;
        let service: SocialCardAuditService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [SocialCardAuditDetailComponent],
                providers: [
                    SocialCardAuditService
                ]
            })
            .overrideTemplate(SocialCardAuditDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SocialCardAuditDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SocialCardAuditService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new SocialCardAudit(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.socialCardAudit).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
