/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { JestimeTestModule } from '../../../test.module';
import { SourceAuditDetailComponent } from '../../../../../../main/webapp/app/entities/source-audit/source-audit-detail.component';
import { SourceAuditService } from '../../../../../../main/webapp/app/entities/source-audit/source-audit.service';
import { SourceAudit } from '../../../../../../main/webapp/app/entities/source-audit/source-audit.model';

describe('Component Tests', () => {

    describe('SourceAudit Management Detail Component', () => {
        let comp: SourceAuditDetailComponent;
        let fixture: ComponentFixture<SourceAuditDetailComponent>;
        let service: SourceAuditService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [SourceAuditDetailComponent],
                providers: [
                    SourceAuditService
                ]
            })
            .overrideTemplate(SourceAuditDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SourceAuditDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SourceAuditService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new SourceAudit(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.sourceAudit).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
