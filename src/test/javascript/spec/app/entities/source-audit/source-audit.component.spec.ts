/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JestimeTestModule } from '../../../test.module';
import { SourceAuditComponent } from '../../../../../../main/webapp/app/entities/source-audit/source-audit.component';
import { SourceAuditService } from '../../../../../../main/webapp/app/entities/source-audit/source-audit.service';
import { SourceAudit } from '../../../../../../main/webapp/app/entities/source-audit/source-audit.model';

describe('Component Tests', () => {

    describe('SourceAudit Management Component', () => {
        let comp: SourceAuditComponent;
        let fixture: ComponentFixture<SourceAuditComponent>;
        let service: SourceAuditService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JestimeTestModule],
                declarations: [SourceAuditComponent],
                providers: [
                    SourceAuditService
                ]
            })
            .overrideTemplate(SourceAuditComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SourceAuditComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SourceAuditService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new SourceAudit(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.sourceAudits[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
