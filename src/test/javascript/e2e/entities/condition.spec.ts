import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Condition e2e test', () => {

    let navBarPage: NavBarPage;
    let conditionDialogPage: ConditionDialogPage;
    let conditionComponentsPage: ConditionComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Conditions', () => {
        navBarPage.goToEntity('condition');
        conditionComponentsPage = new ConditionComponentsPage();
        expect(conditionComponentsPage.getTitle())
            .toMatch(/jestimeApp.condition.home.title/);

    });

    it('should load create Condition dialog', () => {
        conditionComponentsPage.clickOnCreateButton();
        conditionDialogPage = new ConditionDialogPage();
        expect(conditionDialogPage.getModalTitle())
            .toMatch(/jestimeApp.condition.home.createOrEditLabel/);
        conditionDialogPage.close();
    });

   /* it('should create and save Conditions', () => {
        conditionComponentsPage.clickOnCreateButton();
        conditionDialogPage.setCodeInput('code');
        expect(conditionDialogPage.getCodeInput()).toMatch('code');
        conditionDialogPage.setDescriptionInput('description');
        expect(conditionDialogPage.getDescriptionInput()).toMatch('description');
        conditionDialogPage.setCreationDateInput('2000-12-31');
        expect(conditionDialogPage.getCreationDateInput()).toMatch('2000-12-31');
        conditionDialogPage.setEditDateInput('2000-12-31');
        expect(conditionDialogPage.getEditDateInput()).toMatch('2000-12-31');
        conditionDialogPage.socialCardSelectLastOption();
        conditionDialogPage.creatorSelectLastOption();
        conditionDialogPage.updatorSelectLastOption();
        conditionDialogPage.save();
        expect(conditionDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });*/

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ConditionComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-condition div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ConditionDialogPage {
    modalTitle = element(by.css('h4#myConditionLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    codeInput = element(by.css('input#field_code'));
    descriptionInput = element(by.css('textarea#field_description'));
    creationDateInput = element(by.css('input#field_creationDate'));
    editDateInput = element(by.css('input#field_editDate'));
    socialCardSelect = element(by.css('select#field_socialCard'));
    creatorSelect = element(by.css('select#field_creator'));
    updatorSelect = element(by.css('select#field_updator'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setCodeInput = function(code) {
        this.codeInput.sendKeys(code);
    };

    getCodeInput = function() {
        return this.codeInput.getAttribute('value');
    };

    setDescriptionInput = function(description) {
        this.descriptionInput.sendKeys(description);
    };

    getDescriptionInput = function() {
        return this.descriptionInput.getAttribute('value');
    };

    setCreationDateInput = function(creationDate) {
        this.creationDateInput.sendKeys(creationDate);
    };

    getCreationDateInput = function() {
        return this.creationDateInput.getAttribute('value');
    };

    setEditDateInput = function(editDate) {
        this.editDateInput.sendKeys(editDate);
    };

    getEditDateInput = function() {
        return this.editDateInput.getAttribute('value');
    };

    socialCardSelectLastOption = function() {
        this.socialCardSelect.all(by.tagName('option')).last().click();
    };

    socialCardSelectOption = function(option) {
        this.socialCardSelect.sendKeys(option);
    };

    getSocialCardSelect = function() {
        return this.socialCardSelect;
    };

    getSocialCardSelectedOption = function() {
        return this.socialCardSelect.element(by.css('option:checked')).getText();
    };

    creatorSelectLastOption = function() {
        this.creatorSelect.all(by.tagName('option')).last().click();
    };

    creatorSelectOption = function(option) {
        this.creatorSelect.sendKeys(option);
    };

    getCreatorSelect = function() {
        return this.creatorSelect;
    };

    getCreatorSelectedOption = function() {
        return this.creatorSelect.element(by.css('option:checked')).getText();
    };

    updatorSelectLastOption = function() {
        this.updatorSelect.all(by.tagName('option')).last().click();
    };

    updatorSelectOption = function(option) {
        this.updatorSelect.sendKeys(option);
    };

    getUpdatorSelect = function() {
        return this.updatorSelect;
    };

    getUpdatorSelectedOption = function() {
        return this.updatorSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
