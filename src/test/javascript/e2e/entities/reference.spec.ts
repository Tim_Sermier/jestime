import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Reference e2e test', () => {

    let navBarPage: NavBarPage;
    let referenceDialogPage: ReferenceDialogPage;
    let referenceComponentsPage: ReferenceComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load References', () => {
        navBarPage.goToEntity('reference');
        referenceComponentsPage = new ReferenceComponentsPage();
        expect(referenceComponentsPage.getTitle())
            .toMatch(/jestimeApp.reference.home.title/);

    });

    it('should load create Reference dialog', () => {
        referenceComponentsPage.clickOnCreateButton();
        referenceDialogPage = new ReferenceDialogPage();
        expect(referenceDialogPage.getModalTitle())
            .toMatch(/jestimeApp.reference.home.createOrEditLabel/);
        referenceDialogPage.close();
    });

   /* it('should create and save References', () => {
        referenceComponentsPage.clickOnCreateButton();
        referenceDialogPage.setCodeInput('code');
        expect(referenceDialogPage.getCodeInput()).toMatch('code');
        referenceDialogPage.setTitleInput('title');
        expect(referenceDialogPage.getTitleInput()).toMatch('title');
        referenceDialogPage.sourceSelectLastOption();
        referenceDialogPage.save();
        expect(referenceDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });*/

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ReferenceComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-reference div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ReferenceDialogPage {
    modalTitle = element(by.css('h4#myReferenceLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    codeInput = element(by.css('input#field_code'));
    titleInput = element(by.css('input#field_title'));
    sourceSelect = element(by.css('select#field_source'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setCodeInput = function(code) {
        this.codeInput.sendKeys(code);
    };

    getCodeInput = function() {
        return this.codeInput.getAttribute('value');
    };

    setTitleInput = function(title) {
        this.titleInput.sendKeys(title);
    };

    getTitleInput = function() {
        return this.titleInput.getAttribute('value');
    };

    sourceSelectLastOption = function() {
        this.sourceSelect.all(by.tagName('option')).last().click();
    };

    sourceSelectOption = function(option) {
        this.sourceSelect.sendKeys(option);
    };

    getSourceSelect = function() {
        return this.sourceSelect;
    };

    getSourceSelectedOption = function() {
        return this.sourceSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
