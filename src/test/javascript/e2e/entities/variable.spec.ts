import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Variable e2e test', () => {

    let navBarPage: NavBarPage;
    let variableDialogPage: VariableDialogPage;
    let variableComponentsPage: VariableComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Variables', () => {
        navBarPage.goToEntity('variable');
        variableComponentsPage = new VariableComponentsPage();
        expect(variableComponentsPage.getTitle())
            .toMatch(/jestimeApp.variable.home.title/);

    });

    it('should load create Variable dialog', () => {
        variableComponentsPage.clickOnCreateButton();
        variableDialogPage = new VariableDialogPage();
        expect(variableDialogPage.getModalTitle())
            .toMatch(/jestimeApp.variable.home.createOrEditLabel/);
        variableDialogPage.close();
    });

   /* it('should create and save Variables', () => {
        variableComponentsPage.clickOnCreateButton();
        variableDialogPage.setCodeInput('code');
        expect(variableDialogPage.getCodeInput()).toMatch('code');
        variableDialogPage.setNameInput('name');
        expect(variableDialogPage.getNameInput()).toMatch('name');
        variableDialogPage.setDescriptionInput('description');
        expect(variableDialogPage.getDescriptionInput()).toMatch('description');
        variableDialogPage.typeSelectLastOption();
        variableDialogPage.save();
        expect(variableDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });*/

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class VariableComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-variable div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class VariableDialogPage {
    modalTitle = element(by.css('h4#myVariableLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    codeInput = element(by.css('input#field_code'));
    nameInput = element(by.css('input#field_name'));
    descriptionInput = element(by.css('textarea#field_description'));
    typeSelect = element(by.css('select#field_type'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setCodeInput = function(code) {
        this.codeInput.sendKeys(code);
    };

    getCodeInput = function() {
        return this.codeInput.getAttribute('value');
    };

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    setDescriptionInput = function(description) {
        this.descriptionInput.sendKeys(description);
    };

    getDescriptionInput = function() {
        return this.descriptionInput.getAttribute('value');
    };

    typeSelectLastOption = function() {
        this.typeSelect.all(by.tagName('option')).last().click();
    };

    typeSelectOption = function(option) {
        this.typeSelect.sendKeys(option);
    };

    getTypeSelect = function() {
        return this.typeSelect;
    };

    getTypeSelectedOption = function() {
        return this.typeSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
