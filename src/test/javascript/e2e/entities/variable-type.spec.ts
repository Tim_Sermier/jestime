import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('VariableType e2e test', () => {

    let navBarPage: NavBarPage;
    let variableTypeDialogPage: VariableTypeDialogPage;
    let variableTypeComponentsPage: VariableTypeComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load VariableTypes', () => {
        navBarPage.goToEntity('variable-type');
        variableTypeComponentsPage = new VariableTypeComponentsPage();
        expect(variableTypeComponentsPage.getTitle())
            .toMatch(/jestimeApp.variableType.home.title/);

    });

    it('should load create VariableType dialog', () => {
        variableTypeComponentsPage.clickOnCreateButton();
        variableTypeDialogPage = new VariableTypeDialogPage();
        expect(variableTypeDialogPage.getModalTitle())
            .toMatch(/jestimeApp.variableType.home.createOrEditLabel/);
        variableTypeDialogPage.close();
    });

    it('should create and save VariableTypes', () => {
        variableTypeComponentsPage.clickOnCreateButton();
        variableTypeDialogPage.setCodeInput('code');
        expect(variableTypeDialogPage.getCodeInput()).toMatch('code');
        variableTypeDialogPage.setNameInput('name');
        expect(variableTypeDialogPage.getNameInput()).toMatch('name');
        variableTypeDialogPage.save();
        expect(variableTypeDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class VariableTypeComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-variable-type div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class VariableTypeDialogPage {
    modalTitle = element(by.css('h4#myVariableTypeLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    codeInput = element(by.css('input#field_code'));
    nameInput = element(by.css('input#field_name'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setCodeInput = function(code) {
        this.codeInput.sendKeys(code);
    };

    getCodeInput = function() {
        return this.codeInput.getAttribute('value');
    };

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
