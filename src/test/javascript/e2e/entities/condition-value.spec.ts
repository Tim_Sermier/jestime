import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('ConditionValue e2e test', () => {

    let navBarPage: NavBarPage;
    let conditionValueDialogPage: ConditionValueDialogPage;
    let conditionValueComponentsPage: ConditionValueComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load ConditionValues', () => {
        navBarPage.goToEntity('condition-value');
        conditionValueComponentsPage = new ConditionValueComponentsPage();
        expect(conditionValueComponentsPage.getTitle())
            .toMatch(/jestimeApp.conditionValue.home.title/);

    });

    it('should load create ConditionValue dialog', () => {
        conditionValueComponentsPage.clickOnCreateButton();
        conditionValueDialogPage = new ConditionValueDialogPage();
        expect(conditionValueDialogPage.getModalTitle())
            .toMatch(/jestimeApp.conditionValue.home.createOrEditLabel/);
        conditionValueDialogPage.close();
    });

   /* it('should create and save ConditionValues', () => {
        conditionValueComponentsPage.clickOnCreateButton();
        conditionValueDialogPage.setValueInput('value');
        expect(conditionValueDialogPage.getValueInput()).toMatch('value');
        conditionValueDialogPage.variableSelectLastOption();
        conditionValueDialogPage.conditionSelectLastOption();
        conditionValueDialogPage.referenceSelectLastOption();
        conditionValueDialogPage.operatorSelectLastOption();
        conditionValueDialogPage.save();
        expect(conditionValueDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });*/

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ConditionValueComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-condition-value div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ConditionValueDialogPage {
    modalTitle = element(by.css('h4#myConditionValueLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    valueInput = element(by.css('input#field_value'));
    variableSelect = element(by.css('select#field_variable'));
    conditionSelect = element(by.css('select#field_condition'));
    referenceSelect = element(by.css('select#field_reference'));
    operatorSelect = element(by.css('select#field_operator'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setValueInput = function(value) {
        this.valueInput.sendKeys(value);
    };

    getValueInput = function() {
        return this.valueInput.getAttribute('value');
    };

    variableSelectLastOption = function() {
        this.variableSelect.all(by.tagName('option')).last().click();
    };

    variableSelectOption = function(option) {
        this.variableSelect.sendKeys(option);
    };

    getVariableSelect = function() {
        return this.variableSelect;
    };

    getVariableSelectedOption = function() {
        return this.variableSelect.element(by.css('option:checked')).getText();
    };

    conditionSelectLastOption = function() {
        this.conditionSelect.all(by.tagName('option')).last().click();
    };

    conditionSelectOption = function(option) {
        this.conditionSelect.sendKeys(option);
    };

    getConditionSelect = function() {
        return this.conditionSelect;
    };

    getConditionSelectedOption = function() {
        return this.conditionSelect.element(by.css('option:checked')).getText();
    };

    referenceSelectLastOption = function() {
        this.referenceSelect.all(by.tagName('option')).last().click();
    };

    referenceSelectOption = function(option) {
        this.referenceSelect.sendKeys(option);
    };

    getReferenceSelect = function() {
        return this.referenceSelect;
    };

    getReferenceSelectedOption = function() {
        return this.referenceSelect.element(by.css('option:checked')).getText();
    };

    operatorSelectLastOption = function() {
        this.operatorSelect.all(by.tagName('option')).last().click();
    };

    operatorSelectOption = function(option) {
        this.operatorSelect.sendKeys(option);
    };

    getOperatorSelect = function() {
        return this.operatorSelect;
    };

    getOperatorSelectedOption = function() {
        return this.operatorSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
