import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('AddressAudit e2e test', () => {

    let navBarPage: NavBarPage;
    let addressAuditDialogPage: AddressAuditDialogPage;
    let addressAuditComponentsPage: AddressAuditComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load AddressAudits', () => {
        navBarPage.goToEntity('address-audit');
        addressAuditComponentsPage = new AddressAuditComponentsPage();
        expect(addressAuditComponentsPage.getTitle())
            .toMatch(/jestimeApp.addressAudit.home.title/);

    });

    it('should load create AddressAudit dialog', () => {
        addressAuditComponentsPage.clickOnCreateButton();
        addressAuditDialogPage = new AddressAuditDialogPage();
        expect(addressAuditDialogPage.getModalTitle())
            .toMatch(/jestimeApp.addressAudit.home.createOrEditLabel/);
        addressAuditDialogPage.close();
    });

    it('should create and save AddressAudits', () => {
        addressAuditComponentsPage.clickOnCreateButton();
        addressAuditDialogPage.setActionInput('action');
        expect(addressAuditDialogPage.getActionInput()).toMatch('action');
        addressAuditDialogPage.setActionDateInput('2000-12-31');
        expect(addressAuditDialogPage.getActionDateInput()).toMatch('2000-12-31');
        addressAuditDialogPage.setActionUserInput('5');
        expect(addressAuditDialogPage.getActionUserInput()).toMatch('5');
        addressAuditDialogPage.setCodeInput('code');
        expect(addressAuditDialogPage.getCodeInput()).toMatch('code');
        addressAuditDialogPage.setNameInput('name');
        expect(addressAuditDialogPage.getNameInput()).toMatch('name');
        addressAuditDialogPage.setLocationInput('location');
        expect(addressAuditDialogPage.getLocationInput()).toMatch('location');
        addressAuditDialogPage.setPhoneInput('phone');
        expect(addressAuditDialogPage.getPhoneInput()).toMatch('phone');
        addressAuditDialogPage.setFaxInput('fax');
        expect(addressAuditDialogPage.getFaxInput()).toMatch('fax');
        addressAuditDialogPage.setEmailInput('email');
        expect(addressAuditDialogPage.getEmailInput()).toMatch('email');
        addressAuditDialogPage.setWebsiteInput('website');
        expect(addressAuditDialogPage.getWebsiteInput()).toMatch('website');
        addressAuditDialogPage.save();
        expect(addressAuditDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class AddressAuditComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-address-audit div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class AddressAuditDialogPage {
    modalTitle = element(by.css('h4#myAddressAuditLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    actionInput = element(by.css('input#field_action'));
    actionDateInput = element(by.css('input#field_actionDate'));
    actionUserInput = element(by.css('input#field_actionUser'));
    codeInput = element(by.css('input#field_code'));
    nameInput = element(by.css('input#field_name'));
    locationInput = element(by.css('input#field_location'));
    phoneInput = element(by.css('input#field_phone'));
    faxInput = element(by.css('input#field_fax'));
    emailInput = element(by.css('input#field_email'));
    websiteInput = element(by.css('input#field_website'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setActionInput = function(action) {
        this.actionInput.sendKeys(action);
    };

    getActionInput = function() {
        return this.actionInput.getAttribute('value');
    };

    setActionDateInput = function(actionDate) {
        this.actionDateInput.sendKeys(actionDate);
    };

    getActionDateInput = function() {
        return this.actionDateInput.getAttribute('value');
    };

    setActionUserInput = function(actionUser) {
        this.actionUserInput.sendKeys(actionUser);
    };

    getActionUserInput = function() {
        return this.actionUserInput.getAttribute('value');
    };

    setCodeInput = function(code) {
        this.codeInput.sendKeys(code);
    };

    getCodeInput = function() {
        return this.codeInput.getAttribute('value');
    };

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    setLocationInput = function(location) {
        this.locationInput.sendKeys(location);
    };

    getLocationInput = function() {
        return this.locationInput.getAttribute('value');
    };

    setPhoneInput = function(phone) {
        this.phoneInput.sendKeys(phone);
    };

    getPhoneInput = function() {
        return this.phoneInput.getAttribute('value');
    };

    setFaxInput = function(fax) {
        this.faxInput.sendKeys(fax);
    };

    getFaxInput = function() {
        return this.faxInput.getAttribute('value');
    };

    setEmailInput = function(email) {
        this.emailInput.sendKeys(email);
    };

    getEmailInput = function() {
        return this.emailInput.getAttribute('value');
    };

    setWebsiteInput = function(website) {
        this.websiteInput.sendKeys(website);
    };

    getWebsiteInput = function() {
        return this.websiteInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
