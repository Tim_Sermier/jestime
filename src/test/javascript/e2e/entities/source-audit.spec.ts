import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('SourceAudit e2e test', () => {

    let navBarPage: NavBarPage;
    let sourceAuditDialogPage: SourceAuditDialogPage;
    let sourceAuditComponentsPage: SourceAuditComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load SourceAudits', () => {
        navBarPage.goToEntity('source-audit');
        sourceAuditComponentsPage = new SourceAuditComponentsPage();
        expect(sourceAuditComponentsPage.getTitle())
            .toMatch(/jestimeApp.sourceAudit.home.title/);

    });

    it('should load create SourceAudit dialog', () => {
        sourceAuditComponentsPage.clickOnCreateButton();
        sourceAuditDialogPage = new SourceAuditDialogPage();
        expect(sourceAuditDialogPage.getModalTitle())
            .toMatch(/jestimeApp.sourceAudit.home.createOrEditLabel/);
        sourceAuditDialogPage.close();
    });

    it('should create and save SourceAudits', () => {
        sourceAuditComponentsPage.clickOnCreateButton();
        sourceAuditDialogPage.setActionInput('action');
        expect(sourceAuditDialogPage.getActionInput()).toMatch('action');
        sourceAuditDialogPage.setActionDateInput('2000-12-31');
        expect(sourceAuditDialogPage.getActionDateInput()).toMatch('2000-12-31');
        sourceAuditDialogPage.setActionUserInput('actionUser');
        expect(sourceAuditDialogPage.getActionUserInput()).toMatch('actionUser');
        sourceAuditDialogPage.setTitleInput('title');
        expect(sourceAuditDialogPage.getTitleInput()).toMatch('title');
        sourceAuditDialogPage.setDescriptionInput('description');
        expect(sourceAuditDialogPage.getDescriptionInput()).toMatch('description');
        sourceAuditDialogPage.setUrlInput('url');
        expect(sourceAuditDialogPage.getUrlInput()).toMatch('url');
        sourceAuditDialogPage.setJuridictionIdInput('5');
        expect(sourceAuditDialogPage.getJuridictionIdInput()).toMatch('5');
        sourceAuditDialogPage.save();
        expect(sourceAuditDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class SourceAuditComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-source-audit div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class SourceAuditDialogPage {
    modalTitle = element(by.css('h4#mySourceAuditLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    actionInput = element(by.css('input#field_action'));
    actionDateInput = element(by.css('input#field_actionDate'));
    actionUserInput = element(by.css('input#field_actionUser'));
    titleInput = element(by.css('input#field_title'));
    descriptionInput = element(by.css('textarea#field_description'));
    urlInput = element(by.css('input#field_url'));
    juridictionIdInput = element(by.css('input#field_juridictionId'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setActionInput = function(action) {
        this.actionInput.sendKeys(action);
    };

    getActionInput = function() {
        return this.actionInput.getAttribute('value');
    };

    setActionDateInput = function(actionDate) {
        this.actionDateInput.sendKeys(actionDate);
    };

    getActionDateInput = function() {
        return this.actionDateInput.getAttribute('value');
    };

    setActionUserInput = function(actionUser) {
        this.actionUserInput.sendKeys(actionUser);
    };

    getActionUserInput = function() {
        return this.actionUserInput.getAttribute('value');
    };

    setTitleInput = function(title) {
        this.titleInput.sendKeys(title);
    };

    getTitleInput = function() {
        return this.titleInput.getAttribute('value');
    };

    setDescriptionInput = function(description) {
        this.descriptionInput.sendKeys(description);
    };

    getDescriptionInput = function() {
        return this.descriptionInput.getAttribute('value');
    };

    setUrlInput = function(url) {
        this.urlInput.sendKeys(url);
    };

    getUrlInput = function() {
        return this.urlInput.getAttribute('value');
    };

    setJuridictionIdInput = function(juridictionId) {
        this.juridictionIdInput.sendKeys(juridictionId);
    };

    getJuridictionIdInput = function() {
        return this.juridictionIdInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
