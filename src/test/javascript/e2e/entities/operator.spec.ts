import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Operator e2e test', () => {

    let navBarPage: NavBarPage;
    let operatorDialogPage: OperatorDialogPage;
    let operatorComponentsPage: OperatorComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Operators', () => {
        navBarPage.goToEntity('operator');
        operatorComponentsPage = new OperatorComponentsPage();
        expect(operatorComponentsPage.getTitle())
            .toMatch(/jestimeApp.operator.home.title/);

    });

    it('should load create Operator dialog', () => {
        operatorComponentsPage.clickOnCreateButton();
        operatorDialogPage = new OperatorDialogPage();
        expect(operatorDialogPage.getModalTitle())
            .toMatch(/jestimeApp.operator.home.createOrEditLabel/);
        operatorDialogPage.close();
    });

    it('should create and save Operators', () => {
        operatorComponentsPage.clickOnCreateButton();
        operatorDialogPage.setSymbolInput('symbol');
        expect(operatorDialogPage.getSymbolInput()).toMatch('symbol');
        operatorDialogPage.setNameInput('name');
        expect(operatorDialogPage.getNameInput()).toMatch('name');
        operatorDialogPage.save();
        expect(operatorDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class OperatorComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-operator div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class OperatorDialogPage {
    modalTitle = element(by.css('h4#myOperatorLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    symbolInput = element(by.css('input#field_symbol'));
    nameInput = element(by.css('input#field_name'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setSymbolInput = function(symbol) {
        this.symbolInput.sendKeys(symbol);
    };

    getSymbolInput = function() {
        return this.symbolInput.getAttribute('value');
    };

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
