import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('SocialCard e2e test', () => {

    let navBarPage: NavBarPage;
    let socialCardDialogPage: SocialCardDialogPage;
    let socialCardComponentsPage: SocialCardComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load SocialCards', () => {
        navBarPage.goToEntity('social-card');
        socialCardComponentsPage = new SocialCardComponentsPage();
        expect(socialCardComponentsPage.getTitle())
            .toMatch(/jestimeApp.socialCard.home.title/);

    });

    it('should load create SocialCard dialog', () => {
        socialCardComponentsPage.clickOnCreateButton();
        socialCardDialogPage = new SocialCardDialogPage();
        expect(socialCardDialogPage.getModalTitle())
            .toMatch(/jestimeApp.socialCard.home.createOrEditLabel/);
        socialCardDialogPage.close();
    });

   /* it('should create and save SocialCards', () => {
        socialCardComponentsPage.clickOnCreateButton();
        socialCardDialogPage.setCodeInput('code');
        expect(socialCardDialogPage.getCodeInput()).toMatch('code');
        socialCardDialogPage.setTitleInput('title');
        expect(socialCardDialogPage.getTitleInput()).toMatch('title');
        socialCardDialogPage.setDescriptionInput('description');
        expect(socialCardDialogPage.getDescriptionInput()).toMatch('description');
        socialCardDialogPage.setGeneralInput('general');
        expect(socialCardDialogPage.getGeneralInput()).toMatch('general');
        socialCardDialogPage.setProcedureInput('procedure');
        expect(socialCardDialogPage.getProcedureInput()).toMatch('procedure');
        socialCardDialogPage.setRecourseInput('recourse');
        expect(socialCardDialogPage.getRecourseInput()).toMatch('recourse');
        socialCardDialogPage.setCreationDateInput('2000-12-31');
        expect(socialCardDialogPage.getCreationDateInput()).toMatch('2000-12-31');
        socialCardDialogPage.setEditDateInput('2000-12-31');
        expect(socialCardDialogPage.getEditDateInput()).toMatch('2000-12-31');
        socialCardDialogPage.juridictionSelectLastOption();
        socialCardDialogPage.parentSocialCardSelectLastOption();
        socialCardDialogPage.categorySelectLastOption();
        // socialCardDialogPage.addressesSelectLastOption();
        socialCardDialogPage.creatorSelectLastOption();
        socialCardDialogPage.updatorSelectLastOption();
        // socialCardDialogPage.sourcesSelectLastOption();
        socialCardDialogPage.save();
        expect(socialCardDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });*/

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class SocialCardComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-social-card div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class SocialCardDialogPage {
    modalTitle = element(by.css('h4#mySocialCardLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    codeInput = element(by.css('input#field_code'));
    titleInput = element(by.css('input#field_title'));
    descriptionInput = element(by.css('input#field_description'));
    generalInput = element(by.css('input#field_general'));
    procedureInput = element(by.css('input#field_procedure'));
    recourseInput = element(by.css('input#field_recourse'));
    creationDateInput = element(by.css('input#field_creationDate'));
    editDateInput = element(by.css('input#field_editDate'));
    juridictionSelect = element(by.css('select#field_juridiction'));
    parentSocialCardSelect = element(by.css('select#field_parentSocialCard'));
    categorySelect = element(by.css('select#field_category'));
    addressesSelect = element(by.css('select#field_addresses'));
    creatorSelect = element(by.css('select#field_creator'));
    updatorSelect = element(by.css('select#field_updator'));
    sourcesSelect = element(by.css('select#field_sources'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setCodeInput = function(code) {
        this.codeInput.sendKeys(code);
    };

    getCodeInput = function() {
        return this.codeInput.getAttribute('value');
    };

    setTitleInput = function(title) {
        this.titleInput.sendKeys(title);
    };

    getTitleInput = function() {
        return this.titleInput.getAttribute('value');
    };

    setDescriptionInput = function(description) {
        this.descriptionInput.sendKeys(description);
    };

    getDescriptionInput = function() {
        return this.descriptionInput.getAttribute('value');
    };

    setGeneralInput = function(general) {
        this.generalInput.sendKeys(general);
    };

    getGeneralInput = function() {
        return this.generalInput.getAttribute('value');
    };

    setProcedureInput = function(procedure) {
        this.procedureInput.sendKeys(procedure);
    };

    getProcedureInput = function() {
        return this.procedureInput.getAttribute('value');
    };

    setRecourseInput = function(recourse) {
        this.recourseInput.sendKeys(recourse);
    };

    getRecourseInput = function() {
        return this.recourseInput.getAttribute('value');
    };

    setCreationDateInput = function(creationDate) {
        this.creationDateInput.sendKeys(creationDate);
    };

    getCreationDateInput = function() {
        return this.creationDateInput.getAttribute('value');
    };

    setEditDateInput = function(editDate) {
        this.editDateInput.sendKeys(editDate);
    };

    getEditDateInput = function() {
        return this.editDateInput.getAttribute('value');
    };

    juridictionSelectLastOption = function() {
        this.juridictionSelect.all(by.tagName('option')).last().click();
    };

    juridictionSelectOption = function(option) {
        this.juridictionSelect.sendKeys(option);
    };

    getJuridictionSelect = function() {
        return this.juridictionSelect;
    };

    getJuridictionSelectedOption = function() {
        return this.juridictionSelect.element(by.css('option:checked')).getText();
    };

    parentSocialCardSelectLastOption = function() {
        this.parentSocialCardSelect.all(by.tagName('option')).last().click();
    };

    parentSocialCardSelectOption = function(option) {
        this.parentSocialCardSelect.sendKeys(option);
    };

    getParentSocialCardSelect = function() {
        return this.parentSocialCardSelect;
    };

    getParentSocialCardSelectedOption = function() {
        return this.parentSocialCardSelect.element(by.css('option:checked')).getText();
    };

    categorySelectLastOption = function() {
        this.categorySelect.all(by.tagName('option')).last().click();
    };

    categorySelectOption = function(option) {
        this.categorySelect.sendKeys(option);
    };

    getCategorySelect = function() {
        return this.categorySelect;
    };

    getCategorySelectedOption = function() {
        return this.categorySelect.element(by.css('option:checked')).getText();
    };

    addressesSelectLastOption = function() {
        this.addressesSelect.all(by.tagName('option')).last().click();
    };

    addressesSelectOption = function(option) {
        this.addressesSelect.sendKeys(option);
    };

    getAddressesSelect = function() {
        return this.addressesSelect;
    };

    getAddressesSelectedOption = function() {
        return this.addressesSelect.element(by.css('option:checked')).getText();
    };

    creatorSelectLastOption = function() {
        this.creatorSelect.all(by.tagName('option')).last().click();
    };

    creatorSelectOption = function(option) {
        this.creatorSelect.sendKeys(option);
    };

    getCreatorSelect = function() {
        return this.creatorSelect;
    };

    getCreatorSelectedOption = function() {
        return this.creatorSelect.element(by.css('option:checked')).getText();
    };

    updatorSelectLastOption = function() {
        this.updatorSelect.all(by.tagName('option')).last().click();
    };

    updatorSelectOption = function(option) {
        this.updatorSelect.sendKeys(option);
    };

    getUpdatorSelect = function() {
        return this.updatorSelect;
    };

    getUpdatorSelectedOption = function() {
        return this.updatorSelect.element(by.css('option:checked')).getText();
    };

    sourcesSelectLastOption = function() {
        this.sourcesSelect.all(by.tagName('option')).last().click();
    };

    sourcesSelectOption = function(option) {
        this.sourcesSelect.sendKeys(option);
    };

    getSourcesSelect = function() {
        return this.sourcesSelect;
    };

    getSourcesSelectedOption = function() {
        return this.sourcesSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
