import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('SocialCardAudit e2e test', () => {

    let navBarPage: NavBarPage;
    let socialCardAuditDialogPage: SocialCardAuditDialogPage;
    let socialCardAuditComponentsPage: SocialCardAuditComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load SocialCardAudits', () => {
        navBarPage.goToEntity('social-card-audit');
        socialCardAuditComponentsPage = new SocialCardAuditComponentsPage();
        expect(socialCardAuditComponentsPage.getTitle())
            .toMatch(/jestimeApp.socialCardAudit.home.title/);

    });

    it('should load create SocialCardAudit dialog', () => {
        socialCardAuditComponentsPage.clickOnCreateButton();
        socialCardAuditDialogPage = new SocialCardAuditDialogPage();
        expect(socialCardAuditDialogPage.getModalTitle())
            .toMatch(/jestimeApp.socialCardAudit.home.createOrEditLabel/);
        socialCardAuditDialogPage.close();
    });

    it('should create and save SocialCardAudits', () => {
        socialCardAuditComponentsPage.clickOnCreateButton();
        socialCardAuditDialogPage.setActionInput('action');
        expect(socialCardAuditDialogPage.getActionInput()).toMatch('action');
        socialCardAuditDialogPage.setActionDateInput('2000-12-31');
        expect(socialCardAuditDialogPage.getActionDateInput()).toMatch('2000-12-31');
        socialCardAuditDialogPage.setActionUserInput('actionUser');
        expect(socialCardAuditDialogPage.getActionUserInput()).toMatch('actionUser');
        socialCardAuditDialogPage.setTitleInput('title');
        expect(socialCardAuditDialogPage.getTitleInput()).toMatch('title');
        socialCardAuditDialogPage.setCodeInput('code');
        expect(socialCardAuditDialogPage.getCodeInput()).toMatch('code');
        socialCardAuditDialogPage.setGeneralInput('general');
        expect(socialCardAuditDialogPage.getGeneralInput()).toMatch('general');
        socialCardAuditDialogPage.setDescriptionInput('description');
        expect(socialCardAuditDialogPage.getDescriptionInput()).toMatch('description');
        socialCardAuditDialogPage.setProcedureInput('procedure');
        expect(socialCardAuditDialogPage.getProcedureInput()).toMatch('procedure');
        socialCardAuditDialogPage.setRecourseInput('recourse');
        expect(socialCardAuditDialogPage.getRecourseInput()).toMatch('recourse');
        socialCardAuditDialogPage.setJuridictionIdInput('5');
        expect(socialCardAuditDialogPage.getJuridictionIdInput()).toMatch('5');
        socialCardAuditDialogPage.setCategoryIdInput('5');
        expect(socialCardAuditDialogPage.getCategoryIdInput()).toMatch('5');
        socialCardAuditDialogPage.setParentSocialCardIdInput('5');
        expect(socialCardAuditDialogPage.getParentSocialCardIdInput()).toMatch('5');
        socialCardAuditDialogPage.save();
        expect(socialCardAuditDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class SocialCardAuditComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-social-card-audit div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class SocialCardAuditDialogPage {
    modalTitle = element(by.css('h4#mySocialCardAuditLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    actionInput = element(by.css('input#field_action'));
    actionDateInput = element(by.css('input#field_actionDate'));
    actionUserInput = element(by.css('input#field_actionUser'));
    titleInput = element(by.css('input#field_title'));
    codeInput = element(by.css('input#field_code'));
    generalInput = element(by.css('textarea#field_general'));
    descriptionInput = element(by.css('textarea#field_description'));
    procedureInput = element(by.css('textarea#field_procedure'));
    recourseInput = element(by.css('textarea#field_recourse'));
    juridictionIdInput = element(by.css('input#field_juridictionId'));
    categoryIdInput = element(by.css('input#field_categoryId'));
    parentSocialCardIdInput = element(by.css('input#field_parentSocialCardId'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setActionInput = function(action) {
        this.actionInput.sendKeys(action);
    };

    getActionInput = function() {
        return this.actionInput.getAttribute('value');
    };

    setActionDateInput = function(actionDate) {
        this.actionDateInput.sendKeys(actionDate);
    };

    getActionDateInput = function() {
        return this.actionDateInput.getAttribute('value');
    };

    setActionUserInput = function(actionUser) {
        this.actionUserInput.sendKeys(actionUser);
    };

    getActionUserInput = function() {
        return this.actionUserInput.getAttribute('value');
    };

    setTitleInput = function(title) {
        this.titleInput.sendKeys(title);
    };

    getTitleInput = function() {
        return this.titleInput.getAttribute('value');
    };

    setCodeInput = function(code) {
        this.codeInput.sendKeys(code);
    };

    getCodeInput = function() {
        return this.codeInput.getAttribute('value');
    };

    setGeneralInput = function(general) {
        this.generalInput.sendKeys(general);
    };

    getGeneralInput = function() {
        return this.generalInput.getAttribute('value');
    };

    setDescriptionInput = function(description) {
        this.descriptionInput.sendKeys(description);
    };

    getDescriptionInput = function() {
        return this.descriptionInput.getAttribute('value');
    };

    setProcedureInput = function(procedure) {
        this.procedureInput.sendKeys(procedure);
    };

    getProcedureInput = function() {
        return this.procedureInput.getAttribute('value');
    };

    setRecourseInput = function(recourse) {
        this.recourseInput.sendKeys(recourse);
    };

    getRecourseInput = function() {
        return this.recourseInput.getAttribute('value');
    };

    setJuridictionIdInput = function(juridictionId) {
        this.juridictionIdInput.sendKeys(juridictionId);
    };

    getJuridictionIdInput = function() {
        return this.juridictionIdInput.getAttribute('value');
    };

    setCategoryIdInput = function(categoryId) {
        this.categoryIdInput.sendKeys(categoryId);
    };

    getCategoryIdInput = function() {
        return this.categoryIdInput.getAttribute('value');
    };

    setParentSocialCardIdInput = function(parentSocialCardId) {
        this.parentSocialCardIdInput.sendKeys(parentSocialCardId);
    };

    getParentSocialCardIdInput = function() {
        return this.parentSocialCardIdInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
