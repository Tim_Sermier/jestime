import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Address e2e test', () => {

    let navBarPage: NavBarPage;
    let addressDialogPage: AddressDialogPage;
    let addressComponentsPage: AddressComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Addresses', () => {
        navBarPage.goToEntity('address');
        addressComponentsPage = new AddressComponentsPage();
        expect(addressComponentsPage.getTitle())
            .toMatch(/jestimeApp.address.home.title/);

    });

    it('should load create Address dialog', () => {
        addressComponentsPage.clickOnCreateButton();
        addressDialogPage = new AddressDialogPage();
        expect(addressDialogPage.getModalTitle())
            .toMatch(/jestimeApp.address.home.createOrEditLabel/);
        addressDialogPage.close();
    });

    it('should create and save Addresses', () => {
        addressComponentsPage.clickOnCreateButton();
        addressDialogPage.setCodeInput('code');
        expect(addressDialogPage.getCodeInput()).toMatch('code');
        addressDialogPage.setNameInput('name');
        expect(addressDialogPage.getNameInput()).toMatch('name');
        addressDialogPage.setLocationInput('location');
        expect(addressDialogPage.getLocationInput()).toMatch('location');
        addressDialogPage.setPhoneInput('phone');
        expect(addressDialogPage.getPhoneInput()).toMatch('phone');
        addressDialogPage.setFaxInput('fax');
        expect(addressDialogPage.getFaxInput()).toMatch('fax');
        addressDialogPage.setEmailInput('email');
        expect(addressDialogPage.getEmailInput()).toMatch('email');
        addressDialogPage.setWebsiteInput('website');
        expect(addressDialogPage.getWebsiteInput()).toMatch('website');
        addressDialogPage.setCreationDateInput('2000-12-31');
        expect(addressDialogPage.getCreationDateInput()).toMatch('2000-12-31');
        addressDialogPage.setEditionDateInput('2000-12-31');
        expect(addressDialogPage.getEditionDateInput()).toMatch('2000-12-31');
        addressDialogPage.creatorSelectLastOption();
        addressDialogPage.updatorSelectLastOption();
        addressDialogPage.save();
        expect(addressDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class AddressComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-address div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class AddressDialogPage {
    modalTitle = element(by.css('h4#myAddressLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    codeInput = element(by.css('input#field_code'));
    nameInput = element(by.css('input#field_name'));
    locationInput = element(by.css('input#field_location'));
    phoneInput = element(by.css('input#field_phone'));
    faxInput = element(by.css('input#field_fax'));
    emailInput = element(by.css('input#field_email'));
    websiteInput = element(by.css('input#field_website'));
    creationDateInput = element(by.css('input#field_creationDate'));
    editionDateInput = element(by.css('input#field_editionDate'));
    creatorSelect = element(by.css('select#field_creator'));
    updatorSelect = element(by.css('select#field_updator'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setCodeInput = function(code) {
        this.codeInput.sendKeys(code);
    };

    getCodeInput = function() {
        return this.codeInput.getAttribute('value');
    };

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    setLocationInput = function(location) {
        this.locationInput.sendKeys(location);
    };

    getLocationInput = function() {
        return this.locationInput.getAttribute('value');
    };

    setPhoneInput = function(phone) {
        this.phoneInput.sendKeys(phone);
    };

    getPhoneInput = function() {
        return this.phoneInput.getAttribute('value');
    };

    setFaxInput = function(fax) {
        this.faxInput.sendKeys(fax);
    };

    getFaxInput = function() {
        return this.faxInput.getAttribute('value');
    };

    setEmailInput = function(email) {
        this.emailInput.sendKeys(email);
    };

    getEmailInput = function() {
        return this.emailInput.getAttribute('value');
    };

    setWebsiteInput = function(website) {
        this.websiteInput.sendKeys(website);
    };

    getWebsiteInput = function() {
        return this.websiteInput.getAttribute('value');
    };

    setCreationDateInput = function(creationDate) {
        this.creationDateInput.sendKeys(creationDate);
    };

    getCreationDateInput = function() {
        return this.creationDateInput.getAttribute('value');
    };

    setEditionDateInput = function(editionDate) {
        this.editionDateInput.sendKeys(editionDate);
    };

    getEditionDateInput = function() {
        return this.editionDateInput.getAttribute('value');
    };

    creatorSelectLastOption = function() {
        this.creatorSelect.all(by.tagName('option')).last().click();
    };

    creatorSelectOption = function(option) {
        this.creatorSelect.sendKeys(option);
    };

    getCreatorSelect = function() {
        return this.creatorSelect;
    };

    getCreatorSelectedOption = function() {
        return this.creatorSelect.element(by.css('option:checked')).getText();
    };

    updatorSelectLastOption = function() {
        this.updatorSelect.all(by.tagName('option')).last().click();
    };

    updatorSelectOption = function(option) {
        this.updatorSelect.sendKeys(option);
    };

    getUpdatorSelect = function() {
        return this.updatorSelect;
    };

    getUpdatorSelectedOption = function() {
        return this.updatorSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
