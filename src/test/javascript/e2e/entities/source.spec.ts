import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Source e2e test', () => {

    let navBarPage: NavBarPage;
    let sourceDialogPage: SourceDialogPage;
    let sourceComponentsPage: SourceComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Sources', () => {
        navBarPage.goToEntity('source');
        sourceComponentsPage = new SourceComponentsPage();
        expect(sourceComponentsPage.getTitle())
            .toMatch(/jestimeApp.source.home.title/);

    });

    it('should load create Source dialog', () => {
        sourceComponentsPage.clickOnCreateButton();
        sourceDialogPage = new SourceDialogPage();
        expect(sourceDialogPage.getModalTitle())
            .toMatch(/jestimeApp.source.home.createOrEditLabel/);
        sourceDialogPage.close();
    });

   /* it('should create and save Sources', () => {
        sourceComponentsPage.clickOnCreateButton();
        sourceDialogPage.setCodeInput('code');
        expect(sourceDialogPage.getCodeInput()).toMatch('code');
        sourceDialogPage.setTitleInput('title');
        expect(sourceDialogPage.getTitleInput()).toMatch('title');
        sourceDialogPage.setDescriptionInput('description');
        expect(sourceDialogPage.getDescriptionInput()).toMatch('description');
        sourceDialogPage.setUrlInput('url');
        expect(sourceDialogPage.getUrlInput()).toMatch('url');
        sourceDialogPage.setCreationDateInput('2000-12-31');
        expect(sourceDialogPage.getCreationDateInput()).toMatch('2000-12-31');
        sourceDialogPage.setEditionDateInput('2000-12-31');
        expect(sourceDialogPage.getEditionDateInput()).toMatch('2000-12-31');
        sourceDialogPage.juridictionSelectLastOption();
        sourceDialogPage.creatorSelectLastOption();
        sourceDialogPage.updatorSelectLastOption();
        sourceDialogPage.save();
        expect(sourceDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });*/

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class SourceComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-source div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class SourceDialogPage {
    modalTitle = element(by.css('h4#mySourceLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    codeInput = element(by.css('input#field_code'));
    titleInput = element(by.css('input#field_title'));
    descriptionInput = element(by.css('textarea#field_description'));
    urlInput = element(by.css('input#field_url'));
    creationDateInput = element(by.css('input#field_creationDate'));
    editionDateInput = element(by.css('input#field_editionDate'));
    juridictionSelect = element(by.css('select#field_juridiction'));
    creatorSelect = element(by.css('select#field_creator'));
    updatorSelect = element(by.css('select#field_updator'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setCodeInput = function(code) {
        this.codeInput.sendKeys(code);
    };

    getCodeInput = function() {
        return this.codeInput.getAttribute('value');
    };

    setTitleInput = function(title) {
        this.titleInput.sendKeys(title);
    };

    getTitleInput = function() {
        return this.titleInput.getAttribute('value');
    };

    setDescriptionInput = function(description) {
        this.descriptionInput.sendKeys(description);
    };

    getDescriptionInput = function() {
        return this.descriptionInput.getAttribute('value');
    };

    setUrlInput = function(url) {
        this.urlInput.sendKeys(url);
    };

    getUrlInput = function() {
        return this.urlInput.getAttribute('value');
    };

    setCreationDateInput = function(creationDate) {
        this.creationDateInput.sendKeys(creationDate);
    };

    getCreationDateInput = function() {
        return this.creationDateInput.getAttribute('value');
    };

    setEditionDateInput = function(editionDate) {
        this.editionDateInput.sendKeys(editionDate);
    };

    getEditionDateInput = function() {
        return this.editionDateInput.getAttribute('value');
    };

    juridictionSelectLastOption = function() {
        this.juridictionSelect.all(by.tagName('option')).last().click();
    };

    juridictionSelectOption = function(option) {
        this.juridictionSelect.sendKeys(option);
    };

    getJuridictionSelect = function() {
        return this.juridictionSelect;
    };

    getJuridictionSelectedOption = function() {
        return this.juridictionSelect.element(by.css('option:checked')).getText();
    };

    creatorSelectLastOption = function() {
        this.creatorSelect.all(by.tagName('option')).last().click();
    };

    creatorSelectOption = function(option) {
        this.creatorSelect.sendKeys(option);
    };

    getCreatorSelect = function() {
        return this.creatorSelect;
    };

    getCreatorSelectedOption = function() {
        return this.creatorSelect.element(by.css('option:checked')).getText();
    };

    updatorSelectLastOption = function() {
        this.updatorSelect.all(by.tagName('option')).last().click();
    };

    updatorSelectOption = function(option) {
        this.updatorSelect.sendKeys(option);
    };

    getUpdatorSelect = function() {
        return this.updatorSelect;
    };

    getUpdatorSelectedOption = function() {
        return this.updatorSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
