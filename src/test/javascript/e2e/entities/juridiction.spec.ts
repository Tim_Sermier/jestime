import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Juridiction e2e test', () => {

    let navBarPage: NavBarPage;
    let juridictionDialogPage: JuridictionDialogPage;
    let juridictionComponentsPage: JuridictionComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Juridictions', () => {
        navBarPage.goToEntity('juridiction');
        juridictionComponentsPage = new JuridictionComponentsPage();
        expect(juridictionComponentsPage.getTitle())
            .toMatch(/jestimeApp.juridiction.home.title/);

    });

    it('should load create Juridiction dialog', () => {
        juridictionComponentsPage.clickOnCreateButton();
        juridictionDialogPage = new JuridictionDialogPage();
        expect(juridictionDialogPage.getModalTitle())
            .toMatch(/jestimeApp.juridiction.home.createOrEditLabel/);
        juridictionDialogPage.close();
    });

    it('should create and save Juridictions', () => {
        juridictionComponentsPage.clickOnCreateButton();
        juridictionDialogPage.setCodeInput('code');
        expect(juridictionDialogPage.getCodeInput()).toMatch('code');
        juridictionDialogPage.setNameInput('name');
        expect(juridictionDialogPage.getNameInput()).toMatch('name');
        juridictionDialogPage.setImageInput('image');
        expect(juridictionDialogPage.getImageInput()).toMatch('image');
        juridictionDialogPage.save();
        expect(juridictionDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class JuridictionComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-juridiction div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class JuridictionDialogPage {
    modalTitle = element(by.css('h4#myJuridictionLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    codeInput = element(by.css('input#field_code'));
    nameInput = element(by.css('input#field_name'));
    imageInput = element(by.css('input#field_image'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setCodeInput = function(code) {
        this.codeInput.sendKeys(code);
    };

    getCodeInput = function() {
        return this.codeInput.getAttribute('value');
    };

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    setImageInput = function(image) {
        this.imageInput.sendKeys(image);
    };

    getImageInput = function() {
        return this.imageInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
