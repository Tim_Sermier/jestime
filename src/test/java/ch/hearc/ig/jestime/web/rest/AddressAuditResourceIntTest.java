package ch.hearc.ig.jestime.web.rest;

import ch.hearc.ig.jestime.JestimeApp;

import ch.hearc.ig.jestime.domain.AddressAudit;
import ch.hearc.ig.jestime.repository.AddressAuditRepository;
import ch.hearc.ig.jestime.service.AddressAuditService;
import ch.hearc.ig.jestime.service.dto.AddressAuditDTO;
import ch.hearc.ig.jestime.service.mapper.AddressAuditMapper;
import ch.hearc.ig.jestime.web.rest.errors.ExceptionTranslator;
import ch.hearc.ig.jestime.service.dto.AddressAuditCriteria;
import ch.hearc.ig.jestime.service.AddressAuditQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static ch.hearc.ig.jestime.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AddressAuditResource REST controller.
 *
 * @see AddressAuditResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JestimeApp.class)
public class AddressAuditResourceIntTest {

    private static final String DEFAULT_ACTION = "AAAAAAAAAA";
    private static final String UPDATED_ACTION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_ACTION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ACTION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_ACTION_USER = 1;
    private static final Integer UPDATED_ACTION_USER = 2;

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_FAX = "AAAAAAAAAA";
    private static final String UPDATED_FAX = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_WEBSITE = "AAAAAAAAAA";
    private static final String UPDATED_WEBSITE = "BBBBBBBBBB";

    @Autowired
    private AddressAuditRepository addressAuditRepository;

    @Autowired
    private AddressAuditMapper addressAuditMapper;

    @Autowired
    private AddressAuditService addressAuditService;

    @Autowired
    private AddressAuditQueryService addressAuditQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAddressAuditMockMvc;

    private AddressAudit addressAudit;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AddressAuditResource addressAuditResource = new AddressAuditResource(addressAuditService, addressAuditQueryService);
        this.restAddressAuditMockMvc = MockMvcBuilders.standaloneSetup(addressAuditResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AddressAudit createEntity(EntityManager em) {
        AddressAudit addressAudit = new AddressAudit()
            .action(DEFAULT_ACTION)
            .actionDate(DEFAULT_ACTION_DATE)
            .actionUser(DEFAULT_ACTION_USER)
            .code(DEFAULT_CODE)
            .name(DEFAULT_NAME)
            .location(DEFAULT_LOCATION)
            .phone(DEFAULT_PHONE)
            .fax(DEFAULT_FAX)
            .email(DEFAULT_EMAIL)
            .website(DEFAULT_WEBSITE);
        return addressAudit;
    }

    @Before
    public void initTest() {
        addressAudit = createEntity(em);
    }

    @Test
    @Transactional
    public void createAddressAudit() throws Exception {
        int databaseSizeBeforeCreate = addressAuditRepository.findAll().size();

        // Create the AddressAudit
        AddressAuditDTO addressAuditDTO = addressAuditMapper.toDto(addressAudit);
        restAddressAuditMockMvc.perform(post("/api/address-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressAuditDTO)))
            .andExpect(status().isCreated());

        // Validate the AddressAudit in the database
        List<AddressAudit> addressAuditList = addressAuditRepository.findAll();
        assertThat(addressAuditList).hasSize(databaseSizeBeforeCreate + 1);
        AddressAudit testAddressAudit = addressAuditList.get(addressAuditList.size() - 1);
        assertThat(testAddressAudit.getAction()).isEqualTo(DEFAULT_ACTION);
        assertThat(testAddressAudit.getActionDate()).isEqualTo(DEFAULT_ACTION_DATE);
        assertThat(testAddressAudit.getActionUser()).isEqualTo(DEFAULT_ACTION_USER);
        assertThat(testAddressAudit.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testAddressAudit.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testAddressAudit.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testAddressAudit.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testAddressAudit.getFax()).isEqualTo(DEFAULT_FAX);
        assertThat(testAddressAudit.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testAddressAudit.getWebsite()).isEqualTo(DEFAULT_WEBSITE);
    }

    @Test
    @Transactional
    public void createAddressAuditWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = addressAuditRepository.findAll().size();

        // Create the AddressAudit with an existing ID
        addressAudit.setId(1L);
        AddressAuditDTO addressAuditDTO = addressAuditMapper.toDto(addressAudit);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAddressAuditMockMvc.perform(post("/api/address-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressAuditDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AddressAudit in the database
        List<AddressAudit> addressAuditList = addressAuditRepository.findAll();
        assertThat(addressAuditList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkActionIsRequired() throws Exception {
        int databaseSizeBeforeTest = addressAuditRepository.findAll().size();
        // set the field null
        addressAudit.setAction(null);

        // Create the AddressAudit, which fails.
        AddressAuditDTO addressAuditDTO = addressAuditMapper.toDto(addressAudit);

        restAddressAuditMockMvc.perform(post("/api/address-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressAuditDTO)))
            .andExpect(status().isBadRequest());

        List<AddressAudit> addressAuditList = addressAuditRepository.findAll();
        assertThat(addressAuditList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkActionDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = addressAuditRepository.findAll().size();
        // set the field null
        addressAudit.setActionDate(null);

        // Create the AddressAudit, which fails.
        AddressAuditDTO addressAuditDTO = addressAuditMapper.toDto(addressAudit);

        restAddressAuditMockMvc.perform(post("/api/address-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressAuditDTO)))
            .andExpect(status().isBadRequest());

        List<AddressAudit> addressAuditList = addressAuditRepository.findAll();
        assertThat(addressAuditList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkActionUserIsRequired() throws Exception {
        int databaseSizeBeforeTest = addressAuditRepository.findAll().size();
        // set the field null
        addressAudit.setActionUser(null);

        // Create the AddressAudit, which fails.
        AddressAuditDTO addressAuditDTO = addressAuditMapper.toDto(addressAudit);

        restAddressAuditMockMvc.perform(post("/api/address-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressAuditDTO)))
            .andExpect(status().isBadRequest());

        List<AddressAudit> addressAuditList = addressAuditRepository.findAll();
        assertThat(addressAuditList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAddressAudits() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList
        restAddressAuditMockMvc.perform(get("/api/address-audits?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(addressAudit.getId().intValue())))
            .andExpect(jsonPath("$.[*].action").value(hasItem(DEFAULT_ACTION.toString())))
            .andExpect(jsonPath("$.[*].actionDate").value(hasItem(DEFAULT_ACTION_DATE.toString())))
            .andExpect(jsonPath("$.[*].actionUser").value(hasItem(DEFAULT_ACTION_USER)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].website").value(hasItem(DEFAULT_WEBSITE.toString())));
    }

    @Test
    @Transactional
    public void getAddressAudit() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get the addressAudit
        restAddressAuditMockMvc.perform(get("/api/address-audits/{id}", addressAudit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(addressAudit.getId().intValue()))
            .andExpect(jsonPath("$.action").value(DEFAULT_ACTION.toString()))
            .andExpect(jsonPath("$.actionDate").value(DEFAULT_ACTION_DATE.toString()))
            .andExpect(jsonPath("$.actionUser").value(DEFAULT_ACTION_USER))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()))
            .andExpect(jsonPath("$.fax").value(DEFAULT_FAX.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.website").value(DEFAULT_WEBSITE.toString()));
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByActionIsEqualToSomething() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where action equals to DEFAULT_ACTION
        defaultAddressAuditShouldBeFound("action.equals=" + DEFAULT_ACTION);

        // Get all the addressAuditList where action equals to UPDATED_ACTION
        defaultAddressAuditShouldNotBeFound("action.equals=" + UPDATED_ACTION);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByActionIsInShouldWork() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where action in DEFAULT_ACTION or UPDATED_ACTION
        defaultAddressAuditShouldBeFound("action.in=" + DEFAULT_ACTION + "," + UPDATED_ACTION);

        // Get all the addressAuditList where action equals to UPDATED_ACTION
        defaultAddressAuditShouldNotBeFound("action.in=" + UPDATED_ACTION);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByActionIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where action is not null
        defaultAddressAuditShouldBeFound("action.specified=true");

        // Get all the addressAuditList where action is null
        defaultAddressAuditShouldNotBeFound("action.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByActionDateIsEqualToSomething() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where actionDate equals to DEFAULT_ACTION_DATE
        defaultAddressAuditShouldBeFound("actionDate.equals=" + DEFAULT_ACTION_DATE);

        // Get all the addressAuditList where actionDate equals to UPDATED_ACTION_DATE
        defaultAddressAuditShouldNotBeFound("actionDate.equals=" + UPDATED_ACTION_DATE);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByActionDateIsInShouldWork() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where actionDate in DEFAULT_ACTION_DATE or UPDATED_ACTION_DATE
        defaultAddressAuditShouldBeFound("actionDate.in=" + DEFAULT_ACTION_DATE + "," + UPDATED_ACTION_DATE);

        // Get all the addressAuditList where actionDate equals to UPDATED_ACTION_DATE
        defaultAddressAuditShouldNotBeFound("actionDate.in=" + UPDATED_ACTION_DATE);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByActionDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where actionDate is not null
        defaultAddressAuditShouldBeFound("actionDate.specified=true");

        // Get all the addressAuditList where actionDate is null
        defaultAddressAuditShouldNotBeFound("actionDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByActionDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where actionDate greater than or equals to DEFAULT_ACTION_DATE
        defaultAddressAuditShouldBeFound("actionDate.greaterOrEqualThan=" + DEFAULT_ACTION_DATE);

        // Get all the addressAuditList where actionDate greater than or equals to UPDATED_ACTION_DATE
        defaultAddressAuditShouldNotBeFound("actionDate.greaterOrEqualThan=" + UPDATED_ACTION_DATE);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByActionDateIsLessThanSomething() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where actionDate less than or equals to DEFAULT_ACTION_DATE
        defaultAddressAuditShouldNotBeFound("actionDate.lessThan=" + DEFAULT_ACTION_DATE);

        // Get all the addressAuditList where actionDate less than or equals to UPDATED_ACTION_DATE
        defaultAddressAuditShouldBeFound("actionDate.lessThan=" + UPDATED_ACTION_DATE);
    }


    @Test
    @Transactional
    public void getAllAddressAuditsByActionUserIsEqualToSomething() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where actionUser equals to DEFAULT_ACTION_USER
        defaultAddressAuditShouldBeFound("actionUser.equals=" + DEFAULT_ACTION_USER);

        // Get all the addressAuditList where actionUser equals to UPDATED_ACTION_USER
        defaultAddressAuditShouldNotBeFound("actionUser.equals=" + UPDATED_ACTION_USER);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByActionUserIsInShouldWork() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where actionUser in DEFAULT_ACTION_USER or UPDATED_ACTION_USER
        defaultAddressAuditShouldBeFound("actionUser.in=" + DEFAULT_ACTION_USER + "," + UPDATED_ACTION_USER);

        // Get all the addressAuditList where actionUser equals to UPDATED_ACTION_USER
        defaultAddressAuditShouldNotBeFound("actionUser.in=" + UPDATED_ACTION_USER);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByActionUserIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where actionUser is not null
        defaultAddressAuditShouldBeFound("actionUser.specified=true");

        // Get all the addressAuditList where actionUser is null
        defaultAddressAuditShouldNotBeFound("actionUser.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByActionUserIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where actionUser greater than or equals to DEFAULT_ACTION_USER
        defaultAddressAuditShouldBeFound("actionUser.greaterOrEqualThan=" + DEFAULT_ACTION_USER);

        // Get all the addressAuditList where actionUser greater than or equals to UPDATED_ACTION_USER
        defaultAddressAuditShouldNotBeFound("actionUser.greaterOrEqualThan=" + UPDATED_ACTION_USER);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByActionUserIsLessThanSomething() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where actionUser less than or equals to DEFAULT_ACTION_USER
        defaultAddressAuditShouldNotBeFound("actionUser.lessThan=" + DEFAULT_ACTION_USER);

        // Get all the addressAuditList where actionUser less than or equals to UPDATED_ACTION_USER
        defaultAddressAuditShouldBeFound("actionUser.lessThan=" + UPDATED_ACTION_USER);
    }


    @Test
    @Transactional
    public void getAllAddressAuditsByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where code equals to DEFAULT_CODE
        defaultAddressAuditShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the addressAuditList where code equals to UPDATED_CODE
        defaultAddressAuditShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where code in DEFAULT_CODE or UPDATED_CODE
        defaultAddressAuditShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the addressAuditList where code equals to UPDATED_CODE
        defaultAddressAuditShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where code is not null
        defaultAddressAuditShouldBeFound("code.specified=true");

        // Get all the addressAuditList where code is null
        defaultAddressAuditShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where name equals to DEFAULT_NAME
        defaultAddressAuditShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the addressAuditList where name equals to UPDATED_NAME
        defaultAddressAuditShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where name in DEFAULT_NAME or UPDATED_NAME
        defaultAddressAuditShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the addressAuditList where name equals to UPDATED_NAME
        defaultAddressAuditShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where name is not null
        defaultAddressAuditShouldBeFound("name.specified=true");

        // Get all the addressAuditList where name is null
        defaultAddressAuditShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByLocationIsEqualToSomething() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where location equals to DEFAULT_LOCATION
        defaultAddressAuditShouldBeFound("location.equals=" + DEFAULT_LOCATION);

        // Get all the addressAuditList where location equals to UPDATED_LOCATION
        defaultAddressAuditShouldNotBeFound("location.equals=" + UPDATED_LOCATION);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByLocationIsInShouldWork() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where location in DEFAULT_LOCATION or UPDATED_LOCATION
        defaultAddressAuditShouldBeFound("location.in=" + DEFAULT_LOCATION + "," + UPDATED_LOCATION);

        // Get all the addressAuditList where location equals to UPDATED_LOCATION
        defaultAddressAuditShouldNotBeFound("location.in=" + UPDATED_LOCATION);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByLocationIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where location is not null
        defaultAddressAuditShouldBeFound("location.specified=true");

        // Get all the addressAuditList where location is null
        defaultAddressAuditShouldNotBeFound("location.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByPhoneIsEqualToSomething() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where phone equals to DEFAULT_PHONE
        defaultAddressAuditShouldBeFound("phone.equals=" + DEFAULT_PHONE);

        // Get all the addressAuditList where phone equals to UPDATED_PHONE
        defaultAddressAuditShouldNotBeFound("phone.equals=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByPhoneIsInShouldWork() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where phone in DEFAULT_PHONE or UPDATED_PHONE
        defaultAddressAuditShouldBeFound("phone.in=" + DEFAULT_PHONE + "," + UPDATED_PHONE);

        // Get all the addressAuditList where phone equals to UPDATED_PHONE
        defaultAddressAuditShouldNotBeFound("phone.in=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByPhoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where phone is not null
        defaultAddressAuditShouldBeFound("phone.specified=true");

        // Get all the addressAuditList where phone is null
        defaultAddressAuditShouldNotBeFound("phone.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByFaxIsEqualToSomething() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where fax equals to DEFAULT_FAX
        defaultAddressAuditShouldBeFound("fax.equals=" + DEFAULT_FAX);

        // Get all the addressAuditList where fax equals to UPDATED_FAX
        defaultAddressAuditShouldNotBeFound("fax.equals=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByFaxIsInShouldWork() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where fax in DEFAULT_FAX or UPDATED_FAX
        defaultAddressAuditShouldBeFound("fax.in=" + DEFAULT_FAX + "," + UPDATED_FAX);

        // Get all the addressAuditList where fax equals to UPDATED_FAX
        defaultAddressAuditShouldNotBeFound("fax.in=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByFaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where fax is not null
        defaultAddressAuditShouldBeFound("fax.specified=true");

        // Get all the addressAuditList where fax is null
        defaultAddressAuditShouldNotBeFound("fax.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where email equals to DEFAULT_EMAIL
        defaultAddressAuditShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the addressAuditList where email equals to UPDATED_EMAIL
        defaultAddressAuditShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultAddressAuditShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the addressAuditList where email equals to UPDATED_EMAIL
        defaultAddressAuditShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where email is not null
        defaultAddressAuditShouldBeFound("email.specified=true");

        // Get all the addressAuditList where email is null
        defaultAddressAuditShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByWebsiteIsEqualToSomething() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where website equals to DEFAULT_WEBSITE
        defaultAddressAuditShouldBeFound("website.equals=" + DEFAULT_WEBSITE);

        // Get all the addressAuditList where website equals to UPDATED_WEBSITE
        defaultAddressAuditShouldNotBeFound("website.equals=" + UPDATED_WEBSITE);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByWebsiteIsInShouldWork() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where website in DEFAULT_WEBSITE or UPDATED_WEBSITE
        defaultAddressAuditShouldBeFound("website.in=" + DEFAULT_WEBSITE + "," + UPDATED_WEBSITE);

        // Get all the addressAuditList where website equals to UPDATED_WEBSITE
        defaultAddressAuditShouldNotBeFound("website.in=" + UPDATED_WEBSITE);
    }

    @Test
    @Transactional
    public void getAllAddressAuditsByWebsiteIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);

        // Get all the addressAuditList where website is not null
        defaultAddressAuditShouldBeFound("website.specified=true");

        // Get all the addressAuditList where website is null
        defaultAddressAuditShouldNotBeFound("website.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultAddressAuditShouldBeFound(String filter) throws Exception {
        restAddressAuditMockMvc.perform(get("/api/address-audits?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(addressAudit.getId().intValue())))
            .andExpect(jsonPath("$.[*].action").value(hasItem(DEFAULT_ACTION.toString())))
            .andExpect(jsonPath("$.[*].actionDate").value(hasItem(DEFAULT_ACTION_DATE.toString())))
            .andExpect(jsonPath("$.[*].actionUser").value(hasItem(DEFAULT_ACTION_USER)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].website").value(hasItem(DEFAULT_WEBSITE.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultAddressAuditShouldNotBeFound(String filter) throws Exception {
        restAddressAuditMockMvc.perform(get("/api/address-audits?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingAddressAudit() throws Exception {
        // Get the addressAudit
        restAddressAuditMockMvc.perform(get("/api/address-audits/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAddressAudit() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);
        int databaseSizeBeforeUpdate = addressAuditRepository.findAll().size();

        // Update the addressAudit
        AddressAudit updatedAddressAudit = addressAuditRepository.findOne(addressAudit.getId());
        // Disconnect from session so that the updates on updatedAddressAudit are not directly saved in db
        em.detach(updatedAddressAudit);
        updatedAddressAudit
            .action(UPDATED_ACTION)
            .actionDate(UPDATED_ACTION_DATE)
            .actionUser(UPDATED_ACTION_USER)
            .code(UPDATED_CODE)
            .name(UPDATED_NAME)
            .location(UPDATED_LOCATION)
            .phone(UPDATED_PHONE)
            .fax(UPDATED_FAX)
            .email(UPDATED_EMAIL)
            .website(UPDATED_WEBSITE);
        AddressAuditDTO addressAuditDTO = addressAuditMapper.toDto(updatedAddressAudit);

        restAddressAuditMockMvc.perform(put("/api/address-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressAuditDTO)))
            .andExpect(status().isOk());

        // Validate the AddressAudit in the database
        List<AddressAudit> addressAuditList = addressAuditRepository.findAll();
        assertThat(addressAuditList).hasSize(databaseSizeBeforeUpdate);
        AddressAudit testAddressAudit = addressAuditList.get(addressAuditList.size() - 1);
        assertThat(testAddressAudit.getAction()).isEqualTo(UPDATED_ACTION);
        assertThat(testAddressAudit.getActionDate()).isEqualTo(UPDATED_ACTION_DATE);
        assertThat(testAddressAudit.getActionUser()).isEqualTo(UPDATED_ACTION_USER);
        assertThat(testAddressAudit.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testAddressAudit.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testAddressAudit.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testAddressAudit.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testAddressAudit.getFax()).isEqualTo(UPDATED_FAX);
        assertThat(testAddressAudit.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testAddressAudit.getWebsite()).isEqualTo(UPDATED_WEBSITE);
    }

    @Test
    @Transactional
    public void updateNonExistingAddressAudit() throws Exception {
        int databaseSizeBeforeUpdate = addressAuditRepository.findAll().size();

        // Create the AddressAudit
        AddressAuditDTO addressAuditDTO = addressAuditMapper.toDto(addressAudit);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAddressAuditMockMvc.perform(put("/api/address-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressAuditDTO)))
            .andExpect(status().isCreated());

        // Validate the AddressAudit in the database
        List<AddressAudit> addressAuditList = addressAuditRepository.findAll();
        assertThat(addressAuditList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAddressAudit() throws Exception {
        // Initialize the database
        addressAuditRepository.saveAndFlush(addressAudit);
        int databaseSizeBeforeDelete = addressAuditRepository.findAll().size();

        // Get the addressAudit
        restAddressAuditMockMvc.perform(delete("/api/address-audits/{id}", addressAudit.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AddressAudit> addressAuditList = addressAuditRepository.findAll();
        assertThat(addressAuditList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AddressAudit.class);
        AddressAudit addressAudit1 = new AddressAudit();
        addressAudit1.setId(1L);
        AddressAudit addressAudit2 = new AddressAudit();
        addressAudit2.setId(addressAudit1.getId());
        assertThat(addressAudit1).isEqualTo(addressAudit2);
        addressAudit2.setId(2L);
        assertThat(addressAudit1).isNotEqualTo(addressAudit2);
        addressAudit1.setId(null);
        assertThat(addressAudit1).isNotEqualTo(addressAudit2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AddressAuditDTO.class);
        AddressAuditDTO addressAuditDTO1 = new AddressAuditDTO();
        addressAuditDTO1.setId(1L);
        AddressAuditDTO addressAuditDTO2 = new AddressAuditDTO();
        assertThat(addressAuditDTO1).isNotEqualTo(addressAuditDTO2);
        addressAuditDTO2.setId(addressAuditDTO1.getId());
        assertThat(addressAuditDTO1).isEqualTo(addressAuditDTO2);
        addressAuditDTO2.setId(2L);
        assertThat(addressAuditDTO1).isNotEqualTo(addressAuditDTO2);
        addressAuditDTO1.setId(null);
        assertThat(addressAuditDTO1).isNotEqualTo(addressAuditDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(addressAuditMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(addressAuditMapper.fromId(null)).isNull();
    }
}
