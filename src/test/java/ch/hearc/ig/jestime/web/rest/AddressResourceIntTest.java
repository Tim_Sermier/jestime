package ch.hearc.ig.jestime.web.rest;

import ch.hearc.ig.jestime.JestimeApp;

import ch.hearc.ig.jestime.domain.Address;
import ch.hearc.ig.jestime.domain.SocialCard;
import ch.hearc.ig.jestime.domain.User;
import ch.hearc.ig.jestime.domain.User;
import ch.hearc.ig.jestime.repository.AddressRepository;
import ch.hearc.ig.jestime.service.AddressService;
import ch.hearc.ig.jestime.service.dto.AddressDTO;
import ch.hearc.ig.jestime.service.mapper.AddressMapper;
import ch.hearc.ig.jestime.web.rest.errors.ExceptionTranslator;
import ch.hearc.ig.jestime.service.dto.AddressCriteria;
import ch.hearc.ig.jestime.service.AddressQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static ch.hearc.ig.jestime.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AddressResource REST controller.
 *
 * @see AddressResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JestimeApp.class)
public class AddressResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_FAX = "AAAAAAAAAA";
    private static final String UPDATED_FAX = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_WEBSITE = "AAAAAAAAAA";
    private static final String UPDATED_WEBSITE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_EDITION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EDITION_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private AddressMapper addressMapper;

    @Autowired
    private AddressService addressService;

    @Autowired
    private AddressQueryService addressQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAddressMockMvc;

    private Address address;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AddressResource addressResource = new AddressResource(addressService, addressQueryService);
        this.restAddressMockMvc = MockMvcBuilders.standaloneSetup(addressResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Address createEntity(EntityManager em) {
        Address address = new Address()
            .code(DEFAULT_CODE)
            .name(DEFAULT_NAME)
            .location(DEFAULT_LOCATION)
            .phone(DEFAULT_PHONE)
            .fax(DEFAULT_FAX)
            .email(DEFAULT_EMAIL)
            .website(DEFAULT_WEBSITE)
            .creationDate(DEFAULT_CREATION_DATE)
            .editionDate(DEFAULT_EDITION_DATE);
        return address;
    }

    @Before
    public void initTest() {
        address = createEntity(em);
    }

    @Test
    @Transactional
    public void createAddress() throws Exception {
        int databaseSizeBeforeCreate = addressRepository.findAll().size();

        // Create the Address
        AddressDTO addressDTO = addressMapper.toDto(address);
        restAddressMockMvc.perform(post("/api/addresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isCreated());

        // Validate the Address in the database
        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeCreate + 1);
        Address testAddress = addressList.get(addressList.size() - 1);
        assertThat(testAddress.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testAddress.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testAddress.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testAddress.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testAddress.getFax()).isEqualTo(DEFAULT_FAX);
        assertThat(testAddress.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testAddress.getWebsite()).isEqualTo(DEFAULT_WEBSITE);
        assertThat(testAddress.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testAddress.getEditionDate()).isEqualTo(DEFAULT_EDITION_DATE);
    }

    @Test
    @Transactional
    public void createAddressWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = addressRepository.findAll().size();

        // Create the Address with an existing ID
        address.setId(1L);
        AddressDTO addressDTO = addressMapper.toDto(address);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAddressMockMvc.perform(post("/api/addresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Address in the database
        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = addressRepository.findAll().size();
        // set the field null
        address.setCode(null);

        // Create the Address, which fails.
        AddressDTO addressDTO = addressMapper.toDto(address);

        restAddressMockMvc.perform(post("/api/addresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isBadRequest());

        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = addressRepository.findAll().size();
        // set the field null
        address.setName(null);

        // Create the Address, which fails.
        AddressDTO addressDTO = addressMapper.toDto(address);

        restAddressMockMvc.perform(post("/api/addresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isBadRequest());

        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLocationIsRequired() throws Exception {
        int databaseSizeBeforeTest = addressRepository.findAll().size();
        // set the field null
        address.setLocation(null);

        // Create the Address, which fails.
        AddressDTO addressDTO = addressMapper.toDto(address);

        restAddressMockMvc.perform(post("/api/addresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isBadRequest());

        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPhoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = addressRepository.findAll().size();
        // set the field null
        address.setPhone(null);

        // Create the Address, which fails.
        AddressDTO addressDTO = addressMapper.toDto(address);

        restAddressMockMvc.perform(post("/api/addresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isBadRequest());

        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = addressRepository.findAll().size();
        // set the field null
        address.setEmail(null);

        // Create the Address, which fails.
        AddressDTO addressDTO = addressMapper.toDto(address);

        restAddressMockMvc.perform(post("/api/addresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isBadRequest());

        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAddresses() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList
        restAddressMockMvc.perform(get("/api/addresses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(address.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].website").value(hasItem(DEFAULT_WEBSITE.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].editionDate").value(hasItem(DEFAULT_EDITION_DATE.toString())));
    }

    @Test
    @Transactional
    public void getAddress() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get the address
        restAddressMockMvc.perform(get("/api/addresses/{id}", address.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(address.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()))
            .andExpect(jsonPath("$.fax").value(DEFAULT_FAX.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.website").value(DEFAULT_WEBSITE.toString()))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.editionDate").value(DEFAULT_EDITION_DATE.toString()));
    }

    @Test
    @Transactional
    public void getAllAddressesByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where code equals to DEFAULT_CODE
        defaultAddressShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the addressList where code equals to UPDATED_CODE
        defaultAddressShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllAddressesByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where code in DEFAULT_CODE or UPDATED_CODE
        defaultAddressShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the addressList where code equals to UPDATED_CODE
        defaultAddressShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllAddressesByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where code is not null
        defaultAddressShouldBeFound("code.specified=true");

        // Get all the addressList where code is null
        defaultAddressShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where name equals to DEFAULT_NAME
        defaultAddressShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the addressList where name equals to UPDATED_NAME
        defaultAddressShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllAddressesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where name in DEFAULT_NAME or UPDATED_NAME
        defaultAddressShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the addressList where name equals to UPDATED_NAME
        defaultAddressShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllAddressesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where name is not null
        defaultAddressShouldBeFound("name.specified=true");

        // Get all the addressList where name is null
        defaultAddressShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressesByLocationIsEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where location equals to DEFAULT_LOCATION
        defaultAddressShouldBeFound("location.equals=" + DEFAULT_LOCATION);

        // Get all the addressList where location equals to UPDATED_LOCATION
        defaultAddressShouldNotBeFound("location.equals=" + UPDATED_LOCATION);
    }

    @Test
    @Transactional
    public void getAllAddressesByLocationIsInShouldWork() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where location in DEFAULT_LOCATION or UPDATED_LOCATION
        defaultAddressShouldBeFound("location.in=" + DEFAULT_LOCATION + "," + UPDATED_LOCATION);

        // Get all the addressList where location equals to UPDATED_LOCATION
        defaultAddressShouldNotBeFound("location.in=" + UPDATED_LOCATION);
    }

    @Test
    @Transactional
    public void getAllAddressesByLocationIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where location is not null
        defaultAddressShouldBeFound("location.specified=true");

        // Get all the addressList where location is null
        defaultAddressShouldNotBeFound("location.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressesByPhoneIsEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where phone equals to DEFAULT_PHONE
        defaultAddressShouldBeFound("phone.equals=" + DEFAULT_PHONE);

        // Get all the addressList where phone equals to UPDATED_PHONE
        defaultAddressShouldNotBeFound("phone.equals=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllAddressesByPhoneIsInShouldWork() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where phone in DEFAULT_PHONE or UPDATED_PHONE
        defaultAddressShouldBeFound("phone.in=" + DEFAULT_PHONE + "," + UPDATED_PHONE);

        // Get all the addressList where phone equals to UPDATED_PHONE
        defaultAddressShouldNotBeFound("phone.in=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllAddressesByPhoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where phone is not null
        defaultAddressShouldBeFound("phone.specified=true");

        // Get all the addressList where phone is null
        defaultAddressShouldNotBeFound("phone.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressesByFaxIsEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where fax equals to DEFAULT_FAX
        defaultAddressShouldBeFound("fax.equals=" + DEFAULT_FAX);

        // Get all the addressList where fax equals to UPDATED_FAX
        defaultAddressShouldNotBeFound("fax.equals=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllAddressesByFaxIsInShouldWork() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where fax in DEFAULT_FAX or UPDATED_FAX
        defaultAddressShouldBeFound("fax.in=" + DEFAULT_FAX + "," + UPDATED_FAX);

        // Get all the addressList where fax equals to UPDATED_FAX
        defaultAddressShouldNotBeFound("fax.in=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllAddressesByFaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where fax is not null
        defaultAddressShouldBeFound("fax.specified=true");

        // Get all the addressList where fax is null
        defaultAddressShouldNotBeFound("fax.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressesByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where email equals to DEFAULT_EMAIL
        defaultAddressShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the addressList where email equals to UPDATED_EMAIL
        defaultAddressShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllAddressesByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultAddressShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the addressList where email equals to UPDATED_EMAIL
        defaultAddressShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllAddressesByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where email is not null
        defaultAddressShouldBeFound("email.specified=true");

        // Get all the addressList where email is null
        defaultAddressShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressesByWebsiteIsEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where website equals to DEFAULT_WEBSITE
        defaultAddressShouldBeFound("website.equals=" + DEFAULT_WEBSITE);

        // Get all the addressList where website equals to UPDATED_WEBSITE
        defaultAddressShouldNotBeFound("website.equals=" + UPDATED_WEBSITE);
    }

    @Test
    @Transactional
    public void getAllAddressesByWebsiteIsInShouldWork() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where website in DEFAULT_WEBSITE or UPDATED_WEBSITE
        defaultAddressShouldBeFound("website.in=" + DEFAULT_WEBSITE + "," + UPDATED_WEBSITE);

        // Get all the addressList where website equals to UPDATED_WEBSITE
        defaultAddressShouldNotBeFound("website.in=" + UPDATED_WEBSITE);
    }

    @Test
    @Transactional
    public void getAllAddressesByWebsiteIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where website is not null
        defaultAddressShouldBeFound("website.specified=true");

        // Get all the addressList where website is null
        defaultAddressShouldNotBeFound("website.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressesByCreationDateIsEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where creationDate equals to DEFAULT_CREATION_DATE
        defaultAddressShouldBeFound("creationDate.equals=" + DEFAULT_CREATION_DATE);

        // Get all the addressList where creationDate equals to UPDATED_CREATION_DATE
        defaultAddressShouldNotBeFound("creationDate.equals=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllAddressesByCreationDateIsInShouldWork() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where creationDate in DEFAULT_CREATION_DATE or UPDATED_CREATION_DATE
        defaultAddressShouldBeFound("creationDate.in=" + DEFAULT_CREATION_DATE + "," + UPDATED_CREATION_DATE);

        // Get all the addressList where creationDate equals to UPDATED_CREATION_DATE
        defaultAddressShouldNotBeFound("creationDate.in=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllAddressesByCreationDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where creationDate is not null
        defaultAddressShouldBeFound("creationDate.specified=true");

        // Get all the addressList where creationDate is null
        defaultAddressShouldNotBeFound("creationDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressesByCreationDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where creationDate greater than or equals to DEFAULT_CREATION_DATE
        defaultAddressShouldBeFound("creationDate.greaterOrEqualThan=" + DEFAULT_CREATION_DATE);

        // Get all the addressList where creationDate greater than or equals to UPDATED_CREATION_DATE
        defaultAddressShouldNotBeFound("creationDate.greaterOrEqualThan=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllAddressesByCreationDateIsLessThanSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where creationDate less than or equals to DEFAULT_CREATION_DATE
        defaultAddressShouldNotBeFound("creationDate.lessThan=" + DEFAULT_CREATION_DATE);

        // Get all the addressList where creationDate less than or equals to UPDATED_CREATION_DATE
        defaultAddressShouldBeFound("creationDate.lessThan=" + UPDATED_CREATION_DATE);
    }


    @Test
    @Transactional
    public void getAllAddressesByEditionDateIsEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where editionDate equals to DEFAULT_EDITION_DATE
        defaultAddressShouldBeFound("editionDate.equals=" + DEFAULT_EDITION_DATE);

        // Get all the addressList where editionDate equals to UPDATED_EDITION_DATE
        defaultAddressShouldNotBeFound("editionDate.equals=" + UPDATED_EDITION_DATE);
    }

    @Test
    @Transactional
    public void getAllAddressesByEditionDateIsInShouldWork() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where editionDate in DEFAULT_EDITION_DATE or UPDATED_EDITION_DATE
        defaultAddressShouldBeFound("editionDate.in=" + DEFAULT_EDITION_DATE + "," + UPDATED_EDITION_DATE);

        // Get all the addressList where editionDate equals to UPDATED_EDITION_DATE
        defaultAddressShouldNotBeFound("editionDate.in=" + UPDATED_EDITION_DATE);
    }

    @Test
    @Transactional
    public void getAllAddressesByEditionDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where editionDate is not null
        defaultAddressShouldBeFound("editionDate.specified=true");

        // Get all the addressList where editionDate is null
        defaultAddressShouldNotBeFound("editionDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllAddressesByEditionDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where editionDate greater than or equals to DEFAULT_EDITION_DATE
        defaultAddressShouldBeFound("editionDate.greaterOrEqualThan=" + DEFAULT_EDITION_DATE);

        // Get all the addressList where editionDate greater than or equals to UPDATED_EDITION_DATE
        defaultAddressShouldNotBeFound("editionDate.greaterOrEqualThan=" + UPDATED_EDITION_DATE);
    }

    @Test
    @Transactional
    public void getAllAddressesByEditionDateIsLessThanSomething() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);

        // Get all the addressList where editionDate less than or equals to DEFAULT_EDITION_DATE
        defaultAddressShouldNotBeFound("editionDate.lessThan=" + DEFAULT_EDITION_DATE);

        // Get all the addressList where editionDate less than or equals to UPDATED_EDITION_DATE
        defaultAddressShouldBeFound("editionDate.lessThan=" + UPDATED_EDITION_DATE);
    }


    @Test
    @Transactional
    public void getAllAddressesBySocialCardsIsEqualToSomething() throws Exception {
        // Initialize the database
        SocialCard socialCards = SocialCardResourceIntTest.createEntity(em);
        em.persist(socialCards);
        em.flush();
        address.addSocialCards(socialCards);
        addressRepository.saveAndFlush(address);
        Long socialCardsId = socialCards.getId();

        // Get all the addressList where socialCards equals to socialCardsId
        defaultAddressShouldBeFound("socialCardsId.equals=" + socialCardsId);

        // Get all the addressList where socialCards equals to socialCardsId + 1
        defaultAddressShouldNotBeFound("socialCardsId.equals=" + (socialCardsId + 1));
    }


    @Test
    @Transactional
    public void getAllAddressesByCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        User creator = UserResourceIntTest.createEntity(em);
        em.persist(creator);
        em.flush();
        address.setCreator(creator);
        addressRepository.saveAndFlush(address);
        Long creatorId = creator.getId();

        // Get all the addressList where creator equals to creatorId
        defaultAddressShouldBeFound("creatorId.equals=" + creatorId);

        // Get all the addressList where creator equals to creatorId + 1
        defaultAddressShouldNotBeFound("creatorId.equals=" + (creatorId + 1));
    }


    @Test
    @Transactional
    public void getAllAddressesByUpdatorIsEqualToSomething() throws Exception {
        // Initialize the database
        User updator = UserResourceIntTest.createEntity(em);
        em.persist(updator);
        em.flush();
        address.setUpdator(updator);
        addressRepository.saveAndFlush(address);
        Long updatorId = updator.getId();

        // Get all the addressList where updator equals to updatorId
        defaultAddressShouldBeFound("updatorId.equals=" + updatorId);

        // Get all the addressList where updator equals to updatorId + 1
        defaultAddressShouldNotBeFound("updatorId.equals=" + (updatorId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultAddressShouldBeFound(String filter) throws Exception {
        restAddressMockMvc.perform(get("/api/addresses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(address.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].website").value(hasItem(DEFAULT_WEBSITE.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].editionDate").value(hasItem(DEFAULT_EDITION_DATE.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultAddressShouldNotBeFound(String filter) throws Exception {
        restAddressMockMvc.perform(get("/api/addresses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingAddress() throws Exception {
        // Get the address
        restAddressMockMvc.perform(get("/api/addresses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAddress() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);
        int databaseSizeBeforeUpdate = addressRepository.findAll().size();

        // Update the address
        Address updatedAddress = addressRepository.findOne(address.getId());
        // Disconnect from session so that the updates on updatedAddress are not directly saved in db
        em.detach(updatedAddress);
        updatedAddress
            .code(UPDATED_CODE)
            .name(UPDATED_NAME)
            .location(UPDATED_LOCATION)
            .phone(UPDATED_PHONE)
            .fax(UPDATED_FAX)
            .email(UPDATED_EMAIL)
            .website(UPDATED_WEBSITE)
            .creationDate(UPDATED_CREATION_DATE)
            .editionDate(UPDATED_EDITION_DATE);
        AddressDTO addressDTO = addressMapper.toDto(updatedAddress);

        restAddressMockMvc.perform(put("/api/addresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isOk());

        // Validate the Address in the database
        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeUpdate);
        Address testAddress = addressList.get(addressList.size() - 1);
        assertThat(testAddress.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testAddress.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testAddress.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testAddress.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testAddress.getFax()).isEqualTo(UPDATED_FAX);
        assertThat(testAddress.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testAddress.getWebsite()).isEqualTo(UPDATED_WEBSITE);
        assertThat(testAddress.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testAddress.getEditionDate()).isEqualTo(UPDATED_EDITION_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingAddress() throws Exception {
        int databaseSizeBeforeUpdate = addressRepository.findAll().size();

        // Create the Address
        AddressDTO addressDTO = addressMapper.toDto(address);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAddressMockMvc.perform(put("/api/addresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressDTO)))
            .andExpect(status().isCreated());

        // Validate the Address in the database
        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAddress() throws Exception {
        // Initialize the database
        addressRepository.saveAndFlush(address);
        int databaseSizeBeforeDelete = addressRepository.findAll().size();

        // Get the address
        restAddressMockMvc.perform(delete("/api/addresses/{id}", address.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Address> addressList = addressRepository.findAll();
        assertThat(addressList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Address.class);
        Address address1 = new Address();
        address1.setId(1L);
        Address address2 = new Address();
        address2.setId(address1.getId());
        assertThat(address1).isEqualTo(address2);
        address2.setId(2L);
        assertThat(address1).isNotEqualTo(address2);
        address1.setId(null);
        assertThat(address1).isNotEqualTo(address2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AddressDTO.class);
        AddressDTO addressDTO1 = new AddressDTO();
        addressDTO1.setId(1L);
        AddressDTO addressDTO2 = new AddressDTO();
        assertThat(addressDTO1).isNotEqualTo(addressDTO2);
        addressDTO2.setId(addressDTO1.getId());
        assertThat(addressDTO1).isEqualTo(addressDTO2);
        addressDTO2.setId(2L);
        assertThat(addressDTO1).isNotEqualTo(addressDTO2);
        addressDTO1.setId(null);
        assertThat(addressDTO1).isNotEqualTo(addressDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(addressMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(addressMapper.fromId(null)).isNull();
    }
}
