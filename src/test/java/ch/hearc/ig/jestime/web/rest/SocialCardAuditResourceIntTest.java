package ch.hearc.ig.jestime.web.rest;

import ch.hearc.ig.jestime.JestimeApp;

import ch.hearc.ig.jestime.domain.SocialCardAudit;
import ch.hearc.ig.jestime.repository.SocialCardAuditRepository;
import ch.hearc.ig.jestime.service.SocialCardAuditService;
import ch.hearc.ig.jestime.service.dto.SocialCardAuditDTO;
import ch.hearc.ig.jestime.service.mapper.SocialCardAuditMapper;
import ch.hearc.ig.jestime.web.rest.errors.ExceptionTranslator;
import ch.hearc.ig.jestime.service.dto.SocialCardAuditCriteria;
import ch.hearc.ig.jestime.service.SocialCardAuditQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static ch.hearc.ig.jestime.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SocialCardAuditResource REST controller.
 *
 * @see SocialCardAuditResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JestimeApp.class)
public class SocialCardAuditResourceIntTest {

    private static final String DEFAULT_ACTION = "AAAAAAAAAA";
    private static final String UPDATED_ACTION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_ACTION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ACTION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_ACTION_USER = "AAAAAAAAAA";
    private static final String UPDATED_ACTION_USER = "BBBBBBBBBB";

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_GENERAL = "AAAAAAAAAA";
    private static final String UPDATED_GENERAL = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_PROCEDURE = "AAAAAAAAAA";
    private static final String UPDATED_PROCEDURE = "BBBBBBBBBB";

    private static final String DEFAULT_RECOURSE = "AAAAAAAAAA";
    private static final String UPDATED_RECOURSE = "BBBBBBBBBB";

    private static final Integer DEFAULT_JURIDICTION_ID = 1;
    private static final Integer UPDATED_JURIDICTION_ID = 2;

    private static final Integer DEFAULT_CATEGORY_ID = 1;
    private static final Integer UPDATED_CATEGORY_ID = 2;

    private static final Integer DEFAULT_PARENT_SOCIAL_CARD_ID = 1;
    private static final Integer UPDATED_PARENT_SOCIAL_CARD_ID = 2;

    @Autowired
    private SocialCardAuditRepository socialCardAuditRepository;

    @Autowired
    private SocialCardAuditMapper socialCardAuditMapper;

    @Autowired
    private SocialCardAuditService socialCardAuditService;

    @Autowired
    private SocialCardAuditQueryService socialCardAuditQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSocialCardAuditMockMvc;

    private SocialCardAudit socialCardAudit;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SocialCardAuditResource socialCardAuditResource = new SocialCardAuditResource(socialCardAuditService, socialCardAuditQueryService);
        this.restSocialCardAuditMockMvc = MockMvcBuilders.standaloneSetup(socialCardAuditResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SocialCardAudit createEntity(EntityManager em) {
        SocialCardAudit socialCardAudit = new SocialCardAudit()
            .action(DEFAULT_ACTION)
            .actionDate(DEFAULT_ACTION_DATE)
            .actionUser(DEFAULT_ACTION_USER)
            .title(DEFAULT_TITLE)
            .code(DEFAULT_CODE)
            .general(DEFAULT_GENERAL)
            .description(DEFAULT_DESCRIPTION)
            .procedure(DEFAULT_PROCEDURE)
            .recourse(DEFAULT_RECOURSE)
            .juridictionId(DEFAULT_JURIDICTION_ID)
            .categoryId(DEFAULT_CATEGORY_ID)
            .parentSocialCardId(DEFAULT_PARENT_SOCIAL_CARD_ID);
        return socialCardAudit;
    }

    @Before
    public void initTest() {
        socialCardAudit = createEntity(em);
    }

    @Test
    @Transactional
    public void createSocialCardAudit() throws Exception {
        int databaseSizeBeforeCreate = socialCardAuditRepository.findAll().size();

        // Create the SocialCardAudit
        SocialCardAuditDTO socialCardAuditDTO = socialCardAuditMapper.toDto(socialCardAudit);
        restSocialCardAuditMockMvc.perform(post("/api/social-card-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(socialCardAuditDTO)))
            .andExpect(status().isCreated());

        // Validate the SocialCardAudit in the database
        List<SocialCardAudit> socialCardAuditList = socialCardAuditRepository.findAll();
        assertThat(socialCardAuditList).hasSize(databaseSizeBeforeCreate + 1);
        SocialCardAudit testSocialCardAudit = socialCardAuditList.get(socialCardAuditList.size() - 1);
        assertThat(testSocialCardAudit.getAction()).isEqualTo(DEFAULT_ACTION);
        assertThat(testSocialCardAudit.getActionDate()).isEqualTo(DEFAULT_ACTION_DATE);
        assertThat(testSocialCardAudit.getActionUser()).isEqualTo(DEFAULT_ACTION_USER);
        assertThat(testSocialCardAudit.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testSocialCardAudit.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testSocialCardAudit.getGeneral()).isEqualTo(DEFAULT_GENERAL);
        assertThat(testSocialCardAudit.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testSocialCardAudit.getProcedure()).isEqualTo(DEFAULT_PROCEDURE);
        assertThat(testSocialCardAudit.getRecourse()).isEqualTo(DEFAULT_RECOURSE);
        assertThat(testSocialCardAudit.getJuridictionId()).isEqualTo(DEFAULT_JURIDICTION_ID);
        assertThat(testSocialCardAudit.getCategoryId()).isEqualTo(DEFAULT_CATEGORY_ID);
        assertThat(testSocialCardAudit.getParentSocialCardId()).isEqualTo(DEFAULT_PARENT_SOCIAL_CARD_ID);
    }

    @Test
    @Transactional
    public void createSocialCardAuditWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = socialCardAuditRepository.findAll().size();

        // Create the SocialCardAudit with an existing ID
        socialCardAudit.setId(1L);
        SocialCardAuditDTO socialCardAuditDTO = socialCardAuditMapper.toDto(socialCardAudit);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSocialCardAuditMockMvc.perform(post("/api/social-card-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(socialCardAuditDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SocialCardAudit in the database
        List<SocialCardAudit> socialCardAuditList = socialCardAuditRepository.findAll();
        assertThat(socialCardAuditList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkActionIsRequired() throws Exception {
        int databaseSizeBeforeTest = socialCardAuditRepository.findAll().size();
        // set the field null
        socialCardAudit.setAction(null);

        // Create the SocialCardAudit, which fails.
        SocialCardAuditDTO socialCardAuditDTO = socialCardAuditMapper.toDto(socialCardAudit);

        restSocialCardAuditMockMvc.perform(post("/api/social-card-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(socialCardAuditDTO)))
            .andExpect(status().isBadRequest());

        List<SocialCardAudit> socialCardAuditList = socialCardAuditRepository.findAll();
        assertThat(socialCardAuditList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkActionDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = socialCardAuditRepository.findAll().size();
        // set the field null
        socialCardAudit.setActionDate(null);

        // Create the SocialCardAudit, which fails.
        SocialCardAuditDTO socialCardAuditDTO = socialCardAuditMapper.toDto(socialCardAudit);

        restSocialCardAuditMockMvc.perform(post("/api/social-card-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(socialCardAuditDTO)))
            .andExpect(status().isBadRequest());

        List<SocialCardAudit> socialCardAuditList = socialCardAuditRepository.findAll();
        assertThat(socialCardAuditList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkActionUserIsRequired() throws Exception {
        int databaseSizeBeforeTest = socialCardAuditRepository.findAll().size();
        // set the field null
        socialCardAudit.setActionUser(null);

        // Create the SocialCardAudit, which fails.
        SocialCardAuditDTO socialCardAuditDTO = socialCardAuditMapper.toDto(socialCardAudit);

        restSocialCardAuditMockMvc.perform(post("/api/social-card-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(socialCardAuditDTO)))
            .andExpect(status().isBadRequest());

        List<SocialCardAudit> socialCardAuditList = socialCardAuditRepository.findAll();
        assertThat(socialCardAuditList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSocialCardAudits() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList
        restSocialCardAuditMockMvc.perform(get("/api/social-card-audits?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(socialCardAudit.getId().intValue())))
            .andExpect(jsonPath("$.[*].action").value(hasItem(DEFAULT_ACTION.toString())))
            .andExpect(jsonPath("$.[*].actionDate").value(hasItem(DEFAULT_ACTION_DATE.toString())))
            .andExpect(jsonPath("$.[*].actionUser").value(hasItem(DEFAULT_ACTION_USER.toString())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].general").value(hasItem(DEFAULT_GENERAL.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].procedure").value(hasItem(DEFAULT_PROCEDURE.toString())))
            .andExpect(jsonPath("$.[*].recourse").value(hasItem(DEFAULT_RECOURSE.toString())))
            .andExpect(jsonPath("$.[*].juridictionId").value(hasItem(DEFAULT_JURIDICTION_ID)))
            .andExpect(jsonPath("$.[*].categoryId").value(hasItem(DEFAULT_CATEGORY_ID)))
            .andExpect(jsonPath("$.[*].parentSocialCardId").value(hasItem(DEFAULT_PARENT_SOCIAL_CARD_ID)));
    }

    @Test
    @Transactional
    public void getSocialCardAudit() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get the socialCardAudit
        restSocialCardAuditMockMvc.perform(get("/api/social-card-audits/{id}", socialCardAudit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(socialCardAudit.getId().intValue()))
            .andExpect(jsonPath("$.action").value(DEFAULT_ACTION.toString()))
            .andExpect(jsonPath("$.actionDate").value(DEFAULT_ACTION_DATE.toString()))
            .andExpect(jsonPath("$.actionUser").value(DEFAULT_ACTION_USER.toString()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.general").value(DEFAULT_GENERAL.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.procedure").value(DEFAULT_PROCEDURE.toString()))
            .andExpect(jsonPath("$.recourse").value(DEFAULT_RECOURSE.toString()))
            .andExpect(jsonPath("$.juridictionId").value(DEFAULT_JURIDICTION_ID))
            .andExpect(jsonPath("$.categoryId").value(DEFAULT_CATEGORY_ID))
            .andExpect(jsonPath("$.parentSocialCardId").value(DEFAULT_PARENT_SOCIAL_CARD_ID));
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByActionIsEqualToSomething() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where action equals to DEFAULT_ACTION
        defaultSocialCardAuditShouldBeFound("action.equals=" + DEFAULT_ACTION);

        // Get all the socialCardAuditList where action equals to UPDATED_ACTION
        defaultSocialCardAuditShouldNotBeFound("action.equals=" + UPDATED_ACTION);
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByActionIsInShouldWork() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where action in DEFAULT_ACTION or UPDATED_ACTION
        defaultSocialCardAuditShouldBeFound("action.in=" + DEFAULT_ACTION + "," + UPDATED_ACTION);

        // Get all the socialCardAuditList where action equals to UPDATED_ACTION
        defaultSocialCardAuditShouldNotBeFound("action.in=" + UPDATED_ACTION);
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByActionIsNullOrNotNull() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where action is not null
        defaultSocialCardAuditShouldBeFound("action.specified=true");

        // Get all the socialCardAuditList where action is null
        defaultSocialCardAuditShouldNotBeFound("action.specified=false");
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByActionDateIsEqualToSomething() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where actionDate equals to DEFAULT_ACTION_DATE
        defaultSocialCardAuditShouldBeFound("actionDate.equals=" + DEFAULT_ACTION_DATE);

        // Get all the socialCardAuditList where actionDate equals to UPDATED_ACTION_DATE
        defaultSocialCardAuditShouldNotBeFound("actionDate.equals=" + UPDATED_ACTION_DATE);
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByActionDateIsInShouldWork() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where actionDate in DEFAULT_ACTION_DATE or UPDATED_ACTION_DATE
        defaultSocialCardAuditShouldBeFound("actionDate.in=" + DEFAULT_ACTION_DATE + "," + UPDATED_ACTION_DATE);

        // Get all the socialCardAuditList where actionDate equals to UPDATED_ACTION_DATE
        defaultSocialCardAuditShouldNotBeFound("actionDate.in=" + UPDATED_ACTION_DATE);
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByActionDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where actionDate is not null
        defaultSocialCardAuditShouldBeFound("actionDate.specified=true");

        // Get all the socialCardAuditList where actionDate is null
        defaultSocialCardAuditShouldNotBeFound("actionDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByActionDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where actionDate greater than or equals to DEFAULT_ACTION_DATE
        defaultSocialCardAuditShouldBeFound("actionDate.greaterOrEqualThan=" + DEFAULT_ACTION_DATE);

        // Get all the socialCardAuditList where actionDate greater than or equals to UPDATED_ACTION_DATE
        defaultSocialCardAuditShouldNotBeFound("actionDate.greaterOrEqualThan=" + UPDATED_ACTION_DATE);
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByActionDateIsLessThanSomething() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where actionDate less than or equals to DEFAULT_ACTION_DATE
        defaultSocialCardAuditShouldNotBeFound("actionDate.lessThan=" + DEFAULT_ACTION_DATE);

        // Get all the socialCardAuditList where actionDate less than or equals to UPDATED_ACTION_DATE
        defaultSocialCardAuditShouldBeFound("actionDate.lessThan=" + UPDATED_ACTION_DATE);
    }


    @Test
    @Transactional
    public void getAllSocialCardAuditsByActionUserIsEqualToSomething() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where actionUser equals to DEFAULT_ACTION_USER
        defaultSocialCardAuditShouldBeFound("actionUser.equals=" + DEFAULT_ACTION_USER);

        // Get all the socialCardAuditList where actionUser equals to UPDATED_ACTION_USER
        defaultSocialCardAuditShouldNotBeFound("actionUser.equals=" + UPDATED_ACTION_USER);
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByActionUserIsInShouldWork() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where actionUser in DEFAULT_ACTION_USER or UPDATED_ACTION_USER
        defaultSocialCardAuditShouldBeFound("actionUser.in=" + DEFAULT_ACTION_USER + "," + UPDATED_ACTION_USER);

        // Get all the socialCardAuditList where actionUser equals to UPDATED_ACTION_USER
        defaultSocialCardAuditShouldNotBeFound("actionUser.in=" + UPDATED_ACTION_USER);
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByActionUserIsNullOrNotNull() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where actionUser is not null
        defaultSocialCardAuditShouldBeFound("actionUser.specified=true");

        // Get all the socialCardAuditList where actionUser is null
        defaultSocialCardAuditShouldNotBeFound("actionUser.specified=false");
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByTitleIsEqualToSomething() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where title equals to DEFAULT_TITLE
        defaultSocialCardAuditShouldBeFound("title.equals=" + DEFAULT_TITLE);

        // Get all the socialCardAuditList where title equals to UPDATED_TITLE
        defaultSocialCardAuditShouldNotBeFound("title.equals=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByTitleIsInShouldWork() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where title in DEFAULT_TITLE or UPDATED_TITLE
        defaultSocialCardAuditShouldBeFound("title.in=" + DEFAULT_TITLE + "," + UPDATED_TITLE);

        // Get all the socialCardAuditList where title equals to UPDATED_TITLE
        defaultSocialCardAuditShouldNotBeFound("title.in=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByTitleIsNullOrNotNull() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where title is not null
        defaultSocialCardAuditShouldBeFound("title.specified=true");

        // Get all the socialCardAuditList where title is null
        defaultSocialCardAuditShouldNotBeFound("title.specified=false");
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where code equals to DEFAULT_CODE
        defaultSocialCardAuditShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the socialCardAuditList where code equals to UPDATED_CODE
        defaultSocialCardAuditShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where code in DEFAULT_CODE or UPDATED_CODE
        defaultSocialCardAuditShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the socialCardAuditList where code equals to UPDATED_CODE
        defaultSocialCardAuditShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where code is not null
        defaultSocialCardAuditShouldBeFound("code.specified=true");

        // Get all the socialCardAuditList where code is null
        defaultSocialCardAuditShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByJuridictionIdIsEqualToSomething() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where juridictionId equals to DEFAULT_JURIDICTION_ID
        defaultSocialCardAuditShouldBeFound("juridictionId.equals=" + DEFAULT_JURIDICTION_ID);

        // Get all the socialCardAuditList where juridictionId equals to UPDATED_JURIDICTION_ID
        defaultSocialCardAuditShouldNotBeFound("juridictionId.equals=" + UPDATED_JURIDICTION_ID);
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByJuridictionIdIsInShouldWork() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where juridictionId in DEFAULT_JURIDICTION_ID or UPDATED_JURIDICTION_ID
        defaultSocialCardAuditShouldBeFound("juridictionId.in=" + DEFAULT_JURIDICTION_ID + "," + UPDATED_JURIDICTION_ID);

        // Get all the socialCardAuditList where juridictionId equals to UPDATED_JURIDICTION_ID
        defaultSocialCardAuditShouldNotBeFound("juridictionId.in=" + UPDATED_JURIDICTION_ID);
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByJuridictionIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where juridictionId is not null
        defaultSocialCardAuditShouldBeFound("juridictionId.specified=true");

        // Get all the socialCardAuditList where juridictionId is null
        defaultSocialCardAuditShouldNotBeFound("juridictionId.specified=false");
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByJuridictionIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where juridictionId greater than or equals to DEFAULT_JURIDICTION_ID
        defaultSocialCardAuditShouldBeFound("juridictionId.greaterOrEqualThan=" + DEFAULT_JURIDICTION_ID);

        // Get all the socialCardAuditList where juridictionId greater than or equals to UPDATED_JURIDICTION_ID
        defaultSocialCardAuditShouldNotBeFound("juridictionId.greaterOrEqualThan=" + UPDATED_JURIDICTION_ID);
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByJuridictionIdIsLessThanSomething() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where juridictionId less than or equals to DEFAULT_JURIDICTION_ID
        defaultSocialCardAuditShouldNotBeFound("juridictionId.lessThan=" + DEFAULT_JURIDICTION_ID);

        // Get all the socialCardAuditList where juridictionId less than or equals to UPDATED_JURIDICTION_ID
        defaultSocialCardAuditShouldBeFound("juridictionId.lessThan=" + UPDATED_JURIDICTION_ID);
    }


    @Test
    @Transactional
    public void getAllSocialCardAuditsByCategoryIdIsEqualToSomething() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where categoryId equals to DEFAULT_CATEGORY_ID
        defaultSocialCardAuditShouldBeFound("categoryId.equals=" + DEFAULT_CATEGORY_ID);

        // Get all the socialCardAuditList where categoryId equals to UPDATED_CATEGORY_ID
        defaultSocialCardAuditShouldNotBeFound("categoryId.equals=" + UPDATED_CATEGORY_ID);
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByCategoryIdIsInShouldWork() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where categoryId in DEFAULT_CATEGORY_ID or UPDATED_CATEGORY_ID
        defaultSocialCardAuditShouldBeFound("categoryId.in=" + DEFAULT_CATEGORY_ID + "," + UPDATED_CATEGORY_ID);

        // Get all the socialCardAuditList where categoryId equals to UPDATED_CATEGORY_ID
        defaultSocialCardAuditShouldNotBeFound("categoryId.in=" + UPDATED_CATEGORY_ID);
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByCategoryIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where categoryId is not null
        defaultSocialCardAuditShouldBeFound("categoryId.specified=true");

        // Get all the socialCardAuditList where categoryId is null
        defaultSocialCardAuditShouldNotBeFound("categoryId.specified=false");
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByCategoryIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where categoryId greater than or equals to DEFAULT_CATEGORY_ID
        defaultSocialCardAuditShouldBeFound("categoryId.greaterOrEqualThan=" + DEFAULT_CATEGORY_ID);

        // Get all the socialCardAuditList where categoryId greater than or equals to UPDATED_CATEGORY_ID
        defaultSocialCardAuditShouldNotBeFound("categoryId.greaterOrEqualThan=" + UPDATED_CATEGORY_ID);
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByCategoryIdIsLessThanSomething() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where categoryId less than or equals to DEFAULT_CATEGORY_ID
        defaultSocialCardAuditShouldNotBeFound("categoryId.lessThan=" + DEFAULT_CATEGORY_ID);

        // Get all the socialCardAuditList where categoryId less than or equals to UPDATED_CATEGORY_ID
        defaultSocialCardAuditShouldBeFound("categoryId.lessThan=" + UPDATED_CATEGORY_ID);
    }


    @Test
    @Transactional
    public void getAllSocialCardAuditsByParentSocialCardIdIsEqualToSomething() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where parentSocialCardId equals to DEFAULT_PARENT_SOCIAL_CARD_ID
        defaultSocialCardAuditShouldBeFound("parentSocialCardId.equals=" + DEFAULT_PARENT_SOCIAL_CARD_ID);

        // Get all the socialCardAuditList where parentSocialCardId equals to UPDATED_PARENT_SOCIAL_CARD_ID
        defaultSocialCardAuditShouldNotBeFound("parentSocialCardId.equals=" + UPDATED_PARENT_SOCIAL_CARD_ID);
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByParentSocialCardIdIsInShouldWork() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where parentSocialCardId in DEFAULT_PARENT_SOCIAL_CARD_ID or UPDATED_PARENT_SOCIAL_CARD_ID
        defaultSocialCardAuditShouldBeFound("parentSocialCardId.in=" + DEFAULT_PARENT_SOCIAL_CARD_ID + "," + UPDATED_PARENT_SOCIAL_CARD_ID);

        // Get all the socialCardAuditList where parentSocialCardId equals to UPDATED_PARENT_SOCIAL_CARD_ID
        defaultSocialCardAuditShouldNotBeFound("parentSocialCardId.in=" + UPDATED_PARENT_SOCIAL_CARD_ID);
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByParentSocialCardIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where parentSocialCardId is not null
        defaultSocialCardAuditShouldBeFound("parentSocialCardId.specified=true");

        // Get all the socialCardAuditList where parentSocialCardId is null
        defaultSocialCardAuditShouldNotBeFound("parentSocialCardId.specified=false");
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByParentSocialCardIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where parentSocialCardId greater than or equals to DEFAULT_PARENT_SOCIAL_CARD_ID
        defaultSocialCardAuditShouldBeFound("parentSocialCardId.greaterOrEqualThan=" + DEFAULT_PARENT_SOCIAL_CARD_ID);

        // Get all the socialCardAuditList where parentSocialCardId greater than or equals to UPDATED_PARENT_SOCIAL_CARD_ID
        defaultSocialCardAuditShouldNotBeFound("parentSocialCardId.greaterOrEqualThan=" + UPDATED_PARENT_SOCIAL_CARD_ID);
    }

    @Test
    @Transactional
    public void getAllSocialCardAuditsByParentSocialCardIdIsLessThanSomething() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);

        // Get all the socialCardAuditList where parentSocialCardId less than or equals to DEFAULT_PARENT_SOCIAL_CARD_ID
        defaultSocialCardAuditShouldNotBeFound("parentSocialCardId.lessThan=" + DEFAULT_PARENT_SOCIAL_CARD_ID);

        // Get all the socialCardAuditList where parentSocialCardId less than or equals to UPDATED_PARENT_SOCIAL_CARD_ID
        defaultSocialCardAuditShouldBeFound("parentSocialCardId.lessThan=" + UPDATED_PARENT_SOCIAL_CARD_ID);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultSocialCardAuditShouldBeFound(String filter) throws Exception {
        restSocialCardAuditMockMvc.perform(get("/api/social-card-audits?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(socialCardAudit.getId().intValue())))
            .andExpect(jsonPath("$.[*].action").value(hasItem(DEFAULT_ACTION.toString())))
            .andExpect(jsonPath("$.[*].actionDate").value(hasItem(DEFAULT_ACTION_DATE.toString())))
            .andExpect(jsonPath("$.[*].actionUser").value(hasItem(DEFAULT_ACTION_USER.toString())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].general").value(hasItem(DEFAULT_GENERAL.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].procedure").value(hasItem(DEFAULT_PROCEDURE.toString())))
            .andExpect(jsonPath("$.[*].recourse").value(hasItem(DEFAULT_RECOURSE.toString())))
            .andExpect(jsonPath("$.[*].juridictionId").value(hasItem(DEFAULT_JURIDICTION_ID)))
            .andExpect(jsonPath("$.[*].categoryId").value(hasItem(DEFAULT_CATEGORY_ID)))
            .andExpect(jsonPath("$.[*].parentSocialCardId").value(hasItem(DEFAULT_PARENT_SOCIAL_CARD_ID)));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultSocialCardAuditShouldNotBeFound(String filter) throws Exception {
        restSocialCardAuditMockMvc.perform(get("/api/social-card-audits?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingSocialCardAudit() throws Exception {
        // Get the socialCardAudit
        restSocialCardAuditMockMvc.perform(get("/api/social-card-audits/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSocialCardAudit() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);
        int databaseSizeBeforeUpdate = socialCardAuditRepository.findAll().size();

        // Update the socialCardAudit
        SocialCardAudit updatedSocialCardAudit = socialCardAuditRepository.findOne(socialCardAudit.getId());
        // Disconnect from session so that the updates on updatedSocialCardAudit are not directly saved in db
        em.detach(updatedSocialCardAudit);
        updatedSocialCardAudit
            .action(UPDATED_ACTION)
            .actionDate(UPDATED_ACTION_DATE)
            .actionUser(UPDATED_ACTION_USER)
            .title(UPDATED_TITLE)
            .code(UPDATED_CODE)
            .general(UPDATED_GENERAL)
            .description(UPDATED_DESCRIPTION)
            .procedure(UPDATED_PROCEDURE)
            .recourse(UPDATED_RECOURSE)
            .juridictionId(UPDATED_JURIDICTION_ID)
            .categoryId(UPDATED_CATEGORY_ID)
            .parentSocialCardId(UPDATED_PARENT_SOCIAL_CARD_ID);
        SocialCardAuditDTO socialCardAuditDTO = socialCardAuditMapper.toDto(updatedSocialCardAudit);

        restSocialCardAuditMockMvc.perform(put("/api/social-card-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(socialCardAuditDTO)))
            .andExpect(status().isOk());

        // Validate the SocialCardAudit in the database
        List<SocialCardAudit> socialCardAuditList = socialCardAuditRepository.findAll();
        assertThat(socialCardAuditList).hasSize(databaseSizeBeforeUpdate);
        SocialCardAudit testSocialCardAudit = socialCardAuditList.get(socialCardAuditList.size() - 1);
        assertThat(testSocialCardAudit.getAction()).isEqualTo(UPDATED_ACTION);
        assertThat(testSocialCardAudit.getActionDate()).isEqualTo(UPDATED_ACTION_DATE);
        assertThat(testSocialCardAudit.getActionUser()).isEqualTo(UPDATED_ACTION_USER);
        assertThat(testSocialCardAudit.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testSocialCardAudit.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testSocialCardAudit.getGeneral()).isEqualTo(UPDATED_GENERAL);
        assertThat(testSocialCardAudit.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testSocialCardAudit.getProcedure()).isEqualTo(UPDATED_PROCEDURE);
        assertThat(testSocialCardAudit.getRecourse()).isEqualTo(UPDATED_RECOURSE);
        assertThat(testSocialCardAudit.getJuridictionId()).isEqualTo(UPDATED_JURIDICTION_ID);
        assertThat(testSocialCardAudit.getCategoryId()).isEqualTo(UPDATED_CATEGORY_ID);
        assertThat(testSocialCardAudit.getParentSocialCardId()).isEqualTo(UPDATED_PARENT_SOCIAL_CARD_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingSocialCardAudit() throws Exception {
        int databaseSizeBeforeUpdate = socialCardAuditRepository.findAll().size();

        // Create the SocialCardAudit
        SocialCardAuditDTO socialCardAuditDTO = socialCardAuditMapper.toDto(socialCardAudit);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSocialCardAuditMockMvc.perform(put("/api/social-card-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(socialCardAuditDTO)))
            .andExpect(status().isCreated());

        // Validate the SocialCardAudit in the database
        List<SocialCardAudit> socialCardAuditList = socialCardAuditRepository.findAll();
        assertThat(socialCardAuditList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSocialCardAudit() throws Exception {
        // Initialize the database
        socialCardAuditRepository.saveAndFlush(socialCardAudit);
        int databaseSizeBeforeDelete = socialCardAuditRepository.findAll().size();

        // Get the socialCardAudit
        restSocialCardAuditMockMvc.perform(delete("/api/social-card-audits/{id}", socialCardAudit.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SocialCardAudit> socialCardAuditList = socialCardAuditRepository.findAll();
        assertThat(socialCardAuditList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SocialCardAudit.class);
        SocialCardAudit socialCardAudit1 = new SocialCardAudit();
        socialCardAudit1.setId(1L);
        SocialCardAudit socialCardAudit2 = new SocialCardAudit();
        socialCardAudit2.setId(socialCardAudit1.getId());
        assertThat(socialCardAudit1).isEqualTo(socialCardAudit2);
        socialCardAudit2.setId(2L);
        assertThat(socialCardAudit1).isNotEqualTo(socialCardAudit2);
        socialCardAudit1.setId(null);
        assertThat(socialCardAudit1).isNotEqualTo(socialCardAudit2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SocialCardAuditDTO.class);
        SocialCardAuditDTO socialCardAuditDTO1 = new SocialCardAuditDTO();
        socialCardAuditDTO1.setId(1L);
        SocialCardAuditDTO socialCardAuditDTO2 = new SocialCardAuditDTO();
        assertThat(socialCardAuditDTO1).isNotEqualTo(socialCardAuditDTO2);
        socialCardAuditDTO2.setId(socialCardAuditDTO1.getId());
        assertThat(socialCardAuditDTO1).isEqualTo(socialCardAuditDTO2);
        socialCardAuditDTO2.setId(2L);
        assertThat(socialCardAuditDTO1).isNotEqualTo(socialCardAuditDTO2);
        socialCardAuditDTO1.setId(null);
        assertThat(socialCardAuditDTO1).isNotEqualTo(socialCardAuditDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(socialCardAuditMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(socialCardAuditMapper.fromId(null)).isNull();
    }
}
