package ch.hearc.ig.jestime.web.rest;

import ch.hearc.ig.jestime.JestimeApp;

import ch.hearc.ig.jestime.domain.SocialCard;
import ch.hearc.ig.jestime.domain.Juridiction;
import ch.hearc.ig.jestime.domain.SocialCard;
import ch.hearc.ig.jestime.domain.Category;
import ch.hearc.ig.jestime.domain.Address;
import ch.hearc.ig.jestime.domain.User;
import ch.hearc.ig.jestime.domain.User;
import ch.hearc.ig.jestime.domain.Source;
import ch.hearc.ig.jestime.domain.Condition;
import ch.hearc.ig.jestime.repository.SocialCardRepository;
import ch.hearc.ig.jestime.service.SocialCardService;
import ch.hearc.ig.jestime.service.dto.SocialCardDTO;
import ch.hearc.ig.jestime.service.mapper.SocialCardMapper;
import ch.hearc.ig.jestime.web.rest.errors.ExceptionTranslator;
import ch.hearc.ig.jestime.service.dto.SocialCardCriteria;
import ch.hearc.ig.jestime.service.SocialCardQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static ch.hearc.ig.jestime.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SocialCardResource REST controller.
 *
 * @see SocialCardResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JestimeApp.class)
public class SocialCardResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_GENERAL = "AAAAAAAAAA";
    private static final String UPDATED_GENERAL = "BBBBBBBBBB";

    private static final String DEFAULT_PROCEDURE = "AAAAAAAAAA";
    private static final String UPDATED_PROCEDURE = "BBBBBBBBBB";

    private static final String DEFAULT_RECOURSE = "AAAAAAAAAA";
    private static final String UPDATED_RECOURSE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_EDIT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EDIT_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private SocialCardRepository socialCardRepository;

    @Autowired
    private SocialCardMapper socialCardMapper;

    @Autowired
    private SocialCardService socialCardService;

    @Autowired
    private SocialCardQueryService socialCardQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSocialCardMockMvc;

    private SocialCard socialCard;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SocialCardResource socialCardResource = new SocialCardResource(socialCardService, socialCardQueryService);
        this.restSocialCardMockMvc = MockMvcBuilders.standaloneSetup(socialCardResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SocialCard createEntity(EntityManager em) {
        SocialCard socialCard = new SocialCard()
            .code(DEFAULT_CODE)
            .title(DEFAULT_TITLE)
            .description(DEFAULT_DESCRIPTION)
            .general(DEFAULT_GENERAL)
            .procedure(DEFAULT_PROCEDURE)
            .recourse(DEFAULT_RECOURSE)
            .creationDate(DEFAULT_CREATION_DATE)
            .editDate(DEFAULT_EDIT_DATE);
        // Add required entity
        Juridiction juridiction = JuridictionResourceIntTest.createEntity(em);
        em.persist(juridiction);
        em.flush();
        socialCard.setJuridiction(juridiction);
        // Add required entity
        Category category = CategoryResourceIntTest.createEntity(em);
        em.persist(category);
        em.flush();
        socialCard.setCategory(category);
        return socialCard;
    }

    @Before
    public void initTest() {
        socialCard = createEntity(em);
    }

    @Test
    @Transactional
    public void createSocialCard() throws Exception {
        int databaseSizeBeforeCreate = socialCardRepository.findAll().size();

        // Create the SocialCard
        SocialCardDTO socialCardDTO = socialCardMapper.toDto(socialCard);
        restSocialCardMockMvc.perform(post("/api/social-cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(socialCardDTO)))
            .andExpect(status().isCreated());

        // Validate the SocialCard in the database
        List<SocialCard> socialCardList = socialCardRepository.findAll();
        assertThat(socialCardList).hasSize(databaseSizeBeforeCreate + 1);
        SocialCard testSocialCard = socialCardList.get(socialCardList.size() - 1);
        assertThat(testSocialCard.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testSocialCard.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testSocialCard.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testSocialCard.getGeneral()).isEqualTo(DEFAULT_GENERAL);
        assertThat(testSocialCard.getProcedure()).isEqualTo(DEFAULT_PROCEDURE);
        assertThat(testSocialCard.getRecourse()).isEqualTo(DEFAULT_RECOURSE);
        assertThat(testSocialCard.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testSocialCard.getEditDate()).isEqualTo(DEFAULT_EDIT_DATE);
    }

    @Test
    @Transactional
    public void createSocialCardWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = socialCardRepository.findAll().size();

        // Create the SocialCard with an existing ID
        socialCard.setId(1L);
        SocialCardDTO socialCardDTO = socialCardMapper.toDto(socialCard);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSocialCardMockMvc.perform(post("/api/social-cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(socialCardDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SocialCard in the database
        List<SocialCard> socialCardList = socialCardRepository.findAll();
        assertThat(socialCardList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = socialCardRepository.findAll().size();
        // set the field null
        socialCard.setCode(null);

        // Create the SocialCard, which fails.
        SocialCardDTO socialCardDTO = socialCardMapper.toDto(socialCard);

        restSocialCardMockMvc.perform(post("/api/social-cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(socialCardDTO)))
            .andExpect(status().isBadRequest());

        List<SocialCard> socialCardList = socialCardRepository.findAll();
        assertThat(socialCardList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = socialCardRepository.findAll().size();
        // set the field null
        socialCard.setTitle(null);

        // Create the SocialCard, which fails.
        SocialCardDTO socialCardDTO = socialCardMapper.toDto(socialCard);

        restSocialCardMockMvc.perform(post("/api/social-cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(socialCardDTO)))
            .andExpect(status().isBadRequest());

        List<SocialCard> socialCardList = socialCardRepository.findAll();
        assertThat(socialCardList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreationDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = socialCardRepository.findAll().size();
        // set the field null
        socialCard.setCreationDate(null);

        // Create the SocialCard, which fails.
        SocialCardDTO socialCardDTO = socialCardMapper.toDto(socialCard);

        restSocialCardMockMvc.perform(post("/api/social-cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(socialCardDTO)))
            .andExpect(status().isBadRequest());

        List<SocialCard> socialCardList = socialCardRepository.findAll();
        assertThat(socialCardList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSocialCards() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList
        restSocialCardMockMvc.perform(get("/api/social-cards?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(socialCard.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].general").value(hasItem(DEFAULT_GENERAL.toString())))
            .andExpect(jsonPath("$.[*].procedure").value(hasItem(DEFAULT_PROCEDURE.toString())))
            .andExpect(jsonPath("$.[*].recourse").value(hasItem(DEFAULT_RECOURSE.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].editDate").value(hasItem(DEFAULT_EDIT_DATE.toString())));
    }

    @Test
    @Transactional
    public void getSocialCard() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get the socialCard
        restSocialCardMockMvc.perform(get("/api/social-cards/{id}", socialCard.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(socialCard.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.general").value(DEFAULT_GENERAL.toString()))
            .andExpect(jsonPath("$.procedure").value(DEFAULT_PROCEDURE.toString()))
            .andExpect(jsonPath("$.recourse").value(DEFAULT_RECOURSE.toString()))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.editDate").value(DEFAULT_EDIT_DATE.toString()));
    }

    @Test
    @Transactional
    public void getAllSocialCardsByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where code equals to DEFAULT_CODE
        defaultSocialCardShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the socialCardList where code equals to UPDATED_CODE
        defaultSocialCardShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllSocialCardsByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where code in DEFAULT_CODE or UPDATED_CODE
        defaultSocialCardShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the socialCardList where code equals to UPDATED_CODE
        defaultSocialCardShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllSocialCardsByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where code is not null
        defaultSocialCardShouldBeFound("code.specified=true");

        // Get all the socialCardList where code is null
        defaultSocialCardShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    public void getAllSocialCardsByTitleIsEqualToSomething() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where title equals to DEFAULT_TITLE
        defaultSocialCardShouldBeFound("title.equals=" + DEFAULT_TITLE);

        // Get all the socialCardList where title equals to UPDATED_TITLE
        defaultSocialCardShouldNotBeFound("title.equals=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllSocialCardsByTitleIsInShouldWork() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where title in DEFAULT_TITLE or UPDATED_TITLE
        defaultSocialCardShouldBeFound("title.in=" + DEFAULT_TITLE + "," + UPDATED_TITLE);

        // Get all the socialCardList where title equals to UPDATED_TITLE
        defaultSocialCardShouldNotBeFound("title.in=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllSocialCardsByTitleIsNullOrNotNull() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where title is not null
        defaultSocialCardShouldBeFound("title.specified=true");

        // Get all the socialCardList where title is null
        defaultSocialCardShouldNotBeFound("title.specified=false");
    }

    @Test
    @Transactional
    public void getAllSocialCardsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where description equals to DEFAULT_DESCRIPTION
        defaultSocialCardShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the socialCardList where description equals to UPDATED_DESCRIPTION
        defaultSocialCardShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllSocialCardsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultSocialCardShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the socialCardList where description equals to UPDATED_DESCRIPTION
        defaultSocialCardShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllSocialCardsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where description is not null
        defaultSocialCardShouldBeFound("description.specified=true");

        // Get all the socialCardList where description is null
        defaultSocialCardShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllSocialCardsByGeneralIsEqualToSomething() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where general equals to DEFAULT_GENERAL
        defaultSocialCardShouldBeFound("general.equals=" + DEFAULT_GENERAL);

        // Get all the socialCardList where general equals to UPDATED_GENERAL
        defaultSocialCardShouldNotBeFound("general.equals=" + UPDATED_GENERAL);
    }

    @Test
    @Transactional
    public void getAllSocialCardsByGeneralIsInShouldWork() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where general in DEFAULT_GENERAL or UPDATED_GENERAL
        defaultSocialCardShouldBeFound("general.in=" + DEFAULT_GENERAL + "," + UPDATED_GENERAL);

        // Get all the socialCardList where general equals to UPDATED_GENERAL
        defaultSocialCardShouldNotBeFound("general.in=" + UPDATED_GENERAL);
    }

    @Test
    @Transactional
    public void getAllSocialCardsByGeneralIsNullOrNotNull() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where general is not null
        defaultSocialCardShouldBeFound("general.specified=true");

        // Get all the socialCardList where general is null
        defaultSocialCardShouldNotBeFound("general.specified=false");
    }

    @Test
    @Transactional
    public void getAllSocialCardsByProcedureIsEqualToSomething() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where procedure equals to DEFAULT_PROCEDURE
        defaultSocialCardShouldBeFound("procedure.equals=" + DEFAULT_PROCEDURE);

        // Get all the socialCardList where procedure equals to UPDATED_PROCEDURE
        defaultSocialCardShouldNotBeFound("procedure.equals=" + UPDATED_PROCEDURE);
    }

    @Test
    @Transactional
    public void getAllSocialCardsByProcedureIsInShouldWork() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where procedure in DEFAULT_PROCEDURE or UPDATED_PROCEDURE
        defaultSocialCardShouldBeFound("procedure.in=" + DEFAULT_PROCEDURE + "," + UPDATED_PROCEDURE);

        // Get all the socialCardList where procedure equals to UPDATED_PROCEDURE
        defaultSocialCardShouldNotBeFound("procedure.in=" + UPDATED_PROCEDURE);
    }

    @Test
    @Transactional
    public void getAllSocialCardsByProcedureIsNullOrNotNull() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where procedure is not null
        defaultSocialCardShouldBeFound("procedure.specified=true");

        // Get all the socialCardList where procedure is null
        defaultSocialCardShouldNotBeFound("procedure.specified=false");
    }

    @Test
    @Transactional
    public void getAllSocialCardsByRecourseIsEqualToSomething() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where recourse equals to DEFAULT_RECOURSE
        defaultSocialCardShouldBeFound("recourse.equals=" + DEFAULT_RECOURSE);

        // Get all the socialCardList where recourse equals to UPDATED_RECOURSE
        defaultSocialCardShouldNotBeFound("recourse.equals=" + UPDATED_RECOURSE);
    }

    @Test
    @Transactional
    public void getAllSocialCardsByRecourseIsInShouldWork() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where recourse in DEFAULT_RECOURSE or UPDATED_RECOURSE
        defaultSocialCardShouldBeFound("recourse.in=" + DEFAULT_RECOURSE + "," + UPDATED_RECOURSE);

        // Get all the socialCardList where recourse equals to UPDATED_RECOURSE
        defaultSocialCardShouldNotBeFound("recourse.in=" + UPDATED_RECOURSE);
    }

    @Test
    @Transactional
    public void getAllSocialCardsByRecourseIsNullOrNotNull() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where recourse is not null
        defaultSocialCardShouldBeFound("recourse.specified=true");

        // Get all the socialCardList where recourse is null
        defaultSocialCardShouldNotBeFound("recourse.specified=false");
    }

    @Test
    @Transactional
    public void getAllSocialCardsByCreationDateIsEqualToSomething() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where creationDate equals to DEFAULT_CREATION_DATE
        defaultSocialCardShouldBeFound("creationDate.equals=" + DEFAULT_CREATION_DATE);

        // Get all the socialCardList where creationDate equals to UPDATED_CREATION_DATE
        defaultSocialCardShouldNotBeFound("creationDate.equals=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllSocialCardsByCreationDateIsInShouldWork() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where creationDate in DEFAULT_CREATION_DATE or UPDATED_CREATION_DATE
        defaultSocialCardShouldBeFound("creationDate.in=" + DEFAULT_CREATION_DATE + "," + UPDATED_CREATION_DATE);

        // Get all the socialCardList where creationDate equals to UPDATED_CREATION_DATE
        defaultSocialCardShouldNotBeFound("creationDate.in=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllSocialCardsByCreationDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where creationDate is not null
        defaultSocialCardShouldBeFound("creationDate.specified=true");

        // Get all the socialCardList where creationDate is null
        defaultSocialCardShouldNotBeFound("creationDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllSocialCardsByCreationDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where creationDate greater than or equals to DEFAULT_CREATION_DATE
        defaultSocialCardShouldBeFound("creationDate.greaterOrEqualThan=" + DEFAULT_CREATION_DATE);

        // Get all the socialCardList where creationDate greater than or equals to UPDATED_CREATION_DATE
        defaultSocialCardShouldNotBeFound("creationDate.greaterOrEqualThan=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllSocialCardsByCreationDateIsLessThanSomething() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where creationDate less than or equals to DEFAULT_CREATION_DATE
        defaultSocialCardShouldNotBeFound("creationDate.lessThan=" + DEFAULT_CREATION_DATE);

        // Get all the socialCardList where creationDate less than or equals to UPDATED_CREATION_DATE
        defaultSocialCardShouldBeFound("creationDate.lessThan=" + UPDATED_CREATION_DATE);
    }


    @Test
    @Transactional
    public void getAllSocialCardsByEditDateIsEqualToSomething() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where editDate equals to DEFAULT_EDIT_DATE
        defaultSocialCardShouldBeFound("editDate.equals=" + DEFAULT_EDIT_DATE);

        // Get all the socialCardList where editDate equals to UPDATED_EDIT_DATE
        defaultSocialCardShouldNotBeFound("editDate.equals=" + UPDATED_EDIT_DATE);
    }

    @Test
    @Transactional
    public void getAllSocialCardsByEditDateIsInShouldWork() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where editDate in DEFAULT_EDIT_DATE or UPDATED_EDIT_DATE
        defaultSocialCardShouldBeFound("editDate.in=" + DEFAULT_EDIT_DATE + "," + UPDATED_EDIT_DATE);

        // Get all the socialCardList where editDate equals to UPDATED_EDIT_DATE
        defaultSocialCardShouldNotBeFound("editDate.in=" + UPDATED_EDIT_DATE);
    }

    @Test
    @Transactional
    public void getAllSocialCardsByEditDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where editDate is not null
        defaultSocialCardShouldBeFound("editDate.specified=true");

        // Get all the socialCardList where editDate is null
        defaultSocialCardShouldNotBeFound("editDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllSocialCardsByEditDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where editDate greater than or equals to DEFAULT_EDIT_DATE
        defaultSocialCardShouldBeFound("editDate.greaterOrEqualThan=" + DEFAULT_EDIT_DATE);

        // Get all the socialCardList where editDate greater than or equals to UPDATED_EDIT_DATE
        defaultSocialCardShouldNotBeFound("editDate.greaterOrEqualThan=" + UPDATED_EDIT_DATE);
    }

    @Test
    @Transactional
    public void getAllSocialCardsByEditDateIsLessThanSomething() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);

        // Get all the socialCardList where editDate less than or equals to DEFAULT_EDIT_DATE
        defaultSocialCardShouldNotBeFound("editDate.lessThan=" + DEFAULT_EDIT_DATE);

        // Get all the socialCardList where editDate less than or equals to UPDATED_EDIT_DATE
        defaultSocialCardShouldBeFound("editDate.lessThan=" + UPDATED_EDIT_DATE);
    }


    @Test
    @Transactional
    public void getAllSocialCardsByJuridictionIsEqualToSomething() throws Exception {
        // Initialize the database
        Juridiction juridiction = JuridictionResourceIntTest.createEntity(em);
        em.persist(juridiction);
        em.flush();
        socialCard.setJuridiction(juridiction);
        socialCardRepository.saveAndFlush(socialCard);
        Long juridictionId = juridiction.getId();

        // Get all the socialCardList where juridiction equals to juridictionId
        defaultSocialCardShouldBeFound("juridictionId.equals=" + juridictionId);

        // Get all the socialCardList where juridiction equals to juridictionId + 1
        defaultSocialCardShouldNotBeFound("juridictionId.equals=" + (juridictionId + 1));
    }


    @Test
    @Transactional
    public void getAllSocialCardsByParentSocialCardIsEqualToSomething() throws Exception {
        // Initialize the database
        SocialCard parentSocialCard = SocialCardResourceIntTest.createEntity(em);
        em.persist(parentSocialCard);
        em.flush();
        socialCard.setParentSocialCard(parentSocialCard);
        socialCardRepository.saveAndFlush(socialCard);
        Long parentSocialCardId = parentSocialCard.getId();

        // Get all the socialCardList where parentSocialCard equals to parentSocialCardId
        defaultSocialCardShouldBeFound("parentSocialCardId.equals=" + parentSocialCardId);

        // Get all the socialCardList where parentSocialCard equals to parentSocialCardId + 1
        defaultSocialCardShouldNotBeFound("parentSocialCardId.equals=" + (parentSocialCardId + 1));
    }


    @Test
    @Transactional
    public void getAllSocialCardsByCategoryIsEqualToSomething() throws Exception {
        // Initialize the database
        Category category = CategoryResourceIntTest.createEntity(em);
        em.persist(category);
        em.flush();
        socialCard.setCategory(category);
        socialCardRepository.saveAndFlush(socialCard);
        Long categoryId = category.getId();

        // Get all the socialCardList where category equals to categoryId
        defaultSocialCardShouldBeFound("categoryId.equals=" + categoryId);

        // Get all the socialCardList where category equals to categoryId + 1
        defaultSocialCardShouldNotBeFound("categoryId.equals=" + (categoryId + 1));
    }


    @Test
    @Transactional
    public void getAllSocialCardsByAddressesIsEqualToSomething() throws Exception {
        // Initialize the database
        Address addresses = AddressResourceIntTest.createEntity(em);
        em.persist(addresses);
        em.flush();
        socialCard.addAddresses(addresses);
        socialCardRepository.saveAndFlush(socialCard);
        Long addressesId = addresses.getId();

        // Get all the socialCardList where addresses equals to addressesId
        defaultSocialCardShouldBeFound("addressesId.equals=" + addressesId);

        // Get all the socialCardList where addresses equals to addressesId + 1
        defaultSocialCardShouldNotBeFound("addressesId.equals=" + (addressesId + 1));
    }


    @Test
    @Transactional
    public void getAllSocialCardsByCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        User creator = UserResourceIntTest.createEntity(em);
        em.persist(creator);
        em.flush();
        socialCard.setCreator(creator);
        socialCardRepository.saveAndFlush(socialCard);
        Long creatorId = creator.getId();

        // Get all the socialCardList where creator equals to creatorId
        defaultSocialCardShouldBeFound("creatorId.equals=" + creatorId);

        // Get all the socialCardList where creator equals to creatorId + 1
        defaultSocialCardShouldNotBeFound("creatorId.equals=" + (creatorId + 1));
    }


    @Test
    @Transactional
    public void getAllSocialCardsByUpdatorIsEqualToSomething() throws Exception {
        // Initialize the database
        User updator = UserResourceIntTest.createEntity(em);
        em.persist(updator);
        em.flush();
        socialCard.setUpdator(updator);
        socialCardRepository.saveAndFlush(socialCard);
        Long updatorId = updator.getId();

        // Get all the socialCardList where updator equals to updatorId
        defaultSocialCardShouldBeFound("updatorId.equals=" + updatorId);

        // Get all the socialCardList where updator equals to updatorId + 1
        defaultSocialCardShouldNotBeFound("updatorId.equals=" + (updatorId + 1));
    }


    @Test
    @Transactional
    public void getAllSocialCardsBySourcesIsEqualToSomething() throws Exception {
        // Initialize the database
        Source sources = SourceResourceIntTest.createEntity(em);
        em.persist(sources);
        em.flush();
        socialCard.addSources(sources);
        socialCardRepository.saveAndFlush(socialCard);
        Long sourcesId = sources.getId();

        // Get all the socialCardList where sources equals to sourcesId
        defaultSocialCardShouldBeFound("sourcesId.equals=" + sourcesId);

        // Get all the socialCardList where sources equals to sourcesId + 1
        defaultSocialCardShouldNotBeFound("sourcesId.equals=" + (sourcesId + 1));
    }


    @Test
    @Transactional
    public void getAllSocialCardsByConditionsIsEqualToSomething() throws Exception {
        // Initialize the database
        Condition conditions = ConditionResourceIntTest.createEntity(em);
        em.persist(conditions);
        em.flush();
        socialCard.addConditions(conditions);
        socialCardRepository.saveAndFlush(socialCard);
        Long conditionsId = conditions.getId();

        // Get all the socialCardList where conditions equals to conditionsId
        defaultSocialCardShouldBeFound("conditionsId.equals=" + conditionsId);

        // Get all the socialCardList where conditions equals to conditionsId + 1
        defaultSocialCardShouldNotBeFound("conditionsId.equals=" + (conditionsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultSocialCardShouldBeFound(String filter) throws Exception {
        restSocialCardMockMvc.perform(get("/api/social-cards?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(socialCard.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].general").value(hasItem(DEFAULT_GENERAL.toString())))
            .andExpect(jsonPath("$.[*].procedure").value(hasItem(DEFAULT_PROCEDURE.toString())))
            .andExpect(jsonPath("$.[*].recourse").value(hasItem(DEFAULT_RECOURSE.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].editDate").value(hasItem(DEFAULT_EDIT_DATE.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultSocialCardShouldNotBeFound(String filter) throws Exception {
        restSocialCardMockMvc.perform(get("/api/social-cards?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingSocialCard() throws Exception {
        // Get the socialCard
        restSocialCardMockMvc.perform(get("/api/social-cards/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSocialCard() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);
        int databaseSizeBeforeUpdate = socialCardRepository.findAll().size();

        // Update the socialCard
        SocialCard updatedSocialCard = socialCardRepository.findOne(socialCard.getId());
        // Disconnect from session so that the updates on updatedSocialCard are not directly saved in db
        em.detach(updatedSocialCard);
        updatedSocialCard
            .code(UPDATED_CODE)
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .general(UPDATED_GENERAL)
            .procedure(UPDATED_PROCEDURE)
            .recourse(UPDATED_RECOURSE)
            .creationDate(UPDATED_CREATION_DATE)
            .editDate(UPDATED_EDIT_DATE);
        SocialCardDTO socialCardDTO = socialCardMapper.toDto(updatedSocialCard);

        restSocialCardMockMvc.perform(put("/api/social-cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(socialCardDTO)))
            .andExpect(status().isOk());

        // Validate the SocialCard in the database
        List<SocialCard> socialCardList = socialCardRepository.findAll();
        assertThat(socialCardList).hasSize(databaseSizeBeforeUpdate);
        SocialCard testSocialCard = socialCardList.get(socialCardList.size() - 1);
        assertThat(testSocialCard.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testSocialCard.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testSocialCard.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testSocialCard.getGeneral()).isEqualTo(UPDATED_GENERAL);
        assertThat(testSocialCard.getProcedure()).isEqualTo(UPDATED_PROCEDURE);
        assertThat(testSocialCard.getRecourse()).isEqualTo(UPDATED_RECOURSE);
        assertThat(testSocialCard.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testSocialCard.getEditDate()).isEqualTo(UPDATED_EDIT_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingSocialCard() throws Exception {
        int databaseSizeBeforeUpdate = socialCardRepository.findAll().size();

        // Create the SocialCard
        SocialCardDTO socialCardDTO = socialCardMapper.toDto(socialCard);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSocialCardMockMvc.perform(put("/api/social-cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(socialCardDTO)))
            .andExpect(status().isCreated());

        // Validate the SocialCard in the database
        List<SocialCard> socialCardList = socialCardRepository.findAll();
        assertThat(socialCardList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSocialCard() throws Exception {
        // Initialize the database
        socialCardRepository.saveAndFlush(socialCard);
        int databaseSizeBeforeDelete = socialCardRepository.findAll().size();

        // Get the socialCard
        restSocialCardMockMvc.perform(delete("/api/social-cards/{id}", socialCard.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SocialCard> socialCardList = socialCardRepository.findAll();
        assertThat(socialCardList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SocialCard.class);
        SocialCard socialCard1 = new SocialCard();
        socialCard1.setId(1L);
        SocialCard socialCard2 = new SocialCard();
        socialCard2.setId(socialCard1.getId());
        assertThat(socialCard1).isEqualTo(socialCard2);
        socialCard2.setId(2L);
        assertThat(socialCard1).isNotEqualTo(socialCard2);
        socialCard1.setId(null);
        assertThat(socialCard1).isNotEqualTo(socialCard2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SocialCardDTO.class);
        SocialCardDTO socialCardDTO1 = new SocialCardDTO();
        socialCardDTO1.setId(1L);
        SocialCardDTO socialCardDTO2 = new SocialCardDTO();
        assertThat(socialCardDTO1).isNotEqualTo(socialCardDTO2);
        socialCardDTO2.setId(socialCardDTO1.getId());
        assertThat(socialCardDTO1).isEqualTo(socialCardDTO2);
        socialCardDTO2.setId(2L);
        assertThat(socialCardDTO1).isNotEqualTo(socialCardDTO2);
        socialCardDTO1.setId(null);
        assertThat(socialCardDTO1).isNotEqualTo(socialCardDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(socialCardMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(socialCardMapper.fromId(null)).isNull();
    }
}
