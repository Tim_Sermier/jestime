package ch.hearc.ig.jestime.web.rest;

import ch.hearc.ig.jestime.JestimeApp;

import ch.hearc.ig.jestime.domain.Juridiction;
import ch.hearc.ig.jestime.repository.JuridictionRepository;
import ch.hearc.ig.jestime.service.JuridictionService;
import ch.hearc.ig.jestime.service.dto.JuridictionDTO;
import ch.hearc.ig.jestime.service.mapper.JuridictionMapper;
import ch.hearc.ig.jestime.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static ch.hearc.ig.jestime.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the JuridictionResource REST controller.
 *
 * @see JuridictionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JestimeApp.class)
public class JuridictionResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_IMAGE = "AAAAAAAAAA";
    private static final String UPDATED_IMAGE = "BBBBBBBBBB";

    @Autowired
    private JuridictionRepository juridictionRepository;

    @Autowired
    private JuridictionMapper juridictionMapper;

    @Autowired
    private JuridictionService juridictionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restJuridictionMockMvc;

    private Juridiction juridiction;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final JuridictionResource juridictionResource = new JuridictionResource(juridictionService);
        this.restJuridictionMockMvc = MockMvcBuilders.standaloneSetup(juridictionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Juridiction createEntity(EntityManager em) {
        Juridiction juridiction = new Juridiction()
            .code(DEFAULT_CODE)
            .name(DEFAULT_NAME)
            .image(DEFAULT_IMAGE);
        return juridiction;
    }

    @Before
    public void initTest() {
        juridiction = createEntity(em);
    }

    @Test
    @Transactional
    public void createJuridiction() throws Exception {
        int databaseSizeBeforeCreate = juridictionRepository.findAll().size();

        // Create the Juridiction
        JuridictionDTO juridictionDTO = juridictionMapper.toDto(juridiction);
        restJuridictionMockMvc.perform(post("/api/juridictions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(juridictionDTO)))
            .andExpect(status().isCreated());

        // Validate the Juridiction in the database
        List<Juridiction> juridictionList = juridictionRepository.findAll();
        assertThat(juridictionList).hasSize(databaseSizeBeforeCreate + 1);
        Juridiction testJuridiction = juridictionList.get(juridictionList.size() - 1);
        assertThat(testJuridiction.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testJuridiction.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testJuridiction.getImage()).isEqualTo(DEFAULT_IMAGE);
    }

    @Test
    @Transactional
    public void createJuridictionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = juridictionRepository.findAll().size();

        // Create the Juridiction with an existing ID
        juridiction.setId(1L);
        JuridictionDTO juridictionDTO = juridictionMapper.toDto(juridiction);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJuridictionMockMvc.perform(post("/api/juridictions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(juridictionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Juridiction in the database
        List<Juridiction> juridictionList = juridictionRepository.findAll();
        assertThat(juridictionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = juridictionRepository.findAll().size();
        // set the field null
        juridiction.setCode(null);

        // Create the Juridiction, which fails.
        JuridictionDTO juridictionDTO = juridictionMapper.toDto(juridiction);

        restJuridictionMockMvc.perform(post("/api/juridictions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(juridictionDTO)))
            .andExpect(status().isBadRequest());

        List<Juridiction> juridictionList = juridictionRepository.findAll();
        assertThat(juridictionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = juridictionRepository.findAll().size();
        // set the field null
        juridiction.setName(null);

        // Create the Juridiction, which fails.
        JuridictionDTO juridictionDTO = juridictionMapper.toDto(juridiction);

        restJuridictionMockMvc.perform(post("/api/juridictions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(juridictionDTO)))
            .andExpect(status().isBadRequest());

        List<Juridiction> juridictionList = juridictionRepository.findAll();
        assertThat(juridictionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllJuridictions() throws Exception {
        // Initialize the database
        juridictionRepository.saveAndFlush(juridiction);

        // Get all the juridictionList
        restJuridictionMockMvc.perform(get("/api/juridictions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(juridiction.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].image").value(hasItem(DEFAULT_IMAGE.toString())));
    }

    @Test
    @Transactional
    public void getJuridiction() throws Exception {
        // Initialize the database
        juridictionRepository.saveAndFlush(juridiction);

        // Get the juridiction
        restJuridictionMockMvc.perform(get("/api/juridictions/{id}", juridiction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(juridiction.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.image").value(DEFAULT_IMAGE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingJuridiction() throws Exception {
        // Get the juridiction
        restJuridictionMockMvc.perform(get("/api/juridictions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJuridiction() throws Exception {
        // Initialize the database
        juridictionRepository.saveAndFlush(juridiction);
        int databaseSizeBeforeUpdate = juridictionRepository.findAll().size();

        // Update the juridiction
        Juridiction updatedJuridiction = juridictionRepository.findOne(juridiction.getId());
        // Disconnect from session so that the updates on updatedJuridiction are not directly saved in db
        em.detach(updatedJuridiction);
        updatedJuridiction
            .code(UPDATED_CODE)
            .name(UPDATED_NAME)
            .image(UPDATED_IMAGE);
        JuridictionDTO juridictionDTO = juridictionMapper.toDto(updatedJuridiction);

        restJuridictionMockMvc.perform(put("/api/juridictions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(juridictionDTO)))
            .andExpect(status().isOk());

        // Validate the Juridiction in the database
        List<Juridiction> juridictionList = juridictionRepository.findAll();
        assertThat(juridictionList).hasSize(databaseSizeBeforeUpdate);
        Juridiction testJuridiction = juridictionList.get(juridictionList.size() - 1);
        assertThat(testJuridiction.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testJuridiction.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testJuridiction.getImage()).isEqualTo(UPDATED_IMAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingJuridiction() throws Exception {
        int databaseSizeBeforeUpdate = juridictionRepository.findAll().size();

        // Create the Juridiction
        JuridictionDTO juridictionDTO = juridictionMapper.toDto(juridiction);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restJuridictionMockMvc.perform(put("/api/juridictions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(juridictionDTO)))
            .andExpect(status().isCreated());

        // Validate the Juridiction in the database
        List<Juridiction> juridictionList = juridictionRepository.findAll();
        assertThat(juridictionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteJuridiction() throws Exception {
        // Initialize the database
        juridictionRepository.saveAndFlush(juridiction);
        int databaseSizeBeforeDelete = juridictionRepository.findAll().size();

        // Get the juridiction
        restJuridictionMockMvc.perform(delete("/api/juridictions/{id}", juridiction.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Juridiction> juridictionList = juridictionRepository.findAll();
        assertThat(juridictionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Juridiction.class);
        Juridiction juridiction1 = new Juridiction();
        juridiction1.setId(1L);
        Juridiction juridiction2 = new Juridiction();
        juridiction2.setId(juridiction1.getId());
        assertThat(juridiction1).isEqualTo(juridiction2);
        juridiction2.setId(2L);
        assertThat(juridiction1).isNotEqualTo(juridiction2);
        juridiction1.setId(null);
        assertThat(juridiction1).isNotEqualTo(juridiction2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(JuridictionDTO.class);
        JuridictionDTO juridictionDTO1 = new JuridictionDTO();
        juridictionDTO1.setId(1L);
        JuridictionDTO juridictionDTO2 = new JuridictionDTO();
        assertThat(juridictionDTO1).isNotEqualTo(juridictionDTO2);
        juridictionDTO2.setId(juridictionDTO1.getId());
        assertThat(juridictionDTO1).isEqualTo(juridictionDTO2);
        juridictionDTO2.setId(2L);
        assertThat(juridictionDTO1).isNotEqualTo(juridictionDTO2);
        juridictionDTO1.setId(null);
        assertThat(juridictionDTO1).isNotEqualTo(juridictionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(juridictionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(juridictionMapper.fromId(null)).isNull();
    }
}
