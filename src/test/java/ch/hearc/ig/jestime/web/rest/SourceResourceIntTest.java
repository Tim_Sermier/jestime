package ch.hearc.ig.jestime.web.rest;

import ch.hearc.ig.jestime.JestimeApp;

import ch.hearc.ig.jestime.domain.Source;
import ch.hearc.ig.jestime.domain.SocialCard;
import ch.hearc.ig.jestime.domain.Juridiction;
import ch.hearc.ig.jestime.domain.User;
import ch.hearc.ig.jestime.domain.User;
import ch.hearc.ig.jestime.domain.Reference;
import ch.hearc.ig.jestime.repository.SourceRepository;
import ch.hearc.ig.jestime.service.SourceService;
import ch.hearc.ig.jestime.service.dto.SourceDTO;
import ch.hearc.ig.jestime.service.mapper.SourceMapper;
import ch.hearc.ig.jestime.web.rest.errors.ExceptionTranslator;
import ch.hearc.ig.jestime.service.dto.SourceCriteria;
import ch.hearc.ig.jestime.service.SourceQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static ch.hearc.ig.jestime.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SourceResource REST controller.
 *
 * @see SourceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JestimeApp.class)
public class SourceResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_EDITION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EDITION_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private SourceRepository sourceRepository;

    @Autowired
    private SourceMapper sourceMapper;

    @Autowired
    private SourceService sourceService;

    @Autowired
    private SourceQueryService sourceQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSourceMockMvc;

    private Source source;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SourceResource sourceResource = new SourceResource(sourceService, sourceQueryService);
        this.restSourceMockMvc = MockMvcBuilders.standaloneSetup(sourceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Source createEntity(EntityManager em) {
        Source source = new Source()
            .code(DEFAULT_CODE)
            .title(DEFAULT_TITLE)
            .description(DEFAULT_DESCRIPTION)
            .url(DEFAULT_URL)
            .creationDate(DEFAULT_CREATION_DATE)
            .editionDate(DEFAULT_EDITION_DATE);
        // Add required entity
        Juridiction juridiction = JuridictionResourceIntTest.createEntity(em);
        em.persist(juridiction);
        em.flush();
        source.setJuridiction(juridiction);
        return source;
    }

    @Before
    public void initTest() {
        source = createEntity(em);
    }

    @Test
    @Transactional
    public void createSource() throws Exception {
        int databaseSizeBeforeCreate = sourceRepository.findAll().size();

        // Create the Source
        SourceDTO sourceDTO = sourceMapper.toDto(source);
        restSourceMockMvc.perform(post("/api/sources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sourceDTO)))
            .andExpect(status().isCreated());

        // Validate the Source in the database
        List<Source> sourceList = sourceRepository.findAll();
        assertThat(sourceList).hasSize(databaseSizeBeforeCreate + 1);
        Source testSource = sourceList.get(sourceList.size() - 1);
        assertThat(testSource.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testSource.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testSource.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testSource.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testSource.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testSource.getEditionDate()).isEqualTo(DEFAULT_EDITION_DATE);
    }

    @Test
    @Transactional
    public void createSourceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sourceRepository.findAll().size();

        // Create the Source with an existing ID
        source.setId(1L);
        SourceDTO sourceDTO = sourceMapper.toDto(source);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSourceMockMvc.perform(post("/api/sources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sourceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Source in the database
        List<Source> sourceList = sourceRepository.findAll();
        assertThat(sourceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = sourceRepository.findAll().size();
        // set the field null
        source.setCode(null);

        // Create the Source, which fails.
        SourceDTO sourceDTO = sourceMapper.toDto(source);

        restSourceMockMvc.perform(post("/api/sources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sourceDTO)))
            .andExpect(status().isBadRequest());

        List<Source> sourceList = sourceRepository.findAll();
        assertThat(sourceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = sourceRepository.findAll().size();
        // set the field null
        source.setTitle(null);

        // Create the Source, which fails.
        SourceDTO sourceDTO = sourceMapper.toDto(source);

        restSourceMockMvc.perform(post("/api/sources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sourceDTO)))
            .andExpect(status().isBadRequest());

        List<Source> sourceList = sourceRepository.findAll();
        assertThat(sourceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreationDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = sourceRepository.findAll().size();
        // set the field null
        source.setCreationDate(null);

        // Create the Source, which fails.
        SourceDTO sourceDTO = sourceMapper.toDto(source);

        restSourceMockMvc.perform(post("/api/sources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sourceDTO)))
            .andExpect(status().isBadRequest());

        List<Source> sourceList = sourceRepository.findAll();
        assertThat(sourceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSources() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get all the sourceList
        restSourceMockMvc.perform(get("/api/sources?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(source.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].editionDate").value(hasItem(DEFAULT_EDITION_DATE.toString())));
    }

    @Test
    @Transactional
    public void getSource() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get the source
        restSourceMockMvc.perform(get("/api/sources/{id}", source.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(source.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.editionDate").value(DEFAULT_EDITION_DATE.toString()));
    }

    @Test
    @Transactional
    public void getAllSourcesByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get all the sourceList where code equals to DEFAULT_CODE
        defaultSourceShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the sourceList where code equals to UPDATED_CODE
        defaultSourceShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllSourcesByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get all the sourceList where code in DEFAULT_CODE or UPDATED_CODE
        defaultSourceShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the sourceList where code equals to UPDATED_CODE
        defaultSourceShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllSourcesByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get all the sourceList where code is not null
        defaultSourceShouldBeFound("code.specified=true");

        // Get all the sourceList where code is null
        defaultSourceShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    public void getAllSourcesByTitleIsEqualToSomething() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get all the sourceList where title equals to DEFAULT_TITLE
        defaultSourceShouldBeFound("title.equals=" + DEFAULT_TITLE);

        // Get all the sourceList where title equals to UPDATED_TITLE
        defaultSourceShouldNotBeFound("title.equals=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllSourcesByTitleIsInShouldWork() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get all the sourceList where title in DEFAULT_TITLE or UPDATED_TITLE
        defaultSourceShouldBeFound("title.in=" + DEFAULT_TITLE + "," + UPDATED_TITLE);

        // Get all the sourceList where title equals to UPDATED_TITLE
        defaultSourceShouldNotBeFound("title.in=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllSourcesByTitleIsNullOrNotNull() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get all the sourceList where title is not null
        defaultSourceShouldBeFound("title.specified=true");

        // Get all the sourceList where title is null
        defaultSourceShouldNotBeFound("title.specified=false");
    }

    @Test
    @Transactional
    public void getAllSourcesByUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get all the sourceList where url equals to DEFAULT_URL
        defaultSourceShouldBeFound("url.equals=" + DEFAULT_URL);

        // Get all the sourceList where url equals to UPDATED_URL
        defaultSourceShouldNotBeFound("url.equals=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllSourcesByUrlIsInShouldWork() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get all the sourceList where url in DEFAULT_URL or UPDATED_URL
        defaultSourceShouldBeFound("url.in=" + DEFAULT_URL + "," + UPDATED_URL);

        // Get all the sourceList where url equals to UPDATED_URL
        defaultSourceShouldNotBeFound("url.in=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllSourcesByUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get all the sourceList where url is not null
        defaultSourceShouldBeFound("url.specified=true");

        // Get all the sourceList where url is null
        defaultSourceShouldNotBeFound("url.specified=false");
    }

    @Test
    @Transactional
    public void getAllSourcesByCreationDateIsEqualToSomething() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get all the sourceList where creationDate equals to DEFAULT_CREATION_DATE
        defaultSourceShouldBeFound("creationDate.equals=" + DEFAULT_CREATION_DATE);

        // Get all the sourceList where creationDate equals to UPDATED_CREATION_DATE
        defaultSourceShouldNotBeFound("creationDate.equals=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllSourcesByCreationDateIsInShouldWork() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get all the sourceList where creationDate in DEFAULT_CREATION_DATE or UPDATED_CREATION_DATE
        defaultSourceShouldBeFound("creationDate.in=" + DEFAULT_CREATION_DATE + "," + UPDATED_CREATION_DATE);

        // Get all the sourceList where creationDate equals to UPDATED_CREATION_DATE
        defaultSourceShouldNotBeFound("creationDate.in=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllSourcesByCreationDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get all the sourceList where creationDate is not null
        defaultSourceShouldBeFound("creationDate.specified=true");

        // Get all the sourceList where creationDate is null
        defaultSourceShouldNotBeFound("creationDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllSourcesByCreationDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get all the sourceList where creationDate greater than or equals to DEFAULT_CREATION_DATE
        defaultSourceShouldBeFound("creationDate.greaterOrEqualThan=" + DEFAULT_CREATION_DATE);

        // Get all the sourceList where creationDate greater than or equals to UPDATED_CREATION_DATE
        defaultSourceShouldNotBeFound("creationDate.greaterOrEqualThan=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllSourcesByCreationDateIsLessThanSomething() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get all the sourceList where creationDate less than or equals to DEFAULT_CREATION_DATE
        defaultSourceShouldNotBeFound("creationDate.lessThan=" + DEFAULT_CREATION_DATE);

        // Get all the sourceList where creationDate less than or equals to UPDATED_CREATION_DATE
        defaultSourceShouldBeFound("creationDate.lessThan=" + UPDATED_CREATION_DATE);
    }


    @Test
    @Transactional
    public void getAllSourcesByEditionDateIsEqualToSomething() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get all the sourceList where editionDate equals to DEFAULT_EDITION_DATE
        defaultSourceShouldBeFound("editionDate.equals=" + DEFAULT_EDITION_DATE);

        // Get all the sourceList where editionDate equals to UPDATED_EDITION_DATE
        defaultSourceShouldNotBeFound("editionDate.equals=" + UPDATED_EDITION_DATE);
    }

    @Test
    @Transactional
    public void getAllSourcesByEditionDateIsInShouldWork() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get all the sourceList where editionDate in DEFAULT_EDITION_DATE or UPDATED_EDITION_DATE
        defaultSourceShouldBeFound("editionDate.in=" + DEFAULT_EDITION_DATE + "," + UPDATED_EDITION_DATE);

        // Get all the sourceList where editionDate equals to UPDATED_EDITION_DATE
        defaultSourceShouldNotBeFound("editionDate.in=" + UPDATED_EDITION_DATE);
    }

    @Test
    @Transactional
    public void getAllSourcesByEditionDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get all the sourceList where editionDate is not null
        defaultSourceShouldBeFound("editionDate.specified=true");

        // Get all the sourceList where editionDate is null
        defaultSourceShouldNotBeFound("editionDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllSourcesByEditionDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get all the sourceList where editionDate greater than or equals to DEFAULT_EDITION_DATE
        defaultSourceShouldBeFound("editionDate.greaterOrEqualThan=" + DEFAULT_EDITION_DATE);

        // Get all the sourceList where editionDate greater than or equals to UPDATED_EDITION_DATE
        defaultSourceShouldNotBeFound("editionDate.greaterOrEqualThan=" + UPDATED_EDITION_DATE);
    }

    @Test
    @Transactional
    public void getAllSourcesByEditionDateIsLessThanSomething() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);

        // Get all the sourceList where editionDate less than or equals to DEFAULT_EDITION_DATE
        defaultSourceShouldNotBeFound("editionDate.lessThan=" + DEFAULT_EDITION_DATE);

        // Get all the sourceList where editionDate less than or equals to UPDATED_EDITION_DATE
        defaultSourceShouldBeFound("editionDate.lessThan=" + UPDATED_EDITION_DATE);
    }


    @Test
    @Transactional
    public void getAllSourcesBySocialCardsIsEqualToSomething() throws Exception {
        // Initialize the database
        SocialCard socialCards = SocialCardResourceIntTest.createEntity(em);
        em.persist(socialCards);
        em.flush();
        source.addSocialCards(socialCards);
        sourceRepository.saveAndFlush(source);
        Long socialCardsId = socialCards.getId();

        // Get all the sourceList where socialCards equals to socialCardsId
        defaultSourceShouldBeFound("socialCardsId.equals=" + socialCardsId);

        // Get all the sourceList where socialCards equals to socialCardsId + 1
        defaultSourceShouldNotBeFound("socialCardsId.equals=" + (socialCardsId + 1));
    }


    @Test
    @Transactional
    public void getAllSourcesByJuridictionIsEqualToSomething() throws Exception {
        // Initialize the database
        Juridiction juridiction = JuridictionResourceIntTest.createEntity(em);
        em.persist(juridiction);
        em.flush();
        source.setJuridiction(juridiction);
        sourceRepository.saveAndFlush(source);
        Long juridictionId = juridiction.getId();

        // Get all the sourceList where juridiction equals to juridictionId
        defaultSourceShouldBeFound("juridictionId.equals=" + juridictionId);

        // Get all the sourceList where juridiction equals to juridictionId + 1
        defaultSourceShouldNotBeFound("juridictionId.equals=" + (juridictionId + 1));
    }


    @Test
    @Transactional
    public void getAllSourcesByCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        User creator = UserResourceIntTest.createEntity(em);
        em.persist(creator);
        em.flush();
        source.setCreator(creator);
        sourceRepository.saveAndFlush(source);
        Long creatorId = creator.getId();

        // Get all the sourceList where creator equals to creatorId
        defaultSourceShouldBeFound("creatorId.equals=" + creatorId);

        // Get all the sourceList where creator equals to creatorId + 1
        defaultSourceShouldNotBeFound("creatorId.equals=" + (creatorId + 1));
    }


    @Test
    @Transactional
    public void getAllSourcesByUpdatorIsEqualToSomething() throws Exception {
        // Initialize the database
        User updator = UserResourceIntTest.createEntity(em);
        em.persist(updator);
        em.flush();
        source.setUpdator(updator);
        sourceRepository.saveAndFlush(source);
        Long updatorId = updator.getId();

        // Get all the sourceList where updator equals to updatorId
        defaultSourceShouldBeFound("updatorId.equals=" + updatorId);

        // Get all the sourceList where updator equals to updatorId + 1
        defaultSourceShouldNotBeFound("updatorId.equals=" + (updatorId + 1));
    }


    @Test
    @Transactional
    public void getAllSourcesByReferencesIsEqualToSomething() throws Exception {
        // Initialize the database
        Reference references = ReferenceResourceIntTest.createEntity(em);
        em.persist(references);
        em.flush();
        source.addReferences(references);
        sourceRepository.saveAndFlush(source);
        Long referencesId = references.getId();

        // Get all the sourceList where references equals to referencesId
        defaultSourceShouldBeFound("referencesId.equals=" + referencesId);

        // Get all the sourceList where references equals to referencesId + 1
        defaultSourceShouldNotBeFound("referencesId.equals=" + (referencesId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultSourceShouldBeFound(String filter) throws Exception {
        restSourceMockMvc.perform(get("/api/sources?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(source.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].editionDate").value(hasItem(DEFAULT_EDITION_DATE.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultSourceShouldNotBeFound(String filter) throws Exception {
        restSourceMockMvc.perform(get("/api/sources?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingSource() throws Exception {
        // Get the source
        restSourceMockMvc.perform(get("/api/sources/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSource() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);
        int databaseSizeBeforeUpdate = sourceRepository.findAll().size();

        // Update the source
        Source updatedSource = sourceRepository.findOne(source.getId());
        // Disconnect from session so that the updates on updatedSource are not directly saved in db
        em.detach(updatedSource);
        updatedSource
            .code(UPDATED_CODE)
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .url(UPDATED_URL)
            .creationDate(UPDATED_CREATION_DATE)
            .editionDate(UPDATED_EDITION_DATE);
        SourceDTO sourceDTO = sourceMapper.toDto(updatedSource);

        restSourceMockMvc.perform(put("/api/sources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sourceDTO)))
            .andExpect(status().isOk());

        // Validate the Source in the database
        List<Source> sourceList = sourceRepository.findAll();
        assertThat(sourceList).hasSize(databaseSizeBeforeUpdate);
        Source testSource = sourceList.get(sourceList.size() - 1);
        assertThat(testSource.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testSource.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testSource.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testSource.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testSource.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testSource.getEditionDate()).isEqualTo(UPDATED_EDITION_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingSource() throws Exception {
        int databaseSizeBeforeUpdate = sourceRepository.findAll().size();

        // Create the Source
        SourceDTO sourceDTO = sourceMapper.toDto(source);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSourceMockMvc.perform(put("/api/sources")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sourceDTO)))
            .andExpect(status().isCreated());

        // Validate the Source in the database
        List<Source> sourceList = sourceRepository.findAll();
        assertThat(sourceList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSource() throws Exception {
        // Initialize the database
        sourceRepository.saveAndFlush(source);
        int databaseSizeBeforeDelete = sourceRepository.findAll().size();

        // Get the source
        restSourceMockMvc.perform(delete("/api/sources/{id}", source.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Source> sourceList = sourceRepository.findAll();
        assertThat(sourceList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Source.class);
        Source source1 = new Source();
        source1.setId(1L);
        Source source2 = new Source();
        source2.setId(source1.getId());
        assertThat(source1).isEqualTo(source2);
        source2.setId(2L);
        assertThat(source1).isNotEqualTo(source2);
        source1.setId(null);
        assertThat(source1).isNotEqualTo(source2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SourceDTO.class);
        SourceDTO sourceDTO1 = new SourceDTO();
        sourceDTO1.setId(1L);
        SourceDTO sourceDTO2 = new SourceDTO();
        assertThat(sourceDTO1).isNotEqualTo(sourceDTO2);
        sourceDTO2.setId(sourceDTO1.getId());
        assertThat(sourceDTO1).isEqualTo(sourceDTO2);
        sourceDTO2.setId(2L);
        assertThat(sourceDTO1).isNotEqualTo(sourceDTO2);
        sourceDTO1.setId(null);
        assertThat(sourceDTO1).isNotEqualTo(sourceDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(sourceMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(sourceMapper.fromId(null)).isNull();
    }
}
