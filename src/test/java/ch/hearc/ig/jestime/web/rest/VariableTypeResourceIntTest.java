package ch.hearc.ig.jestime.web.rest;

import ch.hearc.ig.jestime.JestimeApp;

import ch.hearc.ig.jestime.domain.VariableType;
import ch.hearc.ig.jestime.repository.VariableTypeRepository;
import ch.hearc.ig.jestime.service.VariableTypeService;
import ch.hearc.ig.jestime.service.dto.VariableTypeDTO;
import ch.hearc.ig.jestime.service.mapper.VariableTypeMapper;
import ch.hearc.ig.jestime.web.rest.errors.ExceptionTranslator;
import ch.hearc.ig.jestime.service.dto.VariableTypeCriteria;
import ch.hearc.ig.jestime.service.VariableTypeQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static ch.hearc.ig.jestime.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the VariableTypeResource REST controller.
 *
 * @see VariableTypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JestimeApp.class)
public class VariableTypeResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private VariableTypeRepository variableTypeRepository;

    @Autowired
    private VariableTypeMapper variableTypeMapper;

    @Autowired
    private VariableTypeService variableTypeService;

    @Autowired
    private VariableTypeQueryService variableTypeQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restVariableTypeMockMvc;

    private VariableType variableType;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final VariableTypeResource variableTypeResource = new VariableTypeResource(variableTypeService, variableTypeQueryService);
        this.restVariableTypeMockMvc = MockMvcBuilders.standaloneSetup(variableTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VariableType createEntity(EntityManager em) {
        VariableType variableType = new VariableType()
            .code(DEFAULT_CODE)
            .name(DEFAULT_NAME);
        return variableType;
    }

    @Before
    public void initTest() {
        variableType = createEntity(em);
    }

    @Test
    @Transactional
    public void createVariableType() throws Exception {
        int databaseSizeBeforeCreate = variableTypeRepository.findAll().size();

        // Create the VariableType
        VariableTypeDTO variableTypeDTO = variableTypeMapper.toDto(variableType);
        restVariableTypeMockMvc.perform(post("/api/variable-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(variableTypeDTO)))
            .andExpect(status().isCreated());

        // Validate the VariableType in the database
        List<VariableType> variableTypeList = variableTypeRepository.findAll();
        assertThat(variableTypeList).hasSize(databaseSizeBeforeCreate + 1);
        VariableType testVariableType = variableTypeList.get(variableTypeList.size() - 1);
        assertThat(testVariableType.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testVariableType.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createVariableTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = variableTypeRepository.findAll().size();

        // Create the VariableType with an existing ID
        variableType.setId(1L);
        VariableTypeDTO variableTypeDTO = variableTypeMapper.toDto(variableType);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVariableTypeMockMvc.perform(post("/api/variable-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(variableTypeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the VariableType in the database
        List<VariableType> variableTypeList = variableTypeRepository.findAll();
        assertThat(variableTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = variableTypeRepository.findAll().size();
        // set the field null
        variableType.setCode(null);

        // Create the VariableType, which fails.
        VariableTypeDTO variableTypeDTO = variableTypeMapper.toDto(variableType);

        restVariableTypeMockMvc.perform(post("/api/variable-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(variableTypeDTO)))
            .andExpect(status().isBadRequest());

        List<VariableType> variableTypeList = variableTypeRepository.findAll();
        assertThat(variableTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = variableTypeRepository.findAll().size();
        // set the field null
        variableType.setName(null);

        // Create the VariableType, which fails.
        VariableTypeDTO variableTypeDTO = variableTypeMapper.toDto(variableType);

        restVariableTypeMockMvc.perform(post("/api/variable-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(variableTypeDTO)))
            .andExpect(status().isBadRequest());

        List<VariableType> variableTypeList = variableTypeRepository.findAll();
        assertThat(variableTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllVariableTypes() throws Exception {
        // Initialize the database
        variableTypeRepository.saveAndFlush(variableType);

        // Get all the variableTypeList
        restVariableTypeMockMvc.perform(get("/api/variable-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(variableType.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getVariableType() throws Exception {
        // Initialize the database
        variableTypeRepository.saveAndFlush(variableType);

        // Get the variableType
        restVariableTypeMockMvc.perform(get("/api/variable-types/{id}", variableType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(variableType.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getAllVariableTypesByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        variableTypeRepository.saveAndFlush(variableType);

        // Get all the variableTypeList where code equals to DEFAULT_CODE
        defaultVariableTypeShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the variableTypeList where code equals to UPDATED_CODE
        defaultVariableTypeShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllVariableTypesByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        variableTypeRepository.saveAndFlush(variableType);

        // Get all the variableTypeList where code in DEFAULT_CODE or UPDATED_CODE
        defaultVariableTypeShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the variableTypeList where code equals to UPDATED_CODE
        defaultVariableTypeShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllVariableTypesByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        variableTypeRepository.saveAndFlush(variableType);

        // Get all the variableTypeList where code is not null
        defaultVariableTypeShouldBeFound("code.specified=true");

        // Get all the variableTypeList where code is null
        defaultVariableTypeShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    public void getAllVariableTypesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        variableTypeRepository.saveAndFlush(variableType);

        // Get all the variableTypeList where name equals to DEFAULT_NAME
        defaultVariableTypeShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the variableTypeList where name equals to UPDATED_NAME
        defaultVariableTypeShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllVariableTypesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        variableTypeRepository.saveAndFlush(variableType);

        // Get all the variableTypeList where name in DEFAULT_NAME or UPDATED_NAME
        defaultVariableTypeShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the variableTypeList where name equals to UPDATED_NAME
        defaultVariableTypeShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllVariableTypesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        variableTypeRepository.saveAndFlush(variableType);

        // Get all the variableTypeList where name is not null
        defaultVariableTypeShouldBeFound("name.specified=true");

        // Get all the variableTypeList where name is null
        defaultVariableTypeShouldNotBeFound("name.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultVariableTypeShouldBeFound(String filter) throws Exception {
        restVariableTypeMockMvc.perform(get("/api/variable-types?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(variableType.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultVariableTypeShouldNotBeFound(String filter) throws Exception {
        restVariableTypeMockMvc.perform(get("/api/variable-types?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingVariableType() throws Exception {
        // Get the variableType
        restVariableTypeMockMvc.perform(get("/api/variable-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVariableType() throws Exception {
        // Initialize the database
        variableTypeRepository.saveAndFlush(variableType);
        int databaseSizeBeforeUpdate = variableTypeRepository.findAll().size();

        // Update the variableType
        VariableType updatedVariableType = variableTypeRepository.findOne(variableType.getId());
        // Disconnect from session so that the updates on updatedVariableType are not directly saved in db
        em.detach(updatedVariableType);
        updatedVariableType
            .code(UPDATED_CODE)
            .name(UPDATED_NAME);
        VariableTypeDTO variableTypeDTO = variableTypeMapper.toDto(updatedVariableType);

        restVariableTypeMockMvc.perform(put("/api/variable-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(variableTypeDTO)))
            .andExpect(status().isOk());

        // Validate the VariableType in the database
        List<VariableType> variableTypeList = variableTypeRepository.findAll();
        assertThat(variableTypeList).hasSize(databaseSizeBeforeUpdate);
        VariableType testVariableType = variableTypeList.get(variableTypeList.size() - 1);
        assertThat(testVariableType.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testVariableType.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingVariableType() throws Exception {
        int databaseSizeBeforeUpdate = variableTypeRepository.findAll().size();

        // Create the VariableType
        VariableTypeDTO variableTypeDTO = variableTypeMapper.toDto(variableType);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restVariableTypeMockMvc.perform(put("/api/variable-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(variableTypeDTO)))
            .andExpect(status().isCreated());

        // Validate the VariableType in the database
        List<VariableType> variableTypeList = variableTypeRepository.findAll();
        assertThat(variableTypeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteVariableType() throws Exception {
        // Initialize the database
        variableTypeRepository.saveAndFlush(variableType);
        int databaseSizeBeforeDelete = variableTypeRepository.findAll().size();

        // Get the variableType
        restVariableTypeMockMvc.perform(delete("/api/variable-types/{id}", variableType.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<VariableType> variableTypeList = variableTypeRepository.findAll();
        assertThat(variableTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VariableType.class);
        VariableType variableType1 = new VariableType();
        variableType1.setId(1L);
        VariableType variableType2 = new VariableType();
        variableType2.setId(variableType1.getId());
        assertThat(variableType1).isEqualTo(variableType2);
        variableType2.setId(2L);
        assertThat(variableType1).isNotEqualTo(variableType2);
        variableType1.setId(null);
        assertThat(variableType1).isNotEqualTo(variableType2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(VariableTypeDTO.class);
        VariableTypeDTO variableTypeDTO1 = new VariableTypeDTO();
        variableTypeDTO1.setId(1L);
        VariableTypeDTO variableTypeDTO2 = new VariableTypeDTO();
        assertThat(variableTypeDTO1).isNotEqualTo(variableTypeDTO2);
        variableTypeDTO2.setId(variableTypeDTO1.getId());
        assertThat(variableTypeDTO1).isEqualTo(variableTypeDTO2);
        variableTypeDTO2.setId(2L);
        assertThat(variableTypeDTO1).isNotEqualTo(variableTypeDTO2);
        variableTypeDTO1.setId(null);
        assertThat(variableTypeDTO1).isNotEqualTo(variableTypeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(variableTypeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(variableTypeMapper.fromId(null)).isNull();
    }
}
