package ch.hearc.ig.jestime.web.rest;

import ch.hearc.ig.jestime.JestimeApp;

import ch.hearc.ig.jestime.domain.SourceAudit;
import ch.hearc.ig.jestime.repository.SourceAuditRepository;
import ch.hearc.ig.jestime.service.SourceAuditService;
import ch.hearc.ig.jestime.service.dto.SourceAuditDTO;
import ch.hearc.ig.jestime.service.mapper.SourceAuditMapper;
import ch.hearc.ig.jestime.web.rest.errors.ExceptionTranslator;
import ch.hearc.ig.jestime.service.dto.SourceAuditCriteria;
import ch.hearc.ig.jestime.service.SourceAuditQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static ch.hearc.ig.jestime.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SourceAuditResource REST controller.
 *
 * @see SourceAuditResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JestimeApp.class)
public class SourceAuditResourceIntTest {

    private static final String DEFAULT_ACTION = "AAAAAAAAAA";
    private static final String UPDATED_ACTION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_ACTION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ACTION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_ACTION_USER = "AAAAAAAAAA";
    private static final String UPDATED_ACTION_USER = "BBBBBBBBBB";

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final Integer DEFAULT_JURIDICTION_ID = 1;
    private static final Integer UPDATED_JURIDICTION_ID = 2;

    @Autowired
    private SourceAuditRepository sourceAuditRepository;

    @Autowired
    private SourceAuditMapper sourceAuditMapper;

    @Autowired
    private SourceAuditService sourceAuditService;

    @Autowired
    private SourceAuditQueryService sourceAuditQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSourceAuditMockMvc;

    private SourceAudit sourceAudit;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SourceAuditResource sourceAuditResource = new SourceAuditResource(sourceAuditService, sourceAuditQueryService);
        this.restSourceAuditMockMvc = MockMvcBuilders.standaloneSetup(sourceAuditResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SourceAudit createEntity(EntityManager em) {
        SourceAudit sourceAudit = new SourceAudit()
            .action(DEFAULT_ACTION)
            .actionDate(DEFAULT_ACTION_DATE)
            .actionUser(DEFAULT_ACTION_USER)
            .title(DEFAULT_TITLE)
            .description(DEFAULT_DESCRIPTION)
            .url(DEFAULT_URL)
            .juridictionId(DEFAULT_JURIDICTION_ID);
        return sourceAudit;
    }

    @Before
    public void initTest() {
        sourceAudit = createEntity(em);
    }

    @Test
    @Transactional
    public void createSourceAudit() throws Exception {
        int databaseSizeBeforeCreate = sourceAuditRepository.findAll().size();

        // Create the SourceAudit
        SourceAuditDTO sourceAuditDTO = sourceAuditMapper.toDto(sourceAudit);
        restSourceAuditMockMvc.perform(post("/api/source-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sourceAuditDTO)))
            .andExpect(status().isCreated());

        // Validate the SourceAudit in the database
        List<SourceAudit> sourceAuditList = sourceAuditRepository.findAll();
        assertThat(sourceAuditList).hasSize(databaseSizeBeforeCreate + 1);
        SourceAudit testSourceAudit = sourceAuditList.get(sourceAuditList.size() - 1);
        assertThat(testSourceAudit.getAction()).isEqualTo(DEFAULT_ACTION);
        assertThat(testSourceAudit.getActionDate()).isEqualTo(DEFAULT_ACTION_DATE);
        assertThat(testSourceAudit.getActionUser()).isEqualTo(DEFAULT_ACTION_USER);
        assertThat(testSourceAudit.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testSourceAudit.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testSourceAudit.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testSourceAudit.getJuridictionId()).isEqualTo(DEFAULT_JURIDICTION_ID);
    }

    @Test
    @Transactional
    public void createSourceAuditWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sourceAuditRepository.findAll().size();

        // Create the SourceAudit with an existing ID
        sourceAudit.setId(1L);
        SourceAuditDTO sourceAuditDTO = sourceAuditMapper.toDto(sourceAudit);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSourceAuditMockMvc.perform(post("/api/source-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sourceAuditDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SourceAudit in the database
        List<SourceAudit> sourceAuditList = sourceAuditRepository.findAll();
        assertThat(sourceAuditList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkActionIsRequired() throws Exception {
        int databaseSizeBeforeTest = sourceAuditRepository.findAll().size();
        // set the field null
        sourceAudit.setAction(null);

        // Create the SourceAudit, which fails.
        SourceAuditDTO sourceAuditDTO = sourceAuditMapper.toDto(sourceAudit);

        restSourceAuditMockMvc.perform(post("/api/source-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sourceAuditDTO)))
            .andExpect(status().isBadRequest());

        List<SourceAudit> sourceAuditList = sourceAuditRepository.findAll();
        assertThat(sourceAuditList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkActionDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = sourceAuditRepository.findAll().size();
        // set the field null
        sourceAudit.setActionDate(null);

        // Create the SourceAudit, which fails.
        SourceAuditDTO sourceAuditDTO = sourceAuditMapper.toDto(sourceAudit);

        restSourceAuditMockMvc.perform(post("/api/source-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sourceAuditDTO)))
            .andExpect(status().isBadRequest());

        List<SourceAudit> sourceAuditList = sourceAuditRepository.findAll();
        assertThat(sourceAuditList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSourceAudits() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList
        restSourceAuditMockMvc.perform(get("/api/source-audits?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sourceAudit.getId().intValue())))
            .andExpect(jsonPath("$.[*].action").value(hasItem(DEFAULT_ACTION.toString())))
            .andExpect(jsonPath("$.[*].actionDate").value(hasItem(DEFAULT_ACTION_DATE.toString())))
            .andExpect(jsonPath("$.[*].actionUser").value(hasItem(DEFAULT_ACTION_USER.toString())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].juridictionId").value(hasItem(DEFAULT_JURIDICTION_ID)));
    }

    @Test
    @Transactional
    public void getSourceAudit() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get the sourceAudit
        restSourceAuditMockMvc.perform(get("/api/source-audits/{id}", sourceAudit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sourceAudit.getId().intValue()))
            .andExpect(jsonPath("$.action").value(DEFAULT_ACTION.toString()))
            .andExpect(jsonPath("$.actionDate").value(DEFAULT_ACTION_DATE.toString()))
            .andExpect(jsonPath("$.actionUser").value(DEFAULT_ACTION_USER.toString()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.juridictionId").value(DEFAULT_JURIDICTION_ID));
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByActionIsEqualToSomething() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where action equals to DEFAULT_ACTION
        defaultSourceAuditShouldBeFound("action.equals=" + DEFAULT_ACTION);

        // Get all the sourceAuditList where action equals to UPDATED_ACTION
        defaultSourceAuditShouldNotBeFound("action.equals=" + UPDATED_ACTION);
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByActionIsInShouldWork() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where action in DEFAULT_ACTION or UPDATED_ACTION
        defaultSourceAuditShouldBeFound("action.in=" + DEFAULT_ACTION + "," + UPDATED_ACTION);

        // Get all the sourceAuditList where action equals to UPDATED_ACTION
        defaultSourceAuditShouldNotBeFound("action.in=" + UPDATED_ACTION);
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByActionIsNullOrNotNull() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where action is not null
        defaultSourceAuditShouldBeFound("action.specified=true");

        // Get all the sourceAuditList where action is null
        defaultSourceAuditShouldNotBeFound("action.specified=false");
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByActionDateIsEqualToSomething() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where actionDate equals to DEFAULT_ACTION_DATE
        defaultSourceAuditShouldBeFound("actionDate.equals=" + DEFAULT_ACTION_DATE);

        // Get all the sourceAuditList where actionDate equals to UPDATED_ACTION_DATE
        defaultSourceAuditShouldNotBeFound("actionDate.equals=" + UPDATED_ACTION_DATE);
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByActionDateIsInShouldWork() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where actionDate in DEFAULT_ACTION_DATE or UPDATED_ACTION_DATE
        defaultSourceAuditShouldBeFound("actionDate.in=" + DEFAULT_ACTION_DATE + "," + UPDATED_ACTION_DATE);

        // Get all the sourceAuditList where actionDate equals to UPDATED_ACTION_DATE
        defaultSourceAuditShouldNotBeFound("actionDate.in=" + UPDATED_ACTION_DATE);
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByActionDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where actionDate is not null
        defaultSourceAuditShouldBeFound("actionDate.specified=true");

        // Get all the sourceAuditList where actionDate is null
        defaultSourceAuditShouldNotBeFound("actionDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByActionDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where actionDate greater than or equals to DEFAULT_ACTION_DATE
        defaultSourceAuditShouldBeFound("actionDate.greaterOrEqualThan=" + DEFAULT_ACTION_DATE);

        // Get all the sourceAuditList where actionDate greater than or equals to UPDATED_ACTION_DATE
        defaultSourceAuditShouldNotBeFound("actionDate.greaterOrEqualThan=" + UPDATED_ACTION_DATE);
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByActionDateIsLessThanSomething() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where actionDate less than or equals to DEFAULT_ACTION_DATE
        defaultSourceAuditShouldNotBeFound("actionDate.lessThan=" + DEFAULT_ACTION_DATE);

        // Get all the sourceAuditList where actionDate less than or equals to UPDATED_ACTION_DATE
        defaultSourceAuditShouldBeFound("actionDate.lessThan=" + UPDATED_ACTION_DATE);
    }


    @Test
    @Transactional
    public void getAllSourceAuditsByActionUserIsEqualToSomething() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where actionUser equals to DEFAULT_ACTION_USER
        defaultSourceAuditShouldBeFound("actionUser.equals=" + DEFAULT_ACTION_USER);

        // Get all the sourceAuditList where actionUser equals to UPDATED_ACTION_USER
        defaultSourceAuditShouldNotBeFound("actionUser.equals=" + UPDATED_ACTION_USER);
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByActionUserIsInShouldWork() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where actionUser in DEFAULT_ACTION_USER or UPDATED_ACTION_USER
        defaultSourceAuditShouldBeFound("actionUser.in=" + DEFAULT_ACTION_USER + "," + UPDATED_ACTION_USER);

        // Get all the sourceAuditList where actionUser equals to UPDATED_ACTION_USER
        defaultSourceAuditShouldNotBeFound("actionUser.in=" + UPDATED_ACTION_USER);
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByActionUserIsNullOrNotNull() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where actionUser is not null
        defaultSourceAuditShouldBeFound("actionUser.specified=true");

        // Get all the sourceAuditList where actionUser is null
        defaultSourceAuditShouldNotBeFound("actionUser.specified=false");
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByTitleIsEqualToSomething() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where title equals to DEFAULT_TITLE
        defaultSourceAuditShouldBeFound("title.equals=" + DEFAULT_TITLE);

        // Get all the sourceAuditList where title equals to UPDATED_TITLE
        defaultSourceAuditShouldNotBeFound("title.equals=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByTitleIsInShouldWork() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where title in DEFAULT_TITLE or UPDATED_TITLE
        defaultSourceAuditShouldBeFound("title.in=" + DEFAULT_TITLE + "," + UPDATED_TITLE);

        // Get all the sourceAuditList where title equals to UPDATED_TITLE
        defaultSourceAuditShouldNotBeFound("title.in=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByTitleIsNullOrNotNull() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where title is not null
        defaultSourceAuditShouldBeFound("title.specified=true");

        // Get all the sourceAuditList where title is null
        defaultSourceAuditShouldNotBeFound("title.specified=false");
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where url equals to DEFAULT_URL
        defaultSourceAuditShouldBeFound("url.equals=" + DEFAULT_URL);

        // Get all the sourceAuditList where url equals to UPDATED_URL
        defaultSourceAuditShouldNotBeFound("url.equals=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByUrlIsInShouldWork() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where url in DEFAULT_URL or UPDATED_URL
        defaultSourceAuditShouldBeFound("url.in=" + DEFAULT_URL + "," + UPDATED_URL);

        // Get all the sourceAuditList where url equals to UPDATED_URL
        defaultSourceAuditShouldNotBeFound("url.in=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where url is not null
        defaultSourceAuditShouldBeFound("url.specified=true");

        // Get all the sourceAuditList where url is null
        defaultSourceAuditShouldNotBeFound("url.specified=false");
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByJuridictionIdIsEqualToSomething() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where juridictionId equals to DEFAULT_JURIDICTION_ID
        defaultSourceAuditShouldBeFound("juridictionId.equals=" + DEFAULT_JURIDICTION_ID);

        // Get all the sourceAuditList where juridictionId equals to UPDATED_JURIDICTION_ID
        defaultSourceAuditShouldNotBeFound("juridictionId.equals=" + UPDATED_JURIDICTION_ID);
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByJuridictionIdIsInShouldWork() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where juridictionId in DEFAULT_JURIDICTION_ID or UPDATED_JURIDICTION_ID
        defaultSourceAuditShouldBeFound("juridictionId.in=" + DEFAULT_JURIDICTION_ID + "," + UPDATED_JURIDICTION_ID);

        // Get all the sourceAuditList where juridictionId equals to UPDATED_JURIDICTION_ID
        defaultSourceAuditShouldNotBeFound("juridictionId.in=" + UPDATED_JURIDICTION_ID);
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByJuridictionIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where juridictionId is not null
        defaultSourceAuditShouldBeFound("juridictionId.specified=true");

        // Get all the sourceAuditList where juridictionId is null
        defaultSourceAuditShouldNotBeFound("juridictionId.specified=false");
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByJuridictionIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where juridictionId greater than or equals to DEFAULT_JURIDICTION_ID
        defaultSourceAuditShouldBeFound("juridictionId.greaterOrEqualThan=" + DEFAULT_JURIDICTION_ID);

        // Get all the sourceAuditList where juridictionId greater than or equals to UPDATED_JURIDICTION_ID
        defaultSourceAuditShouldNotBeFound("juridictionId.greaterOrEqualThan=" + UPDATED_JURIDICTION_ID);
    }

    @Test
    @Transactional
    public void getAllSourceAuditsByJuridictionIdIsLessThanSomething() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);

        // Get all the sourceAuditList where juridictionId less than or equals to DEFAULT_JURIDICTION_ID
        defaultSourceAuditShouldNotBeFound("juridictionId.lessThan=" + DEFAULT_JURIDICTION_ID);

        // Get all the sourceAuditList where juridictionId less than or equals to UPDATED_JURIDICTION_ID
        defaultSourceAuditShouldBeFound("juridictionId.lessThan=" + UPDATED_JURIDICTION_ID);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultSourceAuditShouldBeFound(String filter) throws Exception {
        restSourceAuditMockMvc.perform(get("/api/source-audits?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sourceAudit.getId().intValue())))
            .andExpect(jsonPath("$.[*].action").value(hasItem(DEFAULT_ACTION.toString())))
            .andExpect(jsonPath("$.[*].actionDate").value(hasItem(DEFAULT_ACTION_DATE.toString())))
            .andExpect(jsonPath("$.[*].actionUser").value(hasItem(DEFAULT_ACTION_USER.toString())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].juridictionId").value(hasItem(DEFAULT_JURIDICTION_ID)));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultSourceAuditShouldNotBeFound(String filter) throws Exception {
        restSourceAuditMockMvc.perform(get("/api/source-audits?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingSourceAudit() throws Exception {
        // Get the sourceAudit
        restSourceAuditMockMvc.perform(get("/api/source-audits/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSourceAudit() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);
        int databaseSizeBeforeUpdate = sourceAuditRepository.findAll().size();

        // Update the sourceAudit
        SourceAudit updatedSourceAudit = sourceAuditRepository.findOne(sourceAudit.getId());
        // Disconnect from session so that the updates on updatedSourceAudit are not directly saved in db
        em.detach(updatedSourceAudit);
        updatedSourceAudit
            .action(UPDATED_ACTION)
            .actionDate(UPDATED_ACTION_DATE)
            .actionUser(UPDATED_ACTION_USER)
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .url(UPDATED_URL)
            .juridictionId(UPDATED_JURIDICTION_ID);
        SourceAuditDTO sourceAuditDTO = sourceAuditMapper.toDto(updatedSourceAudit);

        restSourceAuditMockMvc.perform(put("/api/source-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sourceAuditDTO)))
            .andExpect(status().isOk());

        // Validate the SourceAudit in the database
        List<SourceAudit> sourceAuditList = sourceAuditRepository.findAll();
        assertThat(sourceAuditList).hasSize(databaseSizeBeforeUpdate);
        SourceAudit testSourceAudit = sourceAuditList.get(sourceAuditList.size() - 1);
        assertThat(testSourceAudit.getAction()).isEqualTo(UPDATED_ACTION);
        assertThat(testSourceAudit.getActionDate()).isEqualTo(UPDATED_ACTION_DATE);
        assertThat(testSourceAudit.getActionUser()).isEqualTo(UPDATED_ACTION_USER);
        assertThat(testSourceAudit.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testSourceAudit.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testSourceAudit.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testSourceAudit.getJuridictionId()).isEqualTo(UPDATED_JURIDICTION_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingSourceAudit() throws Exception {
        int databaseSizeBeforeUpdate = sourceAuditRepository.findAll().size();

        // Create the SourceAudit
        SourceAuditDTO sourceAuditDTO = sourceAuditMapper.toDto(sourceAudit);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSourceAuditMockMvc.perform(put("/api/source-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sourceAuditDTO)))
            .andExpect(status().isCreated());

        // Validate the SourceAudit in the database
        List<SourceAudit> sourceAuditList = sourceAuditRepository.findAll();
        assertThat(sourceAuditList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSourceAudit() throws Exception {
        // Initialize the database
        sourceAuditRepository.saveAndFlush(sourceAudit);
        int databaseSizeBeforeDelete = sourceAuditRepository.findAll().size();

        // Get the sourceAudit
        restSourceAuditMockMvc.perform(delete("/api/source-audits/{id}", sourceAudit.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SourceAudit> sourceAuditList = sourceAuditRepository.findAll();
        assertThat(sourceAuditList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SourceAudit.class);
        SourceAudit sourceAudit1 = new SourceAudit();
        sourceAudit1.setId(1L);
        SourceAudit sourceAudit2 = new SourceAudit();
        sourceAudit2.setId(sourceAudit1.getId());
        assertThat(sourceAudit1).isEqualTo(sourceAudit2);
        sourceAudit2.setId(2L);
        assertThat(sourceAudit1).isNotEqualTo(sourceAudit2);
        sourceAudit1.setId(null);
        assertThat(sourceAudit1).isNotEqualTo(sourceAudit2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SourceAuditDTO.class);
        SourceAuditDTO sourceAuditDTO1 = new SourceAuditDTO();
        sourceAuditDTO1.setId(1L);
        SourceAuditDTO sourceAuditDTO2 = new SourceAuditDTO();
        assertThat(sourceAuditDTO1).isNotEqualTo(sourceAuditDTO2);
        sourceAuditDTO2.setId(sourceAuditDTO1.getId());
        assertThat(sourceAuditDTO1).isEqualTo(sourceAuditDTO2);
        sourceAuditDTO2.setId(2L);
        assertThat(sourceAuditDTO1).isNotEqualTo(sourceAuditDTO2);
        sourceAuditDTO1.setId(null);
        assertThat(sourceAuditDTO1).isNotEqualTo(sourceAuditDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(sourceAuditMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(sourceAuditMapper.fromId(null)).isNull();
    }
}
