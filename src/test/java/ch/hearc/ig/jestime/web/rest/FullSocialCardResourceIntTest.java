package ch.hearc.ig.jestime.web.rest;

import ch.hearc.ig.jestime.JestimeApp;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * Test class for the FullSocialCard REST controller.
 *
 * @see FullSocialCardResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JestimeApp.class)
public class FullSocialCardResourceIntTest {

    private MockMvc restMockMvc;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        FullSocialCardResource fullSocialCardResource = new FullSocialCardResource();
        restMockMvc = MockMvcBuilders
            .standaloneSetup(fullSocialCardResource)
            .build();
    }

    /**
    * Test createSocialCard
    */
    @Test
    public void testCreateSocialCard() throws Exception {
        restMockMvc.perform(post("/api/full-social-card/create-social-card"))
            .andExpect(status().isOk());
    }

}
