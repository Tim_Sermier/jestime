package ch.hearc.ig.jestime.web.rest;

import ch.hearc.ig.jestime.JestimeApp;

import ch.hearc.ig.jestime.domain.ConditionValue;
import ch.hearc.ig.jestime.domain.Variable;
import ch.hearc.ig.jestime.domain.Condition;
import ch.hearc.ig.jestime.domain.Reference;
import ch.hearc.ig.jestime.domain.Operator;
import ch.hearc.ig.jestime.repository.ConditionValueRepository;
import ch.hearc.ig.jestime.service.ConditionValueService;
import ch.hearc.ig.jestime.service.dto.ConditionValueDTO;
import ch.hearc.ig.jestime.service.mapper.ConditionValueMapper;
import ch.hearc.ig.jestime.web.rest.errors.ExceptionTranslator;
import ch.hearc.ig.jestime.service.dto.ConditionValueCriteria;
import ch.hearc.ig.jestime.service.ConditionValueQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static ch.hearc.ig.jestime.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ConditionValueResource REST controller.
 *
 * @see ConditionValueResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JestimeApp.class)
public class ConditionValueResourceIntTest {

    private static final String DEFAULT_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_VALUE = "BBBBBBBBBB";

    @Autowired
    private ConditionValueRepository conditionValueRepository;

    @Autowired
    private ConditionValueMapper conditionValueMapper;

    @Autowired
    private ConditionValueService conditionValueService;

    @Autowired
    private ConditionValueQueryService conditionValueQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restConditionValueMockMvc;

    private ConditionValue conditionValue;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ConditionValueResource conditionValueResource = new ConditionValueResource(conditionValueService, conditionValueQueryService);
        this.restConditionValueMockMvc = MockMvcBuilders.standaloneSetup(conditionValueResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ConditionValue createEntity(EntityManager em) {
        ConditionValue conditionValue = new ConditionValue()
            .value(DEFAULT_VALUE);
        // Add required entity
        Variable variable = VariableResourceIntTest.createEntity(em);
        em.persist(variable);
        em.flush();
        conditionValue.setVariable(variable);
        // Add required entity
        Condition condition = ConditionResourceIntTest.createEntity(em);
        em.persist(condition);
        em.flush();
        conditionValue.setCondition(condition);
        // Add required entity
        Operator operator = OperatorResourceIntTest.createEntity(em);
        em.persist(operator);
        em.flush();
        conditionValue.setOperator(operator);
        return conditionValue;
    }

    @Before
    public void initTest() {
        conditionValue = createEntity(em);
    }

    @Test
    @Transactional
    public void createConditionValue() throws Exception {
        int databaseSizeBeforeCreate = conditionValueRepository.findAll().size();

        // Create the ConditionValue
        ConditionValueDTO conditionValueDTO = conditionValueMapper.toDto(conditionValue);
        restConditionValueMockMvc.perform(post("/api/condition-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(conditionValueDTO)))
            .andExpect(status().isCreated());

        // Validate the ConditionValue in the database
        List<ConditionValue> conditionValueList = conditionValueRepository.findAll();
        assertThat(conditionValueList).hasSize(databaseSizeBeforeCreate + 1);
        ConditionValue testConditionValue = conditionValueList.get(conditionValueList.size() - 1);
        assertThat(testConditionValue.getValue()).isEqualTo(DEFAULT_VALUE);
    }

    @Test
    @Transactional
    public void createConditionValueWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = conditionValueRepository.findAll().size();

        // Create the ConditionValue with an existing ID
        conditionValue.setId(1L);
        ConditionValueDTO conditionValueDTO = conditionValueMapper.toDto(conditionValue);

        // An entity with an existing ID cannot be created, so this API call must fail
        restConditionValueMockMvc.perform(post("/api/condition-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(conditionValueDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ConditionValue in the database
        List<ConditionValue> conditionValueList = conditionValueRepository.findAll();
        assertThat(conditionValueList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = conditionValueRepository.findAll().size();
        // set the field null
        conditionValue.setValue(null);

        // Create the ConditionValue, which fails.
        ConditionValueDTO conditionValueDTO = conditionValueMapper.toDto(conditionValue);

        restConditionValueMockMvc.perform(post("/api/condition-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(conditionValueDTO)))
            .andExpect(status().isBadRequest());

        List<ConditionValue> conditionValueList = conditionValueRepository.findAll();
        assertThat(conditionValueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllConditionValues() throws Exception {
        // Initialize the database
        conditionValueRepository.saveAndFlush(conditionValue);

        // Get all the conditionValueList
        restConditionValueMockMvc.perform(get("/api/condition-values?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(conditionValue.getId().intValue())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())));
    }

    @Test
    @Transactional
    public void getConditionValue() throws Exception {
        // Initialize the database
        conditionValueRepository.saveAndFlush(conditionValue);

        // Get the conditionValue
        restConditionValueMockMvc.perform(get("/api/condition-values/{id}", conditionValue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(conditionValue.getId().intValue()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.toString()));
    }

    @Test
    @Transactional
    public void getAllConditionValuesByValueIsEqualToSomething() throws Exception {
        // Initialize the database
        conditionValueRepository.saveAndFlush(conditionValue);

        // Get all the conditionValueList where value equals to DEFAULT_VALUE
        defaultConditionValueShouldBeFound("value.equals=" + DEFAULT_VALUE);

        // Get all the conditionValueList where value equals to UPDATED_VALUE
        defaultConditionValueShouldNotBeFound("value.equals=" + UPDATED_VALUE);
    }

    @Test
    @Transactional
    public void getAllConditionValuesByValueIsInShouldWork() throws Exception {
        // Initialize the database
        conditionValueRepository.saveAndFlush(conditionValue);

        // Get all the conditionValueList where value in DEFAULT_VALUE or UPDATED_VALUE
        defaultConditionValueShouldBeFound("value.in=" + DEFAULT_VALUE + "," + UPDATED_VALUE);

        // Get all the conditionValueList where value equals to UPDATED_VALUE
        defaultConditionValueShouldNotBeFound("value.in=" + UPDATED_VALUE);
    }

    @Test
    @Transactional
    public void getAllConditionValuesByValueIsNullOrNotNull() throws Exception {
        // Initialize the database
        conditionValueRepository.saveAndFlush(conditionValue);

        // Get all the conditionValueList where value is not null
        defaultConditionValueShouldBeFound("value.specified=true");

        // Get all the conditionValueList where value is null
        defaultConditionValueShouldNotBeFound("value.specified=false");
    }

    @Test
    @Transactional
    public void getAllConditionValuesByVariableIsEqualToSomething() throws Exception {
        // Initialize the database
        Variable variable = VariableResourceIntTest.createEntity(em);
        em.persist(variable);
        em.flush();
        conditionValue.setVariable(variable);
        conditionValueRepository.saveAndFlush(conditionValue);
        Long variableId = variable.getId();

        // Get all the conditionValueList where variable equals to variableId
        defaultConditionValueShouldBeFound("variableId.equals=" + variableId);

        // Get all the conditionValueList where variable equals to variableId + 1
        defaultConditionValueShouldNotBeFound("variableId.equals=" + (variableId + 1));
    }


    @Test
    @Transactional
    public void getAllConditionValuesByConditionIsEqualToSomething() throws Exception {
        // Initialize the database
        Condition condition = ConditionResourceIntTest.createEntity(em);
        em.persist(condition);
        em.flush();
        conditionValue.setCondition(condition);
        conditionValueRepository.saveAndFlush(conditionValue);
        Long conditionId = condition.getId();

        // Get all the conditionValueList where condition equals to conditionId
        defaultConditionValueShouldBeFound("conditionId.equals=" + conditionId);

        // Get all the conditionValueList where condition equals to conditionId + 1
        defaultConditionValueShouldNotBeFound("conditionId.equals=" + (conditionId + 1));
    }


    @Test
    @Transactional
    public void getAllConditionValuesByReferenceIsEqualToSomething() throws Exception {
        // Initialize the database
        Reference reference = ReferenceResourceIntTest.createEntity(em);
        em.persist(reference);
        em.flush();
        conditionValue.setReference(reference);
        conditionValueRepository.saveAndFlush(conditionValue);
        Long referenceId = reference.getId();

        // Get all the conditionValueList where reference equals to referenceId
        defaultConditionValueShouldBeFound("referenceId.equals=" + referenceId);

        // Get all the conditionValueList where reference equals to referenceId + 1
        defaultConditionValueShouldNotBeFound("referenceId.equals=" + (referenceId + 1));
    }


    @Test
    @Transactional
    public void getAllConditionValuesByOperatorIsEqualToSomething() throws Exception {
        // Initialize the database
        Operator operator = OperatorResourceIntTest.createEntity(em);
        em.persist(operator);
        em.flush();
        conditionValue.setOperator(operator);
        conditionValueRepository.saveAndFlush(conditionValue);
        Long operatorId = operator.getId();

        // Get all the conditionValueList where operator equals to operatorId
        defaultConditionValueShouldBeFound("operatorId.equals=" + operatorId);

        // Get all the conditionValueList where operator equals to operatorId + 1
        defaultConditionValueShouldNotBeFound("operatorId.equals=" + (operatorId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultConditionValueShouldBeFound(String filter) throws Exception {
        restConditionValueMockMvc.perform(get("/api/condition-values?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(conditionValue.getId().intValue())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultConditionValueShouldNotBeFound(String filter) throws Exception {
        restConditionValueMockMvc.perform(get("/api/condition-values?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingConditionValue() throws Exception {
        // Get the conditionValue
        restConditionValueMockMvc.perform(get("/api/condition-values/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateConditionValue() throws Exception {
        // Initialize the database
        conditionValueRepository.saveAndFlush(conditionValue);
        int databaseSizeBeforeUpdate = conditionValueRepository.findAll().size();

        // Update the conditionValue
        ConditionValue updatedConditionValue = conditionValueRepository.findOne(conditionValue.getId());
        // Disconnect from session so that the updates on updatedConditionValue are not directly saved in db
        em.detach(updatedConditionValue);
        updatedConditionValue
            .value(UPDATED_VALUE);
        ConditionValueDTO conditionValueDTO = conditionValueMapper.toDto(updatedConditionValue);

        restConditionValueMockMvc.perform(put("/api/condition-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(conditionValueDTO)))
            .andExpect(status().isOk());

        // Validate the ConditionValue in the database
        List<ConditionValue> conditionValueList = conditionValueRepository.findAll();
        assertThat(conditionValueList).hasSize(databaseSizeBeforeUpdate);
        ConditionValue testConditionValue = conditionValueList.get(conditionValueList.size() - 1);
        assertThat(testConditionValue.getValue()).isEqualTo(UPDATED_VALUE);
    }

    @Test
    @Transactional
    public void updateNonExistingConditionValue() throws Exception {
        int databaseSizeBeforeUpdate = conditionValueRepository.findAll().size();

        // Create the ConditionValue
        ConditionValueDTO conditionValueDTO = conditionValueMapper.toDto(conditionValue);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restConditionValueMockMvc.perform(put("/api/condition-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(conditionValueDTO)))
            .andExpect(status().isCreated());

        // Validate the ConditionValue in the database
        List<ConditionValue> conditionValueList = conditionValueRepository.findAll();
        assertThat(conditionValueList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteConditionValue() throws Exception {
        // Initialize the database
        conditionValueRepository.saveAndFlush(conditionValue);
        int databaseSizeBeforeDelete = conditionValueRepository.findAll().size();

        // Get the conditionValue
        restConditionValueMockMvc.perform(delete("/api/condition-values/{id}", conditionValue.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ConditionValue> conditionValueList = conditionValueRepository.findAll();
        assertThat(conditionValueList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ConditionValue.class);
        ConditionValue conditionValue1 = new ConditionValue();
        conditionValue1.setId(1L);
        ConditionValue conditionValue2 = new ConditionValue();
        conditionValue2.setId(conditionValue1.getId());
        assertThat(conditionValue1).isEqualTo(conditionValue2);
        conditionValue2.setId(2L);
        assertThat(conditionValue1).isNotEqualTo(conditionValue2);
        conditionValue1.setId(null);
        assertThat(conditionValue1).isNotEqualTo(conditionValue2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ConditionValueDTO.class);
        ConditionValueDTO conditionValueDTO1 = new ConditionValueDTO();
        conditionValueDTO1.setId(1L);
        ConditionValueDTO conditionValueDTO2 = new ConditionValueDTO();
        assertThat(conditionValueDTO1).isNotEqualTo(conditionValueDTO2);
        conditionValueDTO2.setId(conditionValueDTO1.getId());
        assertThat(conditionValueDTO1).isEqualTo(conditionValueDTO2);
        conditionValueDTO2.setId(2L);
        assertThat(conditionValueDTO1).isNotEqualTo(conditionValueDTO2);
        conditionValueDTO1.setId(null);
        assertThat(conditionValueDTO1).isNotEqualTo(conditionValueDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(conditionValueMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(conditionValueMapper.fromId(null)).isNull();
    }
}
