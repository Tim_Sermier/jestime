package ch.hearc.ig.jestime.web.rest;

import ch.hearc.ig.jestime.JestimeApp;

import ch.hearc.ig.jestime.domain.Condition;
import ch.hearc.ig.jestime.domain.SocialCard;
import ch.hearc.ig.jestime.domain.User;
import ch.hearc.ig.jestime.domain.User;
import ch.hearc.ig.jestime.domain.ConditionValue;
import ch.hearc.ig.jestime.repository.ConditionRepository;
import ch.hearc.ig.jestime.service.ConditionService;
import ch.hearc.ig.jestime.service.dto.ConditionDTO;
import ch.hearc.ig.jestime.service.mapper.ConditionMapper;
import ch.hearc.ig.jestime.web.rest.errors.ExceptionTranslator;
import ch.hearc.ig.jestime.service.dto.ConditionCriteria;
import ch.hearc.ig.jestime.service.ConditionQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static ch.hearc.ig.jestime.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ConditionResource REST controller.
 *
 * @see ConditionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JestimeApp.class)
public class ConditionResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_EDIT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EDIT_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private ConditionRepository conditionRepository;

    @Autowired
    private ConditionMapper conditionMapper;

    @Autowired
    private ConditionService conditionService;

    @Autowired
    private ConditionQueryService conditionQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restConditionMockMvc;

    private Condition condition;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ConditionResource conditionResource = new ConditionResource(conditionService, conditionQueryService);
        this.restConditionMockMvc = MockMvcBuilders.standaloneSetup(conditionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Condition createEntity(EntityManager em) {
        Condition condition = new Condition()
            .code(DEFAULT_CODE)
            .description(DEFAULT_DESCRIPTION)
            .creationDate(DEFAULT_CREATION_DATE)
            .editDate(DEFAULT_EDIT_DATE);
        // Add required entity
        SocialCard socialCard = SocialCardResourceIntTest.createEntity(em);
        em.persist(socialCard);
        em.flush();
        condition.setSocialCard(socialCard);
        return condition;
    }

    @Before
    public void initTest() {
        condition = createEntity(em);
    }

    @Test
    @Transactional
    public void createCondition() throws Exception {
        int databaseSizeBeforeCreate = conditionRepository.findAll().size();

        // Create the Condition
        ConditionDTO conditionDTO = conditionMapper.toDto(condition);
        restConditionMockMvc.perform(post("/api/conditions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(conditionDTO)))
            .andExpect(status().isCreated());

        // Validate the Condition in the database
        List<Condition> conditionList = conditionRepository.findAll();
        assertThat(conditionList).hasSize(databaseSizeBeforeCreate + 1);
        Condition testCondition = conditionList.get(conditionList.size() - 1);
        assertThat(testCondition.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testCondition.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testCondition.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testCondition.getEditDate()).isEqualTo(DEFAULT_EDIT_DATE);
    }

    @Test
    @Transactional
    public void createConditionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = conditionRepository.findAll().size();

        // Create the Condition with an existing ID
        condition.setId(1L);
        ConditionDTO conditionDTO = conditionMapper.toDto(condition);

        // An entity with an existing ID cannot be created, so this API call must fail
        restConditionMockMvc.perform(post("/api/conditions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(conditionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Condition in the database
        List<Condition> conditionList = conditionRepository.findAll();
        assertThat(conditionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = conditionRepository.findAll().size();
        // set the field null
        condition.setCode(null);

        // Create the Condition, which fails.
        ConditionDTO conditionDTO = conditionMapper.toDto(condition);

        restConditionMockMvc.perform(post("/api/conditions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(conditionDTO)))
            .andExpect(status().isBadRequest());

        List<Condition> conditionList = conditionRepository.findAll();
        assertThat(conditionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllConditions() throws Exception {
        // Initialize the database
        conditionRepository.saveAndFlush(condition);

        // Get all the conditionList
        restConditionMockMvc.perform(get("/api/conditions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(condition.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].editDate").value(hasItem(DEFAULT_EDIT_DATE.toString())));
    }

    @Test
    @Transactional
    public void getCondition() throws Exception {
        // Initialize the database
        conditionRepository.saveAndFlush(condition);

        // Get the condition
        restConditionMockMvc.perform(get("/api/conditions/{id}", condition.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(condition.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.editDate").value(DEFAULT_EDIT_DATE.toString()));
    }

    @Test
    @Transactional
    public void getAllConditionsByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        conditionRepository.saveAndFlush(condition);

        // Get all the conditionList where code equals to DEFAULT_CODE
        defaultConditionShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the conditionList where code equals to UPDATED_CODE
        defaultConditionShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllConditionsByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        conditionRepository.saveAndFlush(condition);

        // Get all the conditionList where code in DEFAULT_CODE or UPDATED_CODE
        defaultConditionShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the conditionList where code equals to UPDATED_CODE
        defaultConditionShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllConditionsByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        conditionRepository.saveAndFlush(condition);

        // Get all the conditionList where code is not null
        defaultConditionShouldBeFound("code.specified=true");

        // Get all the conditionList where code is null
        defaultConditionShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    public void getAllConditionsByCreationDateIsEqualToSomething() throws Exception {
        // Initialize the database
        conditionRepository.saveAndFlush(condition);

        // Get all the conditionList where creationDate equals to DEFAULT_CREATION_DATE
        defaultConditionShouldBeFound("creationDate.equals=" + DEFAULT_CREATION_DATE);

        // Get all the conditionList where creationDate equals to UPDATED_CREATION_DATE
        defaultConditionShouldNotBeFound("creationDate.equals=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllConditionsByCreationDateIsInShouldWork() throws Exception {
        // Initialize the database
        conditionRepository.saveAndFlush(condition);

        // Get all the conditionList where creationDate in DEFAULT_CREATION_DATE or UPDATED_CREATION_DATE
        defaultConditionShouldBeFound("creationDate.in=" + DEFAULT_CREATION_DATE + "," + UPDATED_CREATION_DATE);

        // Get all the conditionList where creationDate equals to UPDATED_CREATION_DATE
        defaultConditionShouldNotBeFound("creationDate.in=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllConditionsByCreationDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        conditionRepository.saveAndFlush(condition);

        // Get all the conditionList where creationDate is not null
        defaultConditionShouldBeFound("creationDate.specified=true");

        // Get all the conditionList where creationDate is null
        defaultConditionShouldNotBeFound("creationDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllConditionsByCreationDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        conditionRepository.saveAndFlush(condition);

        // Get all the conditionList where creationDate greater than or equals to DEFAULT_CREATION_DATE
        defaultConditionShouldBeFound("creationDate.greaterOrEqualThan=" + DEFAULT_CREATION_DATE);

        // Get all the conditionList where creationDate greater than or equals to UPDATED_CREATION_DATE
        defaultConditionShouldNotBeFound("creationDate.greaterOrEqualThan=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllConditionsByCreationDateIsLessThanSomething() throws Exception {
        // Initialize the database
        conditionRepository.saveAndFlush(condition);

        // Get all the conditionList where creationDate less than or equals to DEFAULT_CREATION_DATE
        defaultConditionShouldNotBeFound("creationDate.lessThan=" + DEFAULT_CREATION_DATE);

        // Get all the conditionList where creationDate less than or equals to UPDATED_CREATION_DATE
        defaultConditionShouldBeFound("creationDate.lessThan=" + UPDATED_CREATION_DATE);
    }


    @Test
    @Transactional
    public void getAllConditionsByEditDateIsEqualToSomething() throws Exception {
        // Initialize the database
        conditionRepository.saveAndFlush(condition);

        // Get all the conditionList where editDate equals to DEFAULT_EDIT_DATE
        defaultConditionShouldBeFound("editDate.equals=" + DEFAULT_EDIT_DATE);

        // Get all the conditionList where editDate equals to UPDATED_EDIT_DATE
        defaultConditionShouldNotBeFound("editDate.equals=" + UPDATED_EDIT_DATE);
    }

    @Test
    @Transactional
    public void getAllConditionsByEditDateIsInShouldWork() throws Exception {
        // Initialize the database
        conditionRepository.saveAndFlush(condition);

        // Get all the conditionList where editDate in DEFAULT_EDIT_DATE or UPDATED_EDIT_DATE
        defaultConditionShouldBeFound("editDate.in=" + DEFAULT_EDIT_DATE + "," + UPDATED_EDIT_DATE);

        // Get all the conditionList where editDate equals to UPDATED_EDIT_DATE
        defaultConditionShouldNotBeFound("editDate.in=" + UPDATED_EDIT_DATE);
    }

    @Test
    @Transactional
    public void getAllConditionsByEditDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        conditionRepository.saveAndFlush(condition);

        // Get all the conditionList where editDate is not null
        defaultConditionShouldBeFound("editDate.specified=true");

        // Get all the conditionList where editDate is null
        defaultConditionShouldNotBeFound("editDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllConditionsByEditDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        conditionRepository.saveAndFlush(condition);

        // Get all the conditionList where editDate greater than or equals to DEFAULT_EDIT_DATE
        defaultConditionShouldBeFound("editDate.greaterOrEqualThan=" + DEFAULT_EDIT_DATE);

        // Get all the conditionList where editDate greater than or equals to UPDATED_EDIT_DATE
        defaultConditionShouldNotBeFound("editDate.greaterOrEqualThan=" + UPDATED_EDIT_DATE);
    }

    @Test
    @Transactional
    public void getAllConditionsByEditDateIsLessThanSomething() throws Exception {
        // Initialize the database
        conditionRepository.saveAndFlush(condition);

        // Get all the conditionList where editDate less than or equals to DEFAULT_EDIT_DATE
        defaultConditionShouldNotBeFound("editDate.lessThan=" + DEFAULT_EDIT_DATE);

        // Get all the conditionList where editDate less than or equals to UPDATED_EDIT_DATE
        defaultConditionShouldBeFound("editDate.lessThan=" + UPDATED_EDIT_DATE);
    }


    @Test
    @Transactional
    public void getAllConditionsBySocialCardIsEqualToSomething() throws Exception {
        // Initialize the database
        SocialCard socialCard = SocialCardResourceIntTest.createEntity(em);
        em.persist(socialCard);
        em.flush();
        condition.setSocialCard(socialCard);
        conditionRepository.saveAndFlush(condition);
        Long socialCardId = socialCard.getId();

        // Get all the conditionList where socialCard equals to socialCardId
        defaultConditionShouldBeFound("socialCardId.equals=" + socialCardId);

        // Get all the conditionList where socialCard equals to socialCardId + 1
        defaultConditionShouldNotBeFound("socialCardId.equals=" + (socialCardId + 1));
    }


    @Test
    @Transactional
    public void getAllConditionsByCreatorIsEqualToSomething() throws Exception {
        // Initialize the database
        User creator = UserResourceIntTest.createEntity(em);
        em.persist(creator);
        em.flush();
        condition.setCreator(creator);
        conditionRepository.saveAndFlush(condition);
        Long creatorId = creator.getId();

        // Get all the conditionList where creator equals to creatorId
        defaultConditionShouldBeFound("creatorId.equals=" + creatorId);

        // Get all the conditionList where creator equals to creatorId + 1
        defaultConditionShouldNotBeFound("creatorId.equals=" + (creatorId + 1));
    }


    @Test
    @Transactional
    public void getAllConditionsByUpdatorIsEqualToSomething() throws Exception {
        // Initialize the database
        User updator = UserResourceIntTest.createEntity(em);
        em.persist(updator);
        em.flush();
        condition.setUpdator(updator);
        conditionRepository.saveAndFlush(condition);
        Long updatorId = updator.getId();

        // Get all the conditionList where updator equals to updatorId
        defaultConditionShouldBeFound("updatorId.equals=" + updatorId);

        // Get all the conditionList where updator equals to updatorId + 1
        defaultConditionShouldNotBeFound("updatorId.equals=" + (updatorId + 1));
    }


    @Test
    @Transactional
    public void getAllConditionsByConditionValuesIsEqualToSomething() throws Exception {
        // Initialize the database
        ConditionValue conditionValues = ConditionValueResourceIntTest.createEntity(em);
        em.persist(conditionValues);
        em.flush();
        condition.addConditionValues(conditionValues);
        conditionRepository.saveAndFlush(condition);
        Long conditionValuesId = conditionValues.getId();

        // Get all the conditionList where conditionValues equals to conditionValuesId
        defaultConditionShouldBeFound("conditionValuesId.equals=" + conditionValuesId);

        // Get all the conditionList where conditionValues equals to conditionValuesId + 1
        defaultConditionShouldNotBeFound("conditionValuesId.equals=" + (conditionValuesId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultConditionShouldBeFound(String filter) throws Exception {
        restConditionMockMvc.perform(get("/api/conditions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(condition.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].editDate").value(hasItem(DEFAULT_EDIT_DATE.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultConditionShouldNotBeFound(String filter) throws Exception {
        restConditionMockMvc.perform(get("/api/conditions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingCondition() throws Exception {
        // Get the condition
        restConditionMockMvc.perform(get("/api/conditions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCondition() throws Exception {
        // Initialize the database
        conditionRepository.saveAndFlush(condition);
        int databaseSizeBeforeUpdate = conditionRepository.findAll().size();

        // Update the condition
        Condition updatedCondition = conditionRepository.findOne(condition.getId());
        // Disconnect from session so that the updates on updatedCondition are not directly saved in db
        em.detach(updatedCondition);
        updatedCondition
            .code(UPDATED_CODE)
            .description(UPDATED_DESCRIPTION)
            .creationDate(UPDATED_CREATION_DATE)
            .editDate(UPDATED_EDIT_DATE);
        ConditionDTO conditionDTO = conditionMapper.toDto(updatedCondition);

        restConditionMockMvc.perform(put("/api/conditions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(conditionDTO)))
            .andExpect(status().isOk());

        // Validate the Condition in the database
        List<Condition> conditionList = conditionRepository.findAll();
        assertThat(conditionList).hasSize(databaseSizeBeforeUpdate);
        Condition testCondition = conditionList.get(conditionList.size() - 1);
        assertThat(testCondition.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCondition.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testCondition.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testCondition.getEditDate()).isEqualTo(UPDATED_EDIT_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingCondition() throws Exception {
        int databaseSizeBeforeUpdate = conditionRepository.findAll().size();

        // Create the Condition
        ConditionDTO conditionDTO = conditionMapper.toDto(condition);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restConditionMockMvc.perform(put("/api/conditions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(conditionDTO)))
            .andExpect(status().isCreated());

        // Validate the Condition in the database
        List<Condition> conditionList = conditionRepository.findAll();
        assertThat(conditionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCondition() throws Exception {
        // Initialize the database
        conditionRepository.saveAndFlush(condition);
        int databaseSizeBeforeDelete = conditionRepository.findAll().size();

        // Get the condition
        restConditionMockMvc.perform(delete("/api/conditions/{id}", condition.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Condition> conditionList = conditionRepository.findAll();
        assertThat(conditionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Condition.class);
        Condition condition1 = new Condition();
        condition1.setId(1L);
        Condition condition2 = new Condition();
        condition2.setId(condition1.getId());
        assertThat(condition1).isEqualTo(condition2);
        condition2.setId(2L);
        assertThat(condition1).isNotEqualTo(condition2);
        condition1.setId(null);
        assertThat(condition1).isNotEqualTo(condition2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ConditionDTO.class);
        ConditionDTO conditionDTO1 = new ConditionDTO();
        conditionDTO1.setId(1L);
        ConditionDTO conditionDTO2 = new ConditionDTO();
        assertThat(conditionDTO1).isNotEqualTo(conditionDTO2);
        conditionDTO2.setId(conditionDTO1.getId());
        assertThat(conditionDTO1).isEqualTo(conditionDTO2);
        conditionDTO2.setId(2L);
        assertThat(conditionDTO1).isNotEqualTo(conditionDTO2);
        conditionDTO1.setId(null);
        assertThat(conditionDTO1).isNotEqualTo(conditionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(conditionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(conditionMapper.fromId(null)).isNull();
    }
}
