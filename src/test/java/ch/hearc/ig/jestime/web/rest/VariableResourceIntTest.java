package ch.hearc.ig.jestime.web.rest;

import ch.hearc.ig.jestime.JestimeApp;

import ch.hearc.ig.jestime.domain.Variable;
import ch.hearc.ig.jestime.domain.VariableType;
import ch.hearc.ig.jestime.repository.VariableRepository;
import ch.hearc.ig.jestime.service.VariableService;
import ch.hearc.ig.jestime.service.dto.VariableDTO;
import ch.hearc.ig.jestime.service.mapper.VariableMapper;
import ch.hearc.ig.jestime.web.rest.errors.ExceptionTranslator;
import ch.hearc.ig.jestime.service.dto.VariableCriteria;
import ch.hearc.ig.jestime.service.VariableQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;

import static ch.hearc.ig.jestime.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the VariableResource REST controller.
 *
 * @see VariableResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JestimeApp.class)
public class VariableResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private VariableRepository variableRepository;

    @Autowired
    private VariableMapper variableMapper;

    @Autowired
    private VariableService variableService;

    @Autowired
    private VariableQueryService variableQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restVariableMockMvc;

    private Variable variable;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final VariableResource variableResource = new VariableResource(variableService, variableQueryService);
        this.restVariableMockMvc = MockMvcBuilders.standaloneSetup(variableResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Variable createEntity(EntityManager em) {
        Variable variable = new Variable()
            .code(DEFAULT_CODE)
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION);
        // Add required entity
        VariableType type = VariableTypeResourceIntTest.createEntity(em);
        em.persist(type);
        em.flush();
        variable.setType(type);
        return variable;
    }

    @Before
    public void initTest() {
        variable = createEntity(em);
    }

    @Test
    @Transactional
    public void createVariable() throws Exception {
        int databaseSizeBeforeCreate = variableRepository.findAll().size();

        // Create the Variable
        VariableDTO variableDTO = variableMapper.toDto(variable);
        restVariableMockMvc.perform(post("/api/variables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(variableDTO)))
            .andExpect(status().isCreated());

        // Validate the Variable in the database
        List<Variable> variableList = variableRepository.findAll();
        assertThat(variableList).hasSize(databaseSizeBeforeCreate + 1);
        Variable testVariable = variableList.get(variableList.size() - 1);
        assertThat(testVariable.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testVariable.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testVariable.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createVariableWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = variableRepository.findAll().size();

        // Create the Variable with an existing ID
        variable.setId(1L);
        VariableDTO variableDTO = variableMapper.toDto(variable);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVariableMockMvc.perform(post("/api/variables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(variableDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Variable in the database
        List<Variable> variableList = variableRepository.findAll();
        assertThat(variableList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = variableRepository.findAll().size();
        // set the field null
        variable.setCode(null);

        // Create the Variable, which fails.
        VariableDTO variableDTO = variableMapper.toDto(variable);

        restVariableMockMvc.perform(post("/api/variables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(variableDTO)))
            .andExpect(status().isBadRequest());

        List<Variable> variableList = variableRepository.findAll();
        assertThat(variableList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = variableRepository.findAll().size();
        // set the field null
        variable.setName(null);

        // Create the Variable, which fails.
        VariableDTO variableDTO = variableMapper.toDto(variable);

        restVariableMockMvc.perform(post("/api/variables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(variableDTO)))
            .andExpect(status().isBadRequest());

        List<Variable> variableList = variableRepository.findAll();
        assertThat(variableList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllVariables() throws Exception {
        // Initialize the database
        variableRepository.saveAndFlush(variable);

        // Get all the variableList
        restVariableMockMvc.perform(get("/api/variables?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(variable.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getVariable() throws Exception {
        // Initialize the database
        variableRepository.saveAndFlush(variable);

        // Get the variable
        restVariableMockMvc.perform(get("/api/variables/{id}", variable.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(variable.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getAllVariablesByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        variableRepository.saveAndFlush(variable);

        // Get all the variableList where code equals to DEFAULT_CODE
        defaultVariableShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the variableList where code equals to UPDATED_CODE
        defaultVariableShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllVariablesByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        variableRepository.saveAndFlush(variable);

        // Get all the variableList where code in DEFAULT_CODE or UPDATED_CODE
        defaultVariableShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the variableList where code equals to UPDATED_CODE
        defaultVariableShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllVariablesByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        variableRepository.saveAndFlush(variable);

        // Get all the variableList where code is not null
        defaultVariableShouldBeFound("code.specified=true");

        // Get all the variableList where code is null
        defaultVariableShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    public void getAllVariablesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        variableRepository.saveAndFlush(variable);

        // Get all the variableList where name equals to DEFAULT_NAME
        defaultVariableShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the variableList where name equals to UPDATED_NAME
        defaultVariableShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllVariablesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        variableRepository.saveAndFlush(variable);

        // Get all the variableList where name in DEFAULT_NAME or UPDATED_NAME
        defaultVariableShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the variableList where name equals to UPDATED_NAME
        defaultVariableShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllVariablesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        variableRepository.saveAndFlush(variable);

        // Get all the variableList where name is not null
        defaultVariableShouldBeFound("name.specified=true");

        // Get all the variableList where name is null
        defaultVariableShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllVariablesByTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        VariableType type = VariableTypeResourceIntTest.createEntity(em);
        em.persist(type);
        em.flush();
        variable.setType(type);
        variableRepository.saveAndFlush(variable);
        Long typeId = type.getId();

        // Get all the variableList where type equals to typeId
        defaultVariableShouldBeFound("typeId.equals=" + typeId);

        // Get all the variableList where type equals to typeId + 1
        defaultVariableShouldNotBeFound("typeId.equals=" + (typeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultVariableShouldBeFound(String filter) throws Exception {
        restVariableMockMvc.perform(get("/api/variables?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(variable.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultVariableShouldNotBeFound(String filter) throws Exception {
        restVariableMockMvc.perform(get("/api/variables?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingVariable() throws Exception {
        // Get the variable
        restVariableMockMvc.perform(get("/api/variables/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVariable() throws Exception {
        // Initialize the database
        variableRepository.saveAndFlush(variable);
        int databaseSizeBeforeUpdate = variableRepository.findAll().size();

        // Update the variable
        Variable updatedVariable = variableRepository.findOne(variable.getId());
        // Disconnect from session so that the updates on updatedVariable are not directly saved in db
        em.detach(updatedVariable);
        updatedVariable
            .code(UPDATED_CODE)
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);
        VariableDTO variableDTO = variableMapper.toDto(updatedVariable);

        restVariableMockMvc.perform(put("/api/variables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(variableDTO)))
            .andExpect(status().isOk());

        // Validate the Variable in the database
        List<Variable> variableList = variableRepository.findAll();
        assertThat(variableList).hasSize(databaseSizeBeforeUpdate);
        Variable testVariable = variableList.get(variableList.size() - 1);
        assertThat(testVariable.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testVariable.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testVariable.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingVariable() throws Exception {
        int databaseSizeBeforeUpdate = variableRepository.findAll().size();

        // Create the Variable
        VariableDTO variableDTO = variableMapper.toDto(variable);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restVariableMockMvc.perform(put("/api/variables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(variableDTO)))
            .andExpect(status().isCreated());

        // Validate the Variable in the database
        List<Variable> variableList = variableRepository.findAll();
        assertThat(variableList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteVariable() throws Exception {
        // Initialize the database
        variableRepository.saveAndFlush(variable);
        int databaseSizeBeforeDelete = variableRepository.findAll().size();

        // Get the variable
        restVariableMockMvc.perform(delete("/api/variables/{id}", variable.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Variable> variableList = variableRepository.findAll();
        assertThat(variableList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Variable.class);
        Variable variable1 = new Variable();
        variable1.setId(1L);
        Variable variable2 = new Variable();
        variable2.setId(variable1.getId());
        assertThat(variable1).isEqualTo(variable2);
        variable2.setId(2L);
        assertThat(variable1).isNotEqualTo(variable2);
        variable1.setId(null);
        assertThat(variable1).isNotEqualTo(variable2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(VariableDTO.class);
        VariableDTO variableDTO1 = new VariableDTO();
        variableDTO1.setId(1L);
        VariableDTO variableDTO2 = new VariableDTO();
        assertThat(variableDTO1).isNotEqualTo(variableDTO2);
        variableDTO2.setId(variableDTO1.getId());
        assertThat(variableDTO1).isEqualTo(variableDTO2);
        variableDTO2.setId(2L);
        assertThat(variableDTO1).isNotEqualTo(variableDTO2);
        variableDTO1.setId(null);
        assertThat(variableDTO1).isNotEqualTo(variableDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(variableMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(variableMapper.fromId(null)).isNull();
    }
}
